ANSI-C/cC++ Compiler for HC12 V-5.0.41 Build 10203, Jul 23 2010

    1:  // CRC.c
    2:  // CRC calculation
    3:  // Version 1.4
    4:  // (c) 2006, 2009, 2012 Innovative Design Solutions, Inc.
    5:  // All rights reserved
    6:  
    7:  // History:
    8:  // 1.0  First released version
    9:  // 1.1  Added CRC8_Reset() and CRC8_Update() routines
   10:  // 1.2  Speed improvement to CRC8_Calculate()
   11:  // 1.3  Made table globally visible to speed up performance
   12:  // 1.4  Added CRC8_UpdateRange()
   13:  
   14:  
   15:  #include "global.h"
   16:  
   17:  ////////////////////////////////////////////////////////////////////////////////
   18:  ////////////////////////////////////////////////////////////////////////////////
   19:  ////////////////////////////////////////////////////////////////////////////////
   20:  ////////////////////////////////////////////////////////////////////////////////
   21:  
   22:  #ifdef HEADER
   23:  
   24:  uint8 CRC8_Calculate(const uint8 * data, uint16 size);
   25:  
   26:  #define CRC8_Reset() 0x55
   27:  #define CRC8_Update(crc, byte) CRC8Table[(uint8)((crc) ^ (byte))]
   28:  uint8 CRC8_UpdateRange(uint8 crc, const uint8 * data, uint16 size);
   29:  
   30:  extern const uint8 CRC8Table[256];
   31:  
   32:  #else
   33:  
   34:  ////////////////////////////////////////////////////////////////////////////////
   35:  ////////////////////////////////////////////////////////////////////////////////
   36:  ////////////////////////////////////////////////////////////////////////////////
   37:  ////////////////////////////////////////////////////////////////////////////////
   38:  
   39:  // CRC table
   40:  // polynomial = x^8 + x^5 + x^4 + 1
   41:  const uint8 CRC8Table[256] = {
   42:  	0x00, 0x5E, 0xBC, 0xE2, 0x61, 0x3F, 0xDD, 0x83, 0xC2, 0x9C, 0x7E, 0x20, 0xA3, 0xFD, 0x1F, 0x41,
   43:  	0x9D, 0xC3, 0x21, 0x7F, 0xFC, 0xA2, 0x40, 0x1E, 0x5F, 0x01, 0xE3, 0xBD, 0x3E, 0x60, 0x82, 0xDC,
   44:  	0x23, 0x7D, 0x9F, 0xC1, 0x42, 0x1C, 0xFE, 0xA0, 0xE1, 0xBF, 0x5D, 0x03, 0x80, 0xDE, 0x3C, 0x62,
   45:  	0xBE, 0xE0, 0x02, 0x5C, 0xDF, 0x81, 0x63, 0x3D, 0x7C, 0x22, 0xC0, 0x9E, 0x1D, 0x43, 0xA1, 0xFF,
   46:  	0x46, 0x18, 0xFA, 0xA4, 0x27, 0x79, 0x9B, 0xC5, 0x84, 0xDA, 0x38, 0x66, 0xE5, 0xBB, 0x59, 0x07,
   47:  	0xDB, 0x85, 0x67, 0x39, 0xBA, 0xE4, 0x06, 0x58, 0x19, 0x47, 0xA5, 0xFB, 0x78, 0x26, 0xC4, 0x9A,
   48:  	0x65, 0x3B, 0xD9, 0x87, 0x04, 0x5A, 0xB8, 0xE6, 0xA7, 0xF9, 0x1B, 0x45, 0xC6, 0x98, 0x7A, 0x24,
   49:  	0xF8, 0xA6, 0x44, 0x1A, 0x99, 0xC7, 0x25, 0x7B, 0x3A, 0x64, 0x86, 0xD8, 0x5B, 0x05, 0xE7, 0xB9,
   50:  	0x8C, 0xD2, 0x30, 0x6E, 0xED, 0xB3, 0x51, 0x0F, 0x4E, 0x10, 0xF2, 0xAC, 0x2F, 0x71, 0x93, 0xCD,
   51:  	0x11, 0x4F, 0xAD, 0xF3, 0x70, 0x2E, 0xCC, 0x92, 0xD3, 0x8D, 0x6F, 0x31, 0xB2, 0xEC, 0x0E, 0x50,
   52:  	0xAF, 0xF1, 0x13, 0x4D, 0xCE, 0x90, 0x72, 0x2C, 0x6D, 0x33, 0xD1, 0x8F, 0x0C, 0x52, 0xB0, 0xEE,
   53:  	0x32, 0x6C, 0x8E, 0xD0, 0x53, 0x0D, 0xEF, 0xB1, 0xF0, 0xAE, 0x4C, 0x12, 0x91, 0xCF, 0x2D, 0x73,
   54:  	0xCA, 0x94, 0x76, 0x28, 0xAB, 0xF5, 0x17, 0x49, 0x08, 0x56, 0xB4, 0xEA, 0x69, 0x37, 0xD5, 0x8B,
   55:  	0x57, 0x09, 0xEB, 0xB5, 0x36, 0x68, 0x8A, 0xD4, 0x95, 0xCB, 0x29, 0x77, 0xF4, 0xAA, 0x48, 0x16,
   56:  	0xE9, 0xB7, 0x55, 0x0B, 0x88, 0xD6, 0x34, 0x6A, 0x2B, 0x75, 0x97, 0xC9, 0x4A, 0x14, 0xF6, 0xA8,
   57:  	0x74, 0x2A, 0xC8, 0x96, 0x15, 0x4B, 0xA9, 0xF7, 0xB6, 0xE8, 0x0A, 0x54, 0xD7, 0x89, 0x6B, 0x35,
   58:  };
   59:  
   60:  // calculate CRC of a data buffer
   61:  uint8 CRC8_UpdateRange(uint8 crc, const uint8 * data, uint16 size)
   62:  {

Function: CRC8_UpdateRange
Source  : H:\Lippert\Projects\PR20170026 MyRV Switch Block\SW\22720 MyRV Switch Block SW\Sources\CRC.c
Options : -Cni -CPUHCS12 -DPPAGE_ADDR=0x15 -D__MAP_FLASH__ -Env"GENPATH=H:\Lippert\Projects\PR20170026 MyRV Switch Block\SW\22720 MyRV Switch Block SW;H:\Lippert\Projects\PR20170026 MyRV Switch Block\SW\22720 MyRV Switch Block SW\bin;H:\Lippert\Projects\PR20170026 MyRV Switch Block\SW\22720 MyRV Switch Block SW\prm;H:\Lippert\Projects\PR20170026 MyRV Switch Block\SW\22720 MyRV Switch Block SW\cmd;H:\Lippert\Projects\PR20170026 MyRV Switch Block\SW\22720 MyRV Switch Block SW\Sources;C:\Program Files (x86)\Freescale\CWS12v5.1\lib\HC12c\lib;C:\Program Files (x86)\Freescale\CWS12v5.1\lib\HC12c\src;C:\Program Files (x86)\Freescale\CWS12v5.1\lib\HC12c\include" -Env"LIBPATH=C:\Program Files (x86)\Freescale\CWS12v5.1\lib\HC12c\include" -Env"OBJPATH=H:\Lippert\Projects\PR20170026 MyRV Switch Block\SW\22720 MyRV Switch Block SW\bin" -Env"TEXTPATH=H:\Lippert\Projects\PR20170026 MyRV Switch Block\SW\22720 MyRV Switch Block SW\bin" -Lasm=%n.lst -Mb -MapFlash -ObjN="H:\Lippert\Projects\PR20170026 MyRV Switch Block\SW\22720 MyRV Switch Block SW\22720_My_RV_Switch_Block_Project_Data\Standard\ObjectCode\CRC.c.o" -TE1uE -WmsgSd1106 -WmsgSd4301

  0000 6cad         [2]     STD   3,-SP
   63:  	register uint8 b = crc;
  0002 e688         [3]     LDAB  8,SP
  0004 6b82         [2]     STAB  2,SP
   64:  
   65:  	// handle straggling byte  
   66:  	if (size & 1)
  0006 0f81010f     [4]     BRCLR 1,SP,#1,*+19 ;abs = 0019
   67:  		b = CRC8Table[b ^ *(data++)];
  000a ee86         [3]     LDX   6,SP
  000c e630         [3]     LDAB  1,X+
  000e 6e86         [2]     STX   6,SP
  0010 e882         [3]     EORB  2,SP
  0012 ce0000       [2]     LDX   #CRC8Table
  0015 a6e5         [3]     LDAA  B,X
  0017 6a82         [2]     STAA  2,SP
   68:  	size >>= 1;
  0019 6480         [3]     LSR   0,SP
  001b 6681         [3]     ROR   1,SP
   69:  
   70:  	// process remaining data 2 bytes at a time
   71:  	size++;
  001d ee80         [3]     LDX   0,SP
  001f 08           [1]     INX   
   72:  	while (--size)
  0020 2015         [3]     BRA   *+23 ;abs = 0037
   73:  	{
   74:  		b = CRC8Table[b ^ *(data++)];
  0022 ed86         [3]     LDY   6,SP
  0024 e670         [3]     LDAB  1,Y+
  0026 e882         [3]     EORB  2,SP
  0028 34           [2]     PSHX  
  0029 ce0000       [2]     LDX   #CRC8Table
  002c a6e5         [3]     LDAA  B,X
   75:  		b = CRC8Table[b ^ *(data++)];
  002e a870         [3]     EORA  1,Y+
  0030 6d88         [2]     STY   8,SP
  0032 e6e4         [3]     LDAB  A,X
  0034 6b84         [2]     STAB  4,SP
  0036 30           [3]     PULX  
  0037 0435e8       [3]     DBNE  X,*-21 ;abs = 0022
   76:  	}
   77:  
   78:  	return b;
  003a e682         [3]     LDAB  2,SP
   79:  }
  003c 1b83         [2]     LEAS  3,SP
  003e 0a           [7]     RTC   
   80:  
   81:  uint8 CRC8_Calculate(const uint8 * data, uint16 size)
   82:  {

Function: CRC8_Calculate
Source  : H:\Lippert\Projects\PR20170026 MyRV Switch Block\SW\22720 MyRV Switch Block SW\Sources\CRC.c
Options : -Cni -CPUHCS12 -DPPAGE_ADDR=0x15 -D__MAP_FLASH__ -Env"GENPATH=H:\Lippert\Projects\PR20170026 MyRV Switch Block\SW\22720 MyRV Switch Block SW;H:\Lippert\Projects\PR20170026 MyRV Switch Block\SW\22720 MyRV Switch Block SW\bin;H:\Lippert\Projects\PR20170026 MyRV Switch Block\SW\22720 MyRV Switch Block SW\prm;H:\Lippert\Projects\PR20170026 MyRV Switch Block\SW\22720 MyRV Switch Block SW\cmd;H:\Lippert\Projects\PR20170026 MyRV Switch Block\SW\22720 MyRV Switch Block SW\Sources;C:\Program Files (x86)\Freescale\CWS12v5.1\lib\HC12c\lib;C:\Program Files (x86)\Freescale\CWS12v5.1\lib\HC12c\src;C:\Program Files (x86)\Freescale\CWS12v5.1\lib\HC12c\include" -Env"LIBPATH=C:\Program Files (x86)\Freescale\CWS12v5.1\lib\HC12c\include" -Env"OBJPATH=H:\Lippert\Projects\PR20170026 MyRV Switch Block\SW\22720 MyRV Switch Block SW\bin" -Env"TEXTPATH=H:\Lippert\Projects\PR20170026 MyRV Switch Block\SW\22720 MyRV Switch Block SW\bin" -Lasm=%n.lst -Mb -MapFlash -ObjN="H:\Lippert\Projects\PR20170026 MyRV Switch Block\SW\22720 MyRV Switch Block SW\22720_My_RV_Switch_Block_Project_Data\Standard\ObjectCode\CRC.c.o" -TE1uE -WmsgSd1106 -WmsgSd4301

  0000 3b           [2]     PSHD  
   83:  	return CRC8_UpdateRange(CRC8_Reset(), data, size);
  0001 c655         [1]     LDAB  #85
  0003 37           [2]     PSHB  
  0004 ec86         [3]     LDD   6,SP
  0006 3b           [2]     PSHD  
  0007 ec83         [3]     LDD   3,SP
  0009 4a000000     [7]     CALL  CRC8_UpdateRange,PAGE(CRC8_UpdateRange)
  000d 1b83         [2]     LEAS  3,SP
   84:  }
  000f 30           [3]     PULX  
  0010 0a           [7]     RTC   
   85:  
   86:  ////////////////////////////////////////////////////////////////////////////////
   87:  ////////////////////////////////////////////////////////////////////////////////
   88:  ////////////////////////////////////////////////////////////////////////////////
   89:  ////////////////////////////////////////////////////////////////////////////////
   90:  
   91:  #endif
   92:  
