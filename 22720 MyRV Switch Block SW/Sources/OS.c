// OS.c
// Program entry point
// (c) 2012 Innovative Design Solutions, Inc.
// All rights reserved

#include "global.h"

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#ifdef HEADER

void OS_Startup(void);
static void DeviceEnable(void);

#pragma CODE_SEG __NEAR_SEG NON_BANKED
__interrupt void OS_OnTIMCH1Interrupt(void);
#pragma CODE_SEG DEFAULT

#pragma CODE_SEG __NEAR_SEG NON_BANKED
uint16 OS_GetElapsedMilliseconds16(void);
uint32 OS_GetElapsedMilliseconds32(void);
#pragma CODE_SEG DEFAULT

#else

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// 1 millisecond interrupt
#define TIMER_INTERRUPT_COUNTS  (uint16)((FBUS * 0.001) + 0.5)

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

static volatile DWORD ElapsedMilliseconds = { 0 };

#pragma CODE_SEG __NEAR_SEG NON_BANKED
uint16 OS_GetElapsedMilliseconds16(void) { return AtomicRead16(&ElapsedMilliseconds.ui16.lo); };
uint32 OS_GetElapsedMilliseconds32(void) { return AtomicRead32(&ElapsedMilliseconds.ui32); }
#pragma CODE_SEG DEFAULT

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

void Startup_InitSFR(void)
{
  DisableInterrupts; 

  // pin routing register
  // this only applies to the 20 pin package
  // PRR0 00000000
  //      ||||||||
  //      |||||| \\__ SCI0 RX / TX routing   00 = PE0 / PE1   01 = PS4 / PS7   10 = PAD4 / PAD57   11 = reserved
  //      |||| \\____ IOC2 routing           00 = PS5   01 = PE0   10 = PAD4   11 = reserved
  //      || \\______ IOC3 routing           00 = PS6   01 = PE1   10 = PAD5   11 = reserved
  //      | \________ PWM2/ETRIG2 routing    0 = PS4   1 = PAD4
  //       \_________ PWM3/ETRIG3 routing    0 = PS7   1 = PAD5
  PRR0 = 0x00;

  // pin routing register
  // this only applies to the 100 pin package
  // PRR1 -------0
  //             |
  //              \__ 0 = AN inputs on PORTAD    1 = AN inputs on PORTC
  //  PRR1 = 0x00;

  // pullup/pulldown control register
  // PUCR -1-11111 data register
  //       | |||||
  //       | |||| \__ PUPAE  1 = enable pullup on PORT A     0 = pullup disabled
  //       | ||| \___ PUPBE  1 = enable pullup on PORT B     0 = pullup disabled
  //       | || \____ PUPCE  1 = enable pullup on PORT C     0 = pullup disabled
  //       | | \_____ PUPDE  1 = enable pullup on PORT D     0 = pullup disabled
  //       |  \______ PDPEE  1 = enable pulldown on PORT E   0 = pulldown disabled
  //        \________ BKPUE  1 = enable pullup on BKGD pin   0 = pullup disabled
  PUCR = 0x5F;

  // PORT AD01
  //  PT01AD  xxxxxxxx xxxxxxxx data register
  //  PTI01AD -------- -------- input only
  //  DDR01AD 00000000 00000000 data direction register
  //  PER01AD 11111111 11111100 pull up enable register
  //  PPS01AD xxxxxxxx xxxxxxxx interrupt polarity select register (1 = pulldown, 0 = pullup)
  //  PIE01AD 00000000 00000000 interrupt enable register
  //  PIF01AD 11111111 11111111 interrupt flag register
  //  ATDDIEN 00000011 11111111 digital input enable (set to 1 to enable digital readout on PT1AD)
  //          |||||||| ||||||||
  //          |||||||| ||||||| \__ F4_EXTEND_SIGNAL
  //          |||||||| |||||| \___ F4_RETRACT_SIGNAL
  //          |||||||| ||||| \____ unused
  //          |||||||| |||| \_____ unused
  //          |||||||| ||| \______ unused
  //          |||||||| || \_______ unused
  //          |||||||| | \________ unused
  //          ||||||||  \_________ unused
  //          ||||||| \___________ unused
  //          |||||| \____________ unused
  //          ||||| \_____________ unused
  //          |||| \______________ unused
  //          ||| \_______________ unused
  //          || \________________ unused
  //          | \_________________ unused
  //           \__________________ unused
  PT01AD  = 0x0000;
  DDR01AD = 0x0000;
  PER01AD = 0xFFFC;
  PPS01AD = 0x0000;
  PIE01AD = 0x0000;
  PIF01AD = 0xFFFF; // clear flags
  ATDDIEN = 0x03FF;

  // PORT A  
  // PORTA xxxxxxxx data register
  //  DDRA 00000000 data data direction register
  //       ||||||||
  //       ||||||| \__ unused
  //       |||||| \___ unused
  //       ||||| \____ unused
  //       |||| \_____ unused
  //       ||| \______ unused
  //       || \_______ unused
  //       | \________ unused
  //        \_________ unused
  PORTA = 0x00;
  DDRA  = 0x00;

  // PORT B
  // PORTB x0xxxxxx data register
  //  DDRB 01000000 data data direction register
  //       ||||||||
  //       ||||||| \__ unused
  //       |||||| \___ unused
  //       ||||| \____ unused
  //       |||| \_____ unused
  //       ||| \______ unused
  //       || \_______ unused
  //       | \________ unused
  //        \_________ unused
  PORTB = 0x00;
  DDRB  = 0x00;

  // PORT C  
  // PORTC xxxxxxxx data register
  //  DDRC 00000000 data data direction register
  //       ||||||||
  //       ||||||| \__ unused
  //       |||||| \___ unused
  //       ||||| \____ unused
  //       |||| \_____ unused
  //       ||| \______ unused
  //       || \_______ unused
  //       | \________ unused
  //        \_________ unused
  PORTC = 0x00;
  DDRC  = 0x00;

  // PORT D
  // PORTD x0xxxxxx data register
  //  DDRD 01000000 data data direction register
  //       ||||||||
  //       ||||||| \__ unused
  //       |||||| \___ unused
  //       ||||| \____ unused
  //       |||| \_____ unused
  //       ||| \______ unused
  //       || \_______ unused
  //       | \________ unused
  //        \_________ unused
  PORTD = 0x00;
  DDRD  = 0x00;

  // PORT E
  // PORTE x0xxxxxx data register
  //  DDRE 01000000 data data direction register
  //       ||||||||
  //       ||||||| \__ unused
  //       |||||| \___ unused
  //       ||||| \____ unused
  //       |||| \_____ unused
  //       ||| \______ unused
  //       || \_______ unused
  //       | \________ unused
  //        \_________ unused
  PORTE = 0x00;
  DDRE  = 0x00;

  // PORT J
  //  PTJ xxxxxxxx data register
  // PTIJ -------- input register
  // DDRJ 00000000 data direction register
  // PERJ 11111111 pull resistor enable register
  // PPSJ 00000000 pull resistor polarity select (0 = pullup, 1 = pulldown)
  // PIEJ 00000000 interrupt enable register
  // PIFJ 11111111 interrupt flag register
  //      ||||||||
  //      ||||||| \__ unused
  //      |||||| \___ unused
  //      ||||| \____ unused
  //      |||| \_____ unused
  //      ||| \______ unused
  //      || \_______ unused
  //      | \________ unused
  //       \_________ unused
  PTJ  = 0x00;
  DDRJ = 0x00;
  PERJ = 0xFF;
  PPSJ = 0x00;
  PIEJ = 0x00;
  PIFJ = 0xFF; // clear flags

  // PORT M
  //  PTM xxxxxx1x data register
  // PTIM -------- input register
  // DDRM 00000010 data direction register
  // PERM 11111101 pull resistor enable register
  // PPSM 00000000 pull resistor polarity select (0 = pullup, 1 = pulldown)
  // WOMM 00000000 wired or mode (0 = push/pull, 1 = open-drain)
  //      ||||||||
  //      ||||||| \__ RXCAN
  //      |||||| \___ TXCAN
  //      ||||| \____ unused
  //      |||| \_____ unused
  //      ||| \______ unused
  //      || \_______ unused
  //      | \________ unused
  //       \_________ unused
  PTM  = 0x02;
  DDRM = 0x02;
  PERM = 0xFD;
  PPSM = 0x00;
  WOMM = 0x00;

  // PORT P
  //  PTP xx000000 data register
  // PTIP -------- input register
  // DDRP 00000000 data direction register
  // PERP 11000000 pull resistor enable register
  // PPSP 00000000 pull resistor polarity select (0 = pullup, 1 = pulldown)
  // PIEP 00000000 interrupt enable register
  // PIFP 11111111 interrupt flag register
  //      ||||||||
  //      ||||||| \__ F1_EXTEND_SIGNAL
  //      |||||| \___ F1_RETRACT_SIGNAL
  //      ||||| \____ F2_EXTEND_SIGNAL
  //      |||| \_____ F2_RETRACT_SIGNAL
  //      ||| \______ F3_EXTEND_SIGNAL
  //      || \_______ F3_RETRACT_SIGNAL
  //      | \________ unused
  //       \_________ unused
  PTP  = 0x00;
  DDRP = 0x00;
  PERP = 0xC0;
  PPSP = 0x00;
  PIEP = 0x00;
  PIFP = 0xFF; // clear flags

  // PORT S
  //  PTS 1xxxxx1x data register
  // PTIS -------- input register
  // DDRS 10000010 data direction register
  // PERS 01111101 pull resistor enable register
  // PPSS 00000000 pull resistor polarity select (0 = pullup, 1 = pulldown)
  // WOMS 00000000 wired or mode (0 = push/pull, 1 = open-drain)
  //      ||||||||
  //      ||||||| \__ RXD0
  //      |||||| \___ TXD0
  //      ||||| \____ unused
  //      |||| \_____ unused
  //      ||| \______ unused
  //      || \_______ unused
  //      | \________ unused
  //       \_________ CAN_STANDBY
  PTS  = 0x82;
  DDRS = 0x82;
  PERS = 0x7C;
  PPSS = 0x00;
  WOMS = 0x00;

  // PORT T
  //  PTT xx0xxx00 data register
  // PTIT -------- input register
  // DDRT 00100011 data direction register
  // PERT 11011100 pull resistor enable register
  // PPST 00000000 pull resistor polarity select (0 = pullup, 1 = pulldown)
  //      ||||||||
  //      ||||||| \__ DEBUG_1
  //      |||||| \___ DEBUG_2
  //      ||||| \____ unused
  //      |||| \_____ unused
  //      ||| \______ unused
  //      || \_______ BACKLIGHT_EN
  //      | \________ unused
  //       \_________ unused
  PTT  = 0x00;
  DDRT = 0x23;
  PERT = 0xDC;
  PPST = 0x00;
}

void main(void)
{
  // device initialization
  // code auto generated by dev environment
  MCU_init();

  Startup_InitSFR();

  // module initialization
  SCI_Init();        // UART driver
  Input_Init();      // Input driver
  EEPROM_Init();     // EEPROM driver
  Config_Init();     // Config driver
  CAN_Init();        // CAN driver
  IDS_CAN_Init(); 	// IDS-CAN network support
  
  Session_Init();
  DeviceEnable();
  

  // intialize real-time interrupt
  TSCR2 = 0x00;   // prescale 0 : clock = FBUS / 1
  TSCR1 = 0xA0;   // enable the timer, stop timer while debugging
  TIOS_IOS1 = 1;  // enable output compare 1
  TTOV = 0x00;    // toggle output compare pin on overflow disabled
  TCTL1 = 0x00;   // no action on output compare
  TCTL2 = 0x00;   // no action on output compare
  TCTL3 = 0x00;   // input capture disabled
  TCTL4 = 0x00;   // input capture disabled
  TIE_C1I = 1;    // enable channel 1 interrupt (output compare)
  TC1 = TCNT + TIMER_INTERRUPT_COUNTS;
#ifdef DEBUG  
#warning ******DEBUG BUILD
  Config.ProductionBlock.ProductionByte = 0x00;
#endif
  
  Assert_BACKLIGHT_EN;
  
  // background program
  for(;;)
  {
    static uint8 _10ms = 0;
    static uint8 timer = 0;
    EnableInterrupts; 

    // 10 millisecond program
    if ((uint8)(ElapsedMilliseconds.word.lo.ui8.lo - _10ms) >= 10)
    {
      _10ms += 10;

      // service the COP while interrupts appear to be running    
      _FEED_COP();
      
      Input_Task();
      Session_Task10ms();
     
    } 
    
    
    Config_Task();
    SCI_Task();
    SCIHost_Task();
    CAN_Task();
    IDS_CAN_Task();
  }
}

static void DeviceEnable() 
{
  uint8 i;
  
  //enable devices
  for (i=0; i< NUM_IDS_CAN_DEVICES;i++) 
  {
       IDS_CAN_EnableDevice(i);
  }
  
}
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

// runs every millisecond
#pragma CODE_SEG __NEAR_SEG NON_BANKED
__interrupt void OS_OnTIMCH1Interrupt(void)
{
  // service interrupt
  TFLG1 = 0x02;
  TC1 += TIMER_INTERRUPT_COUNTS;

#ifdef DEBUG_2
	DEBUG_2 ^= 1;
#endif

	// update millisecond counter
	ElapsedMilliseconds.ui32++;
}
#pragma CODE_SEG DEFAULT

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#endif
