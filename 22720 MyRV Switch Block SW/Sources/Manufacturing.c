// Manufacturing.c
// 17741 LCI UDCS Multiplex 6-Switch Touch Panel SW S12G
// (c) 2012 Innovative Design Solutions, Inc.
// All rights reserved

#include "global.h"

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#ifdef HEADER

uint8 Manufacturing_MessageReceived(const uint8 * message);

void Manufacturing_OnIDSCANSessionOpened(IDS_CAN_DEVICE device, uint8 host_address, IDS_CAN_SESSION_ID id);
void Manufacturing_OnIDSCANSessionClosed(IDS_CAN_DEVICE device, uint8 address, IDS_CAN_SESSION_ID id);

#else

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

void Callback_OnConfigProductionBlockLoadEvent(uint8 success)
{
	if (!success)
	{
		Config.ProductionBlock.ProductionByte = 0xFF;

		Config.ProductionBlock.MAC[0] = Random8(); 
		Config.ProductionBlock.MAC[1] = Random8();
		Config.ProductionBlock.MAC[2] = Random8();
		Config.ProductionBlock.MAC[3] = Random8();
		Config.ProductionBlock.MAC[4] = Random8();
		Config.ProductionBlock.MAC[5] = Random8();
	}
}

#pragma MESSAGE DISABLE C5703 /* variables not used */
void Manufacturing_OnIDSCANSessionOpened(IDS_CAN_DEVICE device, uint8 host_address, IDS_CAN_SESSION_ID id)
{
}
#pragma MESSAGE DEFAULT C5703

#pragma MESSAGE DISABLE C5703 /* variables not used */
void Manufacturing_OnIDSCANSessionClosed(IDS_CAN_DEVICE device, uint8 host_address, IDS_CAN_SESSION_ID id)
{
}
#pragma MESSAGE DEFAULT C5703

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

uint8 Manufacturing_MessageReceived(const uint8 * message)
{
	uint8 len = message[0];

	switch (message[2])
	{
	default:
		break;

	case 0x00:	// Enter/exit manufacturing mode:  F9 00 01/00   
	  switch (len)
		{
    case 3:
			Config.ProductionBlock.ProductionByte = message[3];
			Config_ProductionBlock_Flush();
			//lint -fallthrough
    case 2:
  		return SCI_Tx(3, message[1], message[2], Config.ProductionBlock.ProductionByte);
		}
  	break;
	}

	return TRUE;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#endif
