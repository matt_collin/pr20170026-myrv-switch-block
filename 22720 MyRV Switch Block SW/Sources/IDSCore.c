// IDSCore.c
// IDS Core files for embedded systems
// Version 1.2
// (c) 2017 Innovative Design Solutions, Inc.
// All rights reserved

// History
// Version 1.0   First version
// Version 1.1   Linted
// Version 1.2   Added IDSCore_GetBuildDateString() and IDSCore_GetBuildTimeString()

#include "global.h"

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#ifdef HEADER

////////////////////////////////////////

#if (defined __HC12__) && (defined __HIWARE__) && (__MWERKS__ == 1)
#pragma CODE_SEG __NEAR_SEG NON_BANKED  /* core routines are in near memory */
#endif

////////////////////////////////////////

#if defined __BIG_ENDIAN__
	#define HILO hi,lo
#elif defined __LITTLE_ENDIAN__
	#define HILO lo,hi
#else
	#error __BIG_ENDIAN__ or __LITTLE_ENDIAN__ must be defined
#endif

////////////////////////////////////////

#define elementsof(n) (sizeof(n)/(sizeof((n)[0])))

////////////////////////////////////////

#define  BIT0                        ( (uint32)( 1       ) )
#define  BIT1                        ( (uint32)( 1 <<  1 ) )
#define  BIT2                        ( (uint32)( 1 <<  2 ) )
#define  BIT3                        ( (uint32)( 1 <<  3 ) )
#define  BIT4                        ( (uint32)( 1 <<  4 ) )
#define  BIT5                        ( (uint32)( 1 <<  5 ) )
#define  BIT6                        ( (uint32)( 1 <<  6 ) )
#define  BIT7                        ( (uint32)( 1 <<  7 ) )
#define  BIT8                        ( (uint32)( 1 <<  8 ) )
#define  BIT9                        ( (uint32)( 1 <<  9 ) )
#define BIT10                        ( (uint32)( 1 << 10 ) )
#define BIT11                        ( (uint32)( 1 << 11 ) )
#define BIT12                        ( (uint32)( 1 << 12 ) )
#define BIT13                        ( (uint32)( 1 << 13 ) )
#define BIT14                        ( (uint32)( 1 << 14 ) )
#define BIT15                        ( (uint32)( 1 << 15 ) )
#define BIT16                        ( (uint32)( 1 << 16 ) )
#define BIT17                        ( (uint32)( 1 << 17 ) )
#define BIT18                        ( (uint32)( 1 << 18 ) )
#define BIT19                        ( (uint32)( 1 << 19 ) )
#define BIT20                        ( (uint32)( 1 << 20 ) )
#define BIT21                        ( (uint32)( 1 << 21 ) )
#define BIT22                        ( (uint32)( 1 << 22 ) )
#define BIT23                        ( (uint32)( 1 << 23 ) )
#define BIT24                        ( (uint32)( 1 << 24 ) )
#define BIT25                        ( (uint32)( 1 << 25 ) )
#define BIT26                        ( (uint32)( 1 << 26 ) )
#define BIT27                        ( (uint32)( 1 << 27 ) )
#define BIT28                        ( (uint32)( 1 << 28 ) )
#define BIT29                        ( (uint32)( 1 << 29 ) )
#define BIT30                        ( (uint32)( 1 << 30 ) )
#define BIT31                        ( (uint32)( 1 << 31 ) )

////////////////////////////////////////

#ifndef NULL
  #define NULL 0
#endif
#ifndef TRUE
  #define TRUE 1
#endif
#ifndef FALSE
  #define FALSE 0
#endif

////////////////////////////////////////

// BYTE structure
typedef uint8 BYTE;

// WORD structure
typedef union {
    int16 i16;                     // access as int16
    uint16 ui16;                   // access as uint16
    struct { int8 HILO; } i8;    // access as int8
    struct { uint8 HILO; } ui8;  // access as uint8
} WORD;

// DWORD structure
typedef union {
    int32 i32;                      // access as int32
    uint32 ui32;                    // access as uint32
    struct { int16 HILO; } i16;   // access as int16
    struct { uint16 HILO; } ui16; // access as uint16
    struct { WORD HILO; } word; // access as two separate WORD values
} DWORD;

////////////////////////////////////////

// build date/time

const char * IDSCore_GetBuildDateString(void);
const char * IDSCore_GetBuildTimeString(void);

uint16 IDSCore_GetSoftwareBuildYear(void);  // 2017, etc
uint8 IDSCore_GetSoftwareBuildMonth(void);  // 1 = Jan, 2 = Feb, ... 12 = Dec
uint8 IDSCore_GetSoftwareBuildDay(void);    // 1 - 31
uint8 IDSCore_GetSoftwareBuildHour(void);   // 0 - 23
uint8 IDSCore_GetSoftwareBuildMinute(void); // 0 - 59
uint8 IDSCore_GetSoftwareBuildSecond(void); // 0 - 59

////////////////////////////////////////

// standard math functions
uint8 abs_i8(int8 value);
uint16 abs_i16(int16 value);
uint32 abs_i32(int32 value);
int8 i16_to_i8(int16 val);
uint8 ui16_to_ui8(uint16 val);
int16 i32_to_i16(int32 val);
uint16 ui32_to_ui16(uint32 val);

////////////////////////////////////////

// pseudo-random number generator
uint8 Random8(void);
uint16 Random16(void);
uint32 Random32(void);
void Randomize(uint32 seed);

////////////////////////////////////////

// hash function
uint32 Hash32(uint32 hash, uint8 length, const uint8 * buffer);

////////////////////////////////////////

#if (defined __HC12__) && (defined __HIWARE__) && (__MWERKS__ == 1)
#pragma CODE_SEG DEFAULT
#endif

////////////////////////////////////////

#else // HEADER

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#if defined(__GNUC__)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunknown-pragmas"
#endif

#if (defined __HC12__) && (defined __HIWARE__) && (__MWERKS__ == 1)
#pragma CODE_SEG __NEAR_SEG NON_BANKED  /* core routines are in near memory */
#endif

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

//                                                 0123456789A
static const char BuildDateString[] = __DATE__; // Feb dd yyyy
static const char BuildTimeString[] = __TIME__; // hh:mm:ss

const char * IDSCore_GetBuildDateString(void) { return BuildDateString; }
const char * IDSCore_GetBuildTimeString(void) { return BuildTimeString; }


#define DIGIT(c)   (uint8)(((c) >= '0' && (c) <= '9') ? ((c) - '0') : 0)
#define BYTEVAL(buf, pos)    ((uint8)(DIGIT(buf[pos]) * 10 + DIGIT(buf[pos+1])))

#pragma INLINE
uint16 IDSCore_GetSoftwareBuildYear(void)  { return (uint16)BYTEVAL(BuildDateString, 7) * 100 + BYTEVAL(BuildDateString, 9); }
#pragma INLINE
uint8 IDSCore_GetSoftwareBuildDay(void)    { return BYTEVAL(BuildDateString, 4); }
#pragma INLINE
uint8 IDSCore_GetSoftwareBuildHour(void)   { return BYTEVAL(BuildTimeString, 0); } /*lint !e835 shifting by zero */
#pragma INLINE
uint8 IDSCore_GetSoftwareBuildMinute(void) { return BYTEVAL(BuildTimeString, 3); }
#pragma INLINE
uint8 IDSCore_GetSoftwareBuildSecond(void) { return BYTEVAL(BuildTimeString, 6); }

#pragma MESSAGE DISABLE C4000 /* condition always true */
#pragma MESSAGE DISABLE C4001 /* condition always false */
#pragma MESSAGE DISABLE C5660 /* removed dead code */
uint8 IDSCore_GetSoftwareBuildMonth(void)
{
	// via process of elimination
	// NOTE: this may be optimized on some compilers!
	if (BuildDateString[0] == 'F') return 2; // Feb
	if (BuildDateString[0] == 'S') return 9; // Sep
	if (BuildDateString[0] == 'O') return 10; // Oct
	if (BuildDateString[0] == 'N') return 11; // Nov
	if (BuildDateString[0] == 'D') return 12; // Dec
	if (BuildDateString[1] == 'p') return 4; // Apr
	if (BuildDateString[2] == 'r') return 3; // Mar
	if (BuildDateString[2] == 'y') return 5; // May
	if (BuildDateString[2] == 'l') return 7; // Jul
	if (BuildDateString[2] == 'g') return 8; // Aug
	if (BuildDateString[1] == 'a') return 1; // Jan
	return 6; // Jun
}
#pragma MESSAGE DEFAULT C4000 /* condition always true */
#pragma MESSAGE DEFAULT C4001 /* condition always false */
#pragma MESSAGE DEFAULT C5660 /* removed dead code */

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

uint8 abs_i8(int8 value)
{
  if (value >= 0)
    return (uint8)value;  
  return (uint8)(-value);
}

uint16 abs_i16(int16 value)
{
  if (value >= 0)
    return (uint16)value;  
  return (uint16)(-value);
}

uint32 abs_i32(int32 value)
{
  if ((uint32)value & 0x80000000)
    return (uint32)(-value);
  return (uint32)value;  
}

int8 i16_to_i8(int16 val)
{
  if (val >= 127) return 127;
  if (val <= -128) return -128;
  return (int8)val;  
}

uint8 ui16_to_ui8(uint16 val)
{
  if (val & 0xFF00) val |= 0xFF;
  return (uint8)val;  
}

int16 i32_to_i16(int32 val)
{
  if (val >= 32767) return 32767;
  if (val <= -32768) return -32768;
  return (int16)val;  
}

uint16 ui32_to_ui16(uint32 val)
{
  if (((DWORD*)&val)->ui16.hi) return 0xFFFF;
  return (uint16)val;  
}

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

// pseudo-random number generator

#if (defined __HC12__) && (defined __HIWARE__) && (__MWERKS__ == 1)
#pragma DATA_SEG NO_INIT_RAM
#endif
//lint -e728  not explicitly initialized
static uint32 RandSeed; // no need to initialize
//lint +e728  not explicitly initialized
#if (defined __HC12__) && (defined __HIWARE__) && (__MWERKS__ == 1)
#pragma DATA_SEG DEFAULT
#endif

uint32 Random32(void)
{
	RandSeed *= 0x41C64E6D;
	RandSeed += 0x9EC33039;
	return RandSeed;
}

uint16 Random16(void)
{
	return Random32() >> 16;
}

uint8 Random8(void)
{
	return Random32() >> 24;
}

void Randomize(uint32 seed)
{
	RandSeed += seed;
}

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

// hash function
// sdbm algorithm
uint32 Hash32(uint32 hash, uint8 length, const uint8 * buffer)
{
	while (length--)
	{
		hash = (hash * 65599) + *buffer;
		buffer++;
	}

	return hash;
}

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

#if defined(__GNUC__)
#pragma GCC diagnostic pop
#endif

#if (defined __HC12__) && (defined __HIWARE__) && (__MWERKS__ == 1)
#pragma CODE_SEG DEFAULT
#endif

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#endif // HEADER
