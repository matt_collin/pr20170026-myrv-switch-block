// IDS-CAN_DTC_Manager.c
// DTC manager for IDS-CAN
// Version 2.8
// (c) 2017 Innovative Design Solutions, Inc.
// All rights reserved

// Each DTC code represents a single diagnostic failure detected in the system
//
// DTCs reporting and management is done in a manner similar to that used by automotive OEMs
//
// A DTC can be in one of three states:
//     Active - DTC is presently debounced in the matured state
//     Stored - DTC has matured in the last 127 power cycles
//     Cleared - DTC has not matured in at least 127 power cycles
//
// DTCs record two pieces of information:
//           Active - 1 bit - true if DTC is mature, false if DTC is not mature
//    CyclesCounter - 7 bits - indicates number of power/boot/ignition/etc cycles since DTC last matured
//
// DTC maturation:
//     Diagnostic conditions are monitored by the application.
//     The application can call IDS_CAN_MatureDTC() to mature/de-mature the DTC
//     DTCs are debounced automatically by this module
//
//     When the DTC debounces to "active", the Active flag is set to TRUE, and the CycleCounter is set to 1
//     When the DTC debounces to "inactive", the Active flag is set to FALSE, the CycleCounter remains unchanged
//
//     The Active flag will automatically clear whenever the CycleCounter overflows
//
// DTC cycle counting
//     A CyclesCounter is maintained, which indicates the number of power/boot/etc cycles since the DTC last matured
//     The value is set to 1 whenever the DTC matures, indicating that the DTC is stored
//
//     When the value is non-zero, it will be incremented every reboot, or upon application request via IDS_CAN_IncrementDTCCyclesCounter()
//     Therefore, a non-zero value indicates how many power cycles since the DTC last matured
//
//     The value will overflow at 128 counts.
//     When the counter overflows, the Active flag is automatically cleared, as it has been 128 cycles since the DTC matured

// History
// 1.0  -- First revision
// 2.7  -- Updated to use ASSERT() macro
// 2.8  -- Linted

#include "global.h"

#ifdef DECLARE_SUPPORTED_IDS_CAN_DTCS

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#ifdef HEADER

// enumerate DTCs by name
typedef enum {
	#undef DECLARE_DTC
	#define DECLARE_DTC(dtc, debounce) dtc
	#undef DEFINE_DTC
	#define DEFINE_DTC(id, name) name,
	DECLARE_SUPPORTED_IDS_CAN_DTCS
	NUM_IDS_CAN_DTCS
} IDS_CAN_DTC;

// define structure for reporting DTCs
typedef struct { uint16 ID; uint8 Status; } IDS_CAN_DTC_INFO;

// background management tasks
void _IDS_CAN_DTC_Init(void);     // private function do not call, instead call IDS_CAN_Init()
void _IDS_CAN_DTC_Task10ms(void); // private function do not call, instead call IDS_CAN_Task()

// user API
void IDS_CAN_SetDTC(IDS_CAN_DTC dtc);                        // sets the DTC to active (debounce set to max value) and resets cycle count to 1
void IDS_CAN_ClearDTC(IDS_CAN_DTC dtc);                      // clears the DTC active state (debounce counter cleared)
void IDS_CAN_MatureDTC(IDS_CAN_DTC dtc, uint8 dtc_detected); // matures/de-matures Active DTCs by incrementing/decrementing the debounce counter
void IDS_CAN_IncrementDTCCyclesCounter(void);                // increments all non-zero cycle counters by 1

// DTC state querying
uint16 IDS_CAN_GetNumSupportedDTCs(void);              // returns the number of DTCs supported
uint16 IDS_CAN_GetNumActiveDTCs(void);                 // returns the number of DTCs that are in the active state
uint16 IDS_CAN_GetNumStoredDTCs(void);                 // returns the number of DTCs that are in the stored state
uint8 IDS_CAN_AreAnyDTCsActive(void);                  // returns non-zero if one or more DTCs are active
uint8 IDS_CAN_AreAnyDTCsStored(void);                  // returns non-zero if one or more DTCs are stored
uint8 IDS_CAN_IsDTCActive(IDS_CAN_DTC dtc);            // returns non-zero if DTC is active
uint8 IDS_CAN_IsDTCStored(IDS_CAN_DTC dtc);            // returns non-zero if DTC is stored
IDS_CAN_DTC_INFO IDS_CAN_GetDTCInfo(IDS_CAN_DTC dtc);  // returns DTC ID and status byte

// global DTC clearing
void IDS_CAN_ClearActiveDTCs(void);  // clears all active DTCs and debounce values (stored counters are not modified)
void IDS_CAN_ClearStoredDTCs(void);  // clears stored cycle counters for all DTCs that are not Active
void IDS_CAN_ClearAllDTCs(void);     // clears all active and stored DTCs, and resets debounce values to zero

#else // HEADER

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// determine smallest variable size that can hold debounce information
#undef DECLARE_DTC
#define DECLARE_DTC(dtc, debounce) | (debounce)
#if (0 DECLARE_SUPPORTED_IDS_CAN_DTCS) > 255
	typedef uint16 DEBOUNCE;
#else
	typedef uint8 DEBOUNCE;
#endif

// create ROM table of DTC debounce maximums
static const DEBOUNCE DEBOUNCE_MAX[NUM_IDS_CAN_DTCS] = {
	#undef DECLARE_DTC
	#define DECLARE_DTC(dtc, debounce) (debounce),
	DECLARE_SUPPORTED_IDS_CAN_DTCS
};

// allocate RAM for DTCs
static struct
{
	uint8 Status[NUM_IDS_CAN_DTCS];      // BIT7 = active   BIT0-6 = power cycles counter (stored)
	DEBOUNCE Debounce[NUM_IDS_CAN_DTCS]; // Debounce maturation value for DTC
} DTC;

#define IS_ACTIVE(dtc) (DTC.Status[dtc] & 0x80)
#define IS_STORED(dtc) (DTC.Status[dtc])

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

#ifdef IDS_CAN_DTC_NUM_STORAGE_BLOCKS

// DTC flushing to storage
//
// Storage is performed on a block device, which provides for the following operations:
//     Bulk erase of a block
//     8-bit random block reads
//     32-bit aligned block writes
//
// DTC storage blocks are arranged as a block header, followed by an array of discrete fixed-sizedchunks
//     BLOCK HEADER
//     CHUNK 1
//     CHUNK 2
//     ...
//     CHUNK n
//
// BLOCK HEADER:
// The first 4 bytes of the block are reserved for a header.
//     byte 0  sequence number, incremented each block, used to identify most recently written block
//     byte 1  ASCII 'D'
//     byte 2  ASCII 'T'
//     byte 3  ASCII 'C'
// The header is checked at boot ensure the DTC data in the block is valid/initialized.
// The sequence byte is incremented by one for each block written, letting the firmware identify the most recently written data block
// The sequence byte is the first byte written, to ensure it is stored/valid before the rest of the signature is written
//
// CHUNK:
// Each individual chunk written consists of the following data
//       name           size          description
//     START_FLAG        1            indicates chunk is not empty
//     DTCs        NUM_IDS_CAN_DTCS   status of each DTC
//     CRC               1            CRC of DTC data
//     END_FLAG          1            indicates that complete chunk was successfully written
// START_FLAG and END_FLAG are used as interlocks.
// When writing a chunk, START_FLAG must be written first, and END_FLAG must be written last.
// These interlock flags allows the software to identify "empty" chunks, "partially written" chunks, and "fully written" chunks

// sanity check
// need at least two storage blocks to double buffer
// if we have too many blocks, we can't tell where the sequence begins or ends
#if IDS_CAN_DTC_NUM_STORAGE_BLOCKS < 2
	#error IDS_CAN_DTC_NUM_STORAGE_BLOCKS is too small
#endif
#if IDS_CAN_DTC_NUM_STORAGE_BLOCKS > 250
	#error IDS_CAN_DTC_NUM_STORAGE_BLOCKS is too large
#endif

#define ALIGN_4(size)       ((uint16)((((size) + 3) / 4) * 4))

#define BLOCK_HEADER_SIZE   4
#define CHUNK_SIZE          ALIGN_4((uint16)NUM_IDS_CAN_DTCS + 3) /* MUST align on 4 byte boundary */
#define NUM_CHUNKS          ((uint16)((IDS_CAN_DTC_STORAGE_BLOCK_SIZE - BLOCK_HEADER_SIZE) / CHUNK_SIZE))
#define START_FLAG          0x55
#define END_FLAG            0xAA

#define CHUNK_OFFSET_START_FLAG  0
#define CHUNK_OFFSET_DTC_DATA    1
#define CHUNK_OFFSET_CRC         ((uint16)NUM_IDS_CAN_DTCS + 1)
#define CHUNK_OFFSET_END_FLAG    ((uint16)NUM_IDS_CAN_DTCS + 2)

static struct
{
	uint8 NeedsFlush;   // true when a flush should occur
	uint8 SequenceNum;  // sequential number used to identify newest block, should always be 1 greater than last block
	uint8 Block;        // current storage block being written
	uint16 Chunk;       // current chunk being written
	uint8 CRC;          // running CRC for chunk being flushed
} Flush;

// macro that calculates the base offset of the current Flush.Chunk
#define CHUNK_OFFSET  ((CHUNK_SIZE * Flush.Chunk) + BLOCK_HEADER_SIZE)

// reads data from storage, gives up after 255 failed attempts
static uint8 TryBlockRead(uint16 offset, uint16 count, void* buffer)
{
	uint8 attempts = 0xFF;
     do
     {
		if (IDS_CAN_DTC_STORAGE_READ(Flush.Block, offset, count, buffer))
			return TRUE;
     } while (--attempts);
     return FALSE;
}

// read a byte from current Flush.Chunk in the current Flush.Block
static uint16 TryChunkRead(uint16 offset)
{
	uint8 result;
	if (TryBlockRead(CHUNK_OFFSET + offset, 1, &result))
		return result;
	return 0xFFFF;
}

// reads and verifies the header for the current Flush.Block
static uint8 VerifyBlockHeader(void)
{
	uint8 buf[4];
	if (!TryBlockRead(0, 4, buf)) return FALSE;
	if (buf[1] != 'D') return FALSE;
	if (buf[2] != 'T') return FALSE;
	if (buf[3] != 'C') return FALSE;
	Flush.SequenceNum = buf[0];
	return TRUE;
}

// tries to load a chunk of data into DTC RAM
// checks interlock flags and CRC to ensure clean loading
// returns TRUE on success, FALSE on failure
static uint8 TryLoadDTCsFromChunk(void)
{
	uint8 dtc;

	// check interlock flags
	if (TryChunkRead(CHUNK_OFFSET_START_FLAG) != START_FLAG) return FALSE;
	if (TryChunkRead(CHUNK_OFFSET_END_FLAG) != END_FLAG) return FALSE;

	// load the DTC data into RAM
	if (!TryBlockRead(CHUNK_OFFSET + CHUNK_OFFSET_DTC_DATA, (uint16)NUM_IDS_CAN_DTCS, DTC.Status)) return FALSE;

	// read/verify the CRC
	if (CRC8_Calculate(DTC.Status, (uint16)NUM_IDS_CAN_DTCS) != TryChunkRead(CHUNK_OFFSET_CRC))
		return FALSE;

	// by definition, stored DTCs read at boot are not "active"
	// if the fault was active when flushed, we still need to read it into the inactive state
	for (dtc=0; dtc < (uint8)NUM_IDS_CAN_DTCS; dtc++)
		DTC.Status[dtc] &= 0x7F;

	return TRUE;
}

// locates the newest/valid chunk of data in Flash.Block, and then loads it into DTC RAM
// returns TRUE if a chunk was found and loaded sucessfully, FALSE otherwise
static uint8 TryLoadDTCsFromBlock(void)
{
	uint16 first_free_chunk;

	// starting at the end of the block, scan backwards and find the most recent data chunk
	Flush.Chunk = NUM_CHUNKS - 1; // start with last chunk
	for (;;)
	{
		// check the interlock flag at the start of the chunk
		uint16 result = TryChunkRead(CHUNK_OFFSET_START_FLAG);
		if (result == START_FLAG) break; // this chunk is valid and contains data
		if (result != IDS_CAN_DTC_STORAGE_ERASED_VALUE) return FALSE; // if the chunk is not valid, then this byte should be erased

		// chunk is empty, scan downwards till we find a non-empty chunk
		if (Flush.Chunk == 0) return FALSE; // no more chunks
		Flush.Chunk--;
	}

	// the first free chunk is the one beyond this chunk
	first_free_chunk = Flush.Chunk + 1;

	// try loading chunks until we load one sucessfully
	while (!TryLoadDTCsFromChunk())
	{
		// scan downwards to next chunk, eventually we should find a valid chunk that loads
		if (Flush.Chunk == 0) return FALSE; // no more chunks
		Flush.Chunk--;
	}

	// chunk loaded!

	// we must return pointing to the first free chunk in the block
	// which is where the first flush operation will write to
	Flush.Chunk = first_free_chunk;

	return TRUE;
}

static uint8 LoadDTCsFromStorage(void)
{
	uint8 known_good_block;
	uint8 known_good_seq;

	// initialize static variables
	Flush.NeedsFlush = FALSE;

	// find the first valid block in the system
	Flush.Block = 0;
	while (!VerifyBlockHeader())
	{
		// move to the next block
		if (++Flush.Block >= IDS_CAN_DTC_NUM_STORAGE_BLOCKS)
			return FALSE; // no initialized blocks found, can't continue
	}

	// we found a valid block

	// check subsequent blocks until we reach the end of the block sequence
	for (;;)
	{
		// remember this block
		known_good_block = Flush.Block;
		known_good_seq = Flush.SequenceNum;

		// see if the next block is valid and in sequence
		Flush.Block++;
		if (Flush.Block >= IDS_CAN_DTC_NUM_STORAGE_BLOCKS)
			break; // no more blocks
		if (!VerifyBlockHeader())
			break; // invalid block header, indicates end of sequence reached
		if ((uint8)(Flush.SequenceNum - known_good_seq) != 1)
			break; // next block is out of sequence, therefore end of sequence was reached
	}

	// we've found the last block in the sequence

	// select last known good block in the sequence
	Flush.Block = known_good_block;
	Flush.SequenceNum = known_good_seq;

	// try loading DTCs from the last known good block
	if (!TryLoadDTCsFromBlock())
	{
		// failed to locate a valid data chunk in this block
		// which is odd, as the block is valid/initialized
		// it is possible that we never got the chance to write a complete chunk to the block after we initialized it
		// in which case, we should have good DTC data in the previous block in the sequence

		// move to previous block
		if (Flush.Block == 0)
			Flush.Block = IDS_CAN_DTC_NUM_STORAGE_BLOCKS - 1;
		else
			Flush.Block--;

		// is the previous block valid, and in sequence?
		if (!VerifyBlockHeader())
			return FALSE; // previous block is invalid, give up
		if ((uint8)(known_good_seq - Flush.SequenceNum) != 1)
			return FALSE; // previous block is valid but out of sequence, give up

		// block appears valid
		// try and load DTCs from this block
		if (!TryLoadDTCsFromBlock())
			return FALSE; // no valid data chunks were found in this block either, give up
	}

	// if we get here, we've found valid chunk of data and sucessfully loaded it into DTC RAM

	// at this point the following variables are set to:
	//    Flush.SequenceNum ==> SequenceNum of the block we loaded from
	//    Flush.Block       ==> the block we just loaded from
	//    Flush.Chunk       ==> points to first empty chunk found in the block we just loaded from

	// we're done
	return TRUE;
}

static uint8 GetNextByteToFlush(uint16 index)
{
	if (index == CHUNK_OFFSET_START_FLAG)
		return START_FLAG;
	else if (index < CHUNK_OFFSET_CRC)
	{
		// store off the DTC status
		// NOTE: the "active" is stored, although it is masked as inactive during reboot/init
		//       we store the active state just for additional enhancement in our analysis
		uint8 status = DTC.Status[index - CHUNK_OFFSET_DTC_DATA];
		if (index == CHUNK_OFFSET_DTC_DATA)
			Flush.CRC = CRC8_Reset();
		Flush.CRC = CRC8_Update(Flush.CRC, status);
		return status;
	}
	else if (index == CHUNK_OFFSET_CRC)
		return Flush.CRC;
	else
		return END_FLAG;
}

#endif // IDS_CAN_DTC_NUM_STORAGE_BLOCKS

void _IDS_CAN_DTC_Task10ms(void)
{
	#ifdef IDS_CAN_DTC_NUM_STORAGE_BLOCKS

	typedef enum {
		// these states perform the block erase prior to flush
		// they increment into the SM_FLUSH state
		SM_ERASE_BLOCK,
#if IDS_CAN_DTC_NUM_STORAGE_BLOCKS >= 3
		SM_ERASE_NEXT_BLOCK,
#endif
		SM_WRITE_HEADER,

		SM_FLUSH,

		SM_IDLE,
	} STATE;
	static STATE State = SM_IDLE;
	static uint16 Index = 0;
	static uint8 Timeout = 0;
	uint8 last_block = Flush.Block;
	uint8 abort = FALSE;
	uint8 buf[4];

	// handle flush timeout, range checks, sanity checks
	if (++Timeout == 0xFF)
	{
		// timeout occurred
		abort = TRUE;
		if (State == SM_FLUSH)
			Flush.Chunk++; // stuck while writing to a chunk, move to next chunk
	}
	if (Flush.Chunk >= NUM_CHUNKS) Flush.Block++; // chunk out of range, move to beginning of next block
	if (Flush.Block >= IDS_CAN_DTC_NUM_STORAGE_BLOCKS) Flush.Block = 0; // block out of range, start over at first block

	// if block is changing
	if (Flush.Block != last_block)
	{
		// starting a new block
		Flush.SequenceNum++; // increment block sequence number
		Flush.Chunk = 0; // start at first chunk
		abort = TRUE;
	}

	// if we are aborting during an active flush
	if (abort && State < SM_IDLE)
	{
		// restart the flush
		State = ((Flush.Chunk == 0) ? SM_ERASE_BLOCK : SM_FLUSH);
		Index = 0;
		Timeout = 0;
	}

	// run state machine
	switch (State)
	{
	case SM_ERASE_BLOCK:
		// erase the block before we use it
		if (!IDS_CAN_DTC_STORAGE_ERASE_BLOCK(Flush.Block))
			return; // try again later
		State++; Timeout = 0;
		return;

#if IDS_CAN_DTC_NUM_STORAGE_BLOCKS >= 3
	case SM_ERASE_NEXT_BLOCK:
		// we should erase the block subsequent to the one we are about to use, so that it can't be mistaken as a valid block in sequence
		// with three blocks, we can safely erase it now, and still be guaranteed our previous DTC block is still valid
		if (!IDS_CAN_DTC_STORAGE_ERASE_BLOCK((Flush.Block + 1) % IDS_CAN_DTC_NUM_STORAGE_BLOCKS))
			return; // try again later
		State++; Timeout = 0;
		return;
#endif

	case SM_WRITE_HEADER:
		buf[0] = Flush.SequenceNum; buf[1] = 'D'; buf[2] = 'T'; buf[3] = 'C';
		if (!IDS_CAN_DTC_STORAGE_WRITE32(Flush.Block, 0, buf))
			return; // try again later
		State++; Timeout = 0;
		//lint -fallthrough

	case SM_FLUSH:
		// loop until flush is complete
		if (Index == 0)
			Flush.NeedsFlush = FALSE; // this flag is cleared right before we start writing data to storage
		while (Index < CHUNK_SIZE)
		{
			uint8 crc = Flush.CRC; // remember current CRC
			buf[0] = GetNextByteToFlush(Index);
			buf[1] = GetNextByteToFlush(Index+1);
			buf[2] = GetNextByteToFlush(Index+2);
			buf[3] = GetNextByteToFlush(Index+3);
			if (!IDS_CAN_DTC_STORAGE_WRITE32(Flush.Block, CHUNK_OFFSET + Index, buf))
			{
				// write failed
				Flush.CRC = crc; // restore CRC to value prior to write attempt
				return; // try again later
			}
			Index += 4; // move to next 4 bytes
			Timeout = 0;
		}
		// end the flush
		State++;
		Flush.Chunk++;
		//lint -fallthrough

	case SM_IDLE:
	default:
		// auto-flush 2.5 seconds after a flush request occurs
		if (!Flush.NeedsFlush)
			Timeout = 0;
		else if (Timeout == 0xFF)
		{
			Index = 0;
			State = ((Flush.Chunk == 0) ? SM_ERASE_BLOCK : SM_FLUSH);
			Timeout = 0;
		}
		break;
	}

	#endif // IDS_CAN_DTC_NUM_STORAGE_BLOCKS
}

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

uint16 IDS_CAN_GetNumSupportedDTCs(void)
{
	return (uint16)NUM_IDS_CAN_DTCS;
}

uint16 IDS_CAN_GetNumActiveDTCs(void)
{
	IDS_CAN_DTC dtc = NUM_IDS_CAN_DTCS;
	uint16 count = 0;
	do
	{
		dtc--;
		if (IS_ACTIVE(dtc))
			count++;
	} while (dtc);
	return count;
}

uint16 IDS_CAN_GetNumStoredDTCs(void)
{
	IDS_CAN_DTC dtc = NUM_IDS_CAN_DTCS;
	uint16 count = 0;
	do
	{
		dtc--;
		if (IS_STORED(dtc))
			count++;
	} while (dtc);
	return count;
}

uint8 IDS_CAN_AreAnyDTCsActive(void)
{
	IDS_CAN_DTC dtc = NUM_IDS_CAN_DTCS;
	do
	{
		dtc--;
		if (IS_ACTIVE(dtc))
			return TRUE;
	} while (dtc);
	return FALSE;
}

uint8 IDS_CAN_AreAnyDTCsStored(void)
{
	IDS_CAN_DTC dtc = NUM_IDS_CAN_DTCS;
	do
	{
		dtc--;
		if (IS_STORED(dtc))
			return TRUE;
	} while (dtc);
	return FALSE;
}

uint8 IDS_CAN_IsDTCActive(IDS_CAN_DTC dtc)
{
	ASSERT(dtc < NUM_IDS_CAN_DTCS);
	if (dtc < NUM_IDS_CAN_DTCS)
		return IS_ACTIVE(dtc);
	return FALSE;
}

uint8 IDS_CAN_IsDTCStored(IDS_CAN_DTC dtc)
{
	ASSERT(dtc < NUM_IDS_CAN_DTCS);
	if (dtc < NUM_IDS_CAN_DTCS)
		return IS_STORED(dtc);
	return FALSE;
}

IDS_CAN_DTC_INFO IDS_CAN_GetDTCInfo(IDS_CAN_DTC dtc)
{
	static const IDS_CAN_DTC_INFO Empty = {0,0};

	ASSERT(dtc < NUM_IDS_CAN_DTCS);
	if (dtc < NUM_IDS_CAN_DTCS)
	{
		// create ROM table of DTC IDs
		static const uint16 ID[NUM_IDS_CAN_DTCS] = {
			#undef DECLARE_DTC
			#define DECLARE_DTC(dtc, debounce) dtc
			#undef DEFINE_DTC
			#define DEFINE_DTC(id, name) (id),
			DECLARE_SUPPORTED_IDS_CAN_DTCS
		};

		IDS_CAN_DTC_INFO info;
		info.ID = ID[dtc];
		info.Status = DTC.Status[dtc];
		return info;
	}

	return Empty;
}

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

#ifdef IDS_CAN_DTC_NUM_STORAGE_BLOCKS
static void SetDTCStatus(IDS_CAN_DTC dtc, uint8 status)
{
	ASSERT(dtc < NUM_IDS_CAN_DTCS);
	if (dtc < NUM_IDS_CAN_DTCS)
	{
		// perform a flush if the stored value of the DTC is changing
		// NOTE: when we store, we only store the ignition cycles counter
		//       we do NOT store the active state
		//       therefore, we can safely ignore that bit
		Flush.NeedsFlush |= ((DTC.Status[dtc] ^ status) & 0x7F);
		DTC.Status[dtc] = status;
	}
}
#else // IDS_CAN_DTC_NUM_STORAGE_BLOCKS
#define SetDTCStatus(dtc, status) DTC.Status[dtc] = status
#endif // IDS_CAN_DTC_NUM_STORAGE_BLOCKS

void IDS_CAN_SetDTC(IDS_CAN_DTC dtc)
{
	ASSERT(dtc < NUM_IDS_CAN_DTCS);
	if (dtc < NUM_IDS_CAN_DTCS)
	{
		DTC.Debounce[dtc] = DEBOUNCE_MAX[dtc];
		SetDTCStatus(dtc, 0x81);
	}
}

void IDS_CAN_ClearDTC(IDS_CAN_DTC dtc)
{
	ASSERT(dtc < NUM_IDS_CAN_DTCS);
	if (dtc < NUM_IDS_CAN_DTCS)
	{
		DTC.Debounce[dtc] = 0;
		SetDTCStatus(dtc, DTC.Status[dtc] & 0x7F);
	}
}

void IDS_CAN_MatureDTC(IDS_CAN_DTC dtc, uint8 dtc_detected)
{
	ASSERT(dtc < NUM_IDS_CAN_DTCS);
	if (dtc < NUM_IDS_CAN_DTCS)
	{
		if (dtc_detected)
		{
			// increment debounce counter to maximum
			if (DTC.Debounce[dtc] < DEBOUNCE_MAX[dtc])
			{
				DTC.Debounce[dtc]++;
				if (DTC.Debounce[dtc] < DEBOUNCE_MAX[dtc])
					return; // not yet mature
			}
			IDS_CAN_SetDTC(dtc); // DTC has (re)matured
		}
		else
		{
			// decrement debounce counter
			if (DTC.Debounce[dtc])
			{
				DTC.Debounce[dtc]--;
				if (DTC.Debounce[dtc])
					return; // not yet de-mature
			}
			IDS_CAN_ClearDTC(dtc); // DTC has cleared
		}
	}
}

void IDS_CAN_ClearActiveDTCs(void)
{
	IDS_CAN_DTC dtc;
	for (dtc=(IDS_CAN_DTC)0; dtc < NUM_IDS_CAN_DTCS; dtc++)
		IDS_CAN_ClearDTC(dtc);
}

void IDS_CAN_ClearStoredDTCs(void)
{
	IDS_CAN_DTC dtc;
	for (dtc=(IDS_CAN_DTC)0; dtc < NUM_IDS_CAN_DTCS; dtc++)
	{
		if (!IS_ACTIVE(dtc))
			SetDTCStatus(dtc, 0);
	}
}

void IDS_CAN_ClearAllDTCs(void)
{
	IDS_CAN_DTC dtc;
	(void)memset(DTC.Debounce, 0, (uint16)sizeof(DTC.Debounce));
	for (dtc=(IDS_CAN_DTC)0; dtc < NUM_IDS_CAN_DTCS; dtc++)
		SetDTCStatus(dtc, 0);
}

void IDS_CAN_IncrementDTCCyclesCounter(void)
{
	// increment all counters for any DTC which is stored but is not presently active
	IDS_CAN_DTC dtc;
	for (dtc=(IDS_CAN_DTC)0; dtc < NUM_IDS_CAN_DTCS; dtc++)
	{
		if (IS_STORED(dtc))
		{
			uint8 val = DTC.Status[dtc] + 1;
			if (!(val & 0x7F))
				val = 0; // clear the DTC on overflow
			SetDTCStatus(dtc, val);
		}
	}
}

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

void _IDS_CAN_DTC_Init(void)
{
	// clear DTC RAM
	(void)memset(&DTC, 0, sizeof(DTC));

#ifdef IDS_CAN_DTC_NUM_STORAGE_BLOCKS
	if (LoadDTCsFromStorage())
	{
		// looking good, increment cycles counter and move on
		IDS_CAN_IncrementDTCCyclesCounter();
		return;
	}

	// we get here in case of error

	// restart DTC flushing from beginning
	Flush.Block = 0;
	Flush.Chunk = 0;
	Flush.SequenceNum = 0;

	// clear out whatever got loaded into DTC RAM
	(void)memset(DTC.Status, 0, (uint16)sizeof(DTC.Status));
#endif // IDS_CAN_DTC_NUM_STORAGE_BLOCKS

	// if we get here, then DTC memory was wiped
	// we should set DTC_DTC_STORAGE_FAILURE if it exists
#undef DECLARE_DTC
#define DECLARE_DTC(dtc, debounce) dtc
#undef DEFINE_DTC
#define DEFINE_DTC(id, name) ((id) == 0x0001) ||
#if DECLARE_SUPPORTED_IDS_CAN_DTCS FALSE
	// indicate DTC storage has failed
	IDS_CAN_SetDTC(DTC_DTC_STORAGE_FAILURE);
#endif
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#endif // HEADER
#endif // DECLARE_SUPPORTED_IDS_CAN_DTCS
