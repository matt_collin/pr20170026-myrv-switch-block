// EERPROM.c
// EEPROM driver / Block storage driver for Star12 microcontrollers
// Version 1.5
// (c) 2012, 2017 Innovative Design Solutions, Inc.
// All rights reserved

// history
// 1.0  First revision
// 1.1  EEPROM_ReadByte() changed to EEPROM_Read() for
//      compatibility with standard Config modules
// 1.2  Updated to report errors
// 1.3  Improved storage capability by adding redundancy
//      Redundant erase/writes removed
//      Data hamming encoded to allow for greater redundancy
// 1.4  Added storage block driver support (used by DTCs)
//      Half of EEPROM is reserved for block storage)
//      NOTE: block writes must occur on 4 byte boundaries
// 1.5  Bugfix when block write is larger than 4 bytes
//      added ASSERT() sanity check in PrepareForCommand()
//      Linted

#include "global.h"

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#ifdef HEADER

// processor check / determine DFLASH module version
#if defined MC9S12G96
	#define EEPROM_SIZE_BYTES            (3072/2) /* break EEPROM in half, lower half for EEPROM emulation, upper half for block storage */
	#define EEPROM_ERASED_STATE          0xFF

	#define STORAGE_BLOCK_SIZE_BYTES     128
	#define NUM_STORAGE_BLOCKS           (EEPROM_SIZE_BYTES / STORAGE_BLOCK_SIZE_BYTES)
	#define STORAGE_BLOCK_ERASED_STATE   EEPROM_ERASED_STATE

#else
	#error EEPROM service not designed for this processor
#endif

void EEPROM_Init(void);

uint16 EEPROM_Read(uint16 sector);
uint8 EEPROM_Write(uint8 data, uint16 sector);
#define EEPROM_Sync EEPROM_Write

// block storage driver
#define Block_Init EEPROM_Init
uint8 Block_Erase(uint8 block);
uint8 Block_Read(uint8 block, uint16 offset, uint16 count, void* buffer);
uint8 Block_Write(uint8 block, uint16 offset, uint16 count, const void* buf); // writes must occur on 4 byte boundaries!!!

#else

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// processor check / determine EEPROM module version
#if defined MC9S12G96
	#define EEPROM_BASE_256K   0x0400
	#define EEPROM_BASE_64K    0x0400
#else
	#error EEPROM service not designed for this processor
#endif

#define EEPROM_PROGRAM       0x11
#define EEPROM_ERASE_SECTOR  0x12

void EEPROM_Init(void)
{
	// clear ACCERR & FPVIOL flag in flash status register
	// 0-110000
	// | ||||||
	// | ||||| \__ MGSTAT0  read only
	// | |||| \___ MGSTAT1  read only
	// | ||| \____ RSVD     read only
	// | || \_____ MGBUSY   read only
	// | | \______ FPVIOL   1 = clear flag
	// |  \_______ ACCERR   1 = clear flag
	//  \_________ CCIF
	FSTAT = 0x30;

	// clear DFDIF,SFDIF & EPVIOLIF flag in flash error status register
	// ------11
	//       ||
	//       | \__ SFDIF  1 = clear flag
	//        \___ DFDIF  1 = clear flag
	FERSTAT = 0x03;

	// Set the ignore single bit fault value, so that the single bit faults are ignored
	// x--1--xx
	// |  |  ||
	// |  |  | \__ FSFD   0 = do not simulate single bit fault
	// |  |   \___ FDFD   0 = do not simulate double bit fault
	// |   \______ IGNSF  1 = single bit faults are not reported
	//  \_________ CCIE   0 = command complete interrupt disabled
	FCNFG = 0x10;

	// is the clock set?
	if (FCLKDIV_FDIVLD)
		return; // nothing to do!

#if defined MC9S12G96
	#if FBUS < 1000000 || FBUS > 25600000
	#error FBUS out of range
	#endif
	FCLKDIV = (FBUS - 600000L) / 1000000L;
	FCLKDIV_FDIVLCK = 1; // lock the clock
#else
	#error EEPROM service not designed for this processor
#endif
}

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

#pragma INLINE
static uint16 HammingEncode(uint8 val)
{
	static const uint8 Encode[16] = {
		0x15, 0x02, 0x49, 0x5E, 0x64, 0x73, 0x38, 0x2F,
		0xD0, 0xC7, 0x8C, 0x9B, 0xA1, 0xB6, 0xFD, 0xEA };
	WORD result;
	result.ui8.hi = Encode[val >> 4];
	result.ui8.lo = Encode[val & 0xF];
	return result.ui16;
}

#pragma INLINE
static uint16 HammingDecode(uint16 val)
{
	// this corrects for single bit errors in 84 code
	// double bit errors are detected (255 is returned)
	static const uint8 Decode[256] = {
		0x1, 255, 0x1, 0x1, 255, 0x0, 0x1, 255, 255, 0x2, 0x1, 255, 0xA, 255, 255, 0x7,
		255, 0x0, 0x1, 255, 0x0, 0x0, 255, 0x0, 0x6, 255, 255, 0xB, 255, 0x0, 0x3, 255,
		255, 0xC, 0x1, 255, 0x4, 255, 255, 0x7, 0x6, 255, 255, 0x7, 255, 0x7, 0x7, 0x7,
		0x6, 255, 255, 0x5, 255, 0x0, 0xD, 255, 0x6, 0x6, 0x6, 255, 0x6, 255, 255, 0x7,
		255, 0x2, 0x1, 255, 0x4, 255, 255, 0x9, 0x2, 0x2, 255, 0x2, 255, 0x2, 0x3, 255,
		0x8, 255, 255, 0x5, 255, 0x0, 0x3, 255, 255, 0x2, 0x3, 255, 0x3, 255, 0x3, 0x3,
		0x4, 255, 255, 0x5, 0x4, 0x4, 0x4, 255, 255, 0x2, 0xF, 255, 0x4, 255, 255, 0x7,
		255, 0x5, 0x5, 0x5, 0x4, 255, 255, 0x5, 0x6, 255, 255, 0x5, 255, 0xE, 0x3, 255,
		255, 0xC, 0x1, 255, 0xA, 255, 255, 0x9, 0xA, 255, 255, 0xB, 0xA, 0xA, 0xA, 255,
		0x8, 255, 255, 0xB, 255, 0x0, 0xD, 255, 255, 0xB, 0xB, 0xB, 0xA, 255, 255, 0xB,
		0xC, 0xC, 255, 0xC, 255, 0xC, 0xD, 255, 255, 0xC, 0xF, 255, 0xA, 255, 255, 0x7,
		255, 0xC, 0xD, 255, 0xD, 255, 0xD, 0xD, 0x6, 255, 255, 0xB, 255, 0xE, 0xD, 255,
		0x8, 255, 255, 0x9, 255, 0x9, 0x9, 0x9, 255, 0x2, 0xF, 255, 0xA, 255, 255, 0x9,
		0x8, 0x8, 0x8, 255, 0x8, 255, 255, 0x9, 0x8, 255, 255, 0xB, 255, 0xE, 0x3, 255,
		255, 0xC, 0xF, 255, 0x4, 255, 255, 0x9, 0xF, 255, 0xF, 0xF, 255, 0xE, 0xF, 255,
		0x8, 255, 255, 0x5, 255, 0xE, 0xD, 255, 255, 0xE, 0xF, 255, 0xE, 0xE, 255, 0xE };

	// decode the byte, check for double bit errors
	uint8 msn = Decode[(uint8)(val >> 8)]; // upper nibble
	uint8 lsn = Decode[(uint8)(val >> 0)]; /*lint !e835 shifting by zero */
	if ((msn | lsn) & 0xF0)
		return 0xFFFF; // double bit error!

	return (uint8)(msn << 4) | lsn;
}

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

uint16 EEPROM_Read(uint16 sector)
{
	const uint32 * address = (const uint32 *) (EEPROM_BASE_256K + (sector << 2));
	uint32 result = *address;
	return (uint8)HammingDecode((uint16)result);
}

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

static uint8 IsErased(uint32 address)
{
	return *(const uint32*)address == (uint32)0xFFFFFFFF; /*lint !e511 address is being mangled, but that's OK as Star12 is flexible */
}

#pragma INLINE
static void PrepareForCommand(void)
{
	// sanity check, previous flash command should be complete
	// if this fails then there is a logic bug, or someone else is messing with the flash peripheral
	ASSERT(FSTAT_CCIF);
	
	// wait till previous flash command is complete
	while (!FSTAT_CCIF)
		;

	// clear ACCERR and FPVIOL bits
	FSTAT = 0x30;
}

static uint8 ExecuteCommand(void)
{
	// clear CCIF bit, launching the command
	FSTAT_CCIF = 1;

	// wait for completion
	_FEED_COP();
	while (!FSTAT_CCIF)
		;
	_FEED_COP();

	// check for errors
	return (FSTAT & 0x33) == 0;
}

static uint8 EraseSector(uint32 address)
{
	// 4 Bytes sector erase
	PrepareForCommand();
	FCCOBIX = 0; FCCOB = (EEPROM_ERASE_SECTOR << 8) | (uint16)(address >> 16);
	FCCOBIX = 1; FCCOB = (uint16)address;
	return ExecuteCommand();
}

static uint8 WriteHammingEncodedByteInSector(uint8 data, uint32 address)
{
	// align every 4 bits
	// xx xx data data xx xx data data
	address += 2;

	// program the data
	PrepareForCommand();
	FCCOBIX = 0; FCCOB = EEPROM_PROGRAM << 8;
	FCCOBIX = 1; FCCOB = (uint16)address;
	FCCOBIX = 2; FCCOB = (uint16)HammingEncode(data);
	return ExecuteCommand();
}

uint8 EEPROM_Write(uint8 data, uint16 sector)
{
	uint32 address;
	WORD result;

	// read from target address, to see if a write is actually necessary
	result.ui16 = EEPROM_Read(sector);
	if (result.ui8.hi)
		return FALSE;
	if (result.ui8.lo == data)
		return TRUE; // no write is necessary

	// get address of sector to erase
	address = EEPROM_BASE_256K + ((uint32)sector << 2);
	if (address >= EEPROM_BASE_256K + EEPROM_SIZE_BYTES)
		return FALSE; // out of range

	// erase the sector
	if (!EraseSector(address))
		return FALSE;

	// do we need to write data, or is the empty cell what we want?
	// this is not needed because all data is hamming encoded, thus empty cells are not used
/*
	result.ui16 = EEPROM_Read(sector);
	if (result.ui8.hi)
		return FALSE;
	if (result.ui8.lo == data)
		return TRUE; // no write is necessary
*/

	// write the word to the sector
	if (!WriteHammingEncodedByteInSector(data, address))
		return FALSE;

	return EEPROM_Read(sector) == data;
}

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

// block storage emulation

static uint32 GetBlockAddress(uint8 block, uint16 offset, uint16 count)
{
	if (block >= NUM_STORAGE_BLOCKS)
		return 0;
	block += NUM_STORAGE_BLOCKS; // our address is in the upper 1/2 of memory
	if (offset >= STORAGE_BLOCK_SIZE_BYTES)
		return 0;
	if (count > (uint16)(STORAGE_BLOCK_SIZE_BYTES - offset))
		return 0;
	return (uint32)EEPROM_BASE_256K + ((uint16)block << 7) + offset;
}

uint8 Block_Erase(uint8 block)
{
	uint32 address = GetBlockAddress(block, 0, 0);
	uint8 n;
	if (address == 0)
		return FALSE;
	for (n = 0; n < STORAGE_BLOCK_SIZE_BYTES/4; n++, address += 4)
	{
		if (!IsErased(address))
		{
			(void)EraseSector(address);
			return FALSE;
		}
	}
	return TRUE;
}

uint8 Block_Read(uint8 block, uint16 offset, uint16 count, void* buffer)
{
	const uint8* address = (const uint8*) GetBlockAddress(block, offset, count); /*lint !e511 address is being mangled, but that's OK as Star12 is flexible */
	if (address != NULL)
	{
		(void)memcpy(buffer, address, count);
		return TRUE;
	}
	return FALSE;
}

#pragma INLINE
static uint8 WriteSector(uint32 data, uint32 address)
{
	if (!IsErased(address))
		return FALSE;

	// program the data
	PrepareForCommand();
	FCCOBIX = 0; FCCOB = EEPROM_PROGRAM << 8;
	FCCOBIX = 1; FCCOB = (uint16)address;
	FCCOBIX = 2; FCCOB = (uint16)(data >> 16);
	FCCOBIX = 3; FCCOB = (uint16)(data >> 0); /*lint !e835 shifting by zero */
	return ExecuteCommand();
}

// write data from buffer into block at given offset
// NOTE: writes must occur on 4 byte boundaries!!!
#pragma MESSAGE DISABLE C4001 /* condition always false */
uint8 Block_Write(uint8 block, uint16 offset, uint16 count, const void* buf)
{
	uint32 address = GetBlockAddress(block, offset, count);
	if (address == 0)
		return FALSE;
	
	// make sure we are writing on a 4 byte boundary
	if (address & 3)
	{
		ASSERT(FALSE); /*lint !e506 !e774 constant expression */
		return FALSE; // writes must occur on 4 byte boundaries!!!
	}

	// write 4 bytes at a time
	count = (count + 3) >> 2; // divide by 4 and round up

	// loop until all bytes are written
	while (count--)
	{
		// read from target address, to see if a write is actually necessary
		if (*(const uint32*)address != *(const uint32*)buf) /*lint !e511 address is being mangled, but that's OK as Star12 is flexible */
		{
			(void)WriteSector(*(const uint32*)buf, address);
			return FALSE;
		}

		// move forward 4 bytes
		address += 4;
		buf = (const uint8*) buf + 4; /*lint !e826 pointer conversion looks suspicious but isn't */
	}

	// written and verified via read back
	return TRUE;
}
#pragma MESSAGE DEFAULT C4001

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#endif
