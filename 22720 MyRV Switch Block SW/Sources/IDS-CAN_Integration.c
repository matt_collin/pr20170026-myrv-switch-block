// IDS-CAN Integration.c
// Product specific IDS-CAN integration
// (c) 2012, 2013, 2014 Innovative Design Solutions, Inc.
// All rights reserved

#include "global.h"

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#ifdef HEADER

const uint8* IDS_CAN_GetAdapterMAC(void);
void IDS_CAN_SetAdapterMAC(const uint8* mac);

uint32 IDS_CAN_GetCircuitID(IDS_CAN_DEVICE device);
void IDS_CAN_SetCircuitID(IDS_CAN_DEVICE device, uint32 circuit_id);

IDS_CAN_PRODUCT_ID IDS_CAN_GetProductID(void);

IDS_CAN_FUNCTION_NAME IDS_CAN_GetFunctionName(IDS_CAN_DEVICE device);
uint8 IDS_CAN_GetFunctionInstance(IDS_CAN_DEVICE device);
void IDS_CAN_SetFunctionName(IDS_CAN_DEVICE device, IDS_CAN_FUNCTION_NAME function_name);
void IDS_CAN_SetFunctionInstance(IDS_CAN_DEVICE device, uint8 function_instance);

#else

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

void Callback_OnConfigIDS_CAN_BlockLoadEvent(uint8 success)
{
	if (!success)
	{
		uint8 n;
		for (n=0; n<NUM_IDS_CAN_DEVICES; n++)
		{
			Config.IDS_CAN_Block.FunctionName[n] = IDS_CAN_FUNCTION_NAME_UNKNOWN;
			Config.IDS_CAN_Block.FunctionInstance[n] = 0;
			Config.IDS_CAN_Block.CircuitID[n] = 0;
		}
	}
}

IDS_CAN_PRODUCT_ID IDS_CAN_GetProductID(void)
{
	return IDS_CAN_PRODUCT_ID_18452_LCI_LINCPAD_SWITCH_TO_CAN_CONVERTER_CONTROL_ASSEMBLY;
}

const uint8* IDS_CAN_GetAdapterMAC(void)
{
//	static const uint8 ff[6] = { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
//	return ff;
	return Config.ProductionBlock.MAC;	
}

void IDS_CAN_SetAdapterMAC(const uint8* mac)
{
	Config.ProductionBlock.MAC[0] = mac[0];
	Config.ProductionBlock.MAC[1] = mac[1];
	Config.ProductionBlock.MAC[2] = mac[2];
	Config.ProductionBlock.MAC[3] = mac[3];
	Config.ProductionBlock.MAC[4] = mac[4];
	Config.ProductionBlock.MAC[5] = mac[5];
	Config_ProductionBlock_Flush();
}

uint32 IDS_CAN_GetCircuitID(IDS_CAN_DEVICE device)
{
	return Config.IDS_CAN_Block.CircuitID[device];
}

void IDS_CAN_SetCircuitID(IDS_CAN_DEVICE device, uint32 circuit_id)
{
	if (device < NUM_IDS_CAN_DEVICES)
	{
		Config.IDS_CAN_Block.CircuitID[device] = circuit_id;
		Config_IDS_CAN_Block_Flush();
		
		Session_OnCircuitIDChange(device);
	}
}

IDS_CAN_FUNCTION_NAME IDS_CAN_GetFunctionName(IDS_CAN_DEVICE device)
{
	return Config.IDS_CAN_Block.FunctionName[device];
}

uint8 IDS_CAN_GetFunctionInstance(IDS_CAN_DEVICE device)
{
	return Config.IDS_CAN_Block.FunctionInstance[device];
}

void IDS_CAN_SetFunctionName(IDS_CAN_DEVICE device, IDS_CAN_FUNCTION_NAME function_name)
{
	if (device < NUM_IDS_CAN_DEVICES)
	{
		Config.IDS_CAN_Block.FunctionName[device] = function_name;
		Config_IDS_CAN_Block_Flush();
	}
}

void IDS_CAN_SetFunctionInstance(IDS_CAN_DEVICE device, uint8 function_instance)
{
	if (device < NUM_IDS_CAN_DEVICES)
	{
		Config.IDS_CAN_Block.FunctionInstance[device] = function_instance;
		Config_IDS_CAN_Block_Flush();
	}
}


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#endif
