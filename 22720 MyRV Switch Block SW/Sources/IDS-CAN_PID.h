// IDS-CAN PID.h
// Version 2.8.2
// (c) 2012, 2013, 2014, 2015, 2016, 2017 Innovative Design Solutions, Inc.
// All rights reserved

// History
//
// Version 1.0   First version
// Version 1.1   Added new PIDs
// Version 1.2   Added new PIDs
// Version 1.3   March 2013 PID update
// Version 1.4   April 2013 PID update
// Version 1.5   May 2013 PID update
// Version 1.6   Updated to IDS-CAN v1.2 May 2014
// Version 1.6a  Updated to IDS-CAN v1.5 July 2015
// Version 1.6b  Updated to IDS-CAN v1.5b Sept 2015
// Version 2.0   Updated to IDS-CAN v2.0 Feb 2016
// Version 2.2   Updated to IDS-CAN v2.2 Apr 2016
// Version 2.3   Updated to IDS-CAN v2.3 May 2016
// Version 2.4   Updated to IDS-CAN v2.4 Jun 2016
// Version 2.6   Updated to IDS-CAN v2.6 Mar 2017
// Version 2.7   Updated to IDS-CAN v2.7 Apr 2017
// Version 2.7a  Updated to IDS-CAN v2.7a May 2017
// Version 2.8.0 PIDs as of 7/13/17
// Version 2.8.1 PIDs as of 8/11/17
// Version 2.8.2 PIDs as of 8/31/17

#ifndef IDS_CAN_PID_H
#define IDS_CAN_PID_H

typedef uint16 IDS_CAN_PID;

#define IDS_CAN_PID_UNKNOWN                                     0x0000
#define IDS_CAN_PID_PRODUCTION_BYTES                            0x0001
#define IDS_CAN_PID_CAN_ADAPTER_MAC                             0x0002
#define IDS_CAN_PID_IDS_CAN_CIRCUIT_ID                          0x0003
#define IDS_CAN_PID_IDS_CAN_FUNCTION_NAME                       0x0004
#define IDS_CAN_PID_IDS_CAN_FUNCTION_INSTANCE                   0x0005
#define IDS_CAN_PID_IDS_CAN_NUM_DEVICES_ON_NETWORK              0x0006
#define IDS_CAN_PID_IDS_CAN_MAX_NETWORK_HEARTBEAT_TIME          0x0007
#define IDS_CAN_PID_SERIAL_NUMBER                               0x0008
#define IDS_CAN_PID_CAN_BYTES_TX                                0x0009
#define IDS_CAN_PID_CAN_BYTES_RX                                0x000A
#define IDS_CAN_PID_CAN_MESSAGES_TX                             0x000B
#define IDS_CAN_PID_CAN_MESSAGES_RX                             0x000C
#define IDS_CAN_PID_CAN_TX_BUFFER_OVERFLOW_COUNT                0x000D
#define IDS_CAN_PID_CAN_RX_BUFFER_OVERFLOW_COUNT                0x000E
#define IDS_CAN_PID_CAN_TX_MAX_BYTES_QUEUED                     0x000F
#define IDS_CAN_PID_CAN_RX_MAX_BYTES_QUEUED                     0x0010
#define IDS_CAN_PID_UART_BYTES_TX                               0x0011
#define IDS_CAN_PID_UART_BYTES_RX                               0x0012
#define IDS_CAN_PID_UART_MESSAGES_TX                            0x0013
#define IDS_CAN_PID_UART_MESSAGES_RX                            0x0014
#define IDS_CAN_PID_UART_TX_BUFFER_OVERFLOW_COUNT               0x0015
#define IDS_CAN_PID_UART_RX_BUFFER_OVERFLOW_COUNT               0x0016
#define IDS_CAN_PID_UART_TX_MAX_BYTES_QUEUED                    0x0017
#define IDS_CAN_PID_UART_RX_MAX_BYTES_QUEUED                    0x0018
#define IDS_CAN_PID_WIFI_BYTES_TX                               0x0019
#define IDS_CAN_PID_WIFI_BYTES_RX                               0x001A
#define IDS_CAN_PID_WIFI_MESSAGES_TX                            0x001B
#define IDS_CAN_PID_WIFI_MESSAGES_RX                            0x001C
#define IDS_CAN_PID_WIFI_TX_BUFFER_OVERFLOW_COUNT               0x001D
#define IDS_CAN_PID_WIFI_RX_BUFFER_OVERFLOW_COUNT               0x001E
#define IDS_CAN_PID_WIFI_TX_MAX_BYTES_QUEUED                    0x001F
#define IDS_CAN_PID_WIFI_RX_MAX_BYTES_QUEUED                    0x0020
#define IDS_CAN_PID_WIFI_RSSI                                   0x0021
#define IDS_CAN_PID_RF_BYTES_TX                                 0x0022
#define IDS_CAN_PID_RF_BYTES_RX                                 0x0023
#define IDS_CAN_PID_RF_MESSAGES_TX                              0x0024
#define IDS_CAN_PID_RF_MESSAGES_RX                              0x0025
#define IDS_CAN_PID_RF_TX_BUFFER_OVERFLOW_COUNT                 0x0026
#define IDS_CAN_PID_RF_RX_BUFFER_OVERFLOW_COUNT                 0x0027
#define IDS_CAN_PID_RF_TX_MAX_BYTES_QUEUED                      0x0028
#define IDS_CAN_PID_RF_RX_MAX_BYTES_QUEUED                      0x0029
#define IDS_CAN_PID_RF_RSSI                                     0x002A
#define IDS_CAN_PID_BATTERY_VOLTAGE                             0x002B
#define IDS_CAN_PID_REGULATOR_VOLTAGE                           0x002C
#define IDS_CAN_PID_NUM_TILT_SENSOR_AXES                        0x002D
#define IDS_CAN_PID_TILT_AXIS_1_ANGLE                           0x002E
#define IDS_CAN_PID_TILT_AXIS_2_ANGLE                           0x002F
#define IDS_CAN_PID_TILT_AXIS_3_ANGLE                           0x0030
#define IDS_CAN_PID_TILT_AXIS_4_ANGLE                           0x0031
#define IDS_CAN_PID_TILT_AXIS_5_ANGLE                           0x0032
#define IDS_CAN_PID_TILT_AXIS_6_ANGLE                           0x0033
#define IDS_CAN_PID_TILT_AXIS_7_ANGLE                           0x0034
#define IDS_CAN_PID_TILT_AXIS_8_ANGLE                           0x0035
#define IDS_CAN_PID_IDS_CAN_FIXED_ADDRESS                       0x0036
#define IDS_CAN_PID_FUSE_SETTING_1                              0x0037
#define IDS_CAN_PID_FUSE_SETTING_2                              0x0038
#define IDS_CAN_PID_FUSE_SETTING_3                              0x0039
#define IDS_CAN_PID_FUSE_SETTING_4                              0x003A
#define IDS_CAN_PID_FUSE_SETTING_5                              0x003B
#define IDS_CAN_PID_FUSE_SETTING_6                              0x003C
#define IDS_CAN_PID_FUSE_SETTING_7                              0x003D
#define IDS_CAN_PID_FUSE_SETTING_8                              0x003E
#define IDS_CAN_PID_FUSE_SETTING_9                              0x003F
#define IDS_CAN_PID_FUSE_SETTING_10                             0x0040
#define IDS_CAN_PID_FUSE_SETTING_11                             0x0041
#define IDS_CAN_PID_FUSE_SETTING_12                             0x0042
#define IDS_CAN_PID_FUSE_SETTING_13                             0x0043
#define IDS_CAN_PID_FUSE_SETTING_14                             0x0044
#define IDS_CAN_PID_FUSE_SETTING_15                             0x0045
#define IDS_CAN_PID_FUSE_SETTING_16                             0x0046
#define IDS_CAN_PID_MANUFACTURING_PID_1                         0x0047
#define IDS_CAN_PID_MANUFACTURING_PID_2                         0x0048
#define IDS_CAN_PID_MANUFACTURING_PID_3                         0x0049
#define IDS_CAN_PID_MANUFACTURING_PID_4                         0x004A
#define IDS_CAN_PID_MANUFACTURING_PID_5                         0x004B
#define IDS_CAN_PID_MANUFACTURING_PID_6                         0x004C
#define IDS_CAN_PID_MANUFACTURING_PID_7                         0x004D
#define IDS_CAN_PID_MANUFACTURING_PID_8                         0x004E
#define IDS_CAN_PID_MANUFACTURING_PID_9                         0x004F
#define IDS_CAN_PID_MANUFACTURING_PID_10                        0x0050
#define IDS_CAN_PID_MANUFACTURING_PID_11                        0x0051
#define IDS_CAN_PID_MANUFACTURING_PID_12                        0x0052
#define IDS_CAN_PID_MANUFACTURING_PID_13                        0x0053
#define IDS_CAN_PID_MANUFACTURING_PID_14                        0x0054
#define IDS_CAN_PID_MANUFACTURING_PID_15                        0x0055
#define IDS_CAN_PID_MANUFACTURING_PID_16                        0x0056
#define IDS_CAN_PID_MANUFACTURING_PID_17                        0x0057
#define IDS_CAN_PID_MANUFACTURING_PID_18                        0x0058
#define IDS_CAN_PID_MANUFACTURING_PID_19                        0x0059
#define IDS_CAN_PID_MANUFACTURING_PID_20                        0x005A
#define IDS_CAN_PID_MANUFACTURING_PID_21                        0x005B
#define IDS_CAN_PID_MANUFACTURING_PID_22                        0x005C
#define IDS_CAN_PID_MANUFACTURING_PID_23                        0x005D
#define IDS_CAN_PID_MANUFACTURING_PID_24                        0x005E
#define IDS_CAN_PID_MANUFACTURING_PID_25                        0x005F
#define IDS_CAN_PID_MANUFACTURING_PID_26                        0x0060
#define IDS_CAN_PID_MANUFACTURING_PID_27                        0x0061
#define IDS_CAN_PID_MANUFACTURING_PID_28                        0x0062
#define IDS_CAN_PID_MANUFACTURING_PID_29                        0x0063
#define IDS_CAN_PID_MANUFACTURING_PID_30                        0x0064
#define IDS_CAN_PID_MANUFACTURING_PID_31                        0x0065
#define IDS_CAN_PID_MANUFACTURING_PID_32                        0x0066
#define IDS_CAN_PID_METERED_TIME_SEC                            0x0067
#define IDS_CAN_PID_MAINTENANCE_PERIOD_SEC                      0x0068
#define IDS_CAN_PID_LAST_MAINTENANCE_TIME_SEC                   0x0069
#define IDS_CAN_PID_TIME_ZONE                                   0x006A
#define IDS_CAN_PID_RTC_TIME_SEC                                0x006B
#define IDS_CAN_PID_RTC_TIME_MIN                                0x006C
#define IDS_CAN_PID_RTC_TIME_HOUR                               0x006D
#define IDS_CAN_PID_RTC_TIME_DAY                                0x006E
#define IDS_CAN_PID_RTC_TIME_MONTH                              0x006F
#define IDS_CAN_PID_RTC_TIME_YEAR                               0x0070
#define IDS_CAN_PID_RTC_EPOCH_SEC                               0x0071
#define IDS_CAN_PID_RTC_SET_TIME_SEC                            0x0072
#define IDS_CAN_PID_BLE_DEVICE_NAME_1                           0x0073
#define IDS_CAN_PID_BLE_DEVICE_NAME_2                           0x0074
#define IDS_CAN_PID_BLE_DEVICE_NAME_3                           0x0075
#define IDS_CAN_PID_BLE_PIN                                     0x0076
#define IDS_CAN_PID_SYSTEM_UPTIME_MS                            0x0077
#define IDS_CAN_PID_ETH_ADAPTER_MAC                             0x0078
#define IDS_CAN_PID_ETH_BYTES_TX                                0x0079
#define IDS_CAN_PID_ETH_BYTES_RX                                0x007A
#define IDS_CAN_PID_ETH_MESSAGES_TX                             0x007B
#define IDS_CAN_PID_ETH_MESSAGES_RX                             0x007C
#define IDS_CAN_PID_ETH_TX_BUFFER_OVERFLOW_COUNT                0x007D
#define IDS_CAN_PID_ETH_RX_BUFFER_OVERFLOW_COUNT                0x007E
#define IDS_CAN_PID_ETH_PACKETS_TX_DISCARDED                    0x007F
#define IDS_CAN_PID_ETH_PACKETS_RX_DISCARDED                    0x0080
#define IDS_CAN_PID_ETH_PACKETS_TX_ERROR                        0x0081
#define IDS_CAN_PID_ETH_PACKETS_RX_ERROR                        0x0082
#define IDS_CAN_PID_ETH_PACKETS_TX_OVERFLOW                     0x0083
#define IDS_CAN_PID_ETH_PACKETS_TX_LATE_COLLISION               0x0084
#define IDS_CAN_PID_ETH_PACKETS_TX_EXCESS_COLLISION             0x0085
#define IDS_CAN_PID_ETH_PACKETS_TX_UNDERFLOW                    0x0086
#define IDS_CAN_PID_ETH_PACKETS_RX_ALIGN_ERR                    0x0087
#define IDS_CAN_PID_ETH_PACKETS_RX_CRC_ERR                      0x0088
#define IDS_CAN_PID_ETH_PACKETS_RX_TRUNC_ERR                    0x0089
#define IDS_CAN_PID_ETH_PACKETS_RX_LEN_ERR                      0x008A
#define IDS_CAN_PID_ETH_PACKETS_RX_COLLISION                    0x008B
#define IDS_CAN_PID_IP_ADDRESS                                  0x008C
#define IDS_CAN_PID_IP_SUBNETMASK                               0x008D
#define IDS_CAN_PID_IP_GATEWAY                                  0x008E
#define IDS_CAN_PID_TCP_NUM_CONNECTIONS                         0x008F
#define IDS_CAN_PID_AUX_BATTERY_VOLTAGE                         0x0090
#define IDS_CAN_PID_RGB_LIGHTING_GANG_ENABLE                    0x0091
#define IDS_CAN_PID_INPUT_SWITCH_TYPE                           0x0092
#define IDS_CAN_PID_DOOR_LOCK_STATE                             0x0093
#define IDS_CAN_PID_GENERATOR_QUIET_HOURS_START_TIME            0x0094
#define IDS_CAN_PID_GENERATOR_QUIET_HOURS_END_TIME              0x0095
#define IDS_CAN_PID_GENERATOR_AUTO_START_LOW_VOLTAGE            0x0096
#define IDS_CAN_PID_GENERATOR_AUTO_START_HI_TEMP_C              0x0097
#define IDS_CAN_PID_GENERATOR_AUTO_RUN_DURATION_MINUTES         0x0098
#define IDS_CAN_PID_GENERATOR_AUTO_RUN_MIN_OFF_TIME_MINUTES     0x0099
#define IDS_CAN_PID_SOFTWARE_BUILD_DATE_TIME                    0x009A
#define IDS_CAN_PID_GENERATOR_QUIET_HOURS_ENABLED               0x009B
#define IDS_CAN_PID_SHORE_POWER_AMP_RATING                      0x009C
#define IDS_CAN_PID_BATTERY_CAPACITY_AMP_HOURS                  0x009D
#define IDS_CAN_PID_PCB_ASSEMBLY_PART_NUMBER                    0x009E
#define IDS_CAN_PID_UNLOCK_PIN                                  0x009F
#define IDS_CAN_PID_UNLOCK_PIN_MODE                             0x00A0
#define IDS_CAN_PID_SIMULATE_ON_OFF_STYLE_LIGHT                 0x00A1
#define IDS_CAN_PID_FAN_SPEED_CONTROL_TYPE                      0x00A2
#define IDS_CAN_PID_HVAC_CONTROL_TYPE                           0x00A3
#define IDS_CAN_PID_SOFTWARE_FUSE_RATING_AMPS                   0x00A4
#define IDS_CAN_PID_SOFTWARE_FUSE_MAX_RATING_AMPS               0x00A5
#define IDS_CAN_PID_CUMMINS_ONAN_GENERATOR_FAULT_CODE           0x00A6

#endif // IDS_CAN_PID_H
