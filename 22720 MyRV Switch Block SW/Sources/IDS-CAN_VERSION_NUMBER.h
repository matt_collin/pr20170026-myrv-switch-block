// IDS-CAN VERSION_NUMBER.h
// Version 2.8
// (c) 2013, 2014, 2015, 2016, 2017 Innovative Design Solutions, Inc.
// All rights reserved

// History
//
// Version 1.0   First version
// Version 1.1   June 2013 IDS-CAN v1.1 update
// Version 1.2   Updated to IDS-CAN v1.2 May 2014
// Version 1.3   Updated to IDS-CAN v1.3 March 2015
// Version 1.4   Updated to IDS-CAN v1.4 June 2015
// Version 1.5   Updated to IDS-CAN v1.5 July 2015
// Version 1.6   Updated to IDS-CAN v1.6 January 2016
// Version 2.0   Updated to IDS-CAN v2.0 Feb 2016
// Version 2.1   Updated to IDS-CAN v2.1 Mar 2016
// Version 2.2   Updated to IDS-CAN v2.2 Apr 2016
// Version 2.3   Updated to IDS-CAN v2.3 May 2016
// Version 2.4   Updated to IDS-CAN v2.4 Jun 2016
// Version 2.5   Updated to IDS-CAN v2.5 Feb 2017
// Version 2.6   Updated to IDS-CAN v2.6 Mar 2017
// Version 2.7   Updated to IDS-CAN v2.7 Apr 2017
// Version 2.8   Updated to IDS-CAN v2.8 Jun 2017

#ifndef IDS_CAN_VERSION_NUMBER_H
#define IDS_CAN_VERSION_NUMBER_H

typedef uint8 IDS_CAN_VERSION_NUMBER;

#define IDS_CAN_VERSION_NUMBER_0_9             0x00
#define IDS_CAN_VERSION_NUMBER_1_0             0x01
#define IDS_CAN_VERSION_NUMBER_1_1             0x02
#define IDS_CAN_VERSION_NUMBER_1_2             0x03
#define IDS_CAN_VERSION_NUMBER_1_3             0x04
#define IDS_CAN_VERSION_NUMBER_1_4             0x05
#define IDS_CAN_VERSION_NUMBER_1_5             0x06
#define IDS_CAN_VERSION_NUMBER_1_6             0x07
#define IDS_CAN_VERSION_NUMBER_2_0             0x08
#define IDS_CAN_VERSION_NUMBER_2_1             0x09
#define IDS_CAN_VERSION_NUMBER_2_2             0x0A
#define IDS_CAN_VERSION_NUMBER_2_3             0x0B
#define IDS_CAN_VERSION_NUMBER_2_4             0x0C
#define IDS_CAN_VERSION_NUMBER_2_5             0x0D
#define IDS_CAN_VERSION_NUMBER_2_6             0x0E
#define IDS_CAN_VERSION_NUMBER_2_7             0x0F
#define IDS_CAN_VERSION_NUMBER_2_8             0x10

#define IDS_CAN_VERSION_NUMBER_LATEST          IDS_CAN_VERSION_NUMBER_2_8

#endif // IDS_CAN_VERSION_NUMBER_H
