// IDS-CAN_Request.c
// IDS CAN request management
// Version 2.8.1
// (c) 2012, 2013, 2015, 2016, 2017 Innovative Design Solutions, Inc.
// All rights reserved

// History
// Version 1.0   First version - WIP
// Version 1.0a  Updated to get rid of compiler warning
// Version 1.0b  Updated to get rid of compiler warning
// Version 1.1   Added CAN TX filter to allow local requests to be processed
// Version 1.2   Updated to work with IDS-CAN changes March 2013
// Version 1.3   Debugged SESSION_ID support
// Version 1.4   replaced IDS_CAN_IsSessionOpen() with IDS_CAN_GetSessionOwner()
// Version 1.5   moved encryption out of this module into IDS-CAN.c
//               enumerated supported requests
// Version 1.5a  deprecated IDS_CAN_REQUEST_FILTERS
// Version 1.5b  added support for a user defined callback to check if a
//               session is allowed to be opened or not
// Version 2.1a  Minor bugfix
// Version 2.4   IDS-CAN v2.4 June 2016
//               Working DTC support
//               Modifications to support both MC9S12 and Kinetis microcontrollers
// Version 2.4a  Added IDS_CAN_KillSession() to allow application to directly terminate sessions
//               Open Session Request $43 now checks IsSessionAllowedCallback() before opening the session
//               Changed how errors are reported in Session Heartbeat Request $44
// Version 2.5   Added IDS_CAN_IsAnySessionOpen();
// Version 2.5a  "session killed" message is now sent after IDS_CAN_KillSession() kills a session
// Version 2.6   IDS-CAN v2.6 Mar 2017
// Version 2.7   IDS-CAN v2.7 Apr 2017
// Version 2.8   Linted
// Version 2.8.1 Fixed ASSERT failure

#include "global.h"

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#ifdef HEADER

typedef struct { uint8 byte[6]; } IDS_CAN_PID_DATA;

typedef enum {
	IDS_CAN_REQUEST_READ_PART_NUMBER = 0x00,
	IDS_CAN_REQUEST_MUTE_DEVICE = 0x01,

	IDS_CAN_REQUEST_PID_READ_LIST = 0x10,
	IDS_CAN_REQUEST_PID_READ_WRITE = 0x11,

	IDS_CAN_REQUEST_BLOCK_READ_LIST = 0x20,
	IDS_CAN_REQUEST_BLOCK_READ_SIZE = 0x21,
	IDS_CAN_REQUEST_BLOCK_READ_WRITE = 0x22,

	IDS_CAN_REQUEST_READ_CONTINUOUS_DTCS = 0x30,
	IDS_CAN_REQUEST_CONTINUOUS_DTC_COMMAND = 0x31,

	IDS_CAN_REQUEST_SESSION_READ_LIST = 0x40,
	IDS_CAN_REQUEST_SESSION_READ_STATUS = 0x41,
	IDS_CAN_REQUEST_SESSION_REQUEST_SEED = 0x42,
	IDS_CAN_REQUEST_SESSION_TRANSMIT_KEY = 0x43,
	IDS_CAN_REQUEST_SESSION_HEARTBEAT = 0x44,
	IDS_CAN_REQUEST_SESSION_END = 0x45,

//	IDS_CAN_REQUEST_MEMORY_READ_MAP = 0x50,
	IDS_CAN_REQUEST_DAQ_NUM_CHANNELS = 0x51,
	IDS_CAN_REQUEST_DAQ_AUTO_TX_SETTINGS = 0x52,
	IDS_CAN_REQUEST_DAQ_CHANNEL_SETTINGS = 0x53,
	IDS_CAN_REQUEST_DAQ_PID_ADDRESS = 0x54,
} IDS_CAN_REQUEST;

typedef enum {
	IDS_CAN_RESPONSE_SUCCESS                      = 0x00,
	IDS_CAN_RESPONSE_REQUEST_NOT_SUPPORTED        = 0x01,
	IDS_CAN_RESPONSE_BAD_REQUEST                  = 0x02,
	IDS_CAN_RESPONSE_VALUE_OUT_OF_RANGE           = 0x03,
	IDS_CAN_RESPONSE_UNKNOWN_ID                   = 0x04,
	IDS_CAN_RESPONSE_WRITE_VALUE_TOO_LARGE        = 0x05,
	IDS_CAN_RESPONSE_INVALID_ADDRESS              = 0x06,
	IDS_CAN_RESPONSE_READ_ONLY                    = 0x07,
	IDS_CAN_RESPONSE_WRITE_ONLY                   = 0x08,
	IDS_CAN_RESPONSE_CONDITIONS_NOT_CORRECT       = 0x09,
	IDS_CAN_RESPONSE_FEATURE_NOT_SUPPORTED        = 0x0A,
	IDS_CAN_RESPONSE_BUSY                         = 0x0B,
	IDS_CAN_RESPONSE_SEED_NOT_REQUESTED           = 0x0C,
	IDS_CAN_RESPONSE_KEY_NOT_CORRECT              = 0x0D,
	IDS_CAN_RESPONSE_SESSION_NOT_OPEN             = 0x0E,
	IDS_CAN_RESPONSE_TIMEOUT                      = 0x0F,
	IDS_CAN_RESPONSE_REMOTE_REQUEST_NOT_SUPPORTED = 0x10,
} IDS_CAN_RESPONSE_CODE;

// management routines - private API - do not call
void _IDS_CAN_Request_Task(uint16 clock_ms);
void _IDS_CAN_OnRequestMessageRx(const IDS_CAN_RX_MESSAGE *message);

// read/write value by PID
// data is passed in IDS_CAN_PID_DATA structures, which are network byte ordered (MSB first)
uint8 IDS_CAN_IsPIDSupported(IDS_CAN_PID pid);
IDS_CAN_PID_DATA IDS_CAN_ReadPID(IDS_CAN_DEVICE device, IDS_CAN_PID pid);
uint8 IDS_CAN_WritePID(IDS_CAN_DEVICE device, IDS_CAN_PID pid, const IDS_CAN_PID_DATA * data);

// optional session support
IDS_CAN_ADDRESS IDS_CAN_GetSessionOwner(IDS_CAN_DEVICE device, IDS_CAN_SESSION_ID session_id);
void IDS_CAN_KillSession(IDS_CAN_DEVICE device, IDS_CAN_SESSION_ID session_id);
uint8 IDS_CAN_IsAnySessionOpen(IDS_CAN_DEVICE device);

#else // HEADER

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#if defined(__GNUC__)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunknown-pragmas"
#pragma GCC diagnostic ignored "-Wmissing-braces"
#pragma GCC diagnostic ignored "-Wunused-function"
#endif

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

typedef struct { uint8 byte[5]; } U40;
typedef IDS_CAN_PID_DATA U48;
typedef struct { uint8 Data[8]; } PAYLOAD;
#define I40 U40
#define I48 U48

#if defined __BIG_ENDIAN__
	// CPU byte order is same as network byte order
	#define SWAP16(val) (uint16)(val)
	#define SWAP32(val) (uint32)(val)
	#define SWAP40(val) *(const U40 *)(val)
	#define SWAP48(val) *(const U48 *)(val)
#elif defined __LITTLE_ENDIAN__
	// CPU byte order is reverse of network byte order
	static uint16 SWAP16(uint16 val)
	{
		uint8 temp[2];
		temp[0] = ((uint8*)&val)[1];
		temp[1] = ((uint8*)&val)[0];
		return *(uint16*)temp;
	}
		
	static uint32 SWAP32(uint32 val)
	{
		uint8 temp[4];
		temp[0] = ((uint8*)&val)[0];
		temp[1] = ((uint8*)&val)[1];
		temp[2] = ((uint8*)&val)[2];
		temp[3] = ((uint8*)&val)[3];
		return *(uint32*)temp;
	}
	
	static U40 SWAP40(const U40 * val)
	{
		U40 temp;
		temp.byte[0] = val->byte[4];
		temp.byte[1] = val->byte[3];
		temp.byte[2] = val->byte[2];
		temp.byte[3] = val->byte[1];
		temp.byte[4] = val->byte[0];
		return temp;
	}
	
	static U48 SWAP48(const U48 * val)
	{
		U48 temp;
		temp.byte[0] = val->byte[5];
		temp.byte[1] = val->byte[4];
		temp.byte[2] = val->byte[3];
		temp.byte[3] = val->byte[2];
		temp.byte[4] = val->byte[1];
		temp.byte[5] = val->byte[0];
		return temp;
	}
	
#else
	#error Must define either __BIG_ENDIAN__ or __LITTLE_ENDIAN__
#endif

static uint16 Read_uint16(const uint8 * ptr)
{
    WORD result;
    result.ui8.hi = ptr[0];
    result.ui8.lo = ptr[1];
    return result.ui16;
}

static uint32 Read_uint32(const uint8 * ptr)
{
    DWORD result;
    result.word.hi.ui8.hi = ptr[0];
    result.word.hi.ui8.lo = ptr[1];
    result.word.lo.ui8.hi = ptr[2];
    result.word.lo.ui8.lo = ptr[3];
    return result.ui32;
}

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

static U48 _IDS_CAN_GetSoftwareBuildDate(void)
{
	U48 result;
#if defined __BIG_ENDIAN__
	result.byte[0] = (uint8)(IDSCore_GetSoftwareBuildYear() % 100);
	result.byte[1] = IDSCore_GetSoftwareBuildMonth();
	result.byte[2] = IDSCore_GetSoftwareBuildDay();
	result.byte[3] = IDSCore_GetSoftwareBuildHour();
	result.byte[4] = IDSCore_GetSoftwareBuildMinute();
	result.byte[5] = IDSCore_GetSoftwareBuildSecond();
#else
	result.byte[5] = (uint8)(IDSCore_GetSoftwareBuildYear() % 100);
	result.byte[4] = IDSCore_GetSoftwareBuildMonth();
	result.byte[3] = IDSCore_GetSoftwareBuildDay();
	result.byte[2] = IDSCore_GetSoftwareBuildHour();
	result.byte[1] = IDSCore_GetSoftwareBuildMinute();
	result.byte[0] = IDSCore_GetSoftwareBuildSecond();
#endif
	return result;
}

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

static uint16 LookupIndex16(uint16 value, const uint16 *table, uint16 tablesize)
{
	for (;;)
	{
		if (!tablesize)
			return 0xFFFF; // not found
		tablesize--;
		if (table[tablesize] == value)
			return tablesize;
	}
}

typedef struct
{
	const uint16 * Table; // points to 16-bit values to return
	const uint8* FlagTable; // points to optional flags to return
	uint16 TableSize; // number of elements in Table
	uint16 (*ListSize)(void); // optional: function pointer that returns number of elements in table (overrides TableSize)
	uint16 (*Iterate)(uint16); // optional: function pointer used to iterate through a table
} ENUM_INFO;

// generic function to enumerate a list of ID objects in the system
//        request = message buffer containing the request
//                  16-bit index is in Data[2] and Data[3]
//       response = message buffer for the response
//     info.Table = pointer to a table of 16-bit values
// info.TableSize = number of entries in the table
//  info.ListSize = callback function to read the number of items that the iteration will return
//                  effectively the request index can be from 0 to this value
//                  if the function pointer is NULL, then it is assumed that the full table is to be iterated
//   info.Iterate = callback function to lookup the next item iterated, by table index
//                      index into table = Iterate(index of iteration)
//                  if NULL is used, then an identity mapping is assumed
//                      index into table = index of iteration
// examples
//  { Table, elementsof(Table), NULL, NULL }
//      this iterates the full table, in sequential order
//  { Table, elementsof(Table), GetItemsInList, GetIndexOfNextItemInList }
//      this iterates a sub list of items from the table
//      total size of the list is returned by GetItemsInList()
//      the order of items is determined by GetIndexOfNextItemInList(nth position)
static IDS_CAN_RESPONSE_CODE Enumerate16(const IDS_CAN_RX_MESSAGE *request, CAN_TX_MESSAGE *response, ENUM_INFO info)
{
	uint16 index, value;
	uint8 num_fields, n;

	if (request->Length < 2)
		return IDS_CAN_RESPONSE_BAD_REQUEST;
	num_fields = (info.FlagTable ? 2 : 3);

	response->Length = 8;
	n = 2;
	//response->Data[0] = request->Data[0];
	//response->Data[1] = request->Data[1];
	index = Read_uint16(&request->Data[0]) * num_fields;

	// if index is zero, then this is a special case
	if (index == 0)
	{
		// first element returned is always the size of the list
		value = info.ListSize ? info.ListSize() : info.TableSize;
		response->Data[n++] = (uint8)(value >> 8);
		response->Data[n++] = (uint8)(value >> 0); /*lint !e835 shifting by zero */
		if (info.FlagTable)
			response->Data[n++] = 0; // add NULL flag
	}
	else
	{
		// adjust index accordingly
		index--;
	}

	// iterate the IDs from the table
	// until the end of the message is reached
	while (n <= 6)
	{
		value = info.Iterate ? info.Iterate(index) : index;
		if (value >= info.TableSize)
			break;
		response->Data[n++] = (uint8)(info.Table[value] >> 8);
		response->Data[n++] = (uint8)(info.Table[value] >> 0); /*lint !e835 shifting by zero */
		if (info.FlagTable && n < 8)
			response->Data[n++] = info.FlagTable[value];
		index++;
	}

	return IDS_CAN_RESPONSE_SUCCESS;
}

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

#pragma INLINE
static IDS_CAN_RESPONSE_CODE Message00_ReadSoftwarePartNumber(const IDS_CAN_RX_MESSAGE *request, CAN_TX_MESSAGE *response)
{
	if (request->Length != 0) return IDS_CAN_RESPONSE_BAD_REQUEST;
	response->Length = 8;
	response->Data[0] = (uint8)IDS_PartNumberDigit(0);
	response->Data[1] = (uint8)IDS_PartNumberDigit(1);
	response->Data[2] = (uint8)IDS_PartNumberDigit(2);
	response->Data[3] = (uint8)IDS_PartNumberDigit(3);
	response->Data[4] = (uint8)IDS_PartNumberDigit(4);
	response->Data[5] = (uint8)IDS_PartNumberDigit(5);
	response->Data[6] = (uint8)IDS_PartNumberDigit(6);
	response->Data[7] = 0;
	return IDS_CAN_RESPONSE_SUCCESS;
}

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

// private API for internal IDS-CAN use only
extern void _IDS_CAN_MuteDevice(IDS_CAN_DEVICE device, uint8 seconds);
extern void _IDS_CAN_UnmuteDevice(IDS_CAN_DEVICE device);
extern uint8 _IDS_CAN_IsDeviceMuted(IDS_CAN_DEVICE device);

#pragma INLINE
static IDS_CAN_RESPONSE_CODE Message01_RequestMuteDevice(IDS_CAN_DEVICE device, const IDS_CAN_RX_MESSAGE *request, CAN_TX_MESSAGE *response)
{
	if (request->Length == 2)
	{
		switch (request->Data[0])
		{
		case 0x00: // unmute
			_IDS_CAN_UnmuteDevice(device); // private API
			response->Length = 2;
			response->Data[0] = 0x00;
			response->Data[1] = 0x00;
			return IDS_CAN_RESPONSE_SUCCESS;

		case 0x01: // mute
			response->Length = 2;
			response->Data[0] = 0x01;
			response->Data[1] = request->Data[1];

			// transmit response code before muting
			// this bypasses normal response chain
			if (!IDS_CAN_Tx(device, response))
			{
				// out of buffer space, this hopefully never occurs!
			}

			// mute device
			_IDS_CAN_MuteDevice(device, request->Data[1]); // private API

			// since we already transmitted a response, no need to send anything
			response->Length = 0xFF; // illegal length, no response is sent
			return IDS_CAN_RESPONSE_SUCCESS;

		case 0x02: // reset mute timer
			// this command resets the mute timer if the device is muted
			if (_IDS_CAN_IsDeviceMuted(device))
				_IDS_CAN_MuteDevice(device, request->Data[1]); // private API
			response->Length = 0xFF; // illegal length, no response is sent
			return IDS_CAN_RESPONSE_SUCCESS;

		default: // unknown
			break;
		}
	}

	return IDS_CAN_RESPONSE_BAD_REQUEST;
}

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

// PID support

// define PIDs that can be supported by this module

#define MANDATORY_PIDS \
	PID_READ_ONLY(IDS_CAN_PID_SOFTWARE_BUILD_DATE_TIME,       VAL_U48 = _IDS_CAN_GetSoftwareBuildDate())

// define CAN network PIDs
#ifdef CAN_MAINTAIN_STATISTICS
	#define NETWORK_PIDS \
	PID_READ_ONLY(IDS_CAN_PID_IDS_CAN_NUM_DEVICES_ON_NETWORK, VAL_U8 = IDS_CAN_NumDevicesOnNetwork()) \
	PID_READ_ONLY(IDS_CAN_PID_CAN_BYTES_TX,                   VAL_U32 = CAN_GetNumBytesSent())\
	PID_READ_ONLY(IDS_CAN_PID_CAN_BYTES_RX,                   VAL_U32 = CAN_GetNumBytesReceived())\
	PID_READ_ONLY(IDS_CAN_PID_CAN_MESSAGES_TX,                VAL_U32 = CAN_GetNumMessagesSent())\
	PID_READ_ONLY(IDS_CAN_PID_CAN_MESSAGES_RX,                VAL_U32 = CAN_GetNumMessagesReceived())\
	PID_READ_ONLY(IDS_CAN_PID_CAN_TX_BUFFER_OVERFLOW_COUNT,   VAL_U32 = CAN_GetTxBufferOverflowCount())\
	PID_READ_ONLY(IDS_CAN_PID_CAN_RX_BUFFER_OVERFLOW_COUNT,   VAL_U32 = CAN_GetRxBufferOverflowCount())\
	PID_READ_ONLY(IDS_CAN_PID_CAN_TX_MAX_BYTES_QUEUED,        VAL_U32 = CAN_GetMaxTxQueueCount())\
	PID_READ_ONLY(IDS_CAN_PID_CAN_RX_MAX_BYTES_QUEUED,        VAL_U32 = CAN_GetMaxRxQueueCount())
#else
	#define NETWORK_PIDS \
	PID_READ_ONLY(IDS_CAN_PID_IDS_CAN_NUM_DEVICES_ON_NETWORK, VAL_U8 = IDS_CAN_NumDevicesOnNetwork())
#endif // CAN_MAINTAIN_STATISTICS

// define RTC related PIDs
#ifdef IDS_CAN_RTC_HARDWARE_READ
	#define RTC_PIDS \
	PID_READ_WRITE_NVR(IDS_CAN_PID_TIME_ZONE,        VAL_U8 = IDS_CAN_RTC_GetTimeZone(),        IDS_CAN_SESSION_ID_UNKNOWN, IDS_CAN_RTC_SetTimeZone(VAL_U8))\
	PID_READ_ONLY     (IDS_CAN_PID_RTC_TIME_SEC,     VAL_U8 = IDS_CAN_RTC_GetDateTime().Second)\
	PID_READ_ONLY     (IDS_CAN_PID_RTC_TIME_MIN,     VAL_U8 = IDS_CAN_RTC_GetDateTime().Minute)\
	PID_READ_ONLY     (IDS_CAN_PID_RTC_TIME_HOUR,    VAL_U8 = IDS_CAN_RTC_GetDateTime().Hour)\
	PID_READ_ONLY     (IDS_CAN_PID_RTC_TIME_DAY,     VAL_U8 = IDS_CAN_RTC_GetDateTime().Day)\
	PID_READ_ONLY     (IDS_CAN_PID_RTC_TIME_MONTH,   VAL_U8 = IDS_CAN_RTC_GetDateTime().Month)\
	PID_READ_ONLY     (IDS_CAN_PID_RTC_TIME_YEAR,    VAL_U16 = IDS_CAN_RTC_GetDateTime().Year)\
	PID_READ_WRITE_NVR(IDS_CAN_PID_RTC_EPOCH_SEC,    VAL_U32 = IDS_CAN_RTC_GetTimeEpoch(),      IDS_CAN_SESSION_ID_UNKNOWN, IDS_CAN_RTC_SetTimeEpoch(VAL_U32))\
	PID_READ_ONLY     (IDS_CAN_PID_RTC_SET_TIME_SEC, VAL_U32 = IDS_CAN_RTC_GetTimeSetEpoch())
#else
	#define RTC_PIDS \
	PID_READ_WRITE    (IDS_CAN_PID_TIME_ZONE,        VAL_U8 = IDS_CAN_RTC_GetTimeZone(),        IDS_CAN_SESSION_ID_UNKNOWN, IDS_CAN_RTC_SetTimeZone(VAL_U8))\
	PID_READ_ONLY     (IDS_CAN_PID_RTC_TIME_SEC,     VAL_U8 = IDS_CAN_RTC_GetDateTime().Second)\
	PID_READ_ONLY     (IDS_CAN_PID_RTC_TIME_MIN,     VAL_U8 = IDS_CAN_RTC_GetDateTime().Minute)\
	PID_READ_ONLY     (IDS_CAN_PID_RTC_TIME_HOUR,    VAL_U8 = IDS_CAN_RTC_GetDateTime().Hour)\
	PID_READ_ONLY     (IDS_CAN_PID_RTC_TIME_DAY,     VAL_U8 = IDS_CAN_RTC_GetDateTime().Day)\
	PID_READ_ONLY     (IDS_CAN_PID_RTC_TIME_MONTH,   VAL_U8 = IDS_CAN_RTC_GetDateTime().Month)\
	PID_READ_ONLY     (IDS_CAN_PID_RTC_TIME_YEAR,    VAL_U16 = IDS_CAN_RTC_GetDateTime().Year)\
	PID_READ_WRITE    (IDS_CAN_PID_RTC_EPOCH_SEC,    VAL_U32 = IDS_CAN_RTC_GetTimeEpoch(),      IDS_CAN_SESSION_ID_UNKNOWN, IDS_CAN_RTC_SetTimeEpoch(VAL_U32))\
	PID_READ_ONLY     (IDS_CAN_PID_RTC_SET_TIME_SEC, VAL_U32 = IDS_CAN_RTC_GetTimeSetEpoch())
#endif // IDS_CAN_RTC_HARDWARE_READ

// combined macro that defines all supported PIDs
#define ALL_PIDS   MANDATORY_PIDS  NETWORK_PIDS  RTC_PIDS  IDS_CAN_PID_SUPPORT

// enumerate the PIDs
enum
{
	#undef PID_READ_ONLY
	#undef PID_READ_WRITE
	#undef PID_READ_WRITE_NVR
	#define PID_READ_ONLY(id, read)                       ENUM_##id,
	#define PID_READ_WRITE(id, read, session, write)      ENUM_##id,
	#define PID_READ_WRITE_NVR(id, read, session, write)  ENUM_##id,
	ALL_PIDS
	NUM_PIDS
};

// create table of supported PIDs
static const IDS_CAN_PID PID_List[NUM_PIDS] = {
	#undef PID_READ_ONLY
	#undef PID_READ_WRITE
	#undef PID_READ_WRITE_NVR
	#define PID_READ_ONLY(id, read)                       id,
	#define PID_READ_WRITE(id, read, session, write)      id,
	#define PID_READ_WRITE_NVR(id, read, session, write)  id,
	ALL_PIDS
};

static uint16 GetPIDIndex(uint16 pid) { return LookupIndex16(pid, PID_List, NUM_PIDS); }

uint8 IDS_CAN_IsPIDSupported(IDS_CAN_PID pid) { return (GetPIDIndex(pid) < NUM_PIDS); }

// macros to read/write PID values
#if defined __BIG_ENDIAN__
#define VAL_PTR  &val.byte[0]
#define VAL_U8   (*((uint8*)&val.byte[5]))
#define VAL_I8   (*((int8*)&val.byte[5]))
#define VAL_U16  (*((uint16*)&val.byte[4]))
#define VAL_I16  (*((int16*)&val.byte[4]))
#define VAL_U32  (*((uint32*)&val.byte[2]))
#define VAL_I32  (*((int32*)&val.byte[2]))
#define VAL_U40  (*((U40*)&val.byte[1]))
#define VAL_I40  (*((I40*)&val.byte[1]))
#define VAL_U48  (*((U48*)&val.byte[0]))
#define VAL_I48  (*((I48*)&val.byte[0]))
#else
#define VAL_PTR  &val.byte[0]
#define VAL_U8   (*((uint8*)&val.byte[0]))
#define VAL_I8   (*((int8*)&val.byte[0]))
#define VAL_U16  (*((uint16*)&val.byte[0]))
#define VAL_I16  (*((int16*)&val.byte[0]))
#define VAL_U32  (*((uint32*)&val.byte[0]))
#define VAL_I32  (*((int32*)&val.byte[0]))
#define VAL_U40  (*((U40*)&val.byte[0]))
#define VAL_I40  (*((I40*)&val.byte[0]))
#define VAL_U48  (*((U48*)&val.byte[0]))
#define VAL_I48  (*((I48*)&val.byte[0]))
#endif

#pragma MESSAGE DISABLE C5917 /* removed dead assignment */
static U48 ReadPID(IDS_CAN_DEVICE device, uint16 index)
{
	U48 val;

	// clear the buffer
	val.byte[0] = 0;
	val.byte[1] = 0;
	val.byte[2] = 0;
	val.byte[3] = 0;
	val.byte[4] = 0;
	val.byte[5] = 0;

	if (index >= NUM_PIDS)
		return val; // invalid PID

	// read the PID
	/*lint -save -e744 -e{826} No default: in switch. The pointer conversions in the macro are understood. */
	switch (index)
	{
	#undef PID_READ_ONLY
	#undef PID_READ_WRITE
	#undef PID_READ_WRITE_NVR
	#define PID_READ_ONLY(id, read)                       case ENUM_##id: (read); break;
	#define PID_READ_WRITE(id, read, session, write)      case ENUM_##id: (read); break;
	#define PID_READ_WRITE_NVR(id, read, session, write)  case ENUM_##id: (read); break;
	ALL_PIDS
	}
	/*lint -restore */

	// convert from local to network byte ordering
	val = SWAP48(&val);
	
	return val;
}
#pragma MESSAGE DEFAULT C5917 /* removed dead assignment */

#pragma MESSAGE DISABLE C5703 /* variable declared but not referenced */
/*lint -save -e715 device may not be referenced */
static uint8 WritePID(IDS_CAN_DEVICE device, uint16 index, const IDS_CAN_PID_DATA * data)
{
	U48 val;

	// range check
	if (index >= NUM_PIDS)
		return FALSE;

	// unload the value from the message into RAM
	// and convert from network to local byte ordering
	val = SWAP48(data);

	// perform the write
	switch (index)
	{
	default: return FALSE;
	#undef PID_READ_ONLY
	#undef PID_READ_WRITE
	#undef PID_READ_WRITE_NVR
	#define PID_READ_ONLY(id, read)                       case ENUM_##id: return FALSE;
	#define PID_READ_WRITE(id, read, session, write)      case ENUM_##id: write; break;
	#define PID_READ_WRITE_NVR(id, read, session, write)  case ENUM_##id: write; break;
	ALL_PIDS
	}

	return TRUE;
}
#pragma MESSAGE DEFAULT C5703 /* variable declared but not referenced */
/*lint -restore */


IDS_CAN_PID_DATA IDS_CAN_ReadPID(IDS_CAN_DEVICE device, IDS_CAN_PID pid)
{
	return ReadPID(device, GetPIDIndex(pid));
}

uint8 IDS_CAN_WritePID(IDS_CAN_DEVICE device, IDS_CAN_PID pid, const IDS_CAN_PID_DATA * data)
{
	return WritePID(device, GetPIDIndex(pid), data);
}

#pragma INLINE
static uint8 GetMinimumPIDSizeBytes(const U48 * val)
{
	if (val->byte[0])      return 6;
	else if (val->byte[1]) return 5;
	else if (val->byte[2]) return 4;
	else if (val->byte[3]) return 3;
	else if (val->byte[4]) return 2;
	else                   return 1;
}

#pragma MESSAGE DISABLE C5703 /* device not used */
#pragma MESSAGE DISABLE C12056 /* SP debug info incorrect because of optimization or inline assembler */
/*lint -save -e715 device may not be referenced */
#pragma INLINE
static IDS_CAN_RESPONSE_CODE Message10_PIDSupport(IDS_CAN_DEVICE device, const IDS_CAN_RX_MESSAGE *request, CAN_TX_MESSAGE *response)
{
	// create flag table
	static const uint8 Flags[NUM_PIDS] = {
		#undef PID_READ_ONLY
		#undef PID_READ_WRITE
		#undef PID_READ_WRITE_NVR
		#define PID_READ_ONLY(id, read)                       BIT0,
		#define PID_READ_WRITE(id, read, session, write)      BIT0|BIT1,
		#define PID_READ_WRITE_NVR(id, read, session, write)  BIT0|BIT1|BIT2,
		ALL_PIDS
	};

	const ENUM_INFO Info = { PID_List, Flags, NUM_PIDS, NULL, NULL };

	// length check
	if (request->Length != 2) return IDS_CAN_RESPONSE_BAD_REQUEST;

	return Enumerate16(request, response, Info);
}
#pragma MESSAGE DEFAULT C5703
#pragma MESSAGE DEFAULT C12056
/*lint -restore */

#pragma MESSAGE DISABLE C4000 /* condition always true */
#pragma MESSAGE DISABLE C4001 /* condition always false */
#pragma MESSAGE DISABLE C5703 /* device not used */
#pragma MESSAGE DISABLE C5917 /* removed dead assignment*/
#pragma MESSAGE DISABLE C12056 /* SP debug info incorrect because of optimization or inline assembler */
#pragma INLINE
static IDS_CAN_RESPONSE_CODE Message11_PIDSupport(IDS_CAN_DEVICE device, const IDS_CAN_RX_MESSAGE *request, CAN_TX_MESSAGE *response)
{
	U48 val;
	uint16 index;
	uint8 len;

	// check request length
	switch (request->Length)
	{
	default: return IDS_CAN_RESPONSE_BAD_REQUEST;
	case 2: break; // read only
	case 8: break; // write
	}

	// determine PID index
	index = GetPIDIndex(Read_uint16(&request->Data[0]));
	if (index >= NUM_PIDS) return IDS_CAN_RESPONSE_UNKNOWN_ID;

	// is this a write request?
	if (request->Length == 8)
	{
		// ROM table of SESSION_IDs supported for each PID write
		static const IDS_CAN_SESSION_ID WriteSession[NUM_PIDS] = {
			#undef PID_READ_ONLY
			#undef PID_READ_WRITE
			#undef PID_READ_WRITE_NVR
			#define PID_READ_ONLY(id, read)                       0xFFFF,
			#define PID_READ_WRITE(id, read, session, write)      session,
			#define PID_READ_WRITE_NVR(id, read, session, write)  session,
			ALL_PIDS
		};

		// verify that the request has the appropriate permission to perform the write
		IDS_CAN_SESSION_ID session = WriteSession[index];
		if (session)
		{
			if (session == 0xFFFF)
				return IDS_CAN_RESPONSE_READ_ONLY;
			if (IDS_CAN_GetSessionOwner(device, session) != request->SourceAddress)
				return IDS_CAN_RESPONSE_SESSION_NOT_OPEN;
		}

		// perform the PID write
		if (!WritePID(device, index, (U48*)&request->Data[2]))
			return IDS_CAN_RESPONSE_BAD_REQUEST; // shouldn't get here
	}

	// read the PID
	val = ReadPID(device, index);

	// build the response message
	len = GetMinimumPIDSizeBytes(&val);
	response->Length = 2 + len;
	//response->Data[0] = request->Data[0];
	//response->Data[1] = request->Data[1];
	(void)memcpy(&response->Data[2], &val.byte[6-len], len);

	return IDS_CAN_RESPONSE_SUCCESS;
}
#pragma MESSAGE DEFAULT C4000
#pragma MESSAGE DEFAULT C4001
#pragma MESSAGE DEFAULT C5703
#pragma MESSAGE DEFAULT C5917
#pragma MESSAGE DEFAULT C12056

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

#ifdef IDS_CAN_BLOCK_SUPPORT

#error THIS NEEDS DEBUGGING

// enumerate the BLOCKs
typedef enum
{
	#undef BLOCK
	#define BLOCK(id, arg1, arg2, arg3)   ENUM_##id,
	IDS_CAN_BLOCK_SUPPORT
	NUM_BLOCKS
};

// create table of supported Block IDs
static const IDS_CAN_BLOCK_ID BlockID[NUM_BLOCKS] = {
	#undef BLOCK
	#define BLOCK(id, arg1, arg2, arg3)   id,
	IDS_CAN_BLOCK_SUPPORT
};

static uint16 GetBlockIndex(const uint8 * block)
{
	return LookupIndex16(Read_uint16(block), BlockID, NUM_BLOCKS);
}

#pragma INLINE
static IDS_CAN_RESPONSE_CODE Message20_ReadSupportedBlockIDs(IDS_CAN_DEVICE device, const IDS_CAN_RX_MESSAGE *request, CAN_TX_MESSAGE *response)
{
	const ENUM_INFO Info = { BlockID, NULL, NUM_BLOCKS, NULL, NULL };
	return Enumerate16(request, response, Info);
}

#pragma INLINE
static IDS_CAN_RESPONSE_CODE Message21_ReadBlockSize(IDS_CAN_DEVICE device, const IDS_CAN_RX_MESSAGE *request, CAN_TX_MESSAGE *response)
{
// john
	return IDS_CAN_RESPONSE_SUCCESS;
}

#pragma INLINE
static IDS_CAN_RESPONSE_CODE Message22_ReadWriteBlockID(IDS_CAN_DEVICE device, const IDS_CAN_RX_MESSAGE *request, CAN_TX_MESSAGE *response)
{
// john
	return IDS_CAN_RESPONSE_SUCCESS;
}

#endif // IDS_CAN_BLOCK_SUPPORT

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

#ifdef DECLARE_SUPPORTED_IDS_CAN_DTCS

#pragma INLINE
static IDS_CAN_RESPONSE_CODE Message30_ReadContinuousDTCs(const IDS_CAN_RX_MESSAGE *request, CAN_TX_MESSAGE *response)
{
	uint16 index;

	// request length = 2
	// bytes 0 and 1 are index into DTCs
	if (request->Length != 2)
		return IDS_CAN_RESPONSE_BAD_REQUEST;
	index = Read_uint16(&request->Data[0]) * 2; // 2 values returned per index

	// prepare response
	response->Length = 8;
	//response->Data[0] = request->Data[0];
	//response->Data[1] = request->Data[1];

	if (index >= (uint16)NUM_IDS_CAN_DTCS)	
	{
		response->Data[2] = 0;
		response->Data[3] = 0;
		response->Data[4] = 0;
		response->Data[5] = 0;
		response->Data[6] = 0;
		response->Data[7] = 0;
	}
	else
	{
		IDS_CAN_DTC_INFO info = IDS_CAN_GetDTCInfo((IDS_CAN_DTC) index);
		response->Data[2] = (uint8)(info.ID >> 8);
		response->Data[3] = (uint8)(info.ID >> 0); /*lint !e835 shifting by zero */
		response->Data[4] = info.Status;


		index++;
		if (index < (uint16)NUM_IDS_CAN_DTCS)	
		{
			info = IDS_CAN_GetDTCInfo((IDS_CAN_DTC) index);
			response->Data[5] = (uint8)(info.ID >> 8);
			response->Data[6] = (uint8)(info.ID >> 0); /*lint !e835 shifting by zero */
			response->Data[7] = info.Status;
		}
		else
		{
			response->Data[5] = 0;
			response->Data[6] = 0;
			response->Data[7] = 0;
		}
	}

	return IDS_CAN_RESPONSE_SUCCESS;
}

#pragma INLINE
static IDS_CAN_RESPONSE_CODE Message31_ContinuousDTCCommand(IDS_CAN_DEVICE device, const IDS_CAN_RX_MESSAGE *request, CAN_TX_MESSAGE *response)
{
	IDS_CAN_RESPONSE_CODE cmd_response = IDS_CAN_RESPONSE_SUCCESS;
	uint16 val;

	// requests are 1 byte each
	if (request->Length != 1)
		return IDS_CAN_RESPONSE_BAD_REQUEST;

	// any command to process?
	if (request->Data[0])
	{
		// commands require DIAGNOSTIC session access
		if (IDS_CAN_GetSessionOwner(device, IDS_CAN_SESSION_ID_DIAGNOSTIC) != request->SourceAddress)
			cmd_response = IDS_CAN_RESPONSE_SESSION_NOT_OPEN;
		else
		{
			// process the command
			switch (request->Data[0])
			{
			case 1: IDS_CAN_ClearActiveDTCs(); break;
			case 2: IDS_CAN_ClearStoredDTCs(); break;
			case 3: IDS_CAN_ClearAllDTCs(); break;
			default: cmd_response = IDS_CAN_RESPONSE_VALUE_OUT_OF_RANGE;
			}
		}
	}

	// prepare the response
	response->Length = 8;
	//response->Data[0] = request->Data[0]; // echo command
	response->Data[1] = (uint8)cmd_response;
	val = IDS_CAN_GetNumSupportedDTCs();
	response->Data[2] = (uint8)(val >> 8);
	response->Data[3] = (uint8)(val >> 0); /*lint !e835 shifting by zero */
	val = IDS_CAN_GetNumActiveDTCs();
	response->Data[4] = (uint8)(val >> 8);
	response->Data[5] = (uint8)(val >> 0); /*lint !e835 shifting by zero */
	val = IDS_CAN_GetNumStoredDTCs();
	response->Data[6] = (uint8)(val >> 8);
	response->Data[7] = (uint8)(val >> 0); /*lint !e835 shifting by zero */

	return IDS_CAN_RESPONSE_SUCCESS;
}

#endif // DECLARE_SUPPORTED_IDS_CAN_DTCS

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

#ifndef IDS_CAN_SESSION_SUPPORT
	#define IDS_CAN_SESSION_SUPPORT
#endif

#define IDS_CAN_MANDATORY_SESSIONS \
	SESSION(IDS_CAN_SESSION_ID_DAQ,  0x0B00B135, _IDS_CAN_IsDAQSessionAllowed, _IDS_CAN_OnDAQSessionOpened, _IDS_CAN_OnDAQSessionClosed) \

#define ALL_IDS_CAN_SESSIONS   IDS_CAN_SESSION_SUPPORT  IDS_CAN_MANDATORY_SESSIONS

// enumerate the session IDs
typedef enum
{
	#undef SESSION
	#define SESSION(id, cypher, session_allowed_callback, session_open_callback, session_closed_callback) ENUM_##id,
	ALL_IDS_CAN_SESSIONS
	NUM_IDS_CAN_SESSIONS
} SESSION_INDEX;

// declare table of supported session IDs
static const IDS_CAN_SESSION_ID SessionIDTable[NUM_IDS_CAN_SESSIONS] = {
	#undef SESSION
	#define SESSION(id, cypher, session_allowed_callback, session_open_callback, session_closed_callback) id,
	ALL_IDS_CAN_SESSIONS
};

typedef IDS_CAN_RESPONSE_CODE (*SESSION_ALLOWED_CALLBACK)(IDS_CAN_DEVICE, IDS_CAN_ADDRESS, IDS_CAN_SESSION_ID); // device, address, session_id
typedef void (*SESSION_OPEN_CALLBACK)(IDS_CAN_DEVICE, IDS_CAN_ADDRESS, IDS_CAN_SESSION_ID); // device, address, session_id
typedef void (*SESSION_CLOSE_CALLBACK)(IDS_CAN_DEVICE, IDS_CAN_ADDRESS, IDS_CAN_SESSION_ID);  // device, address, session_id

typedef enum {
	SESSION_CLOSED,
	SESSION_CLOSED_SEND_MESSAGE, // special state when "session kill" occurs
	SESSION_SEED_SENT,
	SESSION_OPEN
} SESSION_STATE;

// session management
typedef struct
{
	uint32 StartTime_ms;         // doubles as seed prior to session opening
	uint16 Timeout_ms;           // times out the current state
	IDS_CAN_ADDRESS HostAddress; // host that owns the session
	uint8 State;                 // state of the device (should be SESSION_STATE, however some compilers force enums to 16 bit, so using a uint8 potentially saves a lot of RAM
} SESSION_STRUCT;

static SESSION_STRUCT Session[NUM_IDS_CAN_SESSIONS][NUM_IDS_CAN_DEVICES] = { 0 };

uint8 IDS_CAN_IsAnySessionOpen(IDS_CAN_DEVICE device)
{
	if (device < NUM_IDS_CAN_DEVICES)
	{
		uint8 i = (uint8)NUM_IDS_CAN_SESSIONS;
		do
		{
			SESSION_STRUCT * ptr = &Session[--i][device];
			if (ptr->HostAddress && ptr->State == (uint8)SESSION_OPEN)
				return TRUE;
		} while (i);
	}

	return FALSE;
}

// looks up the local index to the 16-bit IDS_CAN_SESSION_ID passed
static SESSION_INDEX GetSessionIndex(uint16 session_id)
{
	return (SESSION_INDEX)LookupIndex16(session_id, SessionIDTable, (uint16)NUM_IDS_CAN_SESSIONS);
}

// looks up the local index to the 16-bit IDS_CAN_SESSION_ID passed in a pointer
static SESSION_INDEX GetSessionIndexPtr(const void *ptr)
{
	return GetSessionIndex(Read_uint16(ptr));
}

// looks up the session data associated with the session and device
// returns NULL if no such data was found
static SESSION_STRUCT * GetSessionData(IDS_CAN_DEVICE device, SESSION_INDEX index)
{
	if (device >= NUM_IDS_CAN_DEVICES || index >= NUM_IDS_CAN_SESSIONS)
		return NULL;
	return &Session[index][device];
}

IDS_CAN_ADDRESS IDS_CAN_GetSessionOwner(IDS_CAN_DEVICE device, IDS_CAN_SESSION_ID session)
{
	SESSION_STRUCT *ptr = GetSessionData(device, GetSessionIndex(session));
	if (ptr == NULL) return IDS_CAN_ADDRESS_BROADCAST; // invalid session
	if (ptr->State != (uint8)SESSION_OPEN) return IDS_CAN_ADDRESS_BROADCAST; // invalid
	return ptr->HostAddress;
}

static void OnSessionClosed(IDS_CAN_DEVICE device, IDS_CAN_ADDRESS address, SESSION_INDEX index)
{
	static const SESSION_CLOSE_CALLBACK CloseCallback[NUM_IDS_CAN_SESSIONS] = {
		#undef SESSION
		#define SESSION(id, cypher, session_allowed_callback, session_open_callback, session_closed_callback) session_closed_callback,
		ALL_IDS_CAN_SESSIONS
	};

	// if callback exists
	if (CloseCallback[index] != NULL)
		CloseCallback[index](device, address, SessionIDTable[index]); // execute callback
}

void IDS_CAN_KillSession(IDS_CAN_DEVICE device, IDS_CAN_SESSION_ID session_id)
{
	// if the session exists
	SESSION_INDEX index = GetSessionIndex(session_id);
	SESSION_STRUCT *ptr = GetSessionData(device, index);
	if (ptr != NULL)
	{
		if (ptr->State == (uint8)SESSION_OPEN)
		{
			ptr->State = (uint8)SESSION_CLOSED_SEND_MESSAGE; // send a response message later on
			OnSessionClosed(device, ptr->HostAddress, index);
		}
		else
		{
			ptr->State = (uint8)SESSION_CLOSED; // close the session
		}
	}
}

// looks up the session data associated with the session, device, host, and state
// if no data was found, or the host was wrong, or the state was wrong, then NULL is returned
// NOTE:  host = 0 implies that the host check is skipped
static SESSION_STRUCT * GetOpenSessionData(IDS_CAN_DEVICE device, SESSION_INDEX index, IDS_CAN_ADDRESS host, SESSION_STATE state)
{
	SESSION_STRUCT *ptr = GetSessionData(device, index);
	if (ptr != NULL)
	{
		if (ptr->State != (uint8)state) return NULL;
		if (host != 0 && ptr->HostAddress != host) return NULL;
	}
	return ptr;
}

#pragma INLINE
static IDS_CAN_RESPONSE_CODE Message40_ReadSupportedSessionIDs(const IDS_CAN_RX_MESSAGE *request, CAN_TX_MESSAGE *response)
{
	const ENUM_INFO Info = { SessionIDTable, NULL, (uint16)NUM_IDS_CAN_SESSIONS, NULL, NULL };
	return Enumerate16(request, response, Info);
}

#pragma INLINE
static IDS_CAN_RESPONSE_CODE Message41_ReadSessionStatus(IDS_CAN_DEVICE device, const IDS_CAN_RX_MESSAGE *request, CAN_TX_MESSAGE *response)
{
	SESSION_STRUCT *ptr;

	if (request->Length != 2) return IDS_CAN_RESPONSE_BAD_REQUEST;

	response->Length = 7;

	ptr = GetOpenSessionData(device, GetSessionIndexPtr(&request->Data[0]), 0, SESSION_OPEN);
	if (ptr != NULL)
	{
		uint32 delta;
		//response->Data[0] = request->Data[0];
		//response->Data[1] = request->Data[1];
		response->Data[2] = ptr->HostAddress;
		delta = OS_GetElapsedMilliseconds32() - ptr->StartTime_ms;
		response->Data[3] = (uint8)(delta >> 24);
		response->Data[4] = (uint8)(delta >> 16);
		response->Data[5] = (uint8)(delta >> 8);
		response->Data[6] = (uint8)(delta >> 0); /*lint !e835 shifting by zero */
	}
	return IDS_CAN_RESPONSE_SUCCESS;
}

static IDS_CAN_RESPONSE_CODE IsSessionAllowed(IDS_CAN_DEVICE device, SESSION_INDEX index, IDS_CAN_ADDRESS address)
{
	// table of app callbacks for "is session allowed?"
	static const SESSION_ALLOWED_CALLBACK IsSessionAllowedCallback[NUM_IDS_CAN_SESSIONS] = {
		#undef SESSION
		#define SESSION(id, cypher, session_allowed_callback, session_open_callback, session_closed_callback) session_allowed_callback,
		ALL_IDS_CAN_SESSIONS
	};

	// optional user support to see if session is allowed
	if (index < NUM_IDS_CAN_SESSIONS && IsSessionAllowedCallback[index] != NULL)
		return IsSessionAllowedCallback[index](device, address, SessionIDTable[index]);

	return IDS_CAN_RESPONSE_SUCCESS;
}

#pragma INLINE
static IDS_CAN_RESPONSE_CODE Message42_RequestSessionSeed(IDS_CAN_DEVICE device, const IDS_CAN_RX_MESSAGE *request, CAN_TX_MESSAGE *response)
{
	SESSION_INDEX index;
	SESSION_STRUCT *ptr;
	IDS_CAN_RESPONSE_CODE isallowed;

	if (request->Length != 2) return IDS_CAN_RESPONSE_BAD_REQUEST;
	if (request->SourceAddress == IDS_CAN_ADDRESS_BROADCAST) return IDS_CAN_RESPONSE_BAD_REQUEST;
	if (device >= NUM_IDS_CAN_DEVICES) return IDS_CAN_RESPONSE_SUCCESS;

	// make sure the session exists
	index = GetSessionIndexPtr(&request->Data[0]);
	ptr = GetSessionData(device, index);
	if (ptr == NULL) return IDS_CAN_RESPONSE_UNKNOWN_ID;

	// make sure session is not open
	if (ptr->State == (uint8)SESSION_OPEN) return IDS_CAN_RESPONSE_BUSY;

	// is the session allowed at this time?
	isallowed = IsSessionAllowed(device, index, request->SourceAddress);
	if (isallowed != IDS_CAN_RESPONSE_SUCCESS)
		return isallowed;

	// create a seed and remember the host/id
	ptr->State = (uint8)SESSION_SEED_SENT;
	ptr->HostAddress = request->SourceAddress;
	ptr->StartTime_ms = Random32(); // this holds the seed for now
	ptr->Timeout_ms = OS_GetElapsedMilliseconds16(); // set time for seed expiration

	// transmit the response
	response->Length = 6;
	//response->Data[0] = request->Data[0];
	//response->Data[1] = request->Data[1];
	response->Data[2] = (uint8)(ptr->StartTime_ms >> 24); // seed MSB
	response->Data[3] = (uint8)(ptr->StartTime_ms >> 16);
	response->Data[4] = (uint8)(ptr->StartTime_ms >> 8);
	response->Data[5] = (uint8)(ptr->StartTime_ms >> 0); /*lint !e835 shifting by zero */
	return IDS_CAN_RESPONSE_SUCCESS;
}

#pragma INLINE
static IDS_CAN_RESPONSE_CODE Message43_OpenSession(IDS_CAN_DEVICE device, const IDS_CAN_RX_MESSAGE *request, CAN_TX_MESSAGE *response)
{
	static const uint32 Cypher[NUM_IDS_CAN_SESSIONS] = {
		#undef SESSION
		#define SESSION(id, cypher, session_allowed_callback, session_open_callback, session_closed_callback) cypher,
		ALL_IDS_CAN_SESSIONS
	};

	static const SESSION_OPEN_CALLBACK SessionOpenCallback[NUM_IDS_CAN_SESSIONS] = {
		#undef SESSION
		#define SESSION(id, cypher, session_allowed_callback, session_open_callback, session_closed_callback) session_open_callback,
		ALL_IDS_CAN_SESSIONS
	};

	SESSION_INDEX index;
	SESSION_STRUCT *ptr;
	IDS_CAN_RESPONSE_CODE isallowed;

	if (request->Length != 6) return IDS_CAN_RESPONSE_BAD_REQUEST;

	// make sure a seed was requested
	index = GetSessionIndexPtr(&request->Data[0]);
	ptr = GetOpenSessionData(device, index, request->SourceAddress, SESSION_SEED_SENT);
	if (ptr == NULL)
		return IDS_CAN_RESPONSE_SEED_NOT_REQUESTED;
	
	// clear the seed -- it is one time use only
	ptr->State = (uint8)SESSION_CLOSED; // need to change this from SESSION_SEED_SENT

	// is the session allowed at this time?
	isallowed = IsSessionAllowed(device, index, request->SourceAddress);
	if (isallowed != IDS_CAN_RESPONSE_SUCCESS)
		return isallowed;

	// calculate the key, and check it against the transmitted key
	if (IDS_CAN_Encrypt(ptr->StartTime_ms, Cypher[index]) != Read_uint32(&request->Data[2]))
		return IDS_CAN_RESPONSE_KEY_NOT_CORRECT;

	// yay! open the session
	ptr->State = (uint8)SESSION_OPEN;
	ptr->StartTime_ms = OS_GetElapsedMilliseconds32();
	ptr->Timeout_ms = (uint16)ptr->StartTime_ms;

	// prepare the response
	response->Length = 2;
	//response->Data[0] = request->Data[0]; // ID of open session
	//response->Data[1] = request->Data[1]; // ID of open session

	// execute the callback
	if (SessionOpenCallback[index])
		SessionOpenCallback[index](device, ptr->HostAddress, SessionIDTable[index]);

	return IDS_CAN_RESPONSE_SUCCESS;
}

#pragma INLINE
static IDS_CAN_RESPONSE_CODE Message44_SessionHeartbeat(IDS_CAN_DEVICE device, const IDS_CAN_RX_MESSAGE *request, CAN_TX_MESSAGE *response)
{
	SESSION_STRUCT *ptr;

	if (request->Length != 2) return IDS_CAN_RESPONSE_BAD_REQUEST;

	// make sure the session is open
	ptr = GetOpenSessionData(device, GetSessionIndexPtr(&request->Data[0]), request->SourceAddress, SESSION_OPEN);
	if (ptr == NULL)
	{
		// transmit the response
		response->Length = 3;
		//response->Data[0] = request->Data[0];
		//response->Data[1] = request->Data[1];
		response->Data[2] = (uint8)IDS_CAN_RESPONSE_SESSION_NOT_OPEN; // report that session is not open
		return IDS_CAN_RESPONSE_SUCCESS;
	}

	// reset the heartbeat
	ptr->Timeout_ms = OS_GetElapsedMilliseconds16();

	// no response necessary now, it will be sent later
	response->Length = 0xFF; // illegal length, no response is sent
	return IDS_CAN_RESPONSE_SUCCESS;
}

#pragma INLINE
static IDS_CAN_RESPONSE_CODE Message45_CloseSession(IDS_CAN_DEVICE device, const IDS_CAN_RX_MESSAGE *request, CAN_TX_MESSAGE *response)
{
	SESSION_INDEX index;
	SESSION_STRUCT *ptr;

	if (request->Length != 2) return IDS_CAN_RESPONSE_BAD_REQUEST;

	// make sure the session is open
	index = GetSessionIndexPtr(&request->Data[0]);
	ptr = GetOpenSessionData(device, index, request->SourceAddress, SESSION_OPEN);
	if (ptr == NULL) return IDS_CAN_RESPONSE_SESSION_NOT_OPEN;

	ptr->State = (uint8)SESSION_CLOSED;
	OnSessionClosed(device, ptr->HostAddress, index);

	// transmit the response
	response->Length = 3;
	//response->Data[0] = request->Data[0];
	//response->Data[1] = request->Data[1];
	response->Data[2] = (uint8)IDS_CAN_RESPONSE_SUCCESS; // session closed normally
	return IDS_CAN_RESPONSE_SUCCESS;
}

#pragma INLINE
static void Session_Task(uint16 clock_ms)
{
	static SESSION_INDEX SessionIndex = (SESSION_INDEX)0;
	static IDS_CAN_DEVICE Device = (IDS_CAN_DEVICE)0;
	SESSION_STRUCT *ptr;
	SESSION_STATE state;

	// cycle and wrap indexes
	if (++Device >= NUM_IDS_CAN_DEVICES)
	{
		Device = (IDS_CAN_DEVICE)0;
		SessionIndex++;
	}
	if (SessionIndex >= NUM_IDS_CAN_SESSIONS)
		SessionIndex = (SESSION_INDEX)0;

	// get the next session to check
	ptr = GetSessionData(Device, SessionIndex);
	if (ptr == NULL)
		return;

	// get the current state
	state = (SESSION_STATE)ptr->State;
	switch (state)
	{
	case SESSION_CLOSED: return;

	case SESSION_CLOSED_SEND_MESSAGE:
		// check to see if we need to send the a special message "session was killed for internal reasons"
		if (ptr->HostAddress && IDS_CAN_IsDeviceOnline(Device))
		{
			// transmit response
			CAN_TX_MESSAGE response;
			response.ID = IDS_CAN_CreateCanId29(IDS_CAN_MESSAGE_TYPE_RESPONSE, IDS_CAN_GetDeviceAddress(Device), ptr->HostAddress, (uint8)IDS_CAN_REQUEST_SESSION_END);
			response.Length = 3;
			response.Data[0] = (uint8)(SessionIDTable[SessionIndex] >> 8);
			response.Data[1] = (uint8)(SessionIDTable[SessionIndex] >> 0); /*lint !e835 shifting by zero */
			response.Data[2] = (uint8)IDS_CAN_RESPONSE_CONDITIONS_NOT_CORRECT;
			if (!IDS_CAN_Tx(Device, &response))
				return;
		}
		ptr->State = (uint8)SESSION_CLOSED;
		return;

	case SESSION_SEED_SENT:
	case SESSION_OPEN:
	default:
		// session is not closed
		// make sure the device is still online and hasn't timed out
		if (IDS_CAN_IsDeviceOnline(Device))
		{
			if ((uint16)(clock_ms - ptr->Timeout_ms) < IDS_CAN_SESSION_TIMEOUT_MS)
				return;

			// prepare to close session -- first transmit our response
			if (state == SESSION_OPEN)
			{
				// transmit response
				CAN_TX_MESSAGE response;
				response.ID = IDS_CAN_CreateCanId29(IDS_CAN_MESSAGE_TYPE_RESPONSE, IDS_CAN_GetDeviceAddress(Device), ptr->HostAddress, (uint8)IDS_CAN_REQUEST_SESSION_END);
				response.Length = 3;
				response.Data[0] = (uint8)(SessionIDTable[SessionIndex] >> 8);
				response.Data[1] = (uint8)(SessionIDTable[SessionIndex] >> 0); /*lint !e835 shifting by zero */
				response.Data[2] = (uint8)IDS_CAN_RESPONSE_TIMEOUT;
				if (!IDS_CAN_Tx(Device, &response))
					return;
			}
		}

		// close the session if we get here
		ptr->State = (uint8)SESSION_CLOSED;
		if (state == SESSION_OPEN)
			OnSessionClosed(Device, ptr->HostAddress, SessionIndex); // close the session after sucessful transmit

		return;
	}
}

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

#ifdef IDS_CAN_MEMORY_SUPPORT

#error THIS IS ROUGH AND IS NOT INTENDED FOR PRODUCTION

#pragma INLINE
static IDS_CAN_RESPONSE_CODE Message50_ReadMemoryMap(IDS_CAN_DEVICE device, const IDS_CAN_RX_MESSAGE *request, CAN_TX_MESSAGE *response)
{
	DWORD result;

	//response->Data[2] = request->Data[2];
	switch (request->Data[2])
	{
	default: return IDS_CAN_RESPONSE_BAD_REQUEST;

	case 0: // request memory range count
		if (request->Length != 3) return IDS_CAN_RESPONSE_BAD_REQUEST;
		response->Length = 4;
		response->Data[3] = IDS_CAN_GetNumberOfDirectMemoryRanges();
		return IDS_CAN_RESPONSE_SUCCESS;

	case 1: // request memory range info
		if (request->Length != 4) return IDS_CAN_RESPONSE_BAD_REQUEST;
		if (request->Data[3] >= IDS_CAN_GetNumberOfDirectMemoryRanges())
			return IDS_CAN_RESPONSE_VALUE_OUT_OF_RANGE;
		result.word.hi.ui8.hi = IDS_CAN_GetDirectMemoryType(request->Data[3]);
		result.word.hi.ui8.lo = IDS_CAN_GetDirectMemoryCellSize(request->Data[3]);
		result.ui16.lo = IDS_CAN_GetDirectMemoryBlockSize(request->Data[3]);
		break;

	case 2: // request start address
		if (request->Length != 4) return IDS_CAN_RESPONSE_BAD_REQUEST;
		if (request->Data[3] >= IDS_CAN_GetNumberOfDirectMemoryRanges())
			return IDS_CAN_RESPONSE_VALUE_OUT_OF_RANGE;
		result.ui32 = IDS_CAN_GetDirectMemoryStartAddress(request->Data[3]);
		break;

	case 3: // request end address;
		if (request->Length != 4) return IDS_CAN_RESPONSE_BAD_REQUEST;
		if (request->Data[3] >= IDS_CAN_GetNumberOfDirectMemoryRanges())
			return IDS_CAN_RESPONSE_VALUE_OUT_OF_RANGE;
		result.ui32 = IDS_CAN_GetDirectMemoryEndAddress(request->Data[3]);
		break;
	}

	response->Length = 8;
	//response->Data[3] = request->Data[3];
	response->Data[4] = result.word.hi.ui8.hi;
	response->Data[5] = result.word.hi.ui8.lo;
	response->Data[6] = result.word.lo.ui8.hi;
	response->Data[7] = result.word.lo.ui8.lo;
	return IDS_CAN_RESPONSE_SUCCESS;
}

#endif // IDS_CAN_MEMORY_SUPPORT

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

// called when an request message is received on the bus
#pragma MESSAGE DISABLE C12056 /* SP debug info incorrect because of optimization or inline assembler */
static void HandleRequestRx(IDS_CAN_DEVICE device, const IDS_CAN_RX_MESSAGE *message)
{
	IDS_CAN_RESPONSE_CODE result;
	CAN_TX_MESSAGE response;

	ASSERT(message->MessageType == IDS_CAN_MESSAGE_TYPE_REQUEST);
	ASSERT(device < NUM_IDS_CAN_DEVICES);
	if (device >= NUM_IDS_CAN_DEVICES) return;

	// pre-prepare standard response bytes
	response.Length = 0; // if response sets this to > 8 bytes, no response is sent out at all
	response.ID = IDS_CAN_CreateCanId29(IDS_CAN_MESSAGE_TYPE_RESPONSE, IDS_CAN_GetDeviceAddress(device), message->SourceAddress, message->ExtMessageData);
	*((PAYLOAD*)response.Data) = *((PAYLOAD*)message->Data); // to save ROM, pre-copy all request bytes into the response, later routines will skip copying any identical bytes

	// parse the message mode
	switch (message->ExtMessageData)
	{
	default: result = IDS_CAN_RESPONSE_REQUEST_NOT_SUPPORTED; break;
	
	case IDS_CAN_REQUEST_READ_PART_NUMBER: result = Message00_ReadSoftwarePartNumber(message, &response); break;
	case IDS_CAN_REQUEST_MUTE_DEVICE:      result = Message01_RequestMuteDevice(device, message, &response); break;

	case IDS_CAN_REQUEST_PID_READ_LIST:  result = Message10_PIDSupport(device, message, &response); break;
	case IDS_CAN_REQUEST_PID_READ_WRITE: result = Message11_PIDSupport(device, message, &response); break;

#ifdef IDS_CAN_BLOCK_SUPPORT
	case IDS_CAN_REQUEST_BLOCK_READ_LIST:  result = Message20_ReadSupportedBlockIDs(message, &response); break;
	case IDS_CAN_REQUEST_BLOCK_READ_SIZE:  result = Message21_ReadBlockSize(message, &response); break;
	case IDS_CAN_REQUEST_BLOCK_READ_WRITE: result = Message22_ReadWriteBlockID(message, &response); break;
#endif // IDS_CAN_BLOCK_SUPPORT

#ifdef DECLARE_SUPPORTED_IDS_CAN_DTCS
	case IDS_CAN_REQUEST_READ_CONTINUOUS_DTCS:   result = Message30_ReadContinuousDTCs(message, &response); break;
	case IDS_CAN_REQUEST_CONTINUOUS_DTC_COMMAND: result = Message31_ContinuousDTCCommand(device, message, &response); break;
#endif // DECLARE_SUPPORTED_IDS_CAN_DTCS

	case IDS_CAN_REQUEST_SESSION_READ_LIST:    result = Message40_ReadSupportedSessionIDs(message, &response); break;
	case IDS_CAN_REQUEST_SESSION_READ_STATUS:  result = Message41_ReadSessionStatus(device, message, &response); break;
	case IDS_CAN_REQUEST_SESSION_REQUEST_SEED: result = Message42_RequestSessionSeed(device, message, &response); break;
	case IDS_CAN_REQUEST_SESSION_TRANSMIT_KEY: result = Message43_OpenSession(device, message, &response); break;
	case IDS_CAN_REQUEST_SESSION_HEARTBEAT:    result = Message44_SessionHeartbeat(device, message, &response); break;
	case IDS_CAN_REQUEST_SESSION_END:          result = Message45_CloseSession(device, message, &response); break;

#ifdef IDS_CAN_MEMORY_SUPPORT
	case IDS_CAN_REQUEST_MEMORY_READ_MAP:     result = Message50_ReadMemoryMap(message, &response); break;
#endif // IDS_CAN_MEMORY_SUPPORT
	case IDS_CAN_REQUEST_DAQ_NUM_CHANNELS:     result = _IDS_CAN_DAQMessage51(device, message, &response); break;
	case IDS_CAN_REQUEST_DAQ_AUTO_TX_SETTINGS: result = _IDS_CAN_DAQMessage52(device, message, &response); break;
	case IDS_CAN_REQUEST_DAQ_CHANNEL_SETTINGS: result = _IDS_CAN_DAQMessage53(device, message, &response); break;
	case IDS_CAN_REQUEST_DAQ_PID_ADDRESS:      result = _IDS_CAN_DAQMessage54(device, message, &response); break;
	}

	// was there an error?
	if (result != IDS_CAN_RESPONSE_SUCCESS)
	{
		// send standard error response
		response.Length = 1;
		response.Data[0] = (uint8)result;
	}

	if (response.Length > 8)
		return; // no response message necessary

	// transmit response code
	if (!IDS_CAN_Tx(device, &response))
	{
		// out of buffer space, this hopefully never occurs!
	}
}
#pragma MESSAGE DEFAULT C12056

void _IDS_CAN_OnRequestMessageRx(const IDS_CAN_RX_MESSAGE *message)
{
	IDS_CAN_DEVICE device;

	ASSERT(message->MessageType == IDS_CAN_MESSAGE_TYPE_REQUEST);

	// special handling of broadcast requests
	if (message->TargetAddress == IDS_CAN_ADDRESS_BROADCAST)
	{
		for (device = (IDS_CAN_DEVICE)0; device < NUM_IDS_CAN_DEVICES; device++)
		{
			HandleRequestRx(device, message);
		}
	}
	else
	{
		// is this request for one of our devices?
		device = IDS_CAN_GetDeviceFromAddress(message->TargetAddress);
		if (device < NUM_IDS_CAN_DEVICES)
			HandleRequestRx(device, message);
	}
}


////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

// runs from continuous program and manages network messaging
void _IDS_CAN_Request_Task(uint16 clock_ms)
{
	Session_Task(clock_ms);
}

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

#if defined(__GNUC__)
#pragma GCC diagnostic pop
#endif

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#endif // HEADER
