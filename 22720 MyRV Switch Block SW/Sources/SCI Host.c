// SCI_Host.c
// 17741 LCI UDCS Multiplex 6-Switch Touch Panel SW S12G
// SCI communications interface host
// (c) 2001, 2002, 2006, 2007, 2010, 2012 Innovative Design Solutions, Inc.
// All rights reserved

#include "global.h"

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#ifdef HEADER

void SCIHost_Task(void);

extern uint8 Debug;

#else

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

typedef void (*PBOOTLOADERFUNCTION)(void);
#define Bootloader_Activate ((PBOOTLOADERFUNCTION) (BOOTLOADER_ENTRY_ADDRESS))

uint8 Debug = 0;

//////////
//////////
//////////
//////////

#define MAX_SEND_SIZE SCI_TX_MAX_MESSAGE_LENGTH

// determine max read/write size
// messages look like this:
//    F8 size (addr) (data)
//    addr = 1 to 8 bytes
//    data = whatever we can fit!
#if SCI_RX_MAX_MESSAGE_LENGTH < SCI_TX_MAX_MESSAGE_LENGTH
	#define MAX_MEM_SIZE (SCI_RX_MAX_MESSAGE_LENGTH - 10)
#else
	#define MAX_MEM_SIZE (SCI_TX_MAX_MESSAGE_LENGTH - 10)
#endif

// IDS Unified Memory Access Command:
// supercedes all FC and FD messages (previously supported)
//
// format:
// F8 xx [a0 a1 a2 ... an] [d0 d1 d2 ... dn]
// xx = address bytes, data bytes
//      upper 3 bits = size of address (in bytes) - 1
//                    range: 1 - 8 bytes
//      lower 5 bits = amount of data to read or write (in bytes) - 1
//                    range: 1 - 32 bytes
//
// NOTE: data payload is optional
static uint8 UnifiedMemoryAccessCommand(const uint8 * message)
{
	uint8 buffer[MAX_SEND_SIZE];
	uint8 address_size, bytes;
	uint8 write = FALSE; // assume read
	uint8 length;
	uint8 * out = buffer;
	DWORD address = {0};

	// must be at least 3 bytes long
	length = *message++;
	if (length < 3)
		return TRUE; // toss message
	length -= 2; // ignore fixed part of message

	// prepare message packet header
	// [mode:1] [command:1] [address:8] [data]
	*out++ = *message++; // copy mode (F8)
	bytes = *out++ = *message++; // copy command (size)

	// unload address and data sizes
	address_size = (bytes >> 5) + 1;
	bytes = (bytes & 0x1F) + 1;

	// verify size of outgoing message
	if ((uint8)(address_size + bytes) > sizeof(buffer) - 2)
		return TRUE; // toss message

	// verify message size, and determine read/write operation
	if (length == (uint8) (address_size + bytes))
		write = TRUE; // this is a write operation
	else if (length == address_size)
		length += bytes; // this is a read operation
	else
		return TRUE; // toss message

	// unload address (high endian)
	do
	{
		// shift in next address byte
		address.ui32 <<= 8;
		address.word.lo.ui8.lo = *out++ = *message++; // copy address to transmit buffer
	} while (--address_size);

	// access type depends on address size
	if (!address.word.hi.ui16)
	{
		// 16-bit address
		// regular system memory
		if (write)
			AtomicWrite((uint8*)address.word.lo.ui16, message, bytes);
		AtomicRead((uint8*)address.word.lo.ui16, out, bytes);
	}
	else
	{
		// large address
		// extended memory (off board eeprom)
		// NOTE: writes are ignored

//		do
//		{
//			WORD result;
//			result.word = EEPROM_Read((uint8*)address.word.lo.ui16++);
//			if (result.byte.hi)
//				return FALSE;
//			*out++ = result.byte.lo;
//		} while (--bytes);
	}

	// transmit response message
	return SCI_TxBuffer(2 + length, buffer);
}

//////////
//////////
//////////
//////////

// runs from continuous program and ensures that SCI communications
// interface is satisfied
void SCIHost_Task(void)
{
	const uint8 * message;
	uint8 len;

  // handle received messages
	message = SCI_Rx();
	if (!message)
		return;
	len = message[0];
  
	// parse message mode  
	switch(message[1])
	{
	case 0x40: // set MAC address
		switch (len)
		{
		case 7:
			Config.ProductionBlock.MAC[0] = message[2];
			Config.ProductionBlock.MAC[1] = message[3];
			Config.ProductionBlock.MAC[2] = message[4];
			Config.ProductionBlock.MAC[3] = message[5];
			Config.ProductionBlock.MAC[4] = message[6];
			Config.ProductionBlock.MAC[5] = message[7];
			Config_ProductionBlock_Flush();
			//lint -fallthrough
		case 1:
			if (!SCI_Tx(7, message[1],
				Config.ProductionBlock.MAC[0],
				Config.ProductionBlock.MAC[1],
				Config.ProductionBlock.MAC[2],
				Config.ProductionBlock.MAC[3],
				Config.ProductionBlock.MAC[4],
				Config.ProductionBlock.MAC[5]))
				return;
			break;
		}
		break;
  
   case 0x80: // debugger
		switch (len)
		{
		case 2: Debug = message[2]; //-lint fallthrough
		case 1: if (!SCI_Tx(2, message[1], Debug)) return;
		}
		break;

  	case 0xF8: // unified read/write to address
		if (!UnifiedMemoryAccessCommand(message))
			return;
		break;

	case 0xF9: // Manufacturing mode (production testing and module configuration) message
		if (!Manufacturing_MessageReceived(message))
		  return;
		break;

	case 0xFA: // software part number
		if (len != 1)
			break;
		if (!SCI_Tx(8, message[1],
			IDS_PartNumberDigit(0),
			IDS_PartNumberDigit(1),
			IDS_PartNumberDigit(2),
			IDS_PartNumberDigit(3),
			IDS_PartNumberDigit(4),
			IDS_PartNumberDigit(5),
			IDS_PartNumberDigit(6)))
			return;
		break;

	case 0xFB: // force reset
		if (len == 1)
			ForceReset();
		break;

	case 0xFC: // old style read -- use is deprecated
	case 0xFD: // old style write -- use is deprecated
		if (!SCI_Tx(3, message[1], (uint8) 0xF8, (uint8) MAX_MEM_SIZE))
			return;
		break;

	#define GetDigit(buf, pos)    (uint8)(((buf[pos] - '0') << 4) + buf[pos+1] - '0')
	
	case 0xFE: // software version
	{
		const char * date = IDSCore_GetBuildDateString();
		const char * time = IDSCore_GetBuildTimeString();
	
		if (len != 1)
			break;
		if (!SCI_Tx(7, (uint8) 0xFE,
			(uint8) GetDigit(date, 9),
			(uint8) IDSCore_GetSoftwareBuildMonth(),
			(uint8) IDSCore_GetSoftwareBuildDay(),
			(uint8) GetDigit(time, 0),
			(uint8) GetDigit(time, 3),
			(uint8) GetDigit(time, 6)))
			return;
		break;
	}
	case 0xFF: // bootloader message
#ifdef BOOTLOADER_ENTRY_ADDRESS
		if (len == 2 && message[2] == 0x10)
			Bootloader_Activate();
#endif
		if (!SCI_Tx(2, message[1], (uint8) 0x00))
			return;
		break;

	default:
		break;
	}

	SCI_RxFlushMessage();
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#endif
