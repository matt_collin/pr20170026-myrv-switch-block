// SCI COBS.c
// SCI driver for Freescale MC9S12 microcontrollers
// Uses COBS serial protocol
// Version 1.6
// Copyright (c) 2012, 2013, 2017 Innovative Design Solutions, Inc.
// All rights reserved

// history
// 1.0  First revision
// 1.1  Bug Fixes (TIE/TCIE)
// 1.2  Made compatible with latest CRC module
// 1.3  Bug fix in SCI_DRIVER_POLLED mode
// 1.4  Made compatible with multithreaded scheduler
// 1.5  Bug fix to statistics (wrong data returned by SCI_GetNumMessagesSent() and SCI_GetNumMessagesReceived()
// 1.6  Linted

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
//
// USAGE:
//
// - The COBS algorithm strives for consistent overhead and best bandwidth utilization.
//   It is a better algorithm than PPP, which has variable amount overhead (worst-case
//   100% overhead). Unlike PPP, where there is a major penalty for transmitting the
//   framing character, this algorithm performs best when transmitting the framing character.
//   In fact messages can be highly compressed when they mainly consist of framing
//   characters.  Hence it is best practice to choose a framing character based on
//   a character that is most commonly transmitted in the datagram (such as 0, 1 or FF)
//
// - Of special note, the code byte can NEVER match the frame byte.  If you choose
//   a frame byte of 0x00, this is not a problem, as you will never tx/rx
//   a 0x00 code
//
// - To ensure data integrity, an 8-bit CRC is appended to all packets.  The CRC is
//   considered part of the payload, and therefore the COBS algorithm applies to it.
//
// - The examples below assume that the framing character is 0x00.  However,
//   in practice any framing character will do.
//
//   1: transmit framing character (frame start)
//   2: scan packet for chunks, each of which end in frame character
//   3: for each chunk:
//      a: strip off the framing characters at the end of each chunk
//      b: transmit: [code data....]
//         where code = ffdddddd
//         d = number of data bytes following the code (0-63)
//         f = number of framing characters stripped off (0-3)
//   4: transmit framing character (frame end)
//
//   Packets therefore look like this (using 0x00 as frame character):
//      00 code data..... [code data...] 00
//
//   example:
//   	packet:    11 22 33 44
//   	  data:    4
//   	zeroes:    0
//   	result: 00 04 11 22 33 44 00
//
//   example:
//   	packet:    11 00
//   	  data:    1
//   	zeroes:    1
//   	result: 00 41 11 00
//
//   example:
//   	packet:    00 11
//   	  data:    0  1
//   	zeroes:    1  0
//   	result: 00 40 01 11 00
//
//   example:
//   	packet:    00 00 00 00 00
//   	  data:    0        0
//   	zeroes:    3        2
//   	result: 00 C0       80    00
//
//   example:
//   	packet:    11 00 00 22 33 00 00 00 55 66 77 00 88
//   	  data:    1        2              3           1
//   	zeroes:    2        3              1           0
//   	result: 00 81 11 22 C2 22 33       43 55 66 77 01 88 00
//
//
// - RX buffers are allocated by the user and can be allocated at different sizes
//   depending on the need of the program.  However all RX buffers require a
//   unique COBS_RX_BUFFER structure to manage them.  Instead of forcing the user
//   to allocate a unique structure for each RX buffer, we simply assume that
//   the user allocated buffer holds the COBS_RX_BUFFER followed by a variable
//   number data bytes.  This allows the user to allocate and manage different sized
//   buffers without having to understand the internal workings of this module.
//
//   Therefore, for this scheme to work properly, buffers must first be
//   "initialized" by this module, placing the structure members into a default
//   state.   Because this module supports buffers of different size, the buffer
//   initialization routine must write the effective buffer length (number of data bytes)
//   into the structure, to ensure that buffer overruns never occur.  This makes the
//   buffer size variable of particular importance, especially when invalid/corrupted
//   buffers are passed to us.  To solve this problem, this module also places a unique
//   numeric tag signature at the first two bytes of the buffer, to allow the
//   buffer uninitialized buffers can be easily identified and ignored.
//
// - The following functions are supported by default:
//   -- void SCI_Init(void);                              // initialize module
//   -- const uint8 * SCI_Rx(void);                       // return pointer to RX message (if any)
//   -- void SCI_RxFlushMessage(void);                    // remove oldest message from RX buffer
//   -- uint8 SCI_TxIsBufferFull(void);                   // determine if TX buffer has room for message
//   -- uint16 SCI_TxBufferBytesFree(void);               // returns number of free bytes in the buffer
//   -- uint8 SCI_Tx(uint8 len, ...);                     // queue message into TX buffer
//   -- uint8 SCI_TxBuffer(uint8 len, const uint8 * buf); // queue message into TX buffer
//   -- uint8 SCI_TxBreakCharacter(void);                 // transmits a break character
//
// - Customize this module by adding #define statements in "global.h"
//
// - The following values MUST be declared:
//   -- #define SCI_PORT(reg)              module##reg /* where module is the name of the desired */
//                                                     /* hardware peripheral -- identifies which */
//                                                     /* one to use when more than one exists    */
//                                                     /* example: SCI, SCI0, SCI1, etc...        */
//   -- #define SCI_DEFAULT_BAUD_RATE      val  /* desired baud rate at boot */
//   -- #define SCI_TX_BUFFER_SIZE_BYTES   val  /* size of byte buffer to store outgoing bytes */
//   -- #define SCI_RX_BUFFER_SIZE_BYTES   val  /* size of byte buffer to store incoming bytes (only if interrupt operation defined) */
//   -- #define SCI_RX_MAX_MESSAGE_LENGTH  val  /* maximum message length that can be received */
//
// - exactly ONE of the following must be defined
//   -- #define SCI_DRIVER_POLLED     /* driver operates in polled mode */
//   -- #define SCI_DRIVER_INTERRUPT  /* driver operates in interrupt mode */
//
// - the following are optional
//   -- #define SCI_MAINTAIN_STATISTICS  /* if defined, then module will maintain usage statistics */
//
// - POLLED operation defines the following functions
//   -- void SCI_Task(void);   // polling routine (call from fast background loop)
//
// - INTERRUPT operation defines the following functions
//   -- __interrupt void SCI_OnReceiveInterrupt(void);    // RX ISR
//   -- __interrupt void SCI_OnTransmitInterrupt(void);   // TX ISR
//
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// verify compiler and environment
#if (!defined __HIWARE__) || (__MWERKS__ != 1)
	#error This module designed for Metrowerks CodeWarrior for HC12 compiler
#endif
#if !defined __HC12__
	#error This module designed for HC12 processors
#endif

#include "global.h"

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#ifdef HEADER

#define SCI_TX_MAX_MESSAGE_LENGTH   (SCI_TX_BUFFER_SIZE_BYTES - 1)

void SCI_Init(void);
void SCI_Task(void); // background processing of received bytes

#ifdef SCI_DRIVER_INTERRUPT
#pragma CODE_SEG __NEAR_SEG NON_BANKED
INTERRUPT void SCI_OnInterrupt(void);
#pragma CODE_SEG DEFAULT
#endif

// receive functions
const uint8 * SCI_Rx(void);
void SCI_RxFlushMessage(void);

// transmit functions
uint16 SCI_TxBufferBytesInUse(void);
uint16 SCI_TxBufferBytesFree(void);
uint8 SCI_TxIsBufferFull(void);
uint8 SCI_Tx(uint8 len, ...);
uint8 SCI_TxBuffer(uint8 len, const void * buf);
uint8 SCI_TxBreakCharacter(void);

// baud rate select functions
uint32 SCI_GetBaudRate(void);
uint32 SCI_SetBaudRate(uint32 baud_rate);

#ifdef SCI_MAINTAIN_STATISTICS
uint32 SCI_GetNumBytesSent(void);
uint32 SCI_GetNumBytesReceived(void);
uint32 SCI_GetNumMessagesSent(void);
uint32 SCI_GetNumMessagesReceived(void);
uint16 SCI_GetTxBufferOverflowCount(void);
uint16 SCI_GetRxBufferOverflowCount(void);
uint16 SCI_GetMaxTxQueueCount(void);
uint16 SCI_GetMaxRxQueueCount(void);
#endif // SCI_MAINTAIN_STATISTICS

#else

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// processor check
#if   defined MC9S12G48 || defined MC9S12G64 || defined MC9S12G96 || defined MC9S12G128 || defined MC9S12G192 || defined MC9S12G240
	#define S12SCIV5
#elif defined MC9S12GA192 || defined MC9S12GA240
	#define S12SCIV5
#elif defined MC9S12GN16 || defined MC9S12GN32 || defined MC9S12GN48
	#define S12SCIV5
#elif defined MC9S12HY32 || defined MC9S12HY48 || defined MC9S12HY64
	#define S12SCIV5
#elif defined MC9S12P32 || defined MC9S12P64 || defined MC9S12P96 || defined MC9S12P128
	#define S12SCIV5
#else
	#error SCI driver not designed for this processor
#endif

#if !defined S12SCIV5
	#error SCI module not defined
#endif

#if defined SCI_DRIVER_INTERRUPT && defined SCI_DRIVER_POLLED
	#error SCI driver: cannot define both SCI_DRIVER_POLLED and SCI_DRIVER_INTERRUPT
#elif !defined SCI_DRIVER_INTERRUPT && !defined SCI_DRIVER_POLLED
	#error SCI driver: must define SCI_DRIVER_POLLED or SCI_DRIVER_INTERRUPT
#endif

#ifndef SCI_DEFAULT_BAUD_RATE
	#error SCI_DEFAULT_BAUD_RATE must be defined
#endif

// determine size of index into TX buffer
#ifndef SCI_TX_BUFFER_SIZE_BYTES
	#error must define SCI_TX_BUFFER_SIZE_BYTES
#elif SCI_TX_BUFFER_SIZE_BYTES <= 256
	typedef uint8 TXINDEX;
#else
	typedef uint16 TXINDEX;
#endif

// determine size of index into RX buffer
#ifdef SCI_DRIVER_INTERRUPT
	#ifndef SCI_RX_BUFFER_SIZE_BYTES
		#error must define SCI_RX_BUFFER_SIZE_BYTES when SCI_DRIVER_INTERRUPT is enabled
	#elif SCI_RX_BUFFER_SIZE_BYTES <= 256
		typedef uint8 RXINDEX;
	#else
		typedef uint16 RXINDEX;
	#endif // SCI_RX_BUFFER_SIZE_BYTES
#endif // SCI_DRIVER_INTERRUPT

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

// define the framing character
// 0x00 is strongly encouraged, otherwise you run the risk that the code
// byte is mistaken for the frame character
#define COBS_FRAME_CHARACTER    0x00

// set the number of bits dedicated to data length
// the remaining bits count the number of framing characters
#define COBS_DATA_LENGTH_BITS   6

// create bit masks for code byte frame and data count
#define FRAME_BYTE_COUNT_LSB  (uint8)(1 << COBS_DATA_LENGTH_BITS)
#define MAX_DATA_BYTES        (uint8)(FRAME_BYTE_COUNT_LSB - 1)

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

#ifdef SCI_TRANSMIT_LOOPBACK
static uint8 IgnoreLoopback = FALSE;
#endif

#ifdef SCI_MAINTAIN_STATISTICS
static uint32 BytesSent = 0;
static uint32 BytesReceived = 0;
static uint32 MessagesSent = 0;
static uint32 MessagesReceived = 0;
static uint16 TxOverflowCount = 0;
static uint16 RxOverflowCount = 0;
static TXINDEX MaxTxQueueCount = 0;
static RXINDEX MaxRxQueueCount = 0;
#define STATISTICS(exp) (exp)
uint32 SCI_GetNumBytesSent(void) { return AtomicRead32(&BytesSent); }
uint32 SCI_GetNumBytesReceived(void) { return AtomicRead32(&BytesReceived); }
uint32 SCI_GetNumMessagesSent(void) { return AtomicRead32(&MessagesSent); }
uint32 SCI_GetNumMessagesReceived(void) { return AtomicRead32(&MessagesReceived); }
uint16 SCI_GetTxBufferOverflowCount(void) { return AtomicRead16(&TxOverflowCount); }
uint16 SCI_GetRxBufferOverflowCount(void) { return AtomicRead16(&RxOverflowCount); }
#if SCI_TX_BUFFER_SIZE_BYTES <= 256
uint16 SCI_GetMaxTxQueueCount(void) { return MaxTxQueueCount; }
#else
uint16 SCI_GetMaxTxQueueCount(void) { return AtomicRead16(&MaxTxQueueCount); }
#endif
#if SCI_RX_BUFFER_SIZE_BYTES <= 256
uint16 SCI_GetMaxRxQueueCount(void) { return MaxRxQueueCount; }
#else
uint16 SCI_GetMaxRxQueueCount(void) { return AtomicRead16(&MaxRxQueueCount); }
#endif
#else
#define STATISTICS(exp)
#endif // SCI_MAINTAIN_STATISTICS

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

uint32 SCI_GetBaudRate(void)
{
	uint32 denominator = SCI_PORT(BD);
	uint32 numerator;

	denominator <<= 3;
	numerator = (uint32)(FBUS) + denominator; // rounding
	denominator <<= 1;
	return numerator / denominator;
}

uint32 SCI_SetBaudRate(uint32 baudrate)
{
	uint32 numerator;
	uint16 scibd;

	//              FBUS
	//  SCIBD = -------------
	//          16 * baudrate
	baudrate <<= 3;
	numerator = (uint32)(FBUS) + baudrate; // rounding
	baudrate <<= 1;
	scibd = ui32_to_ui16(numerator / baudrate);

	EnterCriticalSection();
	SCI_PORT(SR2_AMAP) = 0;
	SCI_PORT(BD) = scibd;
	LeaveCriticalSection();
  
	return SCI_GetBaudRate();
}

void SCI_Init(void)
{
	// NOTE: some regsiters are only accessible according to the state of the AMAP bit in SCISR2
	// SCICR2  - always accessible
	// SCISR1  - always accessible
	// SCISR2  - always accessible
	// SCIASR1 - only accessible if AMAP = 1
	// SCIACR1 - only accessible if AMAP = 1
	// SCIACR2 - only accessible if AMAP = 1
	// SCICR1  - only accessible if AMAP = 0
	// SCIBD   - only accessible if AMAP = 0

 	// SCICR2
	// 00?00000
	// ||||||||
	// ||||||| \_ SBK   0 = don't send break character
	// |||||| \__ RWU   0 = standby state off
	// ||||| \___ RE    0 = disable receiver (for now)
	// |||| \____ TE    0 = disable transmitter (for now)
	// ||| \_____ ILIE  0 = idle line interrupt disabled
	// || \______ RIE   ? = receive register full interrupt enabled/disabled
	// | \_______ TCIE  0 = transmission complete interrupt disabled
	//  \________ TIE   0 = transmit register empty interrupt disabled
	#ifdef SCI_DRIVER_INTERRUPT
	SCI_PORT(CR2) = 0x20;
	#else
	SCI_PORT(CR2) = 0x00;
	#endif

	// SCISR1
	// no initialization necessary

	// SCISR2
	// 1xx001xx
	// |  |||||
	// |  |||| \_ RAF    read only
	// |  ||| \__ TXDIR  (only used in single-wire mode)
	// |  || \___ BRK13  1 = break character is 13 or 14 bits long
	// |  | \____ RXPOL  0 = normal recieve polarity
	// |   \_____ TXPOL  0 = normal transmit polarity
	//  \________ AMAP   1 = view alternate registers
	SCI_PORT(SR2) = 0x84;

	// AMAP = 1
	// can read/write alternate registers

	SCI_PORT(ASR1) = 0xFF; // clear status bits

	// SCIACR1
	// 0xxxxx00
	// |     ||
	// |     | \_ BKDIE    0 = break detect interrupt enable
	// |      \__ BERRIE   0 = bit error interrupt disabled
	//  \________ RXEDGIE  0 = receive active edge interrupt disabled
	SCI_PORT(ACR1) = 0x00;
  
	// SCIACR2
	// xxxxx000
	//      |||
	//      || \_ BKDFE     0 = break detect circuit disabled
	//       \\__ BERRM0:1  00 = bit error detect circuit is disabled
	SCI_PORT(ACR2) = 0x00;

	// clear AMAP so we can read/write normal registers
	SCI_PORT(SR2_AMAP) = 0;

	// SCICR1
	// 0100110x
	// ||||||||
	// ||||||| \_ PT      Parity bit - not used
	// |||||| \__ PE      0 = parity disabled
	// ||||| \___ ILT     1 = Idle Line Type Bit - 1 start after stop bit
	// |||| \____ WAKE    1 = SCI Wake up SCI - 1 = Address mark wakeup
	// ||| \_____ M       0 = 8 bit characters
	// || \______ RSRC    0 = duplex mode
	// | \_______ SCISWAI 1 = SCI runs in WAIT mode
	//  \________ LOOPS   0 = disable loopback mode
	SCI_PORT(CR1) = 0x4C;

 	// program initial baud rate
	//  baud rate = FBUS / (16 x BD)
	SCI_PORT(BD) = (uint16) (((FBUS / 16.0) / SCI_DEFAULT_BAUD_RATE) + 0.5);

	// enable SCI receiver and transmitter
	SCI_PORT(CR2_RE) = 1;
	SCI_PORT(CR2_TE) = 1;
 
	(void)SCI_TxBreakCharacter();
 }

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

// receiver

// function type

// reconstructed RX message buffer
typedef struct {
	uint8 Length;
	uint8 Data[SCI_RX_MAX_MESSAGE_LENGTH];
} RX_MESSAGE;

// buffer that holds the latest received message, for background
// processing
static RX_MESSAGE RxMessageBuf = { 0 };

// determine location of messages that are being reconstructed
#ifdef SCI_DRIVER_POLLED
	// in polled mode, since we have no raw byte buffer, we need some
	// sort of buffer to construct messages while RxMessageBuf is in use
	static RX_MESSAGE RxWorkBuf = { 0 };
#else
	// in interrupt mode, we don't need a dedicated work buffer
	// as we already have a large buffer to hold incoming bytes
	// we will write to the RxMessageBuf directly (when available)
	// otherwise they will just remain in the buffer
	#define RxWorkBuf RxMessageBuf
#endif // SCI_DRIVER_POLLED

// in interrupt operation, we need a buffer to store real time data streamed in from the UART
#ifdef SCI_DRIVER_INTERRUPT
static RXINDEX RxHead = 0;
static RXINDEX RxTail = 0;
static uint8 RxBuffer[SCI_RX_BUFFER_SIZE_BYTES];
#endif // SCI_DRIVER_INTERRUPT

const uint8 * SCI_Rx(void)
{
	if (!RxMessageBuf.Length)
		return NULL;
	return (const uint8 *) &RxMessageBuf;
}

// create the function to latch the work buffer
#ifdef SCI_DRIVER_POLLED
static void LatchRxWorkBuf(void)
{
	if (RxWorkBuf.Length)
	{
		RxMessageBuf = RxWorkBuf;
		RxWorkBuf.Length = 0;
	}  
}
#else
	// in interrupt mode we dont have a dedicated work buffer
	// so there is nothing to do
	#define LatchRxWorkBuf()
#endif

void SCI_RxFlushMessage(void)
{
	RxMessageBuf.Length = 0;
	LatchRxWorkBuf();
}

// macro that stores a byte in the COBS buffer
// CRC is updated
// care is taken to ensure that the byte stored does not overrun the buffer
#define ByteIn(b) \
	/* update CRC */ \
	CRCisValid = (b == CRC); \
	CRC = CRC8_Update(CRC, (b)); \
 \
	/* store byte in buffer */ \
	if (Length < elementsof(RxWorkBuf.Data)) RxWorkBuf.Data[Length] = (b); \
 \
	/* increase message length */ \
	if (Length < 0xFF) Length++; \


// NOTE: this function should only be called while the work buffer is empty
#pragma INLINE
static void RxDecodeByte(uint8 val)
{
	static uint8 Length = 0;
	static uint8 CodeByte = 0;
	static uint8 CRC;
	static uint8 CRCisValid = FALSE;

	// handle byte
	if (val == COBS_FRAME_CHARACTER)
	{
		// this is a framing character
		// it indicates end of previous frame, and start of next frame

		// subtract 1 byte to toss the CRC
		Length--;

		// if
		// - at least 1 byte (+ checksum) was received
		// - AND message (without checksum) fits within buffer
		// - AND no code byte is being processed
		// - AND message CRC is valid
		if (Length && Length <= elementsof(RxWorkBuf.Data) && CodeByte == 0 && CRCisValid)
		{
			RxWorkBuf.Length = Length; // save length (without checksum);
			LatchRxWorkBuf(); // latch the buffer if we can
			STATISTICS(MessagesReceived++);
		}

		// reset buffer
		Length = 0;
		CodeByte = 0;
		CRCisValid = FALSE;
		CRC = CRC8_Reset();
		return;
	}
	
	// this is not a framing character
	// it is either code or data
	// if we don't have a code byte, then it must be code
	// otherwise it is data to be processed under the current code byte

	// do we have a code byte?
	if (!CodeByte)
	{
		// no code byte
		// this is the new code byte
		CodeByte = val;
	}
	else // if (rx.CodeByte & MAX_DATA_BYTES)
	{
		// we have a code byte
		// this is data
		CodeByte--; // decrement data byte count
		ByteIn(val);  // store the byte
	}

	// once all characters have been received, the code byte
	// will hold zero in the "data length" bits
	// at this point we simply need to stuff the proper number
	// of frame characters and we are done with the code byte

	// if there are no more data bytes to receive
	if (!(CodeByte & MAX_DATA_BYTES))
	{
		// then stuff as many frame bytes as necessary
		while (CodeByte)
		{
			// stuff a frame characters
			ByteIn(COBS_FRAME_CHARACTER); /*lint !e835 constant is zero */
			CodeByte -= FRAME_BYTE_COUNT_LSB; // 1 less frame character
		}
		// at this point the code byte is cleared and the chunk is finished
		// we are ready to start a new chunk, or receive end of message char
	}
}

#pragma INLINE
static void SCI_RxTask(void)
{
#ifdef SCI_DRIVER_INTERRUPT
	RXINDEX tail;
#endif

	// has a byte been received?
	while (SCI_PORT(SR1_RDRF))
	{
		// get the byte
		uint8 val = SCI_PORT(DRL);

		STATISTICS(BytesReceived++);

#ifdef SCI_TRANSMIT_LOOPBACK
		if (IgnoreLoopback)
		{
			IgnoreLoopback = FALSE;
			return;
		}
#endif

#ifdef SCI_DRIVER_INTERRUPT
		// interrupt operation
		// stuff byte into buffer for later processing
		tail = RxTail + 1;
#if SCI_RX_BUFFER_SIZE_BYTES != 256
		if (tail >= elementsof(RxBuffer)) tail = 0;
#endif
		if (tail != RxHead)
		{
			RxBuffer[RxTail] = val;
			RxTail = tail;
#ifdef SCI_MAINTAIN_STATISTICS
			{
				int16 inuse = RxTail - RxHead;
				if (inuse < 0)
					inuse += (int16)(elementsof(RxBuffer));
				if (MaxRxQueueCount < (RXINDEX)inuse) MaxRxQueueCount = (RXINDEX)inuse;
			}
#endif
			return;
		}
#else
		// polled operation
		// perform immediate decoding
		if (!RxWorkBuf.Length)
		{
			RxDecodeByte(val);
			return;
		}
#endif

		// if we get here, then there was no space to put the byte
		STATISTICS(RxOverflowCount++);
	}
}

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

// transmitter

// TX buffer
// real time data streamed out of the UART
static TXINDEX TxHead = 0;
static TXINDEX TxTail = 0;
static uint8 TxBuffer[SCI_TX_BUFFER_SIZE_BYTES];

#pragma INLINE
uint16 SCI_TxBufferBytesInUse(void)
{
	int16 inuse = TxTail - TxHead;
	if (inuse < 0)
		inuse += (int16)(elementsof(TxBuffer));
	return (uint16)inuse;
}

// this function returns the "effective" number of free bytes in the TX buffer
// by effective I mean the number of bytes that a user can stuff via a call to SCI_Tx()
#pragma INLINE
uint16 SCI_TxBufferBytesFree(void)
{
	return ((elementsof(TxBuffer) - 1) - SCI_TxBufferBytesInUse()); // remove 1 byte for LEN character added to buffer
}

#pragma INLINE
uint8 SCI_TxIsBufferFull(void)
{
	return SCI_TxBufferBytesFree() <= 1;
}

static uint8 QueueForTransmit(uint8 len, const uint8 * msg)
{
	uint8 send_crc = (len ? 1: 0);
#ifdef _lint
	uint8 crc = 0; // this is a hack
#else
	uint8 crc; // speed hack, no reason to zero CRC if the variable is not used
#endif
	uint16 inuse;
	TXINDEX tail;

	if (send_crc)
		crc = CRC8_Calculate(msg, len);

	EnterCriticalSection();

	// is there room for this message?
	inuse = SCI_TxBufferBytesInUse() + 1 + len + send_crc; // +1 byte for length
	if (inuse >= elementsof(TxBuffer))
	{
		STATISTICS(TxOverflowCount++);
		LeaveCriticalSection();
		return FALSE;
	}
#ifdef SCI_MAINTAIN_STATISTICS
	if (MaxTxQueueCount < (TXINDEX)inuse) MaxTxQueueCount = (TXINDEX)inuse;
#endif

	// write length
	tail = TxTail;
	TxBuffer[tail++] = len + send_crc;
#if SCI_TX_BUFFER_SIZE_BYTES != 256
	if (tail >= elementsof(TxBuffer)) tail = 0;
#endif

	// copy bytes (if any)
	while (len--)
	{
		TxBuffer[tail++] = *msg++; /*lint !e661 !e662 We are letting tail wrap around naturally if size = 256 */
#if SCI_TX_BUFFER_SIZE_BYTES != 256
		if (tail >= elementsof(TxBuffer)) tail = 0;
#endif
	}

	// append crc
	if (send_crc)
	{
		TxBuffer[tail++] = crc; /*lint !e661 !e662 We are letting tail wrap around naturally if size = 256 */
#if SCI_TX_BUFFER_SIZE_BYTES != 256
		if (tail >= elementsof(TxBuffer)) tail = 0;
#endif
	}

	TxTail = tail;

	STATISTICS(MessagesSent++);

	LeaveCriticalSection();

 // enable transmit buffer empty interrupts
#ifdef SCI_DRIVER_INTERRUPT
	SCI_PORT(CR2_TIE) = 1;
#endif

	return TRUE;
}

uint8 SCI_TxBreakCharacter(void)
{
	// queue a break character
	return QueueForTransmit(0, NULL);
}

// function commits the temporary message in the buffer for transmit
// returns non-zero on success, zero on failure
uint8 SCI_TxBuffer(uint8 len, const void *buf)
{
	if (len)
		return QueueForTransmit(len, (const uint8 *) buf); 
	return FALSE;
}

uint8 SCI_Tx(uint8 len, ...)
{
	// this function uses an open parameter list (...)
	// as such, it expects the bytes to be transmitted to be tightly packed and on the stack
	// compiler option -Cni must be enabled, and specifies no integral promotion on chars
	// without this option chars are promoted to ints before being stuffed on the stack
	// effectively corrupting the message
	// 
	// example:
	//     SCI_Tx(7, (uint8)x11, (uint16)0x2233, (uint8)0x44, (uint16)0x5566, (uint8)0x77);
	// without -Cni (FAILS)
	//     stack ->  00 11 22 33 00 44 55 66 00 77
	//                \\__________\\__________\\______ padding to promote char to int!
	// with -Cni (WORKS)
	//     stack ->  11 22 33 44 55 66 77
	#ifndef __CNI__
		#error Compiler option -Cni (no integral promotion on characters) must be enabled when using SCI_Tx()
	#endif

	if (len)
		return QueueForTransmit(len, &len + sizeof(char)); 
	return FALSE;
}

static uint8 GetTxCodeByte(uint8 bytesleft)
{
	uint8 codebyte = 0;
	TXINDEX head = TxHead;
  
	// read data bytes until frame character is reached
	do
	{
		// get next data byte
		uint8 val = TxBuffer[head];
		if (val == COBS_FRAME_CHARACTER)
			break; // done with data bytes

		// add a databyte
		codebyte++;

		// move to next byte
		if (!--bytesleft)
			return codebyte;
		head++;
#if SCI_TX_BUFFER_SIZE_BYTES != 256
		if (head >= elementsof(TxBuffer)) head = 0;
#endif
	} while (codebyte < MAX_DATA_BYTES); // exit after data bytes full

	// count frame bytes
	do
	{
		// get next frame byte
		uint8 val = TxBuffer[head];
		if (val != COBS_FRAME_CHARACTER)
			break; // done with frame bytes

		// add a framebyte
		codebyte += FRAME_BYTE_COUNT_LSB;

		// move to next byte
		if (!--bytesleft)
			return codebyte;
		head++;
#if SCI_TX_BUFFER_SIZE_BYTES != 256
		if (head >= elementsof(TxBuffer)) head = 0;
#endif
	} while (codebyte < (uint8)(0xFF - MAX_DATA_BYTES)); // exit after frame bytes are full

	return codebyte;
}


#pragma INLINE
static void SCI_TxTask(void)
{
	static uint8 BytesLeft = 0;
	static uint8 CodeByte;
	TXINDEX head;

	if (!SCI_PORT(SR1_TDRE))
		return;

	// what we do depends on how many bytes remain
	switch (BytesLeft)  
	{
	case 0: // inactive
		// find next message to send
		if (TxHead == TxTail)
 		{
#ifdef SCI_DRIVER_INTERRUPT
			// nothing to do, disable transmitter empty interrupt
			SCI_PORT(CR2_TIE) = 0;
#endif
			return;
		}
		
 		// get new length
 		BytesLeft = TxBuffer[TxHead];
 		head = TxHead + 1;
#if SCI_TX_BUFFER_SIZE_BYTES != 256
		if (head >= elementsof(TxBuffer)) head = 0;
#endif
 		TxHead = head;

		// is there a message?
 		if (!BytesLeft)
 		{
			// queue a break character
			SCI_PORT(CR2_SBK) = 1;
			SCI_PORT(CR2_SBK) = 0;
			break;
		}

		// found a message
		SCI_PORT(DRL) = COBS_FRAME_CHARACTER; // transmit start of frame
		BytesLeft += 1; // 1 extra byte for EOF
		CodeByte = 0;
		break;

	case 1: // all data bytes sent -- only need to transmit end of frame
		SCI_PORT(DRL) = COBS_FRAME_CHARACTER; // transmit end of frame
		BytesLeft = 0;
		break;

	default: // we must transmit next active data byte
#if 0
		// we have bytes, right?
 		if (TxHead == TxTail)
 		{
			// buffer is empty, something got fucked up
			BytesLeft = 0;
			SCI_PORT(DRL) = COBS_FRAME_CHARACTER; // transmit end of frame
#ifdef SCI_DRIVER_INTERRUPT
			// nothing to do, disable transmitter empty interrupt
			SCI_PORT(CR2_TIE) = 0;
#endif
			break;
		}
#endif

		// we are either transmitting a code byte, or data bytes
		if (!CodeByte)
		{
			// prepare the next code byte for transmit  
			CodeByte = GetTxCodeByte(BytesLeft - 1); // 0 = tx CRC only
			SCI_PORT(DRL) = CodeByte; // transmit end of frame
		}
		else
		{
			// transmit the next data byte
			SCI_PORT(DRL) = TxBuffer[TxHead];
			head = TxHead + 1;
#if SCI_TX_BUFFER_SIZE_BYTES != 256
			if (head >= elementsof(TxBuffer)) head = 0;
#endif
			TxHead = head;
			BytesLeft--;
			CodeByte--;
		}

		// if there are no more data bytes to transmit
		if (!(CodeByte & MAX_DATA_BYTES))
		{
			// then stuff as many frame bytes as necessary
			CodeByte >>= COBS_DATA_LENGTH_BITS;
			BytesLeft -= CodeByte;
			head = TxHead + CodeByte;
#if SCI_TX_BUFFER_SIZE_BYTES != 256
			if (head >= elementsof(TxBuffer)) head -= elementsof(TxBuffer);
#endif
			TxHead = head;
			CodeByte = 0;
			// at this point the code byte is cleared and the chunk is finished
			// we are ready to start a new chunk, or receive end of message char
		}
      
		break;
	}

	// we get here if we transmitted something
	STATISTICS(BytesSent++);

	#ifdef SCI_TRANSMIT_LOOPBACK
	// ignore next byte received
	IgnoreLoopback = TRUE;
	#endif
}

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

#ifdef SCI_DRIVER_INTERRUPT

// interrupt service routine
#pragma CODE_SEG __NEAR_SEG NON_BANKED
INTERRUPT void SCI_OnInterrupt(void)
{
#ifdef DEBUG_SCI_INTERRUPT_TASK_PORT
	DEBUG_SCI_INTERRUPT_TASK_PORT = 1;
#endif
	SCI_RxTask(); // get next byte and stuff in queue
	SCI_TxTask(); // transmit next byte from queue
#ifdef DEBUG_SCI_INTERRUPT_TASK_PORT
	DEBUG_SCI_INTERRUPT_TASK_PORT = 0;
#endif
}
#pragma CODE_SEG DEFAULT

// background task
void SCI_Task(void)
{
#ifdef DEBUG_SCI_BACKGROUND_TASK_PORT
	DEBUG_SCI_BACKGROUND_TASK_PORT = 1;
#endif

	LatchRxWorkBuf();

	// get buffered bytes from real time program and process
	while (RxHead != RxTail && !RxWorkBuf.Length)
	{
		uint8 val = RxBuffer[RxHead];
#if SCI_RX_BUFFER_SIZE_BYTES != 256
		RXINDEX head = RxHead + 1;
		if (head >= elementsof(RxBuffer)) head = 0;
		RxHead = head;
#else
		RxHead++;
#endif
		RxDecodeByte(val);
	}

#ifdef DEBUG_SCI_BACKGROUND_TASK_PORT
	DEBUG_SCI_BACKGROUND_TASK_PORT = 0;
#endif
}

#else // SCI_DRIVER_INTERRUPT

// background task
void SCI_Task(void)
{
#ifdef DEBUG_SCI_BACKGROUND_TASK_PORT
	DEBUG_SCI_BACKGROUND_TASK_PORT = 1;
#endif
	LatchRxWorkBuf();
	SCI_RxTask(); // get next byte and and process
	SCI_TxTask(); // transmit next byte from queue
#ifdef DEBUG_SCI_BACKGROUND_TASK_PORT
	DEBUG_SCI_BACKGROUND_TASK_PORT = 0;
#endif
}

#endif // SCI_DRIVER_INTERRUPT


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#endif
