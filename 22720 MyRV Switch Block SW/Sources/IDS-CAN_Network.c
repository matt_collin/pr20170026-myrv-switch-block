// IDS CAN Network.c
// IDS CAN network management
// Version 2.8.1
// (c) 2012, 2013, 2014, 2015, 2016, 2017 Innovative Design Solutions, Inc.
// All rights reserved

// History
// Version 1.0   First stable version
// Version 1.1   LINTed
// Version 1.2   Updated to work without bus master
// Version 1.2a  added check for deprecated IDS_CAN_NETWORK_ID
// Version 1.3   overhauled to work with IDS-CAN changes March 2013
//               added random startup delay before NETWORK.ADDRESS_CLAIM is sent
//               modified NETWORK.HEARTBEAT message to work with circuit ID scheme
//               added NETWORK.DEVICE_ID message
// Version 1.3a  corrected PIDs to March 2013 level
// Version 1.3b  moved broadcast address to IDS_CAN.c module
//               modified code to use IDS_CAN_ADDRESS type
// Version 1.3c  cleanup
// Version 1.4   June 2013 IDS-CAN v1.1 update
// Version 1.5   Changed bus address listen time at boot, making it much less
//               likely that a late booting device will steal addresses already
//               in use
// Version 1.6   Module now detects CAN bus errors and resets all devices accordingly
// Version 1.7   Supports IDS CAN v1.2 May 2014
// Version 1.8   Supports IDS CAN v1.3 March 2015
// Version 1.9   Supports IDS CAN v1.4 June 2015
// Version 1.9a  Supports IDS CAN v1.5 July 2015
//               IDS_CAN_Network_Task10ms() now deprecated, app should instead IDS_CAN_Task10ms()
//               IDS_CAN_NETWORK_FILTERS now deprecated
// Version 1.9b  Supports IDS CAN v1.6 January 2016
// Version 1.9c  Devices can now be selectively enabled or disabled via calls to
//               IDS_CAN_EnableDevice() and IDS_CAN_DisableDevice()
//               Linted module
// Version 2.0   Supports IDS CAN v2.0 Feb 2016
// Version 2.1   Supports IDS-CAN v2.1 Mar 2016
// Version 2.1a  Renamed IDS_CAN_Network_Init() to _IDS_CAN_Network_Init(), to work with IDS_CAN_Init();
// Version 2.2   Supports IDS-CAN v2.2 Apr 2016
// Version 2.3   Supports IDS-CAN v2.3 May 2016
// Version 2.4   IDS-CAN v2.4 Jun 2016
//               Added support for "remote network" devices -- will be used in Cellular systems
//               Added support for DTCs
//               Removed support for fixed addresses - this was deprecated and is now obsolete
//               Device capabilities are now specified in the DECLARE_IDS_CAN_DEVICES macro
// Version 2.4a  Added _IDS_CAN_Network_Task1ms(), pulling the background retransmitting
//               into a faster loop.  Problem is that the loop is round robin, and when you have
//               a lot of devices, it takes way to long to wrap around, which basically means
//               that outgoing messages could be significantly delayed till next wrap around.
// Version 2.4a.1 Bugfix for Kinetis
// Version 2.5   IDS-CAN v2.5 Feb 2017
// Version 2.6   IDS-CAN v2.6 Mar 2017
// Version 2.7   IDS-CAN v2.7 Apr 2017
// Version 2.8   IDS-CAN v2.8 Jun 2017
// Version 2.8.1 Linted

#include "global.h"

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#ifdef HEADER

#define SUPPORTED_IDS_CAN_VERSION_NUMBER   IDS_CAN_VERSION_NUMBER_2_8

// Device ID support
typedef struct {
	IDS_CAN_PRODUCT_ID ProductID;
	uint8 ProductInstance;
	IDS_CAN_DEVICE_TYPE DeviceType;
	uint8 DeviceInstance;
	IDS_CAN_FUNCTION_NAME FunctionName;
	uint8 FunctionInstance;
	uint8 DeviceCapabilities; // IDS-CAN versions 1.5 and below always report $00
} IDS_CAN_DEVICE_ID;

// management routines
void _IDS_CAN_Network_Init(void); // private function do not call, instead call IDS_CAN_Init()
void _IDS_CAN_Network_Task1ms(void); // private function do not call, instead call IDS_CAN_Task()
void _IDS_CAN_Network_Task10ms(void); // private function do not call, instead call IDS_CAN_Task()
void _IDS_CAN_Network_Task1sec(void); // private function do not call, instead call IDS_CAN_Task()

// callback routines
void _IDS_CAN_Network_OnMessageRx(const IDS_CAN_RX_MESSAGE *message);

// network status API
uint8 IDS_CAN_NumDevicesOnNetwork(void);

// device API

// enable or disable devices
// disabled devices do not participate in the CAN bus
void IDS_CAN_EnableDevice(IDS_CAN_DEVICE device); // NOTE: device is disabled at boot by default, must be enabled by app
void IDS_CAN_DisableDevice(IDS_CAN_DEVICE device);
uint8 IDS_CAN_IsDeviceEnabled(IDS_CAN_DEVICE device);

// get device address, if device is online
IDS_CAN_ADDRESS IDS_CAN_GetDeviceAddress(IDS_CAN_DEVICE device);
#define IDS_CAN_IsDeviceOnline(device)   (IDS_CAN_GetDeviceAddress(device) != IDS_CAN_ADDRESS_BROADCAST)

// return the device, but only if it is valid and in use
IDS_CAN_DEVICE IDS_CAN_GetDeviceFromAddress(IDS_CAN_ADDRESS address);

// returns any device that is presently online
IDS_CAN_DEVICE IDS_CAN_GetAnyOnlineDevice(void);

// determines the status of addresses on the bus
uint8 IDS_CAN_IsAddressInUse(IDS_CAN_ADDRESS address); // non-zero if address is detected, in use, on the bus

// gets device information
IDS_CAN_DEVICE_ID IDS_CAN_GetDeviceID(IDS_CAN_DEVICE device);     // get the full device ID of the device
IDS_CAN_DEVICE_TYPE IDS_CAN_GetDeviceType(IDS_CAN_DEVICE device); // gets the DEVICE_TYPE of the device
uint8 IDS_CAN_GetDeviceInstance(IDS_CAN_DEVICE device);           // gets the device instance of the device
uint8 IDS_CAN_GetDeviceCapabilities(IDS_CAN_DEVICE device);       // gets the device capabilities of the device
IDS_CAN_ADDRESS IDS_CAN_GetProductInstance(void);                 // gets the current product instance

#else // HEADER

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#if defined(__GNUC__)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunknown-pragmas"
#endif

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

#define SECONDS(s)  (uint8)(100.0 * (s))

#define IDS_CAN_ADDRESS_CLAIM_TIMEOUT        SECONDS(1.0)
#define IDS_CAN_HEARTBEAT_PERIOD             SECONDS(1.0)
#define IDS_CAN_CIRCUIT_ID_MESSAGE_PERIOD    SECONDS(1.0)
#define IDS_CAN_DEVICE_ID_MESSAGE_PERIOD     SECONDS(1.0)

// defines bit locations in status message
#define STATUS_ACTIVE_DTCS     0x01
#define STATUS_STORED_DTCS     0x02
#define STATUS_OPEN_SESSIONS   0x04

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

// private API for internal IDS-CAN use only
extern void _IDS_CAN_MuteDevice(IDS_CAN_DEVICE device, uint8 seconds);
extern void _IDS_CAN_UnmuteDevice(IDS_CAN_DEVICE device);
extern uint8 _IDS_CAN_IsDeviceMuted(IDS_CAN_DEVICE device);

// table for fast index-to-bit lookup
static const uint8 BIT[8] = { BIT0, BIT1, BIT2, BIT3, BIT4, BIT5, BIT6, BIT7 };

static struct {
	uint8 MuteSeconds[NUM_IDS_CAN_DEVICES];                    // number of seconds to mute the device
	IDS_CAN_ADDRESS AddressBeingClaimed[NUM_IDS_CAN_DEVICES];  // non-zero if we are trying to claim an address
	IDS_CAN_ADDRESS Address[NUM_IDS_CAN_DEVICES];              // non-zero if we successfully claimed an address
	uint8 RetransmitAddressClaim[NUM_IDS_CAN_DEVICES];         // non-zero if address claim message must be re-sent
	uint8 NetworkTimer[NUM_IDS_CAN_DEVICES];                   // when zero it indicates it's time to send the next NETWORK message, also used during address claim
	uint8 CircuitIDTimer[NUM_IDS_CAN_DEVICES];                 // when zero it indicates it's time to send the next CIRCUIT_ID message
	uint8 DeviceIDTimer[NUM_IDS_CAN_DEVICES];                  // when zero it indicates it's time to send the next DEVICE_ID message
} Device = { {0}, {0}, {0}, {0}, {0}, {0}, {0} };

// these flags indicate addresses that we run across that are in use
// saves us time by not claiming addresses we know are already in use

static uint8 AddressInUse[256/4] = {0};  // 2 bits per address, used as a countdown timer
static uint8 NumDevicesDetected = 0;     // should always equal the number of flags set in AddressInUse

// counts the number of devices on the network
// should always equal the number of flags set in AddressInUse[]
uint8 IDS_CAN_NumDevicesOnNetwork(void) { return NumDevicesDetected; }

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

// device information

// these functions must be implemented by the user externally
/*lint -save -e762 These declerations end up being redundant, but they let users easily know what functions they need to implement */
extern uint32 IDS_CAN_GetCircuitID(IDS_CAN_DEVICE device);
extern IDS_CAN_FUNCTION_NAME IDS_CAN_GetFunctionName(IDS_CAN_DEVICE device);
extern uint8 IDS_CAN_GetFunctionInstance(IDS_CAN_DEVICE device);
/*lint -restore */

#pragma INLINE
IDS_CAN_DEVICE_TYPE IDS_CAN_GetDeviceType(IDS_CAN_DEVICE device)
{
	// ROM table of device types
	static const IDS_CAN_DEVICE_TYPE DeviceType[NUM_IDS_CAN_DEVICES] = {
		#undef DECLARE_IDS_CAN_DEVICE
		#define DECLARE_IDS_CAN_DEVICE(device_name, device_info)           (device_info),
		#undef DEVICE
		#define DEVICE(device_type, device_instance, device_capabilities)  (device_type)
		DECLARE_IDS_CAN_DEVICES
	};

	ASSERT(device < NUM_IDS_CAN_DEVICES);

	return (device < NUM_IDS_CAN_DEVICES) ? DeviceType[device] : IDS_CAN_DEVICE_TYPE_UNKNOWN;
}

#pragma INLINE
uint8 IDS_CAN_GetDeviceInstance(IDS_CAN_DEVICE device)
{
	// ROM table of device instances
	static const uint8 DeviceInstance[NUM_IDS_CAN_DEVICES] = {
		#undef DECLARE_IDS_CAN_DEVICE
		#define DECLARE_IDS_CAN_DEVICE(device_name, device_info)            (device_info),
		#undef DEVICE
		#define DEVICE(device_type, device_instance, device_capabilities)   (device_instance)
		DECLARE_IDS_CAN_DEVICES
	};

	ASSERT(device < NUM_IDS_CAN_DEVICES);

	return (device < NUM_IDS_CAN_DEVICES) ? DeviceInstance[device] : 0;
}

#pragma INLINE
uint8 IDS_CAN_GetDeviceCapabilities(IDS_CAN_DEVICE device)
{
	ASSERT(device < NUM_IDS_CAN_DEVICES);

	/*lint -save -e788 Not using NUM_IDS_CAN_DEVICES in this switch; it isn't an actual device */
	switch (device)
	{
	default: return 0;

	#undef DECLARE_IDS_CAN_DEVICE
	#define DECLARE_IDS_CAN_DEVICE(device_name, device_info)            case device_name: return device_info;
	#undef DEVICE
	#define DEVICE(device_type, device_instance, device_capabilities)   (device_capabilities)
	DECLARE_IDS_CAN_DEVICES
	}
	/*lint -restore */
}

IDS_CAN_DEVICE IDS_CAN_GetAnyOnlineDevice(void)
{
	// remember the device that we are using as the "first" product instance
	static IDS_CAN_DEVICE LastDevice = (IDS_CAN_DEVICE)0xFF;

	// is the device we have chosen still valid?
	if (LastDevice < NUM_IDS_CAN_DEVICES)
	{
		if (Device.Address[LastDevice])
			return LastDevice;
	}

#if TRUE
	// unroll loop for speed
	#undef DECLARE_IDS_CAN_DEVICE
	#define DECLARE_IDS_CAN_DEVICE(device_name, device_info)            if (Device.Address[device_name]) { LastDevice = device_name; return device_name; }
	DECLARE_IDS_CAN_DEVICES
#else
	// find a device that is valid
	for (LastDevice = (IDS_CAN_DEVICE)0; LastDevice<NUM_IDS_CAN_DEVICES; LastDevice++)
	{
		if (IDS_CAN_IsDeviceOnline(LastDevice))
			return LastDevice;
	}
#endif

	// only get here if no devices are online
	return (IDS_CAN_DEVICE)0xFF;
}

#pragma INLINE
IDS_CAN_ADDRESS IDS_CAN_GetProductInstance(void)
{
	// get the first online device
	IDS_CAN_DEVICE device = IDS_CAN_GetAnyOnlineDevice();
	return IDS_CAN_GetDeviceAddress(device);
}

IDS_CAN_DEVICE_ID IDS_CAN_GetDeviceID(IDS_CAN_DEVICE device)
{
	IDS_CAN_DEVICE_ID result;
	ASSERT(device < NUM_IDS_CAN_DEVICES);
	result.ProductID = IDS_CAN_GetProductID();
	result.ProductInstance = IDS_CAN_GetProductInstance();
	result.DeviceType = IDS_CAN_GetDeviceType(device);
	result.DeviceInstance = IDS_CAN_GetDeviceInstance(device);
	result.FunctionName = IDS_CAN_GetFunctionName(device);
	result.FunctionInstance = IDS_CAN_GetFunctionInstance(device);
	result.DeviceCapabilities = IDS_CAN_GetDeviceCapabilities(device);
	return result;
}

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

// address monitoring
static const uint8 InUseFlag[4] = { BIT0 | BIT1, BIT2 | BIT3, BIT4 | BIT5, BIT6 | BIT7 };

// non-zero if address is detected, in use, on the bus
uint8 IDS_CAN_IsAddressInUse(IDS_CAN_ADDRESS address)
{
	if (address == IDS_CAN_ADDRESS_BROADCAST)
		return FALSE;
	return AddressInUse[address >> 2] & InUseFlag[address & 3];
}

static void RememberAddressInUse(IDS_CAN_ADDRESS address)
{
	if (address != IDS_CAN_ADDRESS_BROADCAST)
	{
		if (!IDS_CAN_IsAddressInUse(address))
			NumDevicesDetected++;

		AddressInUse[address >> 2] |= InUseFlag[address & 3];
	}
}

// this will loop through all the addresses after 256 calls
// at 10 ms this equals 2.56 seconds
#pragma INLINE
static void ManageAddressInUseTimeout(void)
{
	static IDS_CAN_ADDRESS address = (IDS_CAN_ADDRESS)0;

	// cycle through addresses
	address++;

	// if the address is in use
	if (IDS_CAN_IsAddressInUse(address))
	{
		// decrement the 2 bit counter
		// when the counter reaches zero the device is no longer on the bus
		// this takes between 2 (min) and 3(max) total loop times, based on when device was last detected
		// at 2.56 seconds per loop this means we are between 5.12 and 7.68 seconds address timeout
		static const uint8 Sub[4] = { BIT0, BIT2, BIT4, BIT6 };
		AddressInUse[address >> 2] -= Sub[address & 3];

		// decrement total number of devices when the counter reaches zero
		if (!IDS_CAN_IsAddressInUse(address))
			NumDevicesDetected--;
	}
}

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

#pragma INLINE
IDS_CAN_ADDRESS IDS_CAN_GetDeviceAddress(IDS_CAN_DEVICE device)
{
	ASSERT(device < NUM_IDS_CAN_DEVICES);
	if (device < NUM_IDS_CAN_DEVICES)
		return Device.Address[device];
	return IDS_CAN_ADDRESS_BROADCAST; // illegal
}

// find any local device that is using (or claiming) the given address
static IDS_CAN_DEVICE FindDevice(IDS_CAN_ADDRESS address)
{
	ASSERT(address != IDS_CAN_ADDRESS_BROADCAST);

	// if address is legal
	if (address != IDS_CAN_ADDRESS_BROADCAST)
	{
#if TRUE
		// manual loop unrolling for speed
		#undef DECLARE_IDS_CAN_DEVICE
		#define DECLARE_IDS_CAN_DEVICE(name, device) if (Device.AddressBeingClaimed[name] == address) return name;
		DECLARE_IDS_CAN_DEVICES
#else
		IDS_CAN_DEVICE device;
		for (device = (IDS_CAN_DEVICE)0; device<NUM_IDS_CAN_DEVICES; device++)
		{
			if (Device.AddressBeingClaimed[device] == address)
				return device; // found it
		}
#endif
	}

	return (IDS_CAN_DEVICE)0xFF; // no device found that matches the address
}

// return the device, but only if it is valid and in use
IDS_CAN_DEVICE IDS_CAN_GetDeviceFromAddress(IDS_CAN_ADDRESS address)
{
	IDS_CAN_DEVICE device = FindDevice(address); // could return addresses being claimed

	// filter out addresses being claimed
	if (device < NUM_IDS_CAN_DEVICES && Device.Address[device] == IDS_CAN_ADDRESS_BROADCAST)
		return (IDS_CAN_DEVICE)0xFF;

	return device;
}

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

static void ResetDevice(IDS_CAN_DEVICE device)
{
	ASSERT(device < NUM_IDS_CAN_DEVICES);
	if (device < NUM_IDS_CAN_DEVICES)
	{
		Device.Address[device] = IDS_CAN_ADDRESS_BROADCAST;
		Device.AddressBeingClaimed[device] = IDS_CAN_ADDRESS_BROADCAST;
		Device.RetransmitAddressClaim[device] = 0;
		Device.NetworkTimer[device] = 0; // immediate address claim
		Device.CircuitIDTimer[device] = 0;
		Device.DeviceIDTimer[device] = 0;
		IDS_CAN_SetDeviceStatus(device, 0, NULL);
	}
}

void _IDS_CAN_Network_Init(void)
{
	IDS_CAN_DEVICE device;

	NumDevicesDetected = 0;
	(void)memset(AddressInUse, 0, sizeof(AddressInUse));

	for (device = (IDS_CAN_DEVICE)0; device < NUM_IDS_CAN_DEVICES; device++)
	{
		ResetDevice((IDS_CAN_DEVICE)device);

		// before first address claim is performed at boot
		// listen to the bus for at least 1.25 seconds to get a picture of the addresses that are in use
		// then add a random delay on top of that to ensure multiple devices don't come online simultaneously
		Device.NetworkTimer[device] = SECONDS(1.25) + (Random16() % SECONDS(1.25));

#ifdef DECLARE_CONFIGURATION
		// activate mute mode if production byte is set
		Device.MuteSeconds[device] = (Config.ProductionBlock.ProductionByte ? 1 : 0);
#else
		Device.MuteSeconds[device] = 0; // unmute
#endif // DECLARE_CONFIGURATION
	}
}

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

// device enabling / disabling
// NOTE: devices are disabled by default -- devices must be enabled by application during boot

// flags for enabling/disabling specific devices
static uint8 DeviceEnabledFlag[((uint8)NUM_IDS_CAN_DEVICES + 7) / 8] = { 0 }; // 1 bit per device

#define IsDeviceEnabled(d)  (DeviceEnabledFlag[(uint8)(d) >> 3] & BIT[(uint8)(d) & 7])

uint8 IDS_CAN_IsDeviceEnabled(IDS_CAN_DEVICE device)
{
	ASSERT(device < NUM_IDS_CAN_DEVICES);
	return IsDeviceEnabled(device);
}

void IDS_CAN_EnableDevice(IDS_CAN_DEVICE device)
{
	ASSERT(device < NUM_IDS_CAN_DEVICES);
	if (device < NUM_IDS_CAN_DEVICES && !IsDeviceEnabled(device))
	{
		DeviceEnabledFlag[(uint8)device >> 3] |= BIT[(uint8)device & 7];
		ResetDevice(device);
	}
}

void IDS_CAN_DisableDevice(IDS_CAN_DEVICE device)
{
	ASSERT(device < NUM_IDS_CAN_DEVICES);
	if (device < NUM_IDS_CAN_DEVICES && IsDeviceEnabled(device))
	{
		DeviceEnabledFlag[(uint8)device >> 3] &= ~BIT[(uint8)device & 7];
		ResetDevice(device);
	}
}

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

// mute
// NOTE: devices are unmuted by default (unless production byte is set)

uint8 _IDS_CAN_IsDeviceMuted(IDS_CAN_DEVICE device) { ASSERT(device < NUM_IDS_CAN_DEVICES); return Device.MuteSeconds[device]; } /*lint !e661 out-of-bounds is ok, you just get garbage */

void _IDS_CAN_MuteDevice(IDS_CAN_DEVICE device, uint8 seconds)
{
	ASSERT(device < NUM_IDS_CAN_DEVICES);
	if (device < NUM_IDS_CAN_DEVICES && seconds > 0)
	{
		if (!Device.MuteSeconds[device])
			ResetDevice(device);
		Device.MuteSeconds[device] = seconds;
	}
}

void _IDS_CAN_UnmuteDevice(IDS_CAN_DEVICE device)
{
	ASSERT(device < NUM_IDS_CAN_DEVICES);
	if (device < NUM_IDS_CAN_DEVICES && Device.MuteSeconds[device])
	{
		Device.MuteSeconds[device] = 0;
		ResetDevice(device);
	}
}

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

// address claiming

#pragma MESSAGE DISABLE C5703 /* may not use mac */
#pragma INLINE
static int8 CompareMAC(const uint8 * mac)
{
	const uint8 * adapter = IDS_CAN_GetAdapterMAC();

#if TRUE
	// loop unrolled for speed
	if (adapter[0] > mac[0]) return +1; // we are higher
	if (adapter[0] < mac[0]) return -1; // we are lower
	if (adapter[1] > mac[1]) return +1; // we are higher
	if (adapter[1] < mac[1]) return -1; // we are lower
	if (adapter[2] > mac[2]) return +1; // we are higher
	if (adapter[2] < mac[2]) return -1; // we are lower
	if (adapter[3] > mac[3]) return +1; // we are higher
	if (adapter[3] < mac[3]) return -1; // we are lower
	if (adapter[4] > mac[4]) return +1; // we are higher
	if (adapter[4] < mac[4]) return -1; // we are lower
	if (adapter[5] > mac[5]) return +1; // we are higher
	if (adapter[5] < mac[5]) return -1; // we are lower
#else
	uint8 n;
	for (n=0; n<6; n++)
	{
		if (adapter[n] > mac[n]) return +1; // we are higher
		if (adapter[n] < mac[n]) return -1; // we are lower
	}
#endif

	return 0; // same!
}
#pragma MESSAGE DEFAULT C5703

// handle network messages
void _IDS_CAN_Network_OnMessageRx(const IDS_CAN_RX_MESSAGE *message)
{
	IDS_CAN_ADDRESS address;
	IDS_CAN_DEVICE device;
	int8 result;

	ASSERT(message->MessageType == IDS_CAN_MESSAGE_TYPE_NETWORK);

	if (message->IsEcho) return;

	// pump the RNG
	Randomize((uint32)message->Timestamp_ms);

	// is this BROADCAST or not?
	if (message->SourceAddress == IDS_CAN_ADDRESS_BROADCAST)
	{
		// broadcast -- check if this is the address claim message
		if (message->Length != 8) return;

		// get the address being claimed
		address = message->Data[0];
		if (address == IDS_CAN_ADDRESS_BROADCAST) return; // somebody done goofed

		// add this to our list of in-use addresses
		RememberAddressInUse(address);
	}
	else
	{
		// add the source address to our list of in-use addresses
		address = message->SourceAddress;
		RememberAddressInUse(address);

		// check if this is the address claim message
		if (message->Length != 8) return;
		if (message->TargetAddress != IDS_CAN_ADDRESS_BROADCAST) return;
	}

	// if we get here, then this is an address claim message

	// is the address in the message being used by one of our devices?
	device = FindDevice(address); // this returns local active addresses, and devices trying to claim addresses
	if (device >= NUM_IDS_CAN_DEVICES)
		return; // nope -- this message does not concern us, ignore the claim

	// either we own the address, or are in the process of claiming the address

	// compare the MAC and handle contention
	result = CompareMAC(&message->Data[2]);
	if (result > 0)
	{
		// they are smaller, we lose contention
		ResetDevice(device); // reset the device and re-start address re-claim
	}
	else if (result < 0)
	{
		// they are bigger, we win contention
		// we need to let the other device know we've won contention
		Device.RetransmitAddressClaim[device] = 3;
		Device.NetworkTimer[device] = 0; // re-transmit immediately
	}
	else
	{
		// should never happen, or indicates loopback
		// for added safety, compare protocol version to our own
		if (message->Data[1] != SUPPORTED_IDS_CAN_VERSION_NUMBER)
		{
			// this isn't from us
			// this means that someone else on the bus has a duplicate MAC address
			// should never happen, still lets try and recover
			ResetDevice(device);
		}
	}
}

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

#pragma INLINE
static IDS_CAN_ADDRESS ChooseDeviceAddressToClaim(void)
{
	IDS_CAN_ADDRESS address;
	IDS_CAN_DEVICE n;
	uint8 attempts;

	// get an address at random
	// start at a random address and scan forwards
	address = Random8();
	for (attempts = 0; attempts < 40; address++, attempts++)
	{
		// chose an address at random
		// make sure the address is valid
		if (address == IDS_CAN_ADDRESS_BROADCAST) continue;

		// make sure the address we've chosen isn't active in the scan list
		if (attempts < 20 && IDS_CAN_IsAddressInUse(address))
			continue;

		// make sure the address isn't being used by other devices
		for (n = (IDS_CAN_DEVICE)0;;)
		{
			// is this address already used?
			if (address == Device.Address[n]) break;
			if (address == Device.AddressBeingClaimed[n]) break;

			if (++n >= NUM_IDS_CAN_DEVICES)
				return address; // got it!
		}

		// loop and continue
	}

	return IDS_CAN_ADDRESS_BROADCAST; // give up (for now)
}

static uint8 CreateNetworkMessage(IDS_CAN_DEVICE device, IDS_CAN_ADDRESS source_address, CAN_TX_MESSAGE *msg)
{
	ASSERT(device < NUM_IDS_CAN_DEVICES);

	if (device < NUM_IDS_CAN_DEVICES)
	{
		// get the MAC address of this device
		const uint8 * mac = IDS_CAN_GetAdapterMAC();

		msg-> Length = 8;
		msg->ID = IDS_CAN_CreateCanId11(IDS_CAN_MESSAGE_TYPE_NETWORK, source_address);
		if (source_address == IDS_CAN_ADDRESS_BROADCAST)
		{
			msg->Data[0] = Device.AddressBeingClaimed[device];
		}
		else
		{
			// status xxxxxxxx
			//        ||||||||
			//        ||||||| \___ ACTIVE_DTCS    0 = no active DTCs   /  1 = active DTCs
			//        |||||| \____ STORED_DTCS    0 = no stored DTCs   /  1 = stored DTCs
			//        ||||| \_____ OPEN_SESSIONS  0 = no sessions open /  1 = one or more sessions are open
			//        | \\\\______ undefined
			//         \__________ REMOTE_DEVICE  0 = local device  /  1 = remote device (set by cellular gateway)
			msg->Data[0] = 0;
#ifdef DECLARE_SUPPORTED_IDS_CAN_DTCS 
			if (IDS_CAN_AreAnyDTCsActive()) msg->Data[0] |= STATUS_ACTIVE_DTCS;
			if (IDS_CAN_AreAnyDTCsStored()) msg->Data[0] |= STATUS_STORED_DTCS;
#endif // DECLARE_SUPPORTED_IDS_CAN_DTCS

			if (IDS_CAN_IsAnySessionOpen(device)) msg->Data[0] |= STATUS_OPEN_SESSIONS;
		}
		msg->Data[1] = SUPPORTED_IDS_CAN_VERSION_NUMBER;
		msg->Data[2] = mac[0];
		msg->Data[3] = mac[1];
		msg->Data[4] = mac[2];
		msg->Data[5] = mac[3];
		msg->Data[6] = mac[4];
		msg->Data[7] = mac[5];

		return TRUE;
	}

	return FALSE;
}

// transmits the address claim message
static uint8 TransmitAddressClaim(IDS_CAN_DEVICE device)
{
	ASSERT(device < NUM_IDS_CAN_DEVICES);
	if (device < NUM_IDS_CAN_DEVICES)
	{
		CAN_TX_MESSAGE msg;
		if (CreateNetworkMessage(device, IDS_CAN_ADDRESS_BROADCAST, &msg))
			return IDS_CAN_Tx(device, &msg);
	}
	return TRUE;
}

#pragma INLINE
static void TransmitDeviceHeartbeat(IDS_CAN_DEVICE device)
{
	ASSERT(device < NUM_IDS_CAN_DEVICES);
	if (device < NUM_IDS_CAN_DEVICES)
	{
		CAN_TX_MESSAGE msg;
		if (CreateNetworkMessage(device, Device.Address[device], &msg))
		{
			if (IDS_CAN_Tx(device, &msg))
			{
				Device.NetworkTimer[device] = IDS_CAN_HEARTBEAT_PERIOD;
				RememberAddressInUse(Device.Address[device]);
			}
		}
	}
}

#pragma INLINE
static void TransmitCircuitID(IDS_CAN_DEVICE device)
{
	ASSERT(device < NUM_IDS_CAN_DEVICES);
	if (device < NUM_IDS_CAN_DEVICES)
	{
		CAN_TX_MESSAGE msg;
		uint32 circuit = IDS_CAN_GetCircuitID(device);

		// compose message
		msg.Length = 4;
		msg.ID = IDS_CAN_CreateCanId11(IDS_CAN_MESSAGE_TYPE_CIRCUIT_ID, Device.Address[device]);
		msg.Data[0] = (uint8)(circuit >> 24);
		msg.Data[1] = (uint8)(circuit >> 16);
		msg.Data[2] = (uint8)(circuit >> 8);
		msg.Data[3] = (uint8)(circuit >> 0); /*lint !e835 shifting by zero */

		// and transmit
		if (IDS_CAN_Tx(device, &msg))
			Device.CircuitIDTimer[device] = IDS_CAN_CIRCUIT_ID_MESSAGE_PERIOD;
	}
}

#pragma INLINE
static void TransmitDeviceID(IDS_CAN_DEVICE device)
{
	ASSERT(device < NUM_IDS_CAN_DEVICES);
	if (device < NUM_IDS_CAN_DEVICES)
	{
		CAN_TX_MESSAGE msg;
		IDS_CAN_DEVICE_ID device_id = IDS_CAN_GetDeviceID(device);

		// compose message
		msg.Length = 8;
		msg.ID = IDS_CAN_CreateCanId11(IDS_CAN_MESSAGE_TYPE_DEVICE_ID, Device.Address[device]);
		msg.Data[0] = (uint8)(device_id.ProductID >> 8);
		msg.Data[1] = (uint8)(device_id.ProductID >> 0); /*lint !e835 shifting by zero */
		msg.Data[2] = device_id.ProductInstance;
		msg.Data[3] = device_id.DeviceType;
		msg.Data[4] = (uint8)(device_id.FunctionName >> 8);
		msg.Data[5] = (uint8)(device_id.FunctionName >> 0); /*lint !e835 shifting by zero */
		msg.Data[6] = (uint8)(device_id.DeviceInstance << 4) | (device_id.FunctionInstance & 0xF);
		msg.Data[7] = device_id.DeviceCapabilities;

		// and transmit
		if (IDS_CAN_Tx(device, &msg))
			Device.DeviceIDTimer[device] = IDS_CAN_DEVICE_ID_MESSAGE_PERIOD;
	}
}

// runs from continuous program and manages network messaging
void _IDS_CAN_Network_Task1ms(void)
{
	static IDS_CAN_DEVICE CurrentDevice = (IDS_CAN_DEVICE)0;

	// cycle through all devices round robin
	CurrentDevice++;
	if (CurrentDevice >= NUM_IDS_CAN_DEVICES)
		CurrentDevice = (IDS_CAN_DEVICE)0;

	// disabled devices do no participate in bus traffic
	if (!IsDeviceEnabled(CurrentDevice) || Device.MuteSeconds[CurrentDevice])
		return; // don't bother updating

	// we want to be online

	// if we aren't online, then we need to claim a new address
	if (Device.Address[CurrentDevice] == IDS_CAN_ADDRESS_BROADCAST)
	{
		// we need to claim, or are in the process of claiming, an address

		// is it time to transmit?
		if (Device.NetworkTimer[CurrentDevice])
			return; // not yet

		// were we claiming an address?
		if (Device.AddressBeingClaimed[CurrentDevice] == IDS_CAN_ADDRESS_BROADCAST)
		{
			// no -- lets start by claiming an address at random
			// if we can't get a random address, we will try again later
			Device.AddressBeingClaimed[CurrentDevice] = ChooseDeviceAddressToClaim();
			if (Device.AddressBeingClaimed[CurrentDevice] != IDS_CAN_ADDRESS_BROADCAST)
			{
				// try and claim this address
				if (TransmitAddressClaim(CurrentDevice))
					Device.NetworkTimer[CurrentDevice] = IDS_CAN_ADDRESS_CLAIM_TIMEOUT; // when this times out, we have an address!
				else
					Device.AddressBeingClaimed[CurrentDevice] = IDS_CAN_ADDRESS_BROADCAST; // choose again later
			}
			return; // whether or not we sent a message, now wait for timeout
		}

		// won contention! - got the address
		Device.Address[CurrentDevice] = Device.AddressBeingClaimed[CurrentDevice];
		RememberAddressInUse(Device.Address[CurrentDevice]);

		// continue and transmit first heartbeat
	}

	// if we are here, then the device has a valid address and is using it

	// sanity check
	ASSERT(Device.Address[CurrentDevice] == Device.AddressBeingClaimed[CurrentDevice]);

	// re-transmit address claim messages to let other device know
	// that they've lost contention
	if (Device.RetransmitAddressClaim[CurrentDevice] > 0)
	{
		if (TransmitAddressClaim(CurrentDevice))
			Device.RetransmitAddressClaim[CurrentDevice]--;
		return;
	}

	// is it time to transmit the next device heartbeat?
	if (!Device.NetworkTimer[CurrentDevice])
	{
		TransmitDeviceHeartbeat(CurrentDevice);
		return;
	}

	// transmit CIRCUIT_ID message
	if (!Device.CircuitIDTimer[CurrentDevice])
	{
		TransmitCircuitID(CurrentDevice);
		return;
	}

	// transmit DEVICE_ID message
	if (!Device.DeviceIDTimer[CurrentDevice])
	{
		TransmitDeviceID(CurrentDevice);
		return;
	}
}

// runs from continuous program and manages network message timing
void _IDS_CAN_Network_Task10ms(void)
{
	uint8 d;

	// update list of addresses in use
	ManageAddressInUseTimeout();

	// manage message timers
	d = (uint8)NUM_IDS_CAN_DEVICES - 1; /*lint !e778 evaluates to 0 if only one device exists */
	do
	{
		if (Device.NetworkTimer[d])
			Device.NetworkTimer[d]--;
		if (Device.CircuitIDTimer[d])
			Device.CircuitIDTimer[d]--;
		if (Device.DeviceIDTimer[d])
			Device.DeviceIDTimer[d]--;
	} while (d--);
}

void _IDS_CAN_Network_Task1sec(void)
{
	// manage mute timeout
#ifdef DECLARE_CONFIGURATION
	if (!Config.ProductionBlock.ProductionByte) // don't countdown mute while production byte is set
#endif // DECLARE_CONFIGURATION
	{
		uint8 d = (uint8)NUM_IDS_CAN_DEVICES - 1; /*lint !e778 evaluates to 0 if only one device exists */
		do
		{
			if (Device.MuteSeconds[d] == 1)
				_IDS_CAN_UnmuteDevice((IDS_CAN_DEVICE)d);
			else if (Device.MuteSeconds[d])
				Device.MuteSeconds[d]--;
		} while (d--);
	}
}

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

#if defined(__GNUC__)
#pragma GCC diagnostic pop
#endif

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#endif // HEADER
