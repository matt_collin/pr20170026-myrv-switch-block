// Session.c
// Session control of remote devices for 18589 LCI LincPad Switch to CAN Interface SW S12G
// (c) 2012, 2014 Innovative Design Solutions, Inc.
// All rights reserved

#include "global.h"

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#ifdef HEADER

void Session_Init(void);
void Session_Task10ms(void);

void Session_OnCircuitIDMessageRx(const IDS_CAN_RX_MESSAGE *message);
void Session_OnDeviceIDMessageRx(const IDS_CAN_RX_MESSAGE *message);
void Session_OnResponseMessageRx(const IDS_CAN_RX_MESSAGE *message);
void Session_OnStatusMessageRx(const IDS_CAN_RX_MESSAGE *message);
void Session_OnCircuitIDChange(IDS_CAN_DEVICE device);

#else

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// identify the session id type
#define SESSION_ID   IDS_CAN_SESSION_ID_REMOTE_CONTROL

// enumerate session control state machine
typedef enum { STATE_IDLE = 0, STATE_REQUEST_SEED = 1, STATE_TRANSMIT_HB = 2, STATE_CLOSE_SESSION = 3 } STATE;

const uint8 STATE_SET_IDLE = 0x00;
const uint8 STATE_SET_REQUEST_SEED = 0x55;
const uint8 STATE_SET_TRANSMIT_HB = 0xAA;
const uint8 STATE_SET_CLOSE_SESSION = 0xFF;

// loop timing
#define SWITCH_TIMEOUT_MS 200

static struct
{
	IDS_CAN_ADDRESS FirstAddressInCircuit[NUM_IDS_CAN_DEVICES];
	IDS_CAN_ADDRESS LastAddressInCircuit[NUM_IDS_CAN_DEVICES];
	uint8 TxAttempts[NUM_IDS_CAN_DEVICES];
	uint8 SwitchState[NUM_IDS_CAN_DEVICES];
	uint8 Toggle[NUM_IDS_CAN_DEVICES];
} Device = { 0 };

// device state info is bit packed
// this way we can get away having *only* one page of RAM per device
// xxxxxxxx
// ||||||||
// |||||| \\__ STATE
// |||| \\____ STATE_TIMEOUT
// || \\______ CIRCUIT_VALID
// | \________ unused
//  \_________ unused

static struct {
   uint8 Address[256/4];
} SessState[NUM_IDS_CAN_DEVICES];

static struct {
   uint8 Address[256/4];
} StateTimeout[NUM_IDS_CAN_DEVICES];

static struct {
   uint8 Address[256/4];
} CircuitValid[NUM_IDS_CAN_DEVICES];

static const uint8 Flags[4] = { BIT0 | BIT1, BIT2 | BIT3, BIT4 | BIT5, BIT6 | BIT7 };
static const uint8 Sub[4] = { BIT0, BIT2, BIT4, BIT6 };

#define STATE(device, address)                (STATE)((SessState[device].Address[address >> 2] >> ((address & 3) << 1)))
#define SET_STATE(device, address, state)     SessState[device].Address[address >> 2] &= (uint8) ~Flags[address & 3]; SessState[device].Address[address >> 2] |= (uint8) ((uint8)Flags[address & 3] & state)
#define SET_STATE_TO_IDLE(device, address)    SessState[device].Address[address >> 2] &= (uint8) ~Flags[address & 3]

#define STATE_TIMEOUT(device, address)        (uint8) (StateTimeout[device].Address[address >> 2] & Flags[address & 3])
#define DEC_STATE_TIMEOUT(device, address)    StateTimeout[device].Address[address >> 2] -= Sub[address & 3]
#define RESET_STATE_TIMEOUT(device, address)  StateTimeout[device].Address[address >> 2] |= Flags[address & 3]

#define CIRCUIT_VALID(device, address)        (uint8) (CircuitValid[device].Address[address >> 2] & Flags[address & 3])
#define DEC_CIRCUIT_VALID(device, address)    CircuitValid[device].Address[address >> 2] -= Sub[address & 3]
#define SET_CIRCUIT_VALID(device, address)    CircuitValid[device].Address[address >> 2] |= Flags[address & 3]
#define CLEAR_CIRCUIT_VALID(device, address)  CircuitValid[device].Address[address >> 2] &= (uint8) ~Flags[address & 3]

static uint8 DeviceTypes[256] =  { 0 };
static uint8 RelayState[256] =  { 0 };

static uint8 SwitchEvent[256 / 8] = { 0 };

#define SWITCH_EVENT(address)                (uint8) (SwitchEvent[address >> 4] & (1 << (address & 7)))
#define SET_SWITCH_EVENT(address)            SwitchEvent[address >> 4] |= 1 << (address & 7)
#define CLEAR_SWITCH_EVENT(address)          SwitchEvent[address >> 4] &= ~(1 << (address & 7))
                                                                 
static uint8 OkToClearFault[256/8] =  { 0 }; // 1 bit per address
static const uint8 BIT[8] = { BIT0, BIT1, BIT2, BIT3, BIT4, BIT5, BIT6, BIT7 };
#define OK_TO_CLEAR_FAULT(address)       (OkToClearFault[(address) >> 3] & BIT[(address) & 7])
#define SET_OK_TO_CLEAR_FAULT(address)   (OkToClearFault[(address) >> 3] |= BIT[(address) & 7])
#define CLEAR_OK_TO_CLEAR_FAULT(address) (OkToClearFault[(address) >> 3] &= ~BIT[(address) & 7])

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

static void OnSeedReceived(IDS_CAN_ADDRESS target, IDS_CAN_ADDRESS source, uint32 seed);
static void OnKeyResponse(IDS_CAN_ADDRESS target, IDS_CAN_ADDRESS source);
static void OnSessionEnded(IDS_CAN_ADDRESS target, IDS_CAN_ADDRESS source);

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

void Session_Init(void)
{
}

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

// closes/terminates a session between a given local device and a target address
static void CloseSession(IDS_CAN_DEVICE device, IDS_CAN_ADDRESS address)
{
	if (device < NUM_IDS_CAN_DEVICES)
	{
      if (STATE(device, address) != STATE_IDLE && STATE(device, address) != STATE_CLOSE_SESSION)
      {
         SET_STATE(device, address, STATE_SET_CLOSE_SESSION);
         RESET_STATE_TIMEOUT(device, address);
      }	
	}
}

// opens communications sessions between the local device and all related addresses on our circuit
static void OpenAllSessions(IDS_CAN_DEVICE device)
{
	if (device < NUM_IDS_CAN_DEVICES)
	{
		IDS_CAN_ADDRESS address;
		for (address=0xFF; address; address--)
		{
			// this device needs to be on our circuit
			if (CIRCUIT_VALID(device, address))
			{
				// make sure we want to control this type of device
				switch (DeviceTypes[address])
				{
				case IDS_CAN_DEVICE_TYPE_LATCHING_RELAY:
				case IDS_CAN_DEVICE_TYPE_MOMENTARY_H_BRIDGE:
					// start a communications session as necessary
					if (STATE(device, address) == STATE_IDLE || STATE(device, address) == STATE_CLOSE_SESSION)
					{
						SET_SWITCH_EVENT(address);
						SET_STATE(device, address, STATE_SET_REQUEST_SEED);
						RESET_STATE_TIMEOUT(device, address);
					}
					break;
				}
			}
		}
	}
}

// closes all open communications sessions for the local device
static void CloseAllSessions(IDS_CAN_DEVICE device)
{
	static uint8 flags;
	
	if (device < NUM_IDS_CAN_DEVICES)
	{
		IDS_CAN_ADDRESS address;
	
		Device.TxAttempts[device] = 0;
		for (address=0xFF; address; address--)
		{
			if (STATE(device, address) != STATE_IDLE && STATE(device, address) != STATE_CLOSE_SESSION)
			{
				flags = (uint8) ~Flags[address & 3];
				flags = (uint8) (uint8) ((uint8)Flags[address & 3] & STATE_SET_CLOSE_SESSION);
				SET_STATE(device, address, STATE_SET_CLOSE_SESSION);
				RESET_STATE_TIMEOUT(device, address);
			}
		}
	}
}

// finds the first and last valid addresses in the circuit
static void FindFirstAndLastAddress(IDS_CAN_DEVICE device)
{
	if (device < NUM_IDS_CAN_DEVICES)
	{
		IDS_CAN_ADDRESS first, last;

		Device.FirstAddressInCircuit[device] = 0;
		Device.LastAddressInCircuit[device] = 0;
           
		// re-establish first and last address
		for (first=1; first; first++)
		{
			// find the first address
			if (CIRCUIT_VALID(device, first))
			{
				// found it
				Device.FirstAddressInCircuit[device] = Device.LastAddressInCircuit[device] = first;
	
				// find the last address
				for (last=0xFF; last; last--)
				{
					if (CIRCUIT_VALID(device, last))
					{
						Device.LastAddressInCircuit[device] = last;
						return;
					}
				}
	
				return;
			}
		}
	}
}

// checks for "circuit valid" timeout
// this will loop through all the addresses after 256 calls
// at 10 ms this equals 2.56 seconds
static void ManageCircuitTimeout(IDS_CAN_ADDRESS address)
{
	// check all devices on this address
	IDS_CAN_DEVICE device;
	for (device=0; device<NUM_IDS_CAN_DEVICES; device++)
	{
		// manage circuit timeout      
		if (CIRCUIT_VALID(device, address))
		{
			DEC_CIRCUIT_VALID(device, address);

			// remove from list
			if (!IDS_CAN_IsDeviceOnline(device))
				CLEAR_CIRCUIT_VALID(device, address);

			if (!CIRCUIT_VALID(device, address))
			{
				SET_STATE_TO_IDLE(device, address); // reset the state
				DeviceTypes[address] = 0; // reset the device type
				FindFirstAndLastAddress(device);            
			}
		}
	}
}

// handles state machine timeout
// this will loop through all the addresses after 256 calls
// at 10 ms this equals 2.56 seconds
static void ManageStateTimeout(IDS_CAN_ADDRESS address)
{
	// check all devices on this address
	IDS_CAN_DEVICE device;
	for (device=0; device<NUM_IDS_CAN_DEVICES; device++)
	{
		// decrement timeout
		if (STATE_TIMEOUT(device, address))
		{
			DEC_STATE_TIMEOUT(device, address);
			if (!STATE_TIMEOUT(device, address))
			{
				switch (STATE(device, address))
				{
				default: break;
				case STATE_IDLE: break; // nothing to do
				case STATE_TRANSMIT_HB: break; // keep this state alive
				case STATE_REQUEST_SEED: 
				   // keep this state alive if the switch state has not been latched
				   if (SWITCH_EVENT(address)) 
				   {
				      RESET_STATE_TIMEOUT(device, address); 
				      break;
				   }
				   // lint -fallthrough
				case STATE_CLOSE_SESSION: SET_STATE_TO_IDLE(device, address); break;
				}
			}
		}
	}
}

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
// handle change to circuit ID
void Session_OnCircuitIDChange(IDS_CAN_DEVICE device)
{
   // check all addresses on this device
	IDS_CAN_ADDRESS addr;
	
	if (device < NUM_IDS_CAN_DEVICES)
	{
      for (addr=0xFF; addr; addr--)
	   {
		   CLEAR_CIRCUIT_VALID(device, addr);
		   SET_STATE_TO_IDLE(device, addr); // reset the state
		   DeviceTypes[addr] = 0; // reset the device type
		}

      FindFirstAndLastAddress(device);
	}
}

// handle response messages from remote devices
void Session_OnResponseMessageRx(const IDS_CAN_RX_MESSAGE *message)
{
	// check session ID
	uint16 session_id = *(uint16*)(&message->Data[0]); 
	if (session_id != SESSION_ID)
	   return;
      
	switch (message->ExtMessageData)
	{
	case IDS_CAN_REQUEST_SESSION_REQUEST_SEED:
		if (message->Length == 6)
		{
			uint32 seed = *(uint32*)(&message->Data[2]);
			OnSeedReceived(message->TargetAddress, message->SourceAddress, seed);
		}
		break;
   
	case IDS_CAN_REQUEST_SESSION_TRANSMIT_KEY:
		if (message->Length == 2) // two byte response guarantees the request was sucessfully processed
			(void) OnKeyResponse(message->TargetAddress, message->SourceAddress);
		break;
   
	case IDS_CAN_REQUEST_SESSION_END:
		if (message->Length == 3 && message->Data[2] == IDS_CAN_RESPONSE_SUCCESS)
			OnSessionEnded(message->TargetAddress, message->SourceAddress);
		break;
      
	case IDS_CAN_REQUEST_SESSION_HEARTBEAT:
	default: break;
	}
}

// unload status messages from the relays
void Session_OnStatusMessageRx(const IDS_CAN_RX_MESSAGE *message)
{
	uint8 state, laststate;

	// get relay state
	state = 0;
	if (message->Length == 1 && message->SourceAddress)
		state = message->Data[0];
   
	// set state
	laststate = RelayState[message->SourceAddress];
	RelayState[message->SourceAddress] = state & 0x7F;
   
	// did a fault just get set?
	state &= (state ^ laststate);
	if (state & 0x40)
		CLEAR_OK_TO_CLEAR_FAULT(message->SourceAddress);
}

// handle CIRCUIT_ID reception
void Session_OnCircuitIDMessageRx(const IDS_CAN_RX_MESSAGE *message)
{
	if (message->Length == 4 && message->SourceAddress)
	{
		// read the CIRCUIT_ID     
		uint32 circuitID = *(uint32*)(&message->Data[0]); 
	
		// 0 is invalid
		if (circuitID)
		{
			IDS_CAN_DEVICE device;
			for (device=0; device<NUM_IDS_CAN_DEVICES; device++)
			{
				if (Config.IDS_CAN_Block.CircuitID[device] == circuitID)   
				{
					SET_CIRCUIT_VALID(device, message->SourceAddress);
      
					if (!Device.FirstAddressInCircuit[device]) 
						Device.FirstAddressInCircuit[device] = Device.LastAddressInCircuit[device] = message->SourceAddress;
					else
					{
						if (message->SourceAddress < Device.FirstAddressInCircuit[device]) Device.FirstAddressInCircuit[device] = message->SourceAddress;   
						if (message->SourceAddress > Device.LastAddressInCircuit[device])  Device.LastAddressInCircuit[device] = message->SourceAddress;
					}
				}
			}
		}
	}
}

// handles reception of DEVICE_ID messages
void Session_OnDeviceIDMessageRx(const IDS_CAN_RX_MESSAGE *message)
{
	// remember the device type, so we know how to interoperate with the device
	if (message->Length >= 7 && message->SourceAddress)
		DeviceTypes[message->SourceAddress] = message->Data[3];
}

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

// called when a security seed is transmitted to this device
static void OnSeedReceived(IDS_CAN_ADDRESS target, IDS_CAN_ADDRESS source, uint32 seed)
{
	#define CYPHER 0xB16B00B5
	static uint32 key = 0;
	CAN_TX_MESSAGE msg;
	IDS_CAN_DEVICE device = IDS_CAN_GetDeviceFromAddress(target);
   
	if (device < NUM_IDS_CAN_DEVICES)
	{
		key = IDS_CAN_Encrypt(seed, CYPHER);
   
		// reply to source; source becomes new target and vice versa
		msg.ID = IDS_CAN_CreateCanId29(IDS_CAN_MESSAGE_TYPE_REQUEST, target, source, IDS_CAN_REQUEST_SESSION_TRANSMIT_KEY);
		msg.Length = 6;  
		msg.Data[0] = (uint8) (SESSION_ID >> 8);
		msg.Data[1] = (uint8) (SESSION_ID >> 0);
		msg.Data[2] = (uint8) (key >> 24);
		msg.Data[3] = (uint8) (key >> 16);
		msg.Data[4] = (uint8) (key >> 8);
		msg.Data[5] = (uint8) (key >> 0);
  
		(void)IDS_CAN_Tx(device, &msg);
	}
}

static void OnKeyResponse(IDS_CAN_ADDRESS target, IDS_CAN_ADDRESS source) 
{
	IDS_CAN_DEVICE device = IDS_CAN_GetDeviceFromAddress(target);  
	if (device < NUM_IDS_CAN_DEVICES)
	{
		// if this circuit id is in our list
		if (CIRCUIT_VALID(device, source))
		{
			// and the session state is waiting for key confirmation
			if (STATE(device, source) == STATE_REQUEST_SEED)
				SET_STATE(device, source, STATE_SET_TRANSMIT_HB); // advance the session state   
		}
	}
}

static void OnSessionEnded(IDS_CAN_ADDRESS target, IDS_CAN_ADDRESS source)
{
	IDS_CAN_DEVICE device = IDS_CAN_GetDeviceFromAddress(target);  
	if (device < NUM_IDS_CAN_DEVICES)
	   SET_STATE_TO_IDLE(device, source);
}

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

// returns TRUE if message sent (or does not need to be sent)
// returns FALSE if message was not sent but needed to be sent
static uint8 TxOpenSessionMsg(IDS_CAN_DEVICE device, IDS_CAN_ADDRESS target)
{
	IDS_CAN_ADDRESS source = IDS_CAN_GetDeviceAddress(device);
	CAN_TX_MESSAGE msg;
   
	if (!IDS_CAN_IsDeviceOnline(device) || !target)
		return TRUE;
	if (!Device.TxAttempts[device])
		return TRUE;
   
	msg.ID = IDS_CAN_CreateCanId29(IDS_CAN_MESSAGE_TYPE_REQUEST, source, target, IDS_CAN_REQUEST_SESSION_REQUEST_SEED);
	msg.Length = 2;
	msg.Data[0] = (uint8) (SESSION_ID >> 8);
	msg.Data[1] = (uint8) (SESSION_ID >> 0);

	// attempt transmit
	return IDS_CAN_Tx(device, &msg);
}

// returns TRUE if message sent (or does not need to be sent)
// returns FALSE if message was not sent but needed to be sent
static uint8 TxSessionHeartbeatMsg(IDS_CAN_DEVICE device, IDS_CAN_ADDRESS target)
{
	IDS_CAN_ADDRESS source = IDS_CAN_GetDeviceAddress(device);
	CAN_TX_MESSAGE msg;
   
	if (!IDS_CAN_IsDeviceOnline(device) || !target)
		return TRUE;
   
	msg.ID = IDS_CAN_CreateCanId29(IDS_CAN_MESSAGE_TYPE_REQUEST, source, target, IDS_CAN_REQUEST_SESSION_HEARTBEAT);
	msg.Length = 2;
	msg.Data[0] = (uint8) (SESSION_ID >> 8);
	msg.Data[1] = (uint8) (SESSION_ID >> 0);

	// attempt transmit
	return IDS_CAN_Tx(device, &msg);
}

// returns TRUE if message sent (or does not need to be sent)
// returns FALSE if message was not sent but needed to be sent
static uint8 TxEndSessionMsg(IDS_CAN_DEVICE device, IDS_CAN_ADDRESS target)
{
	IDS_CAN_ADDRESS source = IDS_CAN_GetDeviceAddress(device);
	CAN_TX_MESSAGE msg;
   
	if (!IDS_CAN_IsDeviceOnline(device) || !target)
		return TRUE;

	msg.ID = IDS_CAN_CreateCanId29(IDS_CAN_MESSAGE_TYPE_REQUEST, source, target, IDS_CAN_REQUEST_SESSION_END);
	msg.Length = 2;
	msg.Data[0] = (uint8) (SESSION_ID >> 8);
	msg.Data[1] = (uint8) (SESSION_ID >> 0);

	// attempt transmit
	return IDS_CAN_Tx(device, &msg);
}

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

// returns TRUE if message sent (or does not need to be sent)
// returns FALSE if message was not sent but needed to be sent
static uint8 TxRelayMsg(IDS_CAN_DEVICE device, IDS_CAN_ADDRESS target)
{
	IDS_CAN_ADDRESS source = IDS_CAN_GetDeviceAddress(device);
	uint8 desiredStatus;
	CAN_TX_MESSAGE msg;
   
	if (device >= NUM_IDS_CAN_DEVICES || !source || !target)
		return TRUE;
   
	if (!Device.TxAttempts[device])
		return TRUE; // don't transmit relay message once we run out of attempts
   
	msg.Length = 1;
	msg.ID = IDS_CAN_CreateCanId29(IDS_CAN_MESSAGE_TYPE_COMMAND, source, target, 0);
	msg.Data[0] = 0;
     
	// command format
	// byte 0 : xxxxxxxx
	//          ||  ||||
	//          ||  ||| \__ relay 1
	//          ||  || \___ relay 1 state on loss of comm (only applies when latched bit set)
	//          ||  | \____ relay 2
	//          ||   \_____ relay 2 state on loss of comm (only applies when latched bit set)
	//          | \________ clear relay faults
	//           \_________ latch(1) or momentary(0)
	
	// status message format
	// byte 0 : xxxxxxxx
	//          ||    ||
	//          ||    | \__ relay 1 state
	//          ||     \___ relay 2 state
	//          | \________ faults(1) or no faults(0)
	//           \_________ latch(1) or momentary(0)
	
	switch (DeviceTypes[target])
	{
	case IDS_CAN_DEVICE_TYPE_LATCHING_RELAY:
		msg.Data[0] = 0x80;
		desiredStatus = 0;
      
		if (Device.Toggle[device])
		{
			msg.Data[0] |= 0x03;
			desiredStatus |= 0x01;       
		}
      
		// does the relay state match?
		if (RelayState[target] == desiredStatus)
		{
		   CLEAR_SWITCH_EVENT(target);
		   CloseSession(device, target); // we are done, go ahead and close the session
		}
		break;
   
	case IDS_CAN_DEVICE_TYPE_MOMENTARY_H_BRIDGE:
		if (Device.SwitchState[device] == SWITCH_STATE_OFF)
		{
		   // limit the amount of transmit time if the switch if deasserted
		   if (Device.TxAttempts[device] > 10)
		      Device.TxAttempts[device] = 10;
		}
		else if (Device.SwitchState[device] == SWITCH_STATE_1)       
			msg.Data[0] |= 0x01;     
		else if (Device.SwitchState[device] == SWITCH_STATE_2)  
			msg.Data[0] |= 0x04;
		break;
   
	default: 
		return TRUE;
	}
   
	// OK to clear fault if no buttons are pressed
	if (!(msg.Data[0] & 0x0F))
		SET_OK_TO_CLEAR_FAULT(target);
   
	// fault handling/clearing
	if (RelayState[target] & 0x40)
	{
		if (msg.Data[0])
			msg.Data[0] = OK_TO_CLEAR_FAULT(target) ? 0x40 : 0x00;
	}
   
	return IDS_CAN_Tx(device, &msg);   
}

// attempts to send a message to each device in the circuit, round robin
// returns TRUE once a message has been sent to all devices in one pass
// returns FALSE if there are more messages to send this pass
static uint8 SendMessage(IDS_CAN_DEVICE device)
{
	static IDS_CAN_ADDRESS Target[NUM_IDS_CAN_DEVICES] = { 0 };
	uint8 success;
      
	if (device >= NUM_IDS_CAN_DEVICES)
		return TRUE; // nothing to do
   
	// make sure we have some devices online to talk to
	if (!Device.FirstAddressInCircuit[device])
		return TRUE; // nothing to do
   
	// try and find the next address to talk to, round robin, starting where we left off last time
	for (;;)
	{
		// move to the next address
		Target[device]++;

		// are we outside the list range?
		if (Target[device] < Device.FirstAddressInCircuit[device] || Target[device] > Device.LastAddressInCircuit[device])
		{
			Target[device] = Device.FirstAddressInCircuit[device]; // start over
			break; // this address is guaranteed to be valid
		}

		// is this circuit valid?
		if (CIRCUIT_VALID(device, Target[device]))
			break; // found a match
	}

	// send proper message depending on state machine
	switch (STATE(device, Target[device]))
	{
	default:
	case STATE_IDLE:          success = TRUE; break;
	case STATE_REQUEST_SEED:  success = TxOpenSessionMsg(device, Target[device]); break;
	case STATE_TRANSMIT_HB:   success = TxRelayMsg(device, Target[device]); break;
	case STATE_CLOSE_SESSION: success = TxEndSessionMsg(device, Target[device]); break;
	}

	// if we didn't successfully transmit
	if (!success)
	{
		Target[device]--; // back up one address, ensuring we will re-attempt this transmission next round
		return FALSE;
	}

	return (Target[device] == Device.LastAddressInCircuit[device]);   
}

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

static uint8 AreAnyRelaysInThisCircuitOn(IDS_CAN_DEVICE device)
{
	IDS_CAN_ADDRESS address;
		
	if (device < NUM_IDS_CAN_DEVICES)
	{
		// make sure we have some devices online to talk to
		if (!Device.FirstAddressInCircuit[device])
			return FALSE; // nothing to do
		
		for (address = Device.LastAddressInCircuit[device]; address >= Device.FirstAddressInCircuit[device]; address--)
		{
			// check to see if this device is in our circuit
			if (CIRCUIT_VALID(device, address) && (DeviceTypes[address] == IDS_CAN_DEVICE_TYPE_LATCHING_RELAY))
			{
				if (RelayState[address] & 3) return TRUE;	
			}
		}
	}

	return FALSE;
}

#pragma MESSAGE DISABLE C4001
static void CheckSwitches(void)
{
   static uint8 switch_state[NUM_SWITCHES] = { 0 };
	static uint8 switch_event[NUM_SWITCHES] = { 0 };
	static IDS_CAN_DEVICE switches =  (IDS_CAN_DEVICE) 0;
	SWITCH_STATE state;
	uint8 count = 0;
	
	// determine state of each switch, event occurs on 0->1 transition
	#undef SWITCH
	#define SWITCH(name, pressed, val)						\
	state = SWITCH_STATE_OFF;									\
	if (pressed) state = val;									\
	if (!Device.SwitchState[SWITCH_##name] && state)	\
	   switch_event[SWITCH_##name] = TRUE;					\
	Device.SwitchState[SWITCH_##name] = state;			\
	
	DECLARE_SWITCH_DEVICES
		
	// cycle through switches 5 at a time to limit the number of "for" loops in AreAnyRelaysInThisCircuitOn()
	do
	{
		if (switches >= NUM_SWITCHES) switches = 0;
	   
		if (switch_event[switches])
		{
			// make sure we have some devices online to talk to
			if (Device.FirstAddressInCircuit[switches])
			{
				// determine toggle state	
				if (AreAnyRelaysInThisCircuitOn(switches))
					Device.Toggle[switches] = 0;
				else Device.Toggle[switches] = 1;
		
			   Device.TxAttempts[switches] = 255;
		   
			   // opens communications sessions between the local device and all related addresses on our circuit
				OpenAllSessions(switches);
			}
			// consume the event
			switch_event[switches] = FALSE;
		}

		// keep sending if device should be on
		if (Device.SwitchState[switches] && Device.TxAttempts[switches])
			Device.TxAttempts[switches] = 255; 
	
		switches++;
	   count++;
	}
	while (count < 5);
	
}
#pragma MESSAGE DEFAULT C4001

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

static void HandleCommandMessages(void)
{
	// round robin
	static IDS_CAN_DEVICE device = (IDS_CAN_DEVICE)0;
	if (device >= NUM_IDS_CAN_DEVICES)
		device = 0;
   
	// round robin message transmission, SendMessage() will return TRUE if a message
	// was sent out to each device in its id list, or there are no messages to send
	if (SendMessage(device)) 
	{
		if (Device.TxAttempts[device] && !--Device.TxAttempts[device])
			CloseAllSessions(device);
		device++;
	}
}

// we want to send a heartbeat approx every 500 ms
// as this runs every 10ms, 500 ms equates to 500/10 = 50 passes
// now there are 256 addresses to cover
// so, we should check 256 / 50 = 5 addresses per pass
static void TransmitSessionHeartbeat(void)
{
	// remember target address
	static IDS_CAN_ADDRESS Target = (IDS_CAN_ADDRESS)0;
     uint8 n;

	// we will scan 5 addresses per pass
	for (n=0; n<5; n++)
	{
		IDS_CAN_DEVICE device;

		// cycle to the next address
		Target++;
		if (!Target)
			Target++;

		for (device=0; device<NUM_IDS_CAN_DEVICES; device++)
		{
			if (STATE(device, Target) == STATE_TRANSMIT_HB)
				TxSessionHeartbeatMsg(device, Target);
		}
	}
}

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

void Session_Task10ms(void)
{
	// cycle through addresses
	static IDS_CAN_ADDRESS Address = (IDS_CAN_ADDRESS)0;
	Address++;

	ManageCircuitTimeout(Address);
	ManageStateTimeout(Address);
	CheckSwitches();
   
#ifdef DEBUG_1
	DEBUG_1 = 1;
#endif

	HandleCommandMessages();
   
#ifdef DEBUG_1
	DEBUG_1 = 0;
#endif
   
	TransmitSessionHeartbeat();
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#endif
