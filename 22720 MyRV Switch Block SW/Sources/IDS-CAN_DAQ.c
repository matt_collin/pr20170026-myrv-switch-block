// IDS CAN DAQ.c
// IDS CAN data acquisition and memory peek/poke
// Version 2.8
// (c) 2017 Innovative Design Solutions, Inc.
// All rights reserved

// History
// Version 2.7   First stable version
// Version 2.8   ASSERT fixed in PeekMemory(), Linted

#include "global.h"

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#ifdef HEADER

// private functions - do NOT call
void _IDS_CAN_DAQ_Init(void);
void _IDS_CAN_DAQ_Task(uint16 delta_ms);
void _IDS_CAN_OnDAQMessageRx(const IDS_CAN_RX_MESSAGE *message);
IDS_CAN_RESPONSE_CODE _IDS_CAN_IsDAQSessionAllowed(IDS_CAN_DEVICE device, IDS_CAN_ADDRESS host_address, IDS_CAN_SESSION_ID id);
void _IDS_CAN_OnDAQSessionOpened(IDS_CAN_DEVICE device, IDS_CAN_ADDRESS host_address, IDS_CAN_SESSION_ID id);
void _IDS_CAN_OnDAQSessionClosed(IDS_CAN_DEVICE device, IDS_CAN_ADDRESS host_address, IDS_CAN_SESSION_ID id);
IDS_CAN_RESPONSE_CODE _IDS_CAN_DAQMessage51(IDS_CAN_DEVICE device, const IDS_CAN_RX_MESSAGE *request, CAN_TX_MESSAGE *response);
IDS_CAN_RESPONSE_CODE _IDS_CAN_DAQMessage52(IDS_CAN_DEVICE device, const IDS_CAN_RX_MESSAGE *request, CAN_TX_MESSAGE *response);
IDS_CAN_RESPONSE_CODE _IDS_CAN_DAQMessage53(IDS_CAN_DEVICE device, const IDS_CAN_RX_MESSAGE *request, CAN_TX_MESSAGE *response);
IDS_CAN_RESPONSE_CODE _IDS_CAN_DAQMessage54(IDS_CAN_DEVICE device, const IDS_CAN_RX_MESSAGE *request, CAN_TX_MESSAGE *response);

#else // HEADER

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#if defined(__GNUC__)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunknown-pragmas"
#endif

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

#ifndef IDS_CAN_NUM_DAQ_CHANNELS
	#error IDS_CAN_NUM_DAQ_CHANNELS must be defined
#elif IDS_CAN_NUM_DAQ_CHANNELS < 1 || IDS_CAN_NUM_DAQ_CHANNELS > 16
	#error IDS_CAN_NUM_DAQ_CHANNELS must be between 1 and 16
#endif

#define SEQ_MODULUS  12  /* counts from 0 to 11 and wraps back to zero */

typedef union
{
	uint8 * Pointer; // can be up to 8-bytes long to support 64-bit systems
	uint16 PID;
} ADDRESS;

// variables that keep track of an open session
static IDS_CAN_DEVICE ActiveDevice; // device that is running a DAQ session
static IDS_CAN_ADDRESS HostAddress; // host address of the DAQ session
static uint16 AutoTxEnabled = 0; // 1 bit per channel: signifies which channels are enabled to auto transmit
static uint16 PendingAutoTx = 0; // 1 bit per channel: signifies when channel needs to transmit auto data

// DAQ channel variables
static ADDRESS Address[IDS_CAN_NUM_DAQ_CHANNELS];
static uint8 TxRate_ms[IDS_CAN_NUM_DAQ_CHANNELS];
static uint8 Time_ms[IDS_CAN_NUM_DAQ_CHANNELS];

// packed flag structure
static struct
{
	uint8 Count:3;         // memory peek size 1-8 bytes (0 = 8)
	uint8 IsPID:1;         //
	uint8 Seq:4;           // sequencing info, increments by 1 count each subsequent transmission
} Flags[IDS_CAN_NUM_DAQ_CHANNELS];

#define SET_SIZE(c, v)  (Flags[c].Count = ( ((v)<8) ? (v) : 0 ))
#define GET_SIZE(c)     ((uint8)(Flags[c].Count ? Flags[c].Count : 8))

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

void _IDS_CAN_DAQ_Init(void)
{
	// terminate active session
	ActiveDevice = NUM_IDS_CAN_DEVICES;
	AutoTxEnabled = 0;
	PendingAutoTx = 0;

	// keep settings valid between sessions to prevent end user from re-entering data

	// reset DAQ timers
	(void)memset(Time_ms, 0, sizeof(Time_ms));
}

#pragma MESSAGE DISABLE C5703 /* variable declared but not referenced */
/*lint -save -e715 unused parameter */
IDS_CAN_RESPONSE_CODE _IDS_CAN_IsDAQSessionAllowed(IDS_CAN_DEVICE device, IDS_CAN_ADDRESS host_address, IDS_CAN_SESSION_ID id)
{
	ASSERT(id == IDS_CAN_SESSION_ID_DAQ);

	// only one DAQ session is allowed at a time
	if (id != IDS_CAN_SESSION_ID_DAQ)
		return IDS_CAN_RESPONSE_UNKNOWN_ID;
	if (ActiveDevice < NUM_IDS_CAN_DEVICES && IDS_CAN_IsDeviceOnline(ActiveDevice))
		return IDS_CAN_RESPONSE_BUSY;
	return IDS_CAN_RESPONSE_SUCCESS;
}
#pragma MESSAGE DEFAULT C5703 /* variable declared but not referenced */
/*lint -restore */

#pragma MESSAGE DISABLE C5703 /* variable declared but not referenced */
/*lint -save -e715 unused parameter */
void _IDS_CAN_OnDAQSessionOpened(IDS_CAN_DEVICE device, IDS_CAN_ADDRESS host_address, IDS_CAN_SESSION_ID id)
{
	ASSERT(id == IDS_CAN_SESSION_ID_DAQ);
	if (id == IDS_CAN_SESSION_ID_DAQ)
	{
		_IDS_CAN_DAQ_Init();
		ActiveDevice = device;
		HostAddress = host_address;
	}
}
#pragma MESSAGE DEFAULT C5703 /* variable declared but not referenced */
/*lint -restore */

#pragma MESSAGE DISABLE C5703 /* variable declared but not referenced */
/*lint -save -e715 unused parameter */
void _IDS_CAN_OnDAQSessionClosed(IDS_CAN_DEVICE device, IDS_CAN_ADDRESS host_address, IDS_CAN_SESSION_ID id)
{
	ASSERT(id == IDS_CAN_SESSION_ID_DAQ);
	if (id == IDS_CAN_SESSION_ID_DAQ)
	{
		_IDS_CAN_DAQ_Init();
	}
}
#pragma MESSAGE DEFAULT C5703 /* variable declared but not referenced */
/*lint -restore */

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

static void EnableAutoTx(uint16 channels)
{
	uint16 activating, mask;
	uint8 ch;

	activating = AutoTxEnabled; // hold previous value
	AutoTxEnabled = channels & (uint16)((1L << IDS_CAN_NUM_DAQ_CHANNELS) - 1); // mask out invalid channels
	activating = (activating ^ AutoTxEnabled) & AutoTxEnabled; // bits that have changed from 0->1

	PendingAutoTx &= AutoTxEnabled; // mask out disabled channels

	for (ch=0, mask=1; ch<IDS_CAN_NUM_DAQ_CHANNELS; ch++, mask <<= 1)
	{
		if (activating & mask)
		{
			if (TxRate_ms[ch])
			{
				Time_ms[ch] = 0;
				Flags[ch].Seq = 0; // reset sequence counter on first transmission
				PendingAutoTx |= mask; // enable first auto transmission
			}
			else
			{
				PendingAutoTx &= ~mask; // abort pending transmission
			}
		}
	}
}

static uint8 ReadAddress(uint8 channel, uint8 * buffer)
{
	ASSERT(channel < IDS_CAN_NUM_DAQ_CHANNELS);
	if (channel >= IDS_CAN_NUM_DAQ_CHANNELS)
		return 0;

	if (Flags[channel].IsPID)
	{
		buffer[0] = (uint8)(Address[channel].PID >> 8);
		buffer[1] = (uint8)(Address[channel].PID >> 0); /*lint !e835 shifting by zero */
		return 2;
	}
	else
	{
		DWORD val;
		val.ui32 = (uint32)Address[channel].Pointer;
		buffer[0] = val.word.hi.ui8.hi;
		buffer[1] = val.word.hi.ui8.lo;
		buffer[2] = val.word.lo.ui8.hi;
		buffer[3] = val.word.lo.ui8.lo;
		return 4;
	}
}

static void WriteAddress(uint8 channel, const IDS_CAN_RX_MESSAGE *rx)
{
	uint8 len = rx->Length;
	ASSERT(channel < IDS_CAN_NUM_DAQ_CHANNELS);
	ASSERT(len >= 1 && len <= 8);
	if (channel < IDS_CAN_NUM_DAQ_CHANNELS && len >= 1 && len <= 8)
	{
		uint32 address = 0;
		uint8 n;
		for (n=0; n<len; n++)
		{
			address <<= 8;
			address |= rx->Data[n];
		}

		Address[channel].Pointer = (uint8*)address;  /*lint !e511  incompatibility in cast */

		// reset channel size to maximum whenever a new address pointer is written
		SET_SIZE(channel, 8); /*lint !e506 Constant boolean in this macro.  This is fine, its used in other places with a non-constant parameter */
		
		Flags[channel].IsPID = FALSE;
	}
}

#pragma INLINE
static uint8 GetMinimumPIDTxLength(const IDS_CAN_PID_DATA * val)
{
	if (val->byte[0])      return 6;
	else if (val->byte[1]) return 5;
	else if (val->byte[2]) return 4;
	else if (val->byte[3]) return 3;
	else if (val->byte[4]) return 2;
	else                   return 1;
}

static uint8 PeekMemory(IDS_CAN_DEVICE device, uint8 channel, uint8 * buffer)
{
	uint8 len;

	ASSERT(channel < IDS_CAN_NUM_DAQ_CHANNELS);
	if (channel >= IDS_CAN_NUM_DAQ_CHANNELS)
		return 0;

	// do peek
	if (Flags[channel].IsPID)
	{
		IDS_CAN_PID_DATA data = IDS_CAN_ReadPID(device, Address[channel].PID);
		len = GetMinimumPIDTxLength(&data);
		ASSERT(len >= 1 && len <= 6);
		if (len && len <= 6)
			(void) memcpy(buffer, &data.byte[6-len], len);
	}
	else
	{
		len = GET_SIZE(channel);
		ASSERT(len >= 1 && len <= 8);
		if (len && len <= 8)
		{
			EnterCriticalSection();
			(void) memcpy(buffer, Address[channel].Pointer, len); // copy status to local structure
			LeaveCriticalSection();
		}
	}

	return len;
}

#pragma MESSAGE DISABLE C5703 /* variable declared but not referenced */
/*lint -save -e715 unused parameter */
#pragma INLINE
static void PokeMemory(IDS_CAN_DEVICE device, uint8 channel, const IDS_CAN_RX_MESSAGE *rx)
{
	ASSERT(channel < IDS_CAN_NUM_DAQ_CHANNELS);
	ASSERT(rx->Length >= 1 && rx->Length <= 8);
	if (channel < IDS_CAN_NUM_DAQ_CHANNELS && rx->Length >= 1 && rx->Length <= 8)
	{
		if (Flags[channel].IsPID)
		{
			// not yet supported
		}
		else
		{
			EnterCriticalSection();
			(void) memcpy(Address[channel].Pointer, &rx->Data[0], rx->Length);
			LeaveCriticalSection();
		}
	}
}
#pragma MESSAGE DEFAULT C5703 /* variable declared but not referenced */
/*lint -restore */

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// Read num DAQ channels
#pragma MESSAGE DISABLE C5703 /* variable declared but not referenced */
/*lint -save -e715 unused parameter */
IDS_CAN_RESPONSE_CODE _IDS_CAN_DAQMessage51(IDS_CAN_DEVICE device, const IDS_CAN_RX_MESSAGE *request, CAN_TX_MESSAGE *response)
{
	if (request->Length != 0) return IDS_CAN_RESPONSE_BAD_REQUEST;
	response->Length = 2;
	response->Data[0] = IDS_CAN_NUM_DAQ_CHANNELS;
	response->Data[1] = 0x00;
	return IDS_CAN_RESPONSE_SUCCESS;
}
#pragma MESSAGE DEFAULT C5703 /* variable declared but not referenced */
/*lint -restore */

// Read/Write DAQ auto tx settings
IDS_CAN_RESPONSE_CODE _IDS_CAN_DAQMessage52(IDS_CAN_DEVICE device, const IDS_CAN_RX_MESSAGE *request, CAN_TX_MESSAGE *response)
{
	switch (request->Length)
	{
	default: return IDS_CAN_RESPONSE_BAD_REQUEST;
	case 0: break; // read
	case 2: // write
		// commands require DIAGNOSTIC session access
		if (IDS_CAN_GetSessionOwner(device, IDS_CAN_SESSION_ID_DAQ) != request->SourceAddress)
			return IDS_CAN_RESPONSE_SESSION_NOT_OPEN;
		EnableAutoTx(((uint16)request->Data[0] << 8) | request->Data[1]);
		break;
	}

	response->Length = 2;
	response->Data[0] = (uint8)(AutoTxEnabled >> 8);
	response->Data[1] = (uint8)(AutoTxEnabled >> 0); /*lint !e835 shifting by zero */

	return IDS_CAN_RESPONSE_SUCCESS;
}

// Read/Write DAQ chan settings
IDS_CAN_RESPONSE_CODE _IDS_CAN_DAQMessage53(IDS_CAN_DEVICE device, const IDS_CAN_RX_MESSAGE *request, CAN_TX_MESSAGE *response)
{
	uint8 channel = request->Data[0];

	switch (request->Length)
	{
	default: return IDS_CAN_RESPONSE_BAD_REQUEST;
	case 1: // read
		if (channel >= IDS_CAN_NUM_DAQ_CHANNELS)
			return IDS_CAN_RESPONSE_VALUE_OUT_OF_RANGE;
		break;
	case 3: // write
		// commands require DIAGNOSTIC session access
		if (IDS_CAN_GetSessionOwner(device, IDS_CAN_SESSION_ID_DAQ) != request->SourceAddress)
			return IDS_CAN_RESPONSE_SESSION_NOT_OPEN;
		if (channel >= IDS_CAN_NUM_DAQ_CHANNELS)
			return IDS_CAN_RESPONSE_VALUE_OUT_OF_RANGE;
		if (!Flags[channel].IsPID)
			SET_SIZE(channel, request->Data[1]);
		TxRate_ms[channel] = request->Data[2];
		break;
	}

	response->Length = 4;
	response->Data[0] = channel;
	response->Data[1] = GET_SIZE(channel);
	response->Data[2] = TxRate_ms[channel];
	response->Data[3] = (Flags[channel].IsPID ? 1 : 0);
	if (TxRate_ms[channel] && (AutoTxEnabled & (1 << channel))) response->Data[3] |= 0x01;
	if (Flags[channel].IsPID) response->Data[3] |= 0x02;

	return IDS_CAN_RESPONSE_SUCCESS;
}

// Read/Write PID to DAQ address
IDS_CAN_RESPONSE_CODE _IDS_CAN_DAQMessage54(IDS_CAN_DEVICE device, const IDS_CAN_RX_MESSAGE *request, CAN_TX_MESSAGE *response)
{
	uint8 channel = request->Data[0];
	uint16 pid;

	switch (request->Length)
	{
	default: return IDS_CAN_RESPONSE_BAD_REQUEST;
	case 1: // read
		if (channel >= IDS_CAN_NUM_DAQ_CHANNELS)
			return IDS_CAN_RESPONSE_VALUE_OUT_OF_RANGE;
		break;
	case 3: // write
		// commands require DIAGNOSTIC session access
		if (IDS_CAN_GetSessionOwner(device, IDS_CAN_SESSION_ID_DAQ) != request->SourceAddress)
			return IDS_CAN_RESPONSE_SESSION_NOT_OPEN;
		if (channel >= IDS_CAN_NUM_DAQ_CHANNELS)
			return IDS_CAN_RESPONSE_VALUE_OUT_OF_RANGE;
		pid = ((uint16)request->Data[1] << 8) | request->Data[2];
		if (!IDS_CAN_IsPIDSupported(pid))
			return IDS_CAN_RESPONSE_UNKNOWN_ID;
		Address[channel].PID = pid;
		SET_SIZE(channel, 6); /*lint !e506 Constant boolean in this macro.  This is fine, its used in other places with a non-constant parameter */
		Flags[channel].IsPID = TRUE;
		break;
	}

	// only report a PID when in PID mode
	if (!Flags[channel].IsPID)
		return IDS_CAN_RESPONSE_CONDITIONS_NOT_CORRECT;

	// trick - this also performs a quick memory peek
	{
		// transmit the response
		CAN_TX_MESSAGE tx;
		tx.ID = IDS_CAN_CreateCanId29(IDS_CAN_MESSAGE_TYPE_DAQ, IDS_CAN_GetDeviceAddress(device), request->SourceAddress, (uint8)((channel << 4) | 0xF));
		tx.Length = PeekMemory(device, channel, &tx.Data[0]);
		(void)IDS_CAN_Tx(device, &tx);
	}

	// report the PID
	response->Length = 3;
	response->Data[0] = channel;
	response->Data[1] = (uint8)(Address[channel].PID >> 8);
	response->Data[2] = (uint8)(Address[channel].PID >> 0); /*lint !e835 shifting by zero */

	return IDS_CAN_RESPONSE_SUCCESS;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

static IDS_CAN_RESPONSE_CODE ProcessRequest(IDS_CAN_DEVICE device, const IDS_CAN_RX_MESSAGE *rx)
{
	CAN_TX_MESSAGE tx;
	uint8 channel, mode;

	// sanity check
	ASSERT(device < NUM_IDS_CAN_DEVICES);
	if (device >= NUM_IDS_CAN_DEVICES)
		return IDS_CAN_RESPONSE_VALUE_OUT_OF_RANGE;

	// is the DAQ session open?
	if (ActiveDevice != device || HostAddress != rx->SourceAddress)
		return IDS_CAN_RESPONSE_SESSION_NOT_OPEN;

	// get target channel
	channel = (rx->ExtMessageData >> 4);
	if (channel >= IDS_CAN_NUM_DAQ_CHANNELS)
		return IDS_CAN_RESPONSE_VALUE_OUT_OF_RANGE;

	// parse message
	switch (rx->ExtMessageData & 0x0F)
	{
	default: return IDS_CAN_RESPONSE_SUCCESS; // ignore unknown messages

	case 0xC: // address
		if (rx->Length == 0)
		{
			// read address
			tx.Length = ReadAddress(channel, &tx.Data[0]);
			ASSERT(tx.Length >= 1 && tx.Length <= 8);
			mode = 0xD; // report address
		}
		else
		{
			// write address and fast peek
			WriteAddress(channel, rx);
			//lint -e801  so sue me
			goto peek_memory;
			//lint +e801  so sue me
			}
		break;

	case 0xE: // data peek/poke
		if (rx->Length > 0)
			PokeMemory(device, channel, rx);
peek_memory:
		tx.Length = PeekMemory(device, channel, &tx.Data[0]);
		ASSERT(tx.Length >= 1 && tx.Length <= 8);
		mode = 0xF;
		break;
	}

	// transmit the response
	tx.ID = IDS_CAN_CreateCanId29(IDS_CAN_MESSAGE_TYPE_DAQ, IDS_CAN_GetDeviceAddress(device), rx->SourceAddress, (uint8)(channel << 4) | mode);
	(void)IDS_CAN_Tx(device, &tx);

	return IDS_CAN_RESPONSE_SUCCESS;
}

static void TransmitErrorResponse(IDS_CAN_DEVICE device, const IDS_CAN_RX_MESSAGE *rx, IDS_CAN_RESPONSE_CODE response)
{
	CAN_TX_MESSAGE tx;
	tx.ID = IDS_CAN_CreateCanId29(IDS_CAN_MESSAGE_TYPE_DAQ, IDS_CAN_GetDeviceAddress(device), rx->SourceAddress, (rx->ExtMessageData & 0xF0) | 1);
	tx.Length = 1;
	tx.Data[0] = (uint8)response;
	(void)IDS_CAN_Tx(device, &tx);
}

void _IDS_CAN_OnDAQMessageRx(const IDS_CAN_RX_MESSAGE *rx)
{
	IDS_CAN_DEVICE device;
	IDS_CAN_RESPONSE_CODE response;

	ASSERT(rx->MessageType == IDS_CAN_MESSAGE_TYPE_DAQ);

	if (rx->IsEcho) return;

	// is this message for us?
	device = IDS_CAN_GetDeviceFromAddress(rx->TargetAddress);
	if (device >= NUM_IDS_CAN_DEVICES)
		return; // unrecognized

	// process request
	response = ProcessRequest(device, rx);
	if (response != IDS_CAN_RESPONSE_SUCCESS)
		TransmitErrorResponse(device, rx, response);
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

void _IDS_CAN_DAQ_Task(uint16 delta_ms)
{
	uint16 mask;
	uint8 ch;

	if (ActiveDevice >= NUM_IDS_CAN_DEVICES || HostAddress == IDS_CAN_ADDRESS_BROADCAST)
		return;
	if (!IDS_CAN_IsDeviceOnline(ActiveDevice))
	{
		// shouldn't get here
		_IDS_CAN_DAQ_Init();
		return;
	}

	// update all channels
	for (ch=0, mask=1; ch<IDS_CAN_NUM_DAQ_CHANNELS; ch++, mask <<= 1)
	{
		// is auto transmit on this channel active?
		if (AutoTxEnabled & mask)
		{
			// is auto transmit enabled?
			uint8 rate_ms = TxRate_ms[ch];
			if (rate_ms > 0)
			{
				// determine how much time has elapsed
				uint16 time_ms = delta_ms + Time_ms[ch];
				if (time_ms >= rate_ms)
				{
					// time to send another message
					uint16 count = time_ms / rate_ms;
					time_ms -= count * rate_ms;
					Flags[ch].Seq = ((uint8)Flags[ch].Seq + count) % SEQ_MODULUS; // goes from 0-11 
					PendingAutoTx |= mask; // flag for transmit
				}
				Time_ms[ch] = (uint8)time_ms;

				// transmit as needed
				if (PendingAutoTx & mask)
				{
					CAN_TX_MESSAGE tx;
					ASSERT(Flags[ch].Seq < SEQ_MODULUS);
					tx.ID = IDS_CAN_CreateCanId29(IDS_CAN_MESSAGE_TYPE_DAQ, IDS_CAN_GetDeviceAddress(ActiveDevice), HostAddress, (uint8)(ch << 4) | Flags[ch].Seq);
					tx.Length = PeekMemory(ActiveDevice, ch, &tx.Data[0]);
					if (IDS_CAN_Tx(ActiveDevice, &tx))
						PendingAutoTx &= ~mask;
				}
			}
		}
	}
}

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

#if defined(__GNUC__)
#pragma GCC diagnostic pop
#endif

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#endif // HEADER
