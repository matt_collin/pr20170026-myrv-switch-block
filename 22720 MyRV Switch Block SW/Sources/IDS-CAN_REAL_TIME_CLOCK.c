// IDS-CAN_REAL_TIME_CLOCK.c
// Real-time clock support for the IDS-CAN Network
// Version 2.8
// (c) 2017 Innovative Design Solutions, Inc.
// All rights reserved
//
// Description:
//    Implements the IDS-CAN TIME protocol
//
//    TIME messages are sent once per second by the time authority. Devices will synchronize their clock to the network time authority
//    Only the time authority shall transmit TIME, all other devices shall go into quiet (listen only) mode.
//
//    The time authority transmits the value TimeSinceClockSet indicating how recently the time was set on the authority.
//    If a device on the bus has a more recent set time than the network time authority, contention occurs.
//    During contention, both devices transmit their time message.
//    The device with the most recent set time wins contention and becomes the new network time authority.
//    The device that loses contention must  synchronize it's clock to the device set most recently, and go into quiet mode.
//
//    Therefore, after contention, all of the oldest clocks will stop transmitting, and only the most recent clock remains.
//    All clocks synchronize to this new time.
//
//    Whenever time is set locally on a device, its value of TimeSinceClockSet becomes zero, causing immediate contention with the time authority.
//
//    In the case that no clock messages are seen on the bus, each device will wait a random time, after which time it automatically becomes the new time authority
//    The randomness is added to reduce the likelihood that two devices will attempt to become the time authority simultaneously
//
// Usage:
//
//    There are two operating modes for this module:
//    - RTC hardware mode:
//      -- clock is backed by RTC hardware
//      -- user must supply RTC read/write functions, with the following signatures
//         --- typedef IDS_CAN_RTC_INFO (*RTC_READ)(void);          // reads the RTC hardware and returns IDS_CAN_RTC_INFO
//         --- typedef void (*RTC_WRITE)(const IDS_CAN_RTC_INFO *); // writes the RTC hardware from a pointer to IDS_CAN_RTC_INFO
//    - RAM only mode
//      -- no RTC hardware exists
//      -- clock is maintained in RAM (which implies the clock is reset after every boot)
//      -- clock is incremented every 1000 counts of the system millisecond clock
//
//    The code compiles into RAM mode by default.
//
//    To compile into RTC hardware mode, the user user must supply RTC read/write functions.
//    The functions have the following signatures
//         typedef IDS_CAN_RTC_INFO (*RTC_READ)(void);          // reads the RTC hardware and returns IDS_CAN_RTC_INFO
//         typedef void (*RTC_WRITE)(const IDS_CAN_RTC_INFO *); // writes the RTC hardware from a pointer to IDS_CAN_RTC_INFO
//    The user must define the following macros which link to the user supplied RTC_READ and RTC_WRITE functions
//         #define IDS_CAN_RTC_HARDWARE_READ()       (user_read_func())
//         #define IDS_CAN_RTC_HARDWARE_WRITE(ptr)   (user_write_func(ptr))

// History
// Version 2.6   Initial version supported in IDS-CAN v2.6 Mar 2016
// Version 2.7   Minor update
// Version 2.8   Removed _IDS_CAN_RTC_Init() which was empty
//               Linted

#include "global.h"

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#ifdef HEADER

// structure that holds clock information
typedef struct
{
	uint32 Epoch;        // the current time on the RTC, measured in seconds after Jan 1 2000, 12 midnight
	uint32 TimeSetEpoch; // the clock set time on the RTC, measured in seconds after Jan 1 2000, 12 midnight
	uint8 TimeZone;      // clock time zone
	uint8 IsValid;       // nonzero if the clock time is valid, zero otherwise
} IDS_CAN_RTC_INFO;

// structure that returns current date and time
typedef struct
{
	uint16 Year;
	uint8 Month;
	uint8 Day;
	uint8 Hour;
	uint8 Minute;
	uint8 Second;
} IDS_CAN_DATE_TIME;

// private functions do not call
void _IDS_CAN_RTC_Task10ms(uint16 clock_ms);
void _IDS_CAN_RTC_Task1sec(void);
void _IDS_CAN_RTC_OnTimeMessageRx(const IDS_CAN_RX_MESSAGE *message);

// user API

IDS_CAN_DATE_TIME IDS_CAN_DATE_TIME_FromEpoch(uint32 epoch); // converts an epoch to an IDS_CAN_DATE_TIME structure

uint8 IDS_CAN_RTC_IsTimeValid(void);             // non-zero if local clock is valid
uint8 IDS_CAN_RTC_IsTimeAuthority(void);         // non-zero if local clock is currently the time authority on the network
uint8 IDS_CAN_RTC_GetTimeZone(void);             // returns local clock time zone
uint32 IDS_CAN_RTC_GetTimeEpoch(void);           // returns local clock time, as number of seconds elapsed since Jan 1 2000 12:00:00 AM
uint32 IDS_CAN_RTC_GetTimeSetEpoch(void);        // returns the time the clock was last set, formatted same as IDS_CAN_RTC_GetTimeEpoch()
IDS_CAN_DATE_TIME IDS_CAN_RTC_GetDateTime(void); // returns the local clock time as an IDS_CAN_DATE_TIME structure

void IDS_CAN_RTC_SetTimeEpoch(uint32 epoch);     // sets the local clock time, also sets the "last time clock was set" time
void IDS_CAN_RTC_SetTimeZone(uint8 time_zone);   // sets the local time zone


#else // HEADER

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#if defined(__GNUC__)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunknown-pragmas"
#endif

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

// at least 3 messages should go out when we first become the authority
#define MINIMUM_AUTHORITY_TIME (1+3)

// leap year macro
//
// Year is a leap year if the year can be evenly divided by 4;
// If the year can be evenly divided by 100, it is NOT a leap year, unless;
// The year is also evenly divisible by 400. Then it is a leap year.
//
// so 2000 is a leap year, but 2100 is NOT a leap year
//
// since our epoch format only goes up to year 2136, we can ignore the 100/400 rule
#define IS_LEAP_YEAR(year)   ( (((year) & 3) == 0) && ((year) != 2100) )

// determine days per year
#define DAYS_IN_YEAR(year)   (uint16)(IS_LEAP_YEAR(year) ? 366 : 365)

static uint8 IsTimeAuthority = FALSE; // used as a counter, when becoming the time authority we use this to ignore messages for a little while
static uint8 ClockMessageTimeout = 0;

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// are we in RTC hardware mode, or RAM mode?
#ifdef IDS_CAN_RTC_HARDWARE_READ
	// RTC hardware mode
	#define ReadRTC()   IDS_CAN_RTC_HARDWARE_READ()
#else
	// RAM simulation mode
	static IDS_CAN_RTC_INFO RAM = { 0, 0, 0, FALSE };
	#define ReadRTC()  RAM
#endif // IDS_CAN_RTC_HARDWARE_READ

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// external API

// converts an epoch to an IDS_CAN_DATE_TIME structure
IDS_CAN_DATE_TIME IDS_CAN_DATE_TIME_FromEpoch(uint32 epoch)
{
	IDS_CAN_DATE_TIME result;
	uint32 temp;
	uint16 days;

	temp = epoch / 60; // temp = minutes
	result.Second = (uint8)(epoch - (temp * 60));

	epoch = temp / 60; // epoch = hours
	result.Minute = (uint8)(temp - (epoch * 60));

	days = (uint16)(epoch / 24); // now days, fits in uint16
	result.Hour = (uint8)(epoch - days * (uint32)24);

	// calculate year
	// jpm -- this is not well optimized
	result.Year = 2000;
	for (;;)
	{
		uint16 days_in_year = DAYS_IN_YEAR(result.Year);
		if (days < days_in_year)
			break;
		days -= days_in_year;
		result.Year++;
	}

	// calculate month
	if (IS_LEAP_YEAR(result.Year))
	{
		if      (days > 335) { days -= 335; result.Month = 12; }
		else if (days > 305) { days -= 305; result.Month = 11; }
		else if (days > 274) { days -= 274; result.Month = 10; }
		else if (days > 244) { days -= 244; result.Month = 9; }
		else if (days > 213) { days -= 213; result.Month = 8; }
		else if (days > 182) { days -= 182; result.Month = 7; }
		else if (days > 152) { days -= 152; result.Month = 6; }
		else if (days > 121) { days -= 121; result.Month = 5; }
		else if (days > 91)  { days -= 91;  result.Month = 4; }
		else if (days > 60)  { days -= 60;  result.Month = 3; }
		else if (days > 31)  { days -= 31;  result.Month = 2; }
		else                 {              result.Month = 1; }
	}
	else
	{
		if      (days > 334) { days -= 334; result.Month = 12; }
		else if (days > 304) { days -= 304; result.Month = 11; }
		else if (days > 273) { days -= 273; result.Month = 10; }
		else if (days > 243) { days -= 243; result.Month = 9; }
		else if (days > 212) { days -= 212; result.Month = 8; }
		else if (days > 181) { days -= 181; result.Month = 7; }
		else if (days > 151) { days -= 151; result.Month = 6; }
		else if (days > 120) { days -= 120; result.Month = 5; }
		else if (days > 90)  { days -= 90;  result.Month = 4; }
		else if (days > 59)  { days -= 59;  result.Month = 3; }
		else if (days > 31)  { days -= 31;  result.Month = 2; }
		else                 {              result.Month = 1; }
	}

	result.Day = (uint8)(days + 1); // add 1st of month

	return result;
}

uint8 IDS_CAN_RTC_IsTimeValid(void)             { return ReadRTC().IsValid; }
uint8 IDS_CAN_RTC_IsTimeAuthority(void)         { return IsTimeAuthority; }
uint8 IDS_CAN_RTC_GetTimeZone(void)             { return ReadRTC().TimeZone; }
uint32 IDS_CAN_RTC_GetTimeEpoch(void)           { return ReadRTC().Epoch; }
uint32 IDS_CAN_RTC_GetTimeSetEpoch(void)        { return ReadRTC().TimeSetEpoch; }
IDS_CAN_DATE_TIME IDS_CAN_RTC_GetDateTime(void) { return IDS_CAN_DATE_TIME_FromEpoch(ReadRTC().Epoch); }

// sets the local clock time, also sets the "last time clock was set" time
void IDS_CAN_RTC_SetTimeEpoch(uint32 epoch)
{
	IDS_CAN_RTC_INFO rtc = ReadRTC();
	rtc.Epoch = epoch;
	rtc.TimeSetEpoch = epoch;
	rtc.IsValid = TRUE;

#ifdef IDS_CAN_RTC_HARDWARE_READ
	IDS_CAN_RTC_HARDWARE_WRITE(&rtc);
#else
	RAM = rtc;
#endif // IDS_CAN_RTC_HARDWARE_READ
}

// sets the local time zone
void IDS_CAN_RTC_SetTimeZone(uint8 time_zone)
{
	IDS_CAN_RTC_INFO rtc = ReadRTC();
	rtc.TimeZone = time_zone;

#ifdef IDS_CAN_RTC_HARDWARE_READ
	IDS_CAN_RTC_HARDWARE_WRITE(&rtc);
#else
	RAM = rtc;
#endif // IDS_CAN_RTC_HARDWARE_READ
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// local support functions

#pragma INLINE
static void BecomeTimeAuthority(void)
{
	if (!IsTimeAuthority)
		IsTimeAuthority = MINIMUM_AUTHORITY_TIME;
}

// calculate 16-bit TIME_SINCE_CLOCK_SET value transmitted on the network
static uint16 TIME_SINCE_CLOCK_SET(const IDS_CAN_RTC_INFO * rtc)
{
	// TIME_SINCE_CLOCK_SET formatting
	// xxxxxxxx xxxxxxxx
	// |||||||| ||||||||
	// || \\\\\\_\\\\\\\\__ = 14 bits VALUE 0 - 16383
	// \\__________________ = 2 bits  UNITS (00 = sec, 01 = min, 10 = hours, 11 = days)

	uint32 delta;

	if (!rtc->IsValid)
		return 0xFFFF; // invalid

	delta = rtc->Epoch - rtc->TimeSetEpoch; // seconds
	if (delta < 0x4000)
		return (uint16)(0x0000 | delta); /*lint !e835 using zero */

	delta = delta / 60; // minutes
	if (delta < 0x4000)
		return (uint16)(0x4000 | delta);

	delta = delta / 60; // hours
	if (delta < 0x4000)
		return (uint16)(0x8000 | delta);

	delta = delta / 24; // days
	if (delta < 0x3FFF)
		return (uint16)(0xC000 | delta);

	return 0xFFFE; // max time
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// management routines

void _IDS_CAN_RTC_Task1sec(void)
{
#ifndef IDS_CAN_RTC_HARDWARE_READ
	// update RAM timer
	RAM.Epoch++;
#endif // IDS_CAN_RTC_HARDWARE_READ

	ClockMessageTimeout++;

	// if we aren't the time authority
	if (!IsTimeAuthority)
	{
#ifdef IDS_CAN_RTC_HARDWARE_READ
#define RANDOM_MISSING_COUNT (uint8)(4 + (Random8() & 3))
#else
#define RANDOM_MISSING_COUNT (uint8)(6 + (Random8() & 7))
#endif // IDS_CAN_RTC_HARDWARE_READ

		static uint8 MissingCount = 0;    // random time for authority timeout

		if (!MissingCount)
			MissingCount = RANDOM_MISSING_COUNT;

		// determine how long it has been since an authority has been seen on the bus
		if (ClockMessageTimeout > MissingCount)
		{
			// it's been too long -- become the new time authority
			BecomeTimeAuthority();
			MissingCount = RANDOM_MISSING_COUNT; // choose another random timeout for next time
		}
	}
}

// called from 10ms loop
void _IDS_CAN_RTC_Task10ms(uint16 clock_ms)
{
	IDS_CAN_DEVICE device;

	// if we aren't the time authority
	if (!IsTimeAuthority)
		return;

	// determine an address to transmit from
	device = IDS_CAN_GetAnyOnlineDevice();

	// can we transmit?
	if (device < NUM_IDS_CAN_DEVICES)
	{
		static uint32 LastTxEpoch = 0;
		static uint16 LastTxTime_ms = 0;

		// begin transmitting time once per second, and upon change
		IDS_CAN_RTC_INFO rtc = ReadRTC();
		if (rtc.Epoch != LastTxEpoch || (uint16)(clock_ms - LastTxTime_ms) >= 1100)
		{
			// create outgoing time message
			IDS_CAN_ADDRESS address = IDS_CAN_GetDeviceAddress(device);
			uint16 time_since_clock_set = TIME_SINCE_CLOCK_SET(&rtc);
			CAN_TX_MESSAGE msg;
			msg.Length = 8;
			msg.ID = IDS_CAN_CreateCanId11(IDS_CAN_MESSAGE_TYPE_TIME, address);
			msg.Data[0] = (uint8)(rtc.Epoch >> 24);
			msg.Data[1] = (uint8)(rtc.Epoch >> 16);
			msg.Data[2] = (uint8)(rtc.Epoch >> 8);
			msg.Data[3] = (uint8)(rtc.Epoch >> 0); /*lint !e835 shift by zero */
			msg.Data[4] = (uint8)(time_since_clock_set >> 8);
			msg.Data[5] = (uint8)(time_since_clock_set >> 0); /*lint !e835 shift by zero */
			msg.Data[6] = rtc.TimeZone;
#ifdef IDS_CAN_RTC_HARDWARE_READ
			msg.Data[7] = 0x01; // has RTC hardware
#else
			msg.Data[7] = 0x00; // does not have RTC hardware
#endif

			// and transmit it
			if (IDS_CAN_Tx(device, &msg))
			{
				if (IsTimeAuthority > 1)
					IsTimeAuthority--; // count down "ignore time"
				LastTxEpoch = rtc.Epoch;
				LastTxTime_ms = clock_ms;
			}
		}
	}
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// TIME message RX handling

#pragma INLINE
static uint8 ClocksAreMostlyInSync(uint32 epoch1, uint32 epoch2)
{
	if (epoch1 > epoch2)
		return ((uint32)(epoch1 - epoch2)) < 30;
	else
		return ((uint32)(epoch2 - epoch1)) < 30;
}

#pragma INLINE
static uint32 ConvertTimeSinceClockSetToSeconds(uint16 time_since_clock_set)
{
	uint32 mult;

	switch (time_since_clock_set & 0xC000)
	{
	case 0x0000: return time_since_clock_set;

	case 0x4000: mult = (uint32)60L; break; // minutes
	case 0x8000: mult = (uint32)60L * 60L; break; // hours
	default:
	case 0xC000: mult = (uint32)60L * 60L * 24L; break; // days
	}

	// upper bound of actual age
	return (((time_since_clock_set & 0x3FFF) + 1) * mult) - 1;
}

void _IDS_CAN_RTC_OnTimeMessageRx(const IDS_CAN_RX_MESSAGE *message)
{
	IDS_CAN_RTC_INFO rtc;
	DWORD epoch;
	WORD time_since_clock_set;

	// is this a valid TIME message?
	ASSERT(message->MessageType == IDS_CAN_MESSAGE_TYPE_TIME);

	if (message->IsEcho) return; // ignore echoes if we are the time authority
	if (message->Length != 8) return; // invalid TIME message

	// reset TimeAuthority timeout
	ClockMessageTimeout = 0;

	// unload the message payload
	epoch.word.hi.ui8.hi = message->Data[0];
	epoch.word.hi.ui8.lo = message->Data[1];
	epoch.word.lo.ui8.hi = message->Data[2];
	epoch.word.lo.ui8.lo = message->Data[3];
	time_since_clock_set.ui8.hi = message->Data[4];
	time_since_clock_set.ui8.lo = message->Data[5];

	// read the current RTC for comparison
	rtc = ReadRTC();

	// determine if we should sync to the network
	// - if our clock is close to network clock, no reason to assert authority, just sync and forget about it
	// - if our clock "set time" is older than the network "set time"
	if (ClocksAreMostlyInSync(rtc.Epoch, epoch.ui32) || (time_since_clock_set.ui16 <= TIME_SINCE_CLOCK_SET(&rtc)))
	{
		// we should sync

		// convert time_since_clock_set to seconds
		// due to resolution loss, we should estimate the high end of the actual value
		uint32 est_age_sec_hi = ConvertTimeSinceClockSetToSeconds(time_since_clock_set.ui16);

		// calculate estimated network clock RTC_SET_TIME_SEC, as time_since_clock_set is rough
		// need to estimate to "high end" RTC_SET_TIME_SEC,
		// to ensure local clock "set time" is always is older than authority on network
		uint32 rtc_set_time_sec = epoch.ui32 - est_age_sec_hi;

		// re-set the local clock set time if
		// - network clock set time is newer than our local clock set time
		// - local clock appears to be set in the future (impossible, likely network clock set back)
		if ((rtc_set_time_sec > rtc.TimeSetEpoch) || (epoch.ui32 < rtc.TimeSetEpoch))
			rtc.TimeSetEpoch = rtc_set_time_sec; // clock set time only changed when more recent

		rtc.Epoch = epoch.ui32;
		rtc.TimeZone = message->Data[6];
		rtc.IsValid = (time_since_clock_set.ui16 != 0xFFFF);

        // store the new clock value
#ifdef IDS_CAN_RTC_HARDWARE_READ
		IDS_CAN_RTC_HARDWARE_WRITE(&rtc);
#else
		RAM = rtc;
#endif // IDS_CAN_RTC_HARDWARE_READ


#ifdef IDS_CAN_RTC_HARDWARE_READ
		// if the network time authority doesn't support RTC hardware
		if (!(message->Data[7] & 1))
		{
			// we have RTC hardware and keep better time
			// we should take over being the authority
			BecomeTimeAuthority();
			return;
		}
#endif // IDS_CAN_RTC_HARDWARE_READ

		// because we need to sync, we are no longer the time authority
		// only do this after we've transmitted a few sync messages
		if (IsTimeAuthority <= 1)
			IsTimeAuthority = FALSE;
	}
	else
	{
		// we don't want to sync to this clock as we were set more recently
		// therefore, we should be the time authority
		BecomeTimeAuthority();
	}
}


////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

#if defined(__GNUC__)
#pragma GCC diagnostic pop
#endif

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#endif // HEADER
