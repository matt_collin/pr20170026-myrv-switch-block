// IDS-CAN DTC IDs.h
// Version 1.0
// (c) 2012 Innovative Design Solutions, Inc.
// All rights reserved

// History
//
// Version 1.0   First version

#ifndef IDS_CAN_DTC_IDS_H
#define IDS_CAN_DTC_IDS_H

typedef uint16 IDS_CAN_DTC;

#define IDS_CAN_DTC_NULL                                  0x0000
#define IDS_CAN_DTC_NETWORK_INVALID                       0x0001  /* Set when the bus master reports a CAN Network ID different than our own */
#define IDS_CAN_DTC_NETWORK_MISSING                       0x0002  /* Set if the network heartbeat is not present */
#define IDS_CAN_DTC_NETWORK_TIMEOUT                       0x0003  /* Set if the network heartbeat has timed out */

#endif // IDS_CAN_DTC_IDS_H
