// IDS-CAN BLOCK_ID.h
// Version 1.0
// (c) 2012, 2013 Innovative Design Solutions, Inc.
// All rights reserved

// History
//
// Version 1.0   First version

#ifndef IDS_CAN_BLOCK_IDS_H
#define IDS_CAN_BLOCK_IDS_H

typedef uint16 IDS_CAN_BLOCK_ID;

#define IDS_CAN_BLOCK_ID_NULL                                    0x0000
#define IDS_CAN_BLOCK_ID_TEXT_CONSOLE_LINE_1                     0x0001  /* raw ASCII encoded string */ 
#define IDS_CAN_BLOCK_ID_TEXT_CONSOLE_LINE_2                     0x0002  /* raw ASCII encoded string */ 
#define IDS_CAN_BLOCK_ID_TEXT_CONSOLE_LINE_3                     0x0003  /* raw ASCII encoded string */ 
#define IDS_CAN_BLOCK_ID_TEXT_CONSOLE_LINE_4                     0x0004  /* raw ASCII encoded string */ 
#define IDS_CAN_BLOCK_ID_TEXT_CONSOLE_LINE_5                     0x0005  /* raw ASCII encoded string */ 
#define IDS_CAN_BLOCK_ID_TEXT_CONSOLE_LINE_6                     0x0006  /* raw ASCII encoded string */ 
#define IDS_CAN_BLOCK_ID_TEXT_CONSOLE_LINE_7                     0x0007  /* raw ASCII encoded string */ 
#define IDS_CAN_BLOCK_ID_TEXT_CONSOLE_LINE_8                     0x0008  /* raw ASCII encoded string */ 
#define IDS_CAN_BLOCK_ID_TEXT_CONSOLE_LINE_9                     0x0009  /* raw ASCII encoded string */ 
#define IDS_CAN_BLOCK_ID_TEXT_CONSOLE_LINE_10                    0x000A  /* raw ASCII encoded string */ 
#define IDS_CAN_BLOCK_ID_TEXT_CONSOLE_LINE_11                    0x000B  /* raw ASCII encoded string */ 
#define IDS_CAN_BLOCK_ID_TEXT_CONSOLE_LINE_12                    0x000C  /* raw ASCII encoded string */ 
#define IDS_CAN_BLOCK_ID_TEXT_CONSOLE_LINE_13                    0x000D  /* raw ASCII encoded string */ 
#define IDS_CAN_BLOCK_ID_TEXT_CONSOLE_LINE_14                    0x000E  /* raw ASCII encoded string */ 
#define IDS_CAN_BLOCK_ID_TEXT_CONSOLE_LINE_15                    0x000F  /* raw ASCII encoded string */ 
#define IDS_CAN_BLOCK_ID_TEXT_CONSOLE_LINE_16                    0x0010  /* raw ASCII encoded string */ 
#define IDS_CAN_BLOCK_ID_TEXT_CONSOLE_LINE_17                    0x0011  /* raw ASCII encoded string */ 
#define IDS_CAN_BLOCK_ID_TEXT_CONSOLE_LINE_18                    0x0012  /* raw ASCII encoded string */ 
#define IDS_CAN_BLOCK_ID_TEXT_CONSOLE_LINE_19                    0x0013  /* raw ASCII encoded string */ 
#define IDS_CAN_BLOCK_ID_TEXT_CONSOLE_LINE_20                    0x0014  /* raw ASCII encoded string */ 
#define IDS_CAN_BLOCK_ID_TEXT_CONSOLE_LINE_21                    0x0015  /* raw ASCII encoded string */ 
#define IDS_CAN_BLOCK_ID_TEXT_CONSOLE_LINE_22                    0x0016  /* raw ASCII encoded string */ 
#define IDS_CAN_BLOCK_ID_TEXT_CONSOLE_LINE_23                    0x0017  /* raw ASCII encoded string */ 
#define IDS_CAN_BLOCK_ID_TEXT_CONSOLE_LINE_24                    0x0018  /* raw ASCII encoded string */ 
#define IDS_CAN_BLOCK_ID_TEXT_CONSOLE_LINE_25                    0x0019  /* raw ASCII encoded string */ 
#define IDS_CAN_BLOCK_ID_TEXT_CONSOLE_LINE_26                    0x001A  /* raw ASCII encoded string */ 
#define IDS_CAN_BLOCK_ID_TEXT_CONSOLE_LINE_27                    0x001B  /* raw ASCII encoded string */ 
#define IDS_CAN_BLOCK_ID_TEXT_CONSOLE_LINE_28                    0x001C  /* raw ASCII encoded string */ 
#define IDS_CAN_BLOCK_ID_TEXT_CONSOLE_LINE_29                    0x001D  /* raw ASCII encoded string */ 
#define IDS_CAN_BLOCK_ID_TEXT_CONSOLE_LINE_30                    0x001E  /* raw ASCII encoded string */ 
#define IDS_CAN_BLOCK_ID_TEXT_CONSOLE_LINE_31                    0x001F  /* raw ASCII encoded string */ 
#define IDS_CAN_BLOCK_ID_TEXT_CONSOLE_LINE_32                    0x0020  /* raw ASCII encoded string */

#endif // IDS_CAN_BLOCK_IDS_H
