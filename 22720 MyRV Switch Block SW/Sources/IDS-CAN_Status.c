// IDS CAN Status.c
// IDS CAN status message management
// Version 2.8
// (c) 2017 Innovative Design Solutions, Inc.
// All rights reserved

// History
// Version 2.7   First stable version
// Version 2.8   Minor bugfix

#include "global.h"

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#ifdef HEADER

// private functions - do NOT call
void _IDS_CAN_Status_Task(uint16 clock_ms);

// public API
void IDS_CAN_SetDeviceStatus(IDS_CAN_DEVICE device, uint8 length, const uint8 * status);

#else // HEADER

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#if defined(__GNUC__)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wmissing-braces"
#endif

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

typedef struct { uint8 Data[8]; } STATUS;

static uint8 Length[NUM_IDS_CAN_DEVICES] = {0};
static STATUS Status[NUM_IDS_CAN_DEVICES] = {0};

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

void IDS_CAN_SetDeviceStatus(IDS_CAN_DEVICE device, uint8 length, const uint8 * status)
{
	ASSERT(device < NUM_IDS_CAN_DEVICES);
	if (device < NUM_IDS_CAN_DEVICES)
	{
		ASSERT(status != NULL || length == 0);
		ASSERT(length <= 8);
		if (length > 8)
			length = 8; // no more than 8 data bytes
		Length[device] = length; // save length
		if (status != NULL && length)
			(void) memcpy(&Status[device].Data[0], status, length); // copy status to local structure
	}
}

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

void _IDS_CAN_Status_Task(uint16 clock_ms)
{
	static IDS_CAN_DEVICE Device = (IDS_CAN_DEVICE)0;
	static uint8 LastLength[NUM_IDS_CAN_DEVICES] = {0};
	static STATUS LastStatus[NUM_IDS_CAN_DEVICES] = {0};
	static uint16 LastTxTime[NUM_IDS_CAN_DEVICES] = {0};
	CAN_TX_MESSAGE msg;
	uint16 tx_rate_ms;

	// cycle through all devices round robin
	Device++;
	if (Device >= NUM_IDS_CAN_DEVICES)
		Device = (IDS_CAN_DEVICE)0;

	// get message length
	msg.Length = Length[Device];
	ASSERT(msg.Length <= 8);
	if (msg.Length > 8)
		msg.Length = 8; // prevent bad things from happening

	// if device is offline
	if (!IDS_CAN_IsDeviceOnline(Device))
	{
		// reset our TX cache
		LastLength[Device] = 0;
		LastTxTime[Device] = clock_ms ^ 0x8000;
		return;
	}

	// determine the transmit rate
	if (msg.Length != LastLength[Device] || memcmp(&Status[Device].Data[0], &LastStatus[Device].Data[0], msg.Length))
	{
		// status has changed -- fast update rate
		tx_rate_ms = (IDS_CAN_IsAnySessionOpen(Device) ? 50 : 333);
	}
	else
	{
		// status has not changed -- slow update rate
		tx_rate_ms = (IDS_CAN_IsAnySessionOpen(Device) ? 250 : 1000);
	}

	// is it time to transmit the status?
	if ((uint16)(clock_ms - LastTxTime[Device]) > tx_rate_ms)
	{
		// compose message and prepare transmit
		msg.ID = IDS_CAN_CreateCanId11(IDS_CAN_MESSAGE_TYPE_STATUS, IDS_CAN_GetDeviceAddress(Device));
		(void) memcpy(&msg.Data[0], &Status[Device].Data[0], msg.Length);
		if (IDS_CAN_Tx(Device, &msg))
		{
			// message successfully sent
			LastTxTime[Device] = OS_GetElapsedMilliseconds16(); // update time of last transmit
			LastLength[Device] = msg.Length;                    // remember last transmitted message length
			LastStatus[Device] = Status[Device];               // remember last transmitted status
		}
		else
		{
			// we failed to transmit, but a message needs to go out
			// to prevent any further delay, we will continue to try and transmit this message until it gets out
			// so, we need to stay stuck on this device
			// we perform a little trick, rolling back the device by one count
			// that way, next time the same device will be checked
			Device--;
		}
	}
}

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

#if defined(__GNUC__)
#pragma GCC diagnostic pop
#endif

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#endif // HEADER
