// Config_B.c
// Generic non-volatile configuration management with manual sync
// Version S12_B.1.9
// (c) 2008, 2009, 2012, 2013, 2017 Innovative Design Solutions, Inc.
// All rights reserved

// Features:
// - stores blocks values in any of the following
//   -- NVM/EEPROM memory (with checksum)
//   -- KAM "keep alive memory" (with CRC)
// - good for systems with small RAM
// - data cached in RAM for fast/direct access
// - multiple, separate config blocks, each with its own checksum/crc
// - manual cache flush (must call Config_blockname_Flush()

// History:
// 1.0 - first release
// 1.1 - changes to make compatible with Cosmic STM8 compiler
// 1.2 - added "pre-erase" function, to set bytes to erased value in order to
//       minimize overall programming time
// 1.3 - made compatible with older HC08 platforms
//       changed way config blocks are declared
//       up to 32 configuration blocks are supported
//       added optional KAM storage, selectable by block
//       made NVM support optional, selectable by block
// 1.4   added support for hamming-encoded NVM via
//       #define CONFIG_NVM_USE_HAMMING_CODE
// 1.5   Removed Copy() routine, replaced with AtomicRead()
// 1.6   Changes to make compatible with STM8
// 1.7   Moved KAM variables to NO_INIT_RAM
// 1.8   Bug fix to KAM
// 1.9   Linted

// Notes:
//
// - Up to 32 unique/independent "config blocks" can be defined by the user
//
// - Blocks are defined in the DECLARE_CONFIGURATION macro, which looks like
//   the following:
//       #define DECLARE_CONFIGURATION                          \
//                                                              \
//       CONFIG_BLOCK(block_name_1, nvm_option_1, kam_option_1) \                                         \
//           CONFIG(var_type1, var_name1)                       \
//           CONFIG(var_type2, var_name2)                       \
//           ...                                                \
//                                                              \
//       CONFIG_BLOCK(block_name_2, nvm_option_2, kam_option_2) \
//           CONFIG(var_type3, var_name3)                       \
//           CONFIG(var_type4, var_name4)                       \
//           ...                                                \
//                                                              \
//       ...
//
//   Where:
//       CONFIG_BLOCK(block_name, nvm_option, kam_option)
//           Declares a new block
//               block_name = is a unique name for the block
//               nvm_option = either USE_NVM or NO_NVM
//               kam_option = either USE_KAM or NO_KAM
//       CONFIG(var_type, var_name)
//           Declares a variable in the block
//               var_type = variable type (int, char, etc...)
//               var_name = unique name for variable
//
// - All blocks are cached in RAM for fast access.  Variable is accessed at
//       Config.block_name.var_name
//
// - If USE_NVM is specified, then block can be flushed to NVM memory.  Block
//   is reloaded from NVM at boot, provided a valid KAM copy does not exist.
//
//   Each block has it's own independent cache, and can be flushed independently
//   from the others.  User manually controls cache flushes by calling
//       Config_blockname_Flush()
//   Note that this function also forces a flush to KAM.
//
//   To speed flushing, a block can be pre-erased prior to the flush by calling
//       Config_blockname_EraseNVM()
//
//   Blocks are written to NVM in the following format:
//       checksum data....
//   Where:
//       checksum = value used to ensure that data has been stored properly
//       data... = user data to store
//
//   If CONFIG_NVM_USE_HAMMING_CODE is defined, then each data byte is stored
//   in NVM as two separate hamming encoded nibbles
//      NVM byte 1 = HammingEncode(data byte - high nibble)
//      NVM byte 2 = HammingEncode(data byte - low nibble)
//
// - If USE_KAM is specified, then block can be flushed to KAM memory.  This
//   copy helps protect against unexpected RESET situations.  Block is reloaded
//   from KAM at boot, provided a valid copy exists.  If KAM is invalid, the
//   system attempts to load a backup copy from NVM.
//
//   Each block has it's own independent cache, and can be flushed independently
//   from the others.  User manually controls cache flushes by calling
//       Config_blockname_FlushKAM()
//
//   Note that KAM is also flushed whenever NVM is flushed via a call to
//       Config_blockname_Flush()
//
//   Config data is written to KAM in the following format:
//       crc data....
//   Where:
//       crc = value used to ensure that data has been stored properly
//       data... = user data to store
//
// - Cache flushes are prioritized according to the order that the blocks
//   are declared in DECLARE_CONFIGURATION
//
//         1st CONFIG_BLOCK   highest priority flush
//         2nd CONFIG_BLOCK
//         ...
//         nth CONFIG_BLOCK   lowest priority flush
//

#include "global.h"

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#ifdef HEADER

// management tasks
void Config_Init(void);
void Config_Task(void);

// create a structure for each config block
/*lint -esym(758,_unused) The macro below needs an unused struct at the beginning to get formatting correct */
#undef CONFIG_BLOCK
#define CONFIG_BLOCK(name, nvm, kam)  }; struct CONFIG_##name { uint8 _Checksum;
#undef CONFIG
#define CONFIG(size, name)            size name;
struct _unused { uint8 null; DECLARE_CONFIGURATION };
#undef CONFIG
#define CONFIG(size, name)

// create a structure that holds all configuration data
typedef struct {
	#undef CONFIG_BLOCK
	#define CONFIG_BLOCK(name, nvm, kam)  struct CONFIG_##name name;
	DECLARE_CONFIGURATION
} CONFIG_STRUCTURE;

// provide external, memory mapped access to the configuration data structure
#pragma DATA_SEG NO_INIT_RAM
extern CONFIG_STRUCTURE Config;
#pragma DATA_SEG DEFAULT

// full block flush
#undef CONFIG_BLOCK
#define CONFIG_BLOCK(name, nvm, kam)  void Config_##name##_Flush(void);
DECLARE_CONFIGURATION

// pre-erase the NVM -- prepare it for fast synchronization
#undef USE_NVM
#define USE_NVM(name)                 void Config_##name##_EraseNVM(void);
#undef NO_NVM
#define NO_NVM(name)
#undef CONFIG_BLOCK
#define CONFIG_BLOCK(name, nvm, kam)  nvm(name)
DECLARE_CONFIGURATION

// flush KAM only
#undef USE_KAM
#define USE_KAM(name)                 void Config_##name##_FlushKAM(void);
#undef NO_KAM
#define NO_KAM(name)
#undef CONFIG_BLOCK
#define CONFIG_BLOCK(name, nvm, kam)  kam(name)
DECLARE_CONFIGURATION

// prototype external block init callback functions
// these functions are not part of this module, but need to be created by the app
// if the app does not create these functions you get a linker error
#undef CONFIG_BLOCK
#define CONFIG_BLOCK(name, nvm, kam)  void Callback_OnConfig##name##LoadEvent(uint8 success);
DECLARE_CONFIGURATION

#else // HEADER

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#define CHECKSUM 0x69

// enumerate config block names
typedef enum {
	// NVM blocks go first, since we need to use block name as index into NVM arrays
	#undef USE_NVM
	#define USE_NVM(name)                 name,
	#undef NO_NVM
	#define NO_NVM(name)
	#undef CONFIG_BLOCK
	#define CONFIG_BLOCK(name, nvm, kam)  nvm(name)
	DECLARE_CONFIGURATION

	// non-NVM blocks go at the end
	#undef USE_NVM
	#define USE_NVM(name)
	#undef NO_NVM
	#define NO_NVM(name)                  name,
	#undef CONFIG_BLOCK
	#define CONFIG_BLOCK(name, nvm, kam)  nvm(name)
	DECLARE_CONFIGURATION

	NUM_CONFIG_BLOCKS
} BLOCK_NAME;

// determine number of config blocks
#undef CONFIG_BLOCK
#define CONFIG_BLOCK(name, nvm, kam)  +1
#define _NUM_CONFIG_BLOCKS (0 DECLARE_CONFIGURATION)

// block flag type - each block gets 1 bit
#if _NUM_CONFIG_BLOCKS <= 8
typedef uint8 BLOCK_FLAGS; // up to 8 flags
#elif _NUM_CONFIG_BLOCKS <= 16
typedef uint16 BLOCK_FLAGS; // up to 16 flags
#elif _NUM_CONFIG_BLOCKS <= 32
typedef uint32 BLOCK_FLAGS; // up to 32 flags
#else
	#error Too many configuration blocks defined -- maximum of 32 supported
#endif

// determine if we need to support NVM
#undef USE_NVM
#define USE_NVM(name)                 +1
#undef NO_NVM
#define NO_NVM(name)
#undef CONFIG_BLOCK
#define CONFIG_BLOCK(name, nvm, kam)  nvm(name)
#define _NUM_NVM_BLOCKS (0 DECLARE_CONFIGURATION)
#if _NUM_NVM_BLOCKS > 0
  #define INCLUDE_NVM_SUPPORT
#else
  #undef INCLUDE_NVM_SUPPORT
#endif

// determine if we need to support KAM
#undef USE_KAM
#define USE_KAM(name)                 +1
#undef NO_KAM
#define NO_KAM(name)
#undef CONFIG_BLOCK
#define CONFIG_BLOCK(name, nvm, kam)  kam(name)
#define _NUM_KAM_BLOCKS (0 DECLARE_CONFIGURATION)
#if _NUM_KAM_BLOCKS > 1
  #define INCLUDE_KAM_SUPPORT 2
#elif _NUM_KAM_BLOCKS > 0
  #define INCLUDE_KAM_SUPPORT 1
#else
  #undef INCLUDE_KAM_SUPPORT
#endif

//////////
//////////
//////////
//////////

// block flag lookup
static const BLOCK_FLAGS Flag[NUM_CONFIG_BLOCKS] = {
	// NVM blocks go first, since we need to use block name as index into NVM arrays
	#undef USE_NVM
	#define USE_NVM(name)                 (BLOCK_FLAGS)((BLOCK_FLAGS)1 << ((uint8)name)),
	#undef NO_NVM
	#define NO_NVM(name)
	#undef CONFIG_BLOCK
	#define CONFIG_BLOCK(name, nvm, kam)  nvm(name)
	DECLARE_CONFIGURATION

	// non-NVM blocks go at the end
	#undef USE_NVM
	#define USE_NVM(name)
	#undef NO_NVM
	#define NO_NVM(name)                  (BLOCK_FLAGS)((BLOCK_FLAGS)1 << ((uint8)name)),
	#undef CONFIG_BLOCK
	#define CONFIG_BLOCK(name, nvm, kam)  nvm(name)
	DECLARE_CONFIGURATION
};

// block size lookup table
static const uint8 BlockSize[NUM_CONFIG_BLOCKS] = {
	// NVM blocks go first, since we need to use block name as index into NVM arrays
	#undef USE_NVM
	#define USE_NVM(name)                 sizeof(Config.name),
	#undef NO_NVM
	#define NO_NVM(name)
	#undef CONFIG_BLOCK
	#define CONFIG_BLOCK(name, nvm, kam)  nvm(name)
	DECLARE_CONFIGURATION

	// non-NVM blocks go at the end
	#undef USE_NVM
	#define USE_NVM(name)
	#undef NO_NVM
	#define NO_NVM(name)                  sizeof(Config.name),
	#undef CONFIG_BLOCK
	#define CONFIG_BLOCK(name, nvm, kam)  nvm(name)
	DECLARE_CONFIGURATION
};

// allocate RAM cache
#pragma DATA_SEG NO_INIT_RAM
CONFIG_STRUCTURE Config;
#pragma DATA_SEG DEFAULT

// RAM cache pointer lookup table
static uint8 * const pRAM[NUM_CONFIG_BLOCKS] = {
	// NVM blocks go first, since we need to use block name as index into NVM arrays
	#undef USE_NVM
	#define USE_NVM(name)                 (uint8 *)&Config.name,
	#undef NO_NVM
	#define NO_NVM(name)
	#undef CONFIG_BLOCK
	#define CONFIG_BLOCK(name, nvm, kam)  nvm(name)
	DECLARE_CONFIGURATION

	// non-NVM blocks go at the end
	#undef USE_NVM
	#define USE_NVM(name)
	#undef NO_NVM
	#define NO_NVM(name)                 (uint8 *)&Config.name,
	#undef CONFIG_BLOCK
	#define CONFIG_BLOCK(name, nvm, kam)  nvm(name)
	DECLARE_CONFIGURATION
};

//////////
//////////
//////////
//////////

// NVM enumeration, allocation, and lookup tables

#ifdef INCLUDE_NVM_SUPPORT

// determine number of NVM blocks
typedef enum {
	#undef USE_NVM
	#define USE_NVM(name)                 NVM_BLOCK_##name,
	#undef NO_NVM
	#define NO_NVM(name)
	#undef CONFIG_BLOCK
	#define CONFIG_BLOCK(name, nvm, kam)  nvm(name)
	DECLARE_CONFIGURATION
	NUM_NVM_BLOCKS
} NVM_BLOCK;

#ifdef CONFIG_NVM_USE_HAMMING_CODE
  #define NVM_BYTE_SIZE(ram_bytes) ((ram_bytes) << 1) /* 2 bytes in NVM for every byte in RAM */
#else
  #define NVM_BYTE_SIZE(ram_bytes) (ram_bytes) /* 1 byte in NVM for every byte in RAM */
#endif

// emit flush indices
// index = next location in block to flush
// if index is out of range, then the block does not require a flush
static uint8 NVM_FlushIndex[NUM_NVM_BLOCKS] = {
  #undef USE_NVM
	#define USE_NVM                       0xFF,
	#undef NO_NVM
	#define NO_NVM
	#undef CONFIG_BLOCK
	#define CONFIG_BLOCK(name, nvm, kam)  nvm
	DECLARE_CONFIGURATION };

// set these flags to pre-erase the NVM block
static BLOCK_FLAGS NVM_EraseFlags = (BLOCK_FLAGS)0;

#endif // INCLUDE_NVM_SUPPORT

//////////
//////////
//////////
//////////

// KAM enumeration, allocation, and lookup tables

#ifdef INCLUDE_KAM_SUPPORT

// determine number of KAM blocks
typedef enum {
	#undef USE_KAM
	#define USE_KAM(name)                 KAM_BLOCK_##name,
	#undef NO_KAM
	#define NO_KAM(name)
	#undef CONFIG_BLOCK
	#define CONFIG_BLOCK(name, nvm, kam)  kam(name)
	DECLARE_CONFIGURATION
	NUM_KAM_BLOCKS
} KAM_BLOCK;

// when flag set, the block is tagged for flushing
static BLOCK_FLAGS KAMFlushFlags = (BLOCK_FLAGS)0;

// RAM/KAM lock flags
// used to tell whether RAM or KAM holds a valid copy of data
// 0 = RAM is locked, KAM is unlocked  (data can be written to KAM)
// 1 = RAM is unlocked, KAM is locked  (data can be written to RAM)
#pragma DATA_SEG NO_INIT_RAM
static BLOCK_FLAGS KAMLockFlags; // uninitialized -- keeps it's state between boots
#pragma DATA_SEG DEFAULT

// allocate KAM memory
#pragma DATA_SEG NO_INIT_RAM
static struct
{
	#undef USE_KAM
	#define USE_KAM(name)                 uint8 name[sizeof(Config.name)];
	#undef NO_KAM
	#define NO_KAM(name)
	#undef CONFIG_BLOCK
	#define CONFIG_BLOCK(name, nvm, kam)  kam(name)
	DECLARE_CONFIGURATION
} _KAM;
#pragma DATA_SEG DEFAULT

// create KAM block pointers
static uint8 * const pKAM[NUM_CONFIG_BLOCKS] = {
	#undef USE_KAM
	#define USE_KAM(name)                       _KAM.name,
	#undef NO_KAM
	#define NO_KAM(name)                        NULL,

	// NVM blocks go first, since we need to use block name as index into NVM arrays
	#undef USE_NVM
	#define USE_NVM(name, kam)            kam(name)
	#undef NO_NVM
	#define NO_NVM(name, kam)
	#undef CONFIG_BLOCK
	#define CONFIG_BLOCK(name, nvm, kam)  nvm(name, kam)
	DECLARE_CONFIGURATION

	// non-NVM blocks go at the end
	#undef USE_NVM
	#define USE_NVM(name, kam)
	#undef NO_NVM
	#define NO_NVM(name, kam)             kam(name)
	#undef CONFIG_BLOCK
	#define CONFIG_BLOCK(name, nvm, kam)  nvm(name, kam)
	DECLARE_CONFIGURATION
};

#endif // INCLUDE_KAM_SUPPORT

//////////
//////////
//////////
//////////

// protptype callback function pointer
typedef void (*CALLBACK)(uint8);

// callbackup lookup table
static const CALLBACK Callback[NUM_CONFIG_BLOCKS] = {
	// NVM blocks go first, since we need to use block name as index into NVM arrays
	#undef USE_NVM
	#define USE_NVM(name)                 Callback_OnConfig##name##LoadEvent,
	#undef NO_NVM
	#define NO_NVM(name)
	#undef CONFIG_BLOCK
	#define CONFIG_BLOCK(name, nvm, kam)  nvm(name)
	DECLARE_CONFIGURATION

	// non-NVM blocks go at the end
	#undef USE_NVM
	#define USE_NVM(name)
	#undef NO_NVM
	#define NO_NVM(name)                  Callback_OnConfig##name##LoadEvent,
	#undef CONFIG_BLOCK
	#define CONFIG_BLOCK(name, nvm, kam)  nvm(name)
	DECLARE_CONFIGURATION
};

//////////
//////////
//////////
//////////

// block flush

#ifdef INCLUDE_KAM_SUPPORT
	#define FLUSH_KAM(name)  KAMFlushFlags |= Flag[name]
#endif

#ifdef INCLUDE_NVM_SUPPORT
	#define ERASE_NVM(name) NVM_EraseFlags |=                Flag[name] ; NVM_FlushIndex[name] = (uint8)(BlockSize[name] - 1);
	#define FLUSH_NVM(name) NVM_EraseFlags &= (BLOCK_FLAGS)(~Flag[name]); NVM_FlushIndex[name] = (uint8)(BlockSize[name] - 1);
#endif


// to flush a block -- set the flush index to the last byte in the block
// full block flush
#undef USE_NVM
#define USE_NVM(name)                 FLUSH_NVM(name)
#undef NO_NVM
#define NO_NVM(name)
#undef USE_KAM
#define USE_KAM(name)                 FLUSH_KAM(name)
#undef NO_KAM
#define NO_KAM(name)
#undef CONFIG_BLOCK
#define CONFIG_BLOCK(name, nvm, kam)  void Config_##name##_Flush(void) { kam(name); nvm(name); }
DECLARE_CONFIGURATION

// erase the NVM -- prepare it for fast flush at a later time
#undef USE_NVM
#define USE_NVM(name)                 void Config_##name##_EraseNVM(void) { ERASE_NVM(name); }
#undef NO_NVM
#define NO_NVM(name)
#undef CONFIG_BLOCK
#define CONFIG_BLOCK(name, nvm, kam)  nvm(name)
DECLARE_CONFIGURATION

// flush KAM only
#undef USE_KAM
#define USE_KAM(name)                 void Config_##name##_FlushKAM(void) { FLUSH_KAM(name); }
#undef NO_KAM
#define NO_KAM(name)
#undef CONFIG_BLOCK
#define CONFIG_BLOCK(name, nvm, kam)  kam(name)
DECLARE_CONFIGURATION

//////////
//////////
//////////
//////////

#ifdef INCLUDE_KAM_SUPPORT

static void FlushKAM(BLOCK_NAME block)
{
	// range check
	if (block < NUM_CONFIG_BLOCKS)
	{
		// clear KAM flush flag
		uint8 flag = Flag[block];
		KAMFlushFlags &= (BLOCK_FLAGS)(~flag);

		// does this block have KAM?
		if (pKAM[block])
		{
			uint8* ram = pRAM[block]; // locate RAM associated with this block
			uint8 save = ram[0]; // RAM CRC overwrites RAM checksum, so... we need to save and restore this value
			uint8 size = BlockSize[block]; // get size of block

			// calculate RAM CRC
			ram[0] = CRC8_Calculate(ram + 1, size - 1);

			// lock RAM, unlock KAM
			KAMLockFlags &= (BLOCK_FLAGS)(~flag);

			// copy RAM to KAM
			AtomicRead(ram, pKAM[block], size);
	
			// unlock RAM, lock KAM
			KAMLockFlags |= flag;

			// restore original value in RAM
			ram[0] = save;
		}
	}
}
#endif // INCLUDE_KAM_SUPPORT

#ifdef INCLUDE_NVM_SUPPORT
static uint16 ReadNVMByte(uint16 index)
{
#ifdef CONFIG_NVM_USE_HAMMING_CODE
	// this corrects for single bit errors in 84 code
	// double bit errors are detected (255 is returned)
	static const uint8 Decode[256] = {
		0x1, 255, 0x1, 0x1, 255, 0x0, 0x1, 255, 255, 0x2, 0x1, 255, 0xA, 255, 255, 0x7,
		255, 0x0, 0x1, 255, 0x0, 0x0, 255, 0x0, 0x6, 255, 255, 0xB, 255, 0x0, 0x3, 255,
		255, 0xC, 0x1, 255, 0x4, 255, 255, 0x7, 0x6, 255, 255, 0x7, 255, 0x7, 0x7, 0x7,
		0x6, 255, 255, 0x5, 255, 0x0, 0xD, 255, 0x6, 0x6, 0x6, 255, 0x6, 255, 255, 0x7,
		255, 0x2, 0x1, 255, 0x4, 255, 255, 0x9, 0x2, 0x2, 255, 0x2, 255, 0x2, 0x3, 255,
		0x8, 255, 255, 0x5, 255, 0x0, 0x3, 255, 255, 0x2, 0x3, 255, 0x3, 255, 0x3, 0x3,
		0x4, 255, 255, 0x5, 0x4, 0x4, 0x4, 255, 255, 0x2, 0xF, 255, 0x4, 255, 255, 0x7,
		255, 0x5, 0x5, 0x5, 0x4, 255, 255, 0x5, 0x6, 255, 255, 0x5, 255, 0xE, 0x3, 255,
		255, 0xC, 0x1, 255, 0xA, 255, 255, 0x9, 0xA, 255, 255, 0xB, 0xA, 0xA, 0xA, 255,
		0x8, 255, 255, 0xB, 255, 0x0, 0xD, 255, 255, 0xB, 0xB, 0xB, 0xA, 255, 255, 0xB,
		0xC, 0xC, 255, 0xC, 255, 0xC, 0xD, 255, 255, 0xC, 0xF, 255, 0xA, 255, 255, 0x7,
		255, 0xC, 0xD, 255, 0xD, 255, 0xD, 0xD, 0x6, 255, 255, 0xB, 255, 0xE, 0xD, 255,
		0x8, 255, 255, 0x9, 255, 0x9, 0x9, 0x9, 255, 0x2, 0xF, 255, 0xA, 255, 255, 0x9,
		0x8, 0x8, 0x8, 255, 0x8, 255, 255, 0x9, 0x8, 255, 255, 0xB, 255, 0xE, 0x3, 255,
		255, 0xC, 0xF, 255, 0x4, 255, 255, 0x9, 0xF, 255, 0xF, 0xF, 255, 0xE, 0xF, 255,
		0x8, 255, 255, 0x5, 255, 0xE, 0xD, 255, 255, 0xE, 0xF, 255, 0xE, 0xE, 255, 0xE };

	WORD msb, lsb;

	// it might take a long time to read from some offboard EEPROMs
	_FEED_COP(); 

	// attempt to read the hamming encoded MSB and LSB
	do
	{
		msb.ui16 = EEPROM_Read(index);
	} while (msb.ui8.hi); // continue until we finally read, or watchdog resets us
	do
	{
		lsb.ui16 = EEPROM_Read(index+1);
	} while (lsb.ui8.hi); // continue until we finally read, or watchdog resets us

	// decode the byte, check for double bit errors
	msb.ui8.lo = Decode[msb.ui8.lo];
	lsb.ui8.lo = Decode[lsb.ui8.lo];
	if ((msb.ui8.lo | lsb.ui8.lo) & 0xF0)
		return 0xFFFF; // double bit error!

	return (uint8)(msb.ui8.lo << 4) | lsb.ui8.lo;

#else
	// attempt to read the byte
	WORD result;

	// it might take a long time to read from some offboard EEPROMs
	_FEED_COP(); 

	do
	{
		result.ui16 = EEPROM_Read(index);
	} while (result.ui8.hi); // continue until we finally read, or watchdog resets us

	return result.ui8.lo;
#endif
}
#endif // INCLUDE_NVM_SUPPORT

void Config_Init(void)
{
	#ifdef INCLUDE_KAM_SUPPORT
	#pragma DATA_SEG NO_INIT_RAM
	static uint16 POR; // used to check for POR
	#pragma DATA_SEG DEFAULT
	#endif // INCLUDE_KAM_SUPPORT
	BLOCK_NAME block;

	#ifdef INCLUDE_NVM_SUPPORT
	uint16 nvm_index = 0;
	#endif // INCLUDE_NVM_SUPPORT

	// cycle through all blocks
	for (block=(BLOCK_NAME)0; block < NUM_CONFIG_BLOCKS; block++)
	{
		uint8 * dst = pRAM[block];
		uint8 size = BlockSize[block];
		uint8 success = 0;

		// can the block be loaded from KAM?
		#ifdef INCLUDE_KAM_SUPPORT
		const uint8 * src = pKAM[block];
		if (src && POR == 0x55AA)
		{
			// load from KAM if it is locked
			// if KAM is unlocked, then load from RAM
			if (!(KAMLockFlags & Flag[block]))
				src = dst; // load from RAM

			// is the CRC valid?
			if (src[0] == CRC8_Calculate(src + 1, size - 1))
			{
				// KAM is valid
				AtomicRead(src, dst, size); // initialize RAM with KAM copy
				success = 0x80; // successfully loaded from KAM
			}
		}
		#endif // INCLUDE_KAM_SUPPORT

		// manage NVM
		#ifdef INCLUDE_NVM_SUPPORT
		if ((uint8)block < (uint8)NUM_NVM_BLOCKS)
		{
			// if block has not yet been loaded from KAM
			if (!success) /*lint !e774 Depending on if KAM support is included, this might always be True */
			{
				// read the block from NVM
				uint8 checksum = 0;
				uint16 index = nvm_index;
				for (;;)
				{
					WORD result;
					result.ui16 = ReadNVMByte(index);
					if (result.ui8.hi)
						break;

					// store byte
					*(dst++) = result.ui8.lo;

					// update checksum
					checksum += result.ui8.lo;

					// abort loop when done
					if (!--size)
					{
						success = (uint8)(checksum == CHECKSUM); // pass or fail?
						break;
					}

					// move to next byte
					index += NVM_BYTE_SIZE(1);
				}
			}

			// adjust index to next block			
			nvm_index += NVM_BYTE_SIZE(BlockSize[block]);
		}
		#endif // INCLUDE_NVM_SUPPORT

		// indicate block load success/failure to application
		Callback[block](success);

		// now update KAM with the current state of RAM
		// do this after Callback() -- because application might decide to change values on us!
		#ifdef INCLUDE_KAM_SUPPORT
		if (success)
			FlushKAM(block);
		#endif // INCLUDE_KAM_SUPPORT
	}

	// set the POR bit, so we can recognize warm RESETs
	#ifdef INCLUDE_KAM_SUPPORT
	POR = 0x55AA;
	#endif // INCLUDE_KAM_SUPPORT
}

//////////
//////////
//////////
//////////

// background synchronization task
void Config_Task(void)
{
	#ifdef INCLUDE_NVM_SUPPORT
	BLOCK_NAME block;
	uint16 nvm_index; 
	#endif // INCLUDE_NVM_SUPPORT

	// flush KAM first, since this occurrs quickly
	#ifdef INCLUDE_KAM_SUPPORT
	#if INCLUDE_KAM_SUPPORT > 1
	// check if any blocks need flushing -- this checks all blocks
	if (KAMFlushFlags & (0
		#undef USE_KAM
		#define USE_KAM(name)                 | Flag[name]
		#undef NO_KAM
		#define NO_KAM(name)
		#undef CONFIG_BLOCK
		#define CONFIG_BLOCK(name, nvm, kam)  kam(name)
		DECLARE_CONFIGURATION ))
	#endif // INCLUDE_KAM_SUPPORT > 1
	{
		// check if any blocks need flushing -- check one block at a time
		#undef USE_KAM
		#define USE_KAM(name)                 if (KAMFlushFlags & Flag[name]) { FlushKAM(name); return; }
		#undef NO_KAM
		#define NO_KAM(name)
		#undef CONFIG_BLOCK
		#define CONFIG_BLOCK(name, nvm, kam)  kam(name)
		DECLARE_CONFIGURATION
	}
	#endif // INCLUDE_KAM_SUPPORT

	// flush NVM last, since this takes a long time
	#ifdef INCLUDE_NVM_SUPPORT
	nvm_index = 0;
	for (block=(BLOCK_NAME)0; block < (BLOCK_NAME)NUM_NVM_BLOCKS; nvm_index += NVM_BYTE_SIZE(BlockSize[block++]))  /*lint !e440 !e441 !e443 Lint thinks nvm_index is the loop variable */
	{
		// is this block being flushed?
		uint8 flushindex = NVM_FlushIndex[block];
		if (flushindex < BlockSize[block])
		{
#ifdef CONFIG_NVM_USE_HAMMING_CODE
			static const uint8 Encode[16] = {
				0x15, 0x02, 0x49, 0x5E, 0x64, 0x73, 0x38, 0x2F,
				0xD0, 0xC7, 0x8C, 0x9B, 0xA1, 0xB6, 0xFD, 0xEA };

			uint8* ram = pRAM[block]; // locate base address of block RAM
			uint8 data = ram[flushindex]; // get cached byte
			uint8 msb = Encode[data >> 4];
			uint8 lsb = Encode[data & 0xF];
			 
			// if we are pre-erasing NVM
			if (NVM_EraseFlags & Flag[block])
			{
				// then erase the cell
				msb = lsb = (uint8)(EEPROM_ERASED_STATE ? 0xFF : 0); /*lint !e506 EEPROM_ERASED_STATE is a configuration option, so it will always be TRUE/FALSE */
			}

			// flush to NVM
			nvm_index += flushindex << 1; // offset to byte to write
			if (EEPROM_Sync(msb, nvm_index) && EEPROM_Sync(lsb, nvm_index + 1))
			{
				// synchronization was successfull

				// re-initialize checksum when starting a flush
				if (flushindex == (uint8)(BlockSize[block] - 1))
					ram[0] = CHECKSUM;

				// update checksum
				ram[0] -= data;

				// move to next byte
				// this will underflow when flush operation is complete
				NVM_FlushIndex[block] = (uint8)(flushindex - 1);
			}
#else
			uint8* ram = pRAM[block]; // locate base address of block RAM
			uint8 data = ram[flushindex]; // get cached byte

			// if we are pre-erasing NVM
			if (NVM_EraseFlags & Flag[block])
				// then erase the cell
				data = (uint8)(EEPROM_ERASED_STATE ? 0xFF : 0); /*lint !e506 EEPROM_ERASED_STATE is a configuration option, so it will always be TRUE/FALSE */

			// flush to NVM
			if (EEPROM_Sync(data, nvm_index + flushindex))
			{
				// synchronization was successfull

				// re-initialize checksum when starting a flush
				if (flushindex == (uint8)(BlockSize[block] - 1))
					ram[0] = CHECKSUM;

				// update checksum
				ram[0] -= data;

				// move to next byte
				// this will underflow when flush operation is complete
				NVM_FlushIndex[block] = (uint8)(flushindex - 1);
			}
#endif // CONFIG_NVM_USE_HAMMING_CODE

			return;
		}
	}
	#endif // INCLUDE_NVM_SUPPORT
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#endif // HEADER
