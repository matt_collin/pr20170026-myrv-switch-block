// Sys HC12.c
// Standard IDS system functions for Freescale HC12 microcontrollers
// Version 1.13
// (c) 2010, 2012, 2013, 2016, 2017 Innovative Design Solutions, Inc.
// All rights reserved

// history
// 1.0  First revision - multithreading is not fully debugged and should
//      not be used
// 1.1  Added Random16() to generate a random 16-bit number
// 1.2  Added sprintf() support
// 1.3  Added Randomize16() to initialize random number seed
// 1.4  Added Random32() and Randomize32()
//      Updated Wait() routine to support 24 MHz bus
//      Moved common routines to non-banked memory
// 1.5  Updated Wait() routine to support 25 MHz bus
// 1.6  Moved RNG seed value into NO_INIT RAM
// 1.7  Debugged multithreading support
// 1.8  Multithreading is now interrput aware, switches contexts to a
//      stack dedicated to interrupt use
// 1.9  Multithreading keeps track of stack high water mark for debugging
// 1.10 Added check for __BIG_ENDIAN__ or __LITTLE_ENDIAN__
// 1.11 Added 32-bit hashing function (sdmb)
// 1.12 Removed code duplicated in IDSCore.c
// 1.13 Removed more code duplicated in IDSCore.c

// NOTES:
//
// This module is intended to work with the Metrowerks CodeWarrior for HC12
// compiler.
//
// Microcontroller reset and C environment startup code is provided by the
// compiler.  Hence it is not included here.  User code begins executing
// at main()
//
// This module defines the standard data types (uint8, uint16, etc...)
// 
// This module provides a portable wait routine
//     Wait()
//
// This module provides a portable reset routine
//     ForceReset()
// NOTE: the force reset routine relies on the COP and will not perform
//       a true reset if the COP is disabled
//
// This module provides standard critical section support via
//     EnterCriticalSection();
//     LeaveCriticalSection();
//
// This module provides stlib functions memclr, memset and memcpy
// It also provides for atomic reads/writes

// verify compiler and environment
#if (!defined __HIWARE__) || (__MWERKS__ != 1)
  #error This module designed for Metrowerks CodeWarrior for HC12 compiler
#endif
#if !defined __HC12__
  #error This module designed for HC12 processors
#endif

#include "global.h"

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#ifdef HEADER

#include <string.h>  // for memset() and memcpy() support
extern int sprintf(LIBDEF_StringPtr s, LIBDEF_ConstStringPtr format, ...); // string formatting

// define 8-bit data type
#if defined __CHAR_IS_8BIT__
typedef signed char int8;
typedef unsigned char uint8;
#elif defined __SHORT_IS_8BIT__
typedef signed short int8;
typedef unsigned short uint8;
#elif defined __INT_IS_8BIT__
typedef signed int int8;
typedef unsigned int uint8;
#elif defined __LONG_IS_8BIT__
typedef signed long int8;
typedef unsigned long uint8;
#elif defined __LONG_LONG_IS_8BIT__
typedef signed long long int8;
typedef unsigned long long uint8;
#endif

// define 16-bit data type
#if defined __CHAR_IS_16BIT__
typedef signed char int16;
typedef unsigned char uint16;
#elif defined __SHORT_IS_16BIT__
typedef signed short int16;
typedef unsigned short uint16;
#elif defined __INT_IS_16BIT__
typedef signed int int16;
typedef unsigned int uint16;
#elif defined __LONG_IS_16BIT__
typedef signed long int16;
typedef unsigned long uint16;
#elif defined __LONG_LONG_IS_16BIT__
typedef signed long long int16;
typedef unsigned long long uint16;
#endif

// define 32-bit data type
#if defined __CHAR_IS_32BIT__
typedef signed char int32;
typedef unsigned char uint32;
#elif defined __SHORT_IS_32BIT__
typedef signed short int32;
typedef unsigned short uint32;
#elif defined __INT_IS_32BIT__
typedef signed int int32;
typedef unsigned int uint32;
#elif defined __LONG_IS_32BIT__
typedef signed long int32;
typedef unsigned long uint32;
#elif defined __LONG_LONG_IS_32BIT__
typedef signed long long int16;
typedef unsigned long long uint16;
#endif

#ifdef __ENUM_IS_16BIT__
  #error Make sure enumerations are set to unsigned 8-bit
#endif

// portable reset routine
#pragma CODE_SEG __NEAR_SEG NON_BANKED
void ForceReset(void);
#pragma CODE_SEG DEFAULT
#define UNASSIGNED_ISR ForceReset /* used by CodeWarrior ProcessExpert */

// portable wait routine
#pragma CODE_SEG __NEAR_SEG NON_BANKED
void Wait(uint16 microseconds);
#pragma CODE_SEG DEFAULT

// standard IDS part number
#ifdef PART_NUMBER
#pragma DATA_SEG COPYRIGHT
extern const char IDS_PartNumber[];
#pragma DATA_SEG DEFAULT
#define IDS_PartNumberDigit(n) IDS_PartNumber[n]
#endif

#define ServiceWatchdog()      _FEED_COP

// critical section support
#ifdef NO_CRITICAL_SECTIONS
	#define EnterCriticalSection();
	#define LeaveCriticalSection();
#else
  #pragma CODE_SEG __NEAR_SEG NON_BANKED
	void EnterCriticalSection(void);
	void LeaveCriticalSection(void);
  #pragma CODE_SEG DEFAULT
#endif

// atomic read/write operations
#pragma CODE_SEG __NEAR_SEG NON_BANKED
void AtomicRead(const void * src, void * dst, uint16 count);
#define AtomicRead16(address) (*((uint16*)(address)))
uint32 AtomicRead32(const void * address);
#define AtomicWrite(dst, src, count) AtomicRead(src, dst, count)
#define AtomicWrite16(address, data) (*((uint16*)(address)) = (data))
void AtomicWrite32(void * address, uint32 data);
#pragma CODE_SEG DEFAULT

// standard library functions
#pragma CODE_SEG __NEAR_SEG NON_BANKED
#define memclr(address, count) (void)memset(address, 0, count)
//void memclr(void * address, uint16 count);
//void memset(void * address, uint8 byte, uint16 count)
//void memcpy(void * dst, const void * src, uint16 count)
#pragma CODE_SEG DEFAULT

// interrupt routines are handled through the multithreaded scheduler
#ifdef ENABLE_MULTITHREADING_SUPPORT
#define INTERRUPT /* normal function call, RTI handled by scheduler */
#else
#define INTERRUPT __interrupt /* no scheduler means that user code must RTI themselves */
#endif // ENABLE_MULTITHREADING_SUPPORT

// multithreading
#ifdef ENABLE_MULTITHREADING_SUPPORT

// prototype interrupt service routines
#ifdef DECLARE_MULTITHREADING_INTERRUPTS
#pragma CODE_SEG __NEAR_SEG NON_BANKED
#undef DECLARE_INTERRUPT
#define DECLARE_INTERRUPT(function) __interrupt void THREAD_##function(void);
DECLARE_MULTITHREADING_INTERRUPTS
#pragma CODE_SEG DEFAULT
#endif // DECLARE_MULTITHREADING_INTERRUPTS

// user handle to a thread
typedef struct _THREAD * THREAD_HANDLE;

// a pointer to a function that is allowed to be a thread entry point
// all threads are passed a 32-bit argument, and return a 32-bit argument
// use of these arguments is optional and allows for objects to be passed/returned in the thread
// although defined as a far (banked) pointer, near pointers can be used/passed to this module
typedef uint32(* __far THREAD_ENTRY_POINT)(uint32 argument);

// define thread execution priority
// priority of thread equates to the number of time quantum that are given to the thread before pre-emption
typedef enum {
	THREAD_PRIORITY_LOW = 0,           // low priority threads are given 1 time quantum before pre-emption
	THREAD_PRIORITY_NORMAL = 1,        // normal priority threads are given 2 time quantum before pre-emption
	THREAD_PRIORITY_HIGH = 2,          // high priority threads are given 3 time quantum before pre-emption
	THREAD_PRIORITY_CRITICAL = 3,      // critical priority threads are given 4 time quantum before pre-emption
     THREAD_PRIORITY_TERMINATED = 0xFF, // terminated threads have this priority
} THREAD_PRIORITY;

// activates multi-threading
// thread entry point is immediately called
// NOTE: *** this function does NOT return ***
void Sys12_StartMultiThreadingKernel(THREAD_ENTRY_POINT main_thread);

// creates a new execution thread and registers it with the thread scheduler
// thread is initially paused, must be started by calling Thread_Resume
//    entrypoint - the address of the thread entry point function
//                 scheduler will detect whether pointer is in banked or non-banked memory
//    argument - optional argument that is passed to the thread
//    stackend - points to last valid location of stack
//    priority - defines the execution priority, which determines overall time given to execute this thread
THREAD_HANDLE Thread_CreateThread(THREAD_ENTRY_POINT entrypoint, uint32 argument, uint8* stackend, THREAD_PRIORITY priority);

// puts a thread to sleep by yielding the remainder of it's time quantum
#define Thread_Yield() asm SWI

// yeilds for a miniumum of the specified number of milliseconds
void Thread_Sleep(uint16 milliseconds);

// returns the number of threads in the system
uint8 Thread_GetThreadCount(void);

// returns a handle to the current thread
THREAD_HANDLE Thread_GetCurrentThread(void);

// gets the execution priority of a thread
// returns THREAD_PRIORITY_TERMINATED if the thread has been terminated 
THREAD_PRIORITY Thread_GetExecutionPriority(THREAD_HANDLE handle);

// sets the execution priority of a thread
void Thread_SetExecutionPriority(THREAD_HANDLE handle, THREAD_PRIORITY priority);

// pauses a thread indefinitely
void Thread_Pause(THREAD_HANDLE handle);

// resumes a paused thread
void Thread_Resume(THREAD_HANDLE handle);

// returns whether the given thread is paused
uint8 Thread_IsPaused(THREAD_HANDLE handle);

// unconditionally terminates an active thread
void Thread_Terminate(THREAD_HANDLE handle, uint32 exitcode);

// returns whether the given thread has terminated
uint8 Thread_IsTerminated(THREAD_HANDLE handle);

// gets the exit code from the terminated thread
uint32 Thread_GetExitCode(THREAD_HANDLE handle);

// the interrupt routine that processes context switching
#pragma CODE_SEG __NEAR_SEG NON_BANKED
__interrupt void THREAD_ContextSwitchInterrupt(void);
#pragma CODE_SEG DEFAULT

// basic MUTEX support
// Mutex acquire/release functions do not disable interrupts or otherwise block other threads
typedef uint16 MUTEX; // this is actually two 8 bit values packet, low byte is "get in line", high byte is "have the mutex"
#define NEW_MUTEX  0x0101 /* value to initialize a mutex with */
#pragma CODE_SEG __NEAR_SEG NON_BANKED
void Thread_AcquireMutex(MUTEX *mutex);
void Thread_ReleaseMutex(MUTEX *mutex);
#pragma CODE_SEG DEFAULT

#endif // ENABLE_MULTITHREADING_SUPPORT

#else // HEADER

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#pragma DATA_SEG COPYRIGHT

#ifdef PART_NUMBER
const char IDS_PartNumber[] = PART_NUMBER;
#endif

#ifdef DESCRIPTION
const char _Description[] = DESCRIPTION;
#endif

const char _Copyright[] = "Copyright Innovative Design Solutions, Inc.";

#pragma DATA_SEG DEFAULT

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

#pragma CODE_SEG __NEAR_SEG NON_BANKED

// MC9S12 micros internal reset sources are
// - POR
// - low voltage
// - illegal address
// - clock monitor
// - COP
#pragma NO_EXIT
void ForceReset(void)
{
  // disable interrupts and STOP instruction, as we
  // don't want request to be interrupted or halted
  asm ORCC #0xFF
  
  // attempt to force a COP reset
  // this might not work if the COP is disabled at boot
  _ENABLE_COP(1);
  *(uint8*)_COP_RST_ADR = 0;
 
  // TBD
  // it would be nice to force an illegal address reset
  // question: how do you find an illegal address supported on all platforms?
 
  // we seem to have run out of options  
  // give up, just jump to the reset vector
  // this isn't a real reset, but it will have to do
  asm LDX 0xFFFE
  asm JMP 0,X
}

#pragma CODE_SEG DEFAULT

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

#pragma CODE_SEG __NEAR_SEG NON_BANKED

#pragma MESSAGE DISABLE C5703
/*lint -save -e715 lint doesn't think this function does anything (as it is only assembly, which lint ignores) */
void Wait(uint16 microseconds)
{
#ifdef _lint
    // lint ignores assembly, and so thinks this is an effect-less function
    // this code is enough for lint to consider it a function with effect
    volatile uint8 i = 0;
    i = i + 1;
#endif

    asm TFR D,Y
    asm INY
    asm y_loop:
	#if FBUS == 32000000
    // consume 32 cycles per microsecond
    // Rob made me do it this way even though the waste of ROM drives me crazy
    asm NOP
    asm NOP
    asm NOP
    asm NOP
    asm NOP
    asm NOP
    asm NOP
    asm NOP
    asm NOP
    asm NOP
    asm NOP
    asm NOP
    asm NOP
    asm NOP
    asm NOP
    asm NOP
    asm NOP
    asm NOP
    asm NOP
    asm NOP
    asm NOP
    asm NOP
    asm NOP
    asm NOP
    asm NOP
    asm NOP
    asm NOP
    asm NOP
    asm NOP
	#elif FBUS == 25000000
    // consume 25 cycles per microsecond
    // Rob made me do it this way even though the waste of ROM drives me crazy
    asm NOP
    asm NOP
    asm NOP
    asm NOP
    asm NOP
    asm NOP
    asm NOP
    asm NOP
    asm NOP
    asm NOP
    asm NOP
    asm NOP
    asm NOP
    asm NOP
    asm NOP
    asm NOP
    asm NOP
    asm NOP
    asm NOP
    asm NOP
    asm NOP
    asm NOP
	#elif FBUS == 24000000
    // consume 24 cycles per microsecond
    // Rob made me do it this way even though the waste of ROM drives me crazy
    asm NOP
    asm NOP
    asm NOP
    asm NOP
    asm NOP
    asm NOP
    asm NOP
    asm NOP
    asm NOP
    asm NOP
    asm NOP
    asm NOP
    asm NOP
    asm NOP
    asm NOP
    asm NOP
    asm NOP
    asm NOP
    asm NOP
    asm NOP
    asm NOP
	#else
		#error Wait() routine must be adjusted for FBUS
	#endif

  asm DBNE Y,y_loop  // 3 cycles
}
/*lint -restore */
#pragma CODE_SEG DEFAULT

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

#ifndef NO_CRITICAL_SECTIONS

static struct {
	uint8 Lock;
	uint8 CCR;
} CriticalSection = { 0, 0 };

#pragma CODE_SEG __NEAR_SEG NON_BANKED

void EnterCriticalSection(void)
{
	if (!CriticalSection.Lock) 
	{
		asm TPA
		asm SEI
		asm STAA CriticalSection.CCR
	}
	CriticalSection.Lock++;
}

void LeaveCriticalSection(void)
{
	if (CriticalSection.Lock && !--CriticalSection.Lock)
	{
		asm LDAA CriticalSection.CCR
		asm TAP
	}
}

#pragma CODE_SEG DEFAULT

#endif

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

#if 0

#pragma CODE_SEG __NEAR_SEG NON_BANKED

void memclr(void * address, uint16 count)
{
  // count is in D
  // address is in SP:2
  if (count)
  {   
    do
    {
      *(uint8*)address = 0;
      address = (uint8*)address + 1;
    } while (--count);
  }
}

void memset(void * address, uint8 byte, uint16 count)
{
  // count is in D
  // byte is in SP:2
  // address is in SP:3
  if (count)
  {
    
    do
    {
      *(uint8*)address = byte;    
      address = (uint8*)address + 1;
    } while (--count);
  }
}

void memcpy(void * dst, const void * src, uint16 count)
{
  if (count)
  {
    do
    {
      *(uint8*)dst = *(uint8*)src;
      dst = (uint8*)dst + 1;
      src = (uint8*)src + 1;
    } while (--count);
  }
}

#pragma CODE_SEG DEFAULT

#endif

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

#pragma CODE_SEG __NEAR_SEG NON_BANKED

void AtomicRead(const void * src, void * dst, uint16 count)
{
  EnterCriticalSection();
	(void)memcpy(dst, src, count);
	LeaveCriticalSection();  
}

uint32 AtomicRead32(const void * address)
{
  uint32 value;
	AtomicRead(address, &value, sizeof(value));
	return value;
}

void AtomicWrite32(void * address, uint32 data)
{
  AtomicWrite(address, &data, sizeof(data));
}

#pragma CODE_SEG DEFAULT

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

#ifdef ENABLE_MULTITHREADING_SUPPORT

// PRE-EMPTIVE MULTITHREADING SCHEUDLER:
//
// The scheduler is activated via a call to Sys12_StartMultiThreadingKernel().  This
// function starts the scheduler, and does NOT return.  The address of the first/default
// thread to execute is assed to this function.
//
// Pre-emptive task scheduling is achieved by an independent interrupt that
// periodically interrupts the background process.  The interrupt will save the
// context of the background context, then switch to another background context
// and re-activate the thread.  The function THREAD_ContextSwitchInterrupt();
// performs the context switching.
//
// The Real-Time Interrupt (RTI) is used to generate the periodic context switching interrupt.
// THREAD_ContextSwitchInterrupt() must be installed at the RTI interrupt vector.
//
// Thread yielding/sleeping is achived through the SWI interrupt.  Therefore
// THREAD_ContextSwitchInterrupt() must also be installed at the SWI interrupt vector.
// A thread which yields forfeits the remainder of it's time quantum back to the system.
//
// When switching contexts, the next thread is given at least a full time quantum
// to execute.  Higher priority threads will be given a larger amount of time to
// execute before the next RTI.  Thread priority can be set via a call to
// Thread_SetExecutionPriority()
//
// Threads can be paused and resumed via calls to Thread_Pause() and Thread_Resume().
// When all threads are paused/terminated, the background idle task runs.  Only the
// real-time program will run at this point.
//
// Threads that exit should return a 32-bit value.  This value will be remembered and can
// be read via a call to Thread_GetExitCode().
//
// Thread termination can be forced as necessary, via a call to Thread_Terminate() 
//
// New threads are created via Thread_CreateThread().  Thread management information is held
// on the stack itself.  When creating a thread, the user must pass:
//        a funciton pointer to the thread entry point
//        a 32-bit value to pass as an argument the the thread entry point
//        a pointer to the top of the initial stack
// The 32-bit argument allows for passing of user variables, pointers and other information, allowing
// for context sensitive processing in user thread.  The minimum stack size for a thread is 20 bytes
// (assuming that the thread itself does not use additional stack space).  Keep in mind that
// the real-time program can interrupt any thread, and that each thread must have enough stack space
// to accomodate the real-time interrupt.  Also, note that new threads are initially created in the
// PAUSED state, and must be started via a call to Thread_Resume()
//
// This module includes basic support for blocking MUTEXes.  MUTEX objects can be acquired and released.
// Only one thread at a time may acquire a MUTEX, and all other threads that attempt to acquire
// the MUTEX will be blocked until the owner of the MUTEX releases it.  MUTEX locking/unlocking
// does not disable interrupts otherwise block other threads, helping ensure better real-time performance.
//
// Available thread functions are:
//     Thread_CreateThread();         // creates a new thread, initially paused
//     Thread_Yield();                // yields the remainder of the active threads time quantum
//     Thread_Sleep();                // yeilds for a miniumum of the specified number of milliseconds
//     Thread_GetThreadCount();       // returns total number of active threads in the system
//     Thread_GetCurrentThread();     // returns a handle to the currently executing thread
//     Thread_GetExecutionPriority(); // gets the execution priority of the given thread
//     Thread_SetExecutionPriority(); // sets the execution priority of the given thread
//     Thread_Pause();                // pauses the given thread
//     Thread_Resume();               // resumes a paused thread
//     Thread_IsPaused();             // indicates whether the given thread is in a paused state
//     Thread_Terminate();            // forces abnormal termination of a thread
//     Thread_IsTerminated();         // indicates whether the given thread has terminated or not
//     Thread_GetExitCode();          // returns the 32-bit exit code for a terminated thread
//     Thread_CreateMutex();          // creates a new thread-blocking MUTEX object
//     Thread_AcquireMutex();         // attempts to acquire the MUTEX, thread is blocked until acquired
//     Thread_ReleaseMutex();         // releases the MUTEX and unblocks waiting threads

// processor check / determine ADC module version
#if defined MC9S12P32 || defined MC9S12P64 || defined MC9S12P96 || defined MC9S12P128
#else
	#error Multithreading Scheduler not designed for this processor
#endif

// this defines the state of the CCR when a thread starts
// S = 1   stop instruction disabled
// X = 1   retain state of X mask (clear once, set never)
// I = 0   enable other interrupts
#define CCR_AT_THREAD_ENTRY   0xC0

// every thread requires a structure to manage it's state/context
// memory for this structure is included on the stack that the thread is running on
// the memory is automatically allocated at the top of the stack during Thread_CreateThread()
//
// the structure is passed to the scheduler, which adds it to a linked list
// the linked list contains a list of all active/scheduled threads
typedef struct _THREAD
{
	// stack pointer is accessed via assembly
	// so it must go at the top of the structure and CANNOT be moved
	uint8 *SP;

	uint8* HighWaterMark; // used for debugging -- can easily see how high the stack has gone

	uint8 State; // holds thread priority when running, 0xFF when terminated

	// to conserve space, we use a union
	// union holds linked list pointers when thread is running
	// union holds thread exit code upon termination
	union
	{
		struct
		{
			struct _THREAD *Prev; // points to the previous thread in the scheduling loop
			struct _THREAD *Next; // points to the next thread in the scheudling loop
		} Thread;
		uint32 ExitCode;
	} u;
} THREAD;

// macros for quickly accessing thread information
#define HANDLE             ((THREAD *)handle)
#define IS_TERMINATED(h)   (h->State == 0xFF)
#define IS_SCHEDULED(h)    (h->u.Thread.Next != NULL)

// the main background thread
static THREAD MainThread = {
	NULL, // SP -- unknown until first interrupt
	(uint8*)0xFFFFFFFF, // HighWaterMark
	THREAD_PRIORITY_NORMAL, // State
	{ &MainThread, &MainThread } // this is (initially) the only thread in the scheduling queue
};

// pointer to the currently active thread
static THREAD* ActiveThread = &MainThread;
THREAD_HANDLE Thread_GetCurrentThread(void) { return ActiveThread; }

// pointer to the next thread to activate
static THREAD* NextThread = &MainThread;

// counts the number of threads in the system
static uint8 ThreadCount = 1;
uint8 Thread_GetThreadCount(void) { return ThreadCount; }

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

THREAD_PRIORITY Thread_GetExecutionPriority(THREAD_HANDLE handle) { return HANDLE->State; }
uint8 Thread_IsTerminated(THREAD_HANDLE handle) { return IS_TERMINATED(HANDLE); }
uint32 Thread_GetExitCode(THREAD_HANDLE handle) { return IS_TERMINATED(HANDLE) ? HANDLE->u.ExitCode : 0; }

void Thread_Sleep(uint16 milliseconds)
{
	uint16 now, elapsed;

	if (!milliseconds) 
	{
		// sleep(0) is really just a straight up yield
		Thread_Yield();
		return;
	}

	now = OS_GetElapsedMilliseconds16();
	for (;;)
	{
		Thread_Yield();

		// how much time has elapsed?
		elapsed = now;
		now = OS_GetElapsedMilliseconds16();
		elapsed = now - elapsed;

		// subtract elapsed time
		if (elapsed > milliseconds)
			return; // out of time
		milliseconds -= elapsed;
	}
}

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

// this represents an idle thread task, which does nothing more than go to sleep
// the idle thread is the thread of last resort -- it runs when all other threads are paused/terminated
// this allows the background program to execute code when all threads are dead
#pragma CODE_SEG __NEAR_SEG NON_BANKED
static void IdleTask(void)
{
	for(;;)
	{
		Thread_Yield();
	}
}
#pragma CODE_SEG DEFAULT

// idle thread stack
static struct
{
	uint8 PAGE;
	uint8 CCR;
	uint16 D;
	uint16 X;
	uint16 Y;
	uint16 PC;
} IdleStack = { 0, CCR_AT_THREAD_ENTRY, 0, 0, 0, (uint16)IdleTask };

// idle thread object
static THREAD IdleThread = {
	&IdleStack.PAGE, // SP
	(uint8*)0xFFFFFFFF, // HighWaterMark
	THREAD_PRIORITY_LOW, // State
	{ &IdleThread, &IdleThread } // this is the only thread in the scheduling queue
};

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

// this performs the context switch
#pragma CODE_SEG __NEAR_SEG NON_BANKED
#pragma MESSAGE DISABLE C12053
__interrupt void THREAD_ContextSwitchInterrupt(void)
{
	#ifdef MULTITHREADING_DEBUG_PORT
	MULTITHREADING_DEBUG_PORT = 1;
	#endif

	// save the PPAGE register
	asm LDAB PPAGE
	asm PSHB

	// save stack context of the current thread
	asm LDX ActiveThread
	asm STS 0,X

	// activate next thread
	asm LDX NextThread
	asm STX ActiveThread
	asm LDS 0,X

	// restore the PPAGE register
	asm PULB	
	asm STAB PPAGE

	// debugging -- keep track of the high water mark of the thread
	if (ActiveThread->SP < ActiveThread->HighWaterMark)
		ActiveThread->HighWaterMark = ActiveThread->SP;
  
	// get next thread to execute
	NextThread = ActiveThread->u.Thread.Next;

	// setup next timeslice
	// CPMURTI RTI control register
	// writing this register starts a new timeout period
	// default time quantum = about 1 millisecond when IRCCLK is trimmed to 1MHz
	// 1xxxxxxx
	// ||||||||
	// |||| \\\\___ Modulous counter -- use thread priority
	// | \\\_______ Pre Scale Rate Select  000 = 10^3
	//  \__________ RTDEC  1 = decimal divider value
	CPMURTI = 0x80 | (ActiveThread->State & 3); // maximum 4 time quantum
	CPMUFLG_RTIF = 1; // clear the previous interrupt

	#ifdef MULTITHREADING_DEBUG_PORT
	MULTITHREADING_DEBUG_PORT = 0;
	#endif
}
#pragma MESSAGE DEFAULT C12053
#pragma CODE_SEG DEFAULT

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

// dedicated interrupt stack support

#ifdef DECLARE_MULTITHREADING_INTERRUPTS

// stack used by all interrupts
static uint8 InterruptStack[MULTITHREADING_INTERRUPT_STACK_SIZE];

#pragma CODE_SEG __NEAR_SEG NON_BANKED
#pragma MESSAGE DISABLE C10030
#pragma MESSAGE DISABLE C12053

// switch to the interrupt stack
#define ENTER_INTERRUPT asm TFR SP,X; asm LDS #InterruptStack:MULTITHREADING_INTERRUPT_STACK_SIZE; asm PSHX;

// return to the user stack
#define EXIT_INTERRUPT asm PULX; asm TFR X,SP;

#undef DECLARE_INTERRUPT
#define DECLARE_INTERRUPT(f) __interrupt void THREAD_##f(void) { ENTER_INTERRUPT; f(); EXIT_INTERRUPT; }
DECLARE_MULTITHREADING_INTERRUPTS

#pragma CODE_SEG DEFAULT
#pragma MESSAGE DEFAULT C10030
#pragma MESSAGE DEFAULT C12053

#endif // DECLARE_MULTITHREADING_INTERRUPTS

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

// NOTE: this code must run within a critical section
static void ScheduleThread(THREAD_HANDLE handle)
{
	if (IS_TERMINATED(HANDLE)) return;
	if (IS_SCHEDULED(HANDLE)) return;

	// if the idle task is running
	if (NextThread == &IdleThread)
	{
		// terminate idle task
		NextThread = HANDLE;

		// schedule us exclusively (we are the only thread right now)
		HANDLE->u.Thread.Next = handle;
		HANDLE->u.Thread.Prev = handle;
	}
	else
	{
		// insert into linked list, at the end
		HANDLE->u.Thread.Next = NextThread;
		HANDLE->u.Thread.Prev = NextThread->u.Thread.Prev;

		HANDLE->u.Thread.Next->u.Thread.Prev = handle;
		HANDLE->u.Thread.Prev->u.Thread.Next = handle;

		if (ActiveThread == NextThread)
			NextThread = handle; // we run next
	}
}

// NOTE: this code must run within a critical section
static void UnscheduleThread(THREAD_HANDLE handle)
{
	if (IS_TERMINATED(HANDLE)) return;
	if (!IS_SCHEDULED(HANDLE)) return;

	// are we the only thread executing?
	if (HANDLE->u.Thread.Next == HANDLE)
	{
		// activate the idle thread
		NextThread = &IdleThread;
	}
	else
	{
		// remove from scheduling queue
		HANDLE->u.Thread.Prev->u.Thread.Next = HANDLE->u.Thread.Next;
		HANDLE->u.Thread.Next->u.Thread.Prev = HANDLE->u.Thread.Prev;

		// bypass us if we are the next scheduled
		if (NextThread == HANDLE)
			NextThread = HANDLE->u.Thread.Next;
	}

	// wipe our link pointers    
	HANDLE->u.Thread.Prev = HANDLE->u.Thread.Next = NULL;
}

uint8 Thread_IsPaused(THREAD_HANDLE handle)
{
	return IS_TERMINATED(HANDLE) ? FALSE : !IS_SCHEDULED(HANDLE);
}

void Thread_Pause(THREAD_HANDLE handle)     
{
	if (IS_TERMINATED(HANDLE))
		return;

	if (IS_SCHEDULED(HANDLE))
	{
		EnterCriticalSection();
		UnscheduleThread(HANDLE); 
		LeaveCriticalSection();  
	}

	// yield now if we have paused ourselves
	if (!IS_SCHEDULED(ActiveThread))
		Thread_Yield();
}

void Thread_Resume(THREAD_HANDLE handle)
{
	if (IS_TERMINATED(HANDLE))
		return;
	if (!IS_SCHEDULED(HANDLE))
	{
		EnterCriticalSection();
		ScheduleThread(HANDLE);
		LeaveCriticalSection();
	}
}

#pragma MESSAGE DISABLE C4000 /* condition always true */
#pragma INLINE
static uint8 RangeCheckThreadPriority(THREAD_PRIORITY priority)
{
	if (priority >= THREAD_PRIORITY_LOW && priority <= THREAD_PRIORITY_CRITICAL)
		return priority;
	return THREAD_PRIORITY_NORMAL;
}
#pragma MESSAGE DEFAULT C4001

void Thread_SetExecutionPriority(THREAD_HANDLE handle, THREAD_PRIORITY priority)
{
	if (!IS_TERMINATED(HANDLE))
		HANDLE->State = RangeCheckThreadPriority(priority);
}

// NOTE: must be called from within critical section
static void Terminate(THREAD_HANDLE handle, uint32 exitcode)
{
	if (!IS_TERMINATED(HANDLE))
	{
		UnscheduleThread(HANDLE);
		HANDLE->State = 0xFF;
		HANDLE->u.ExitCode = exitcode;
		ThreadCount--;  
	}
}

// this is where threads go when they die
#pragma CODE_SEG __NEAR_SEG NON_BANKED
static void ThreadExit(uint32 exitcode)
{
	DisableInterrupts;
	Terminate(ActiveThread, exitcode);  
	for (;;)
	{
		Thread_Yield();
	}
}
#pragma CODE_SEG DEFAULT

void Thread_Terminate(THREAD_HANDLE handle, uint32 exitcode)
{
	// if this is the active thread, then we must exit gracefully
	if (handle == ActiveThread)  
		ThreadExit(exitcode); // does not return

	EnterCriticalSection();
	Terminate(handle, exitcode);
	LeaveCriticalSection();
}

// always returns a valid far pointer
static THREAD_ENTRY_POINT FixPointer(THREAD_ENTRY_POINT pointer)
{
	#define raw  ((uint8*)&pointer)
	if (!raw[0])
	{
		raw[0] = raw[1];
		raw[1] = raw[2];
		raw[2] = PPAGE;
	}
	return pointer;
}

// creates a new execution thread and registers it with the thread scheduler
// thread is initially paused, must be started by calling Thread_Resume
//    entrypoint - the address of the thread entry point function
//    argument - optional argument that is passed to the thread
//    stackend - points to last valid location of stack
//    priority - defines the execution priority, which determines overall time given to execute this thread
THREAD_HANDLE Thread_CreateThread(THREAD_ENTRY_POINT entrypoint, uint32 argument, uint8* stackend, THREAD_PRIORITY priority)
{
	THREAD* handle;

	// create space for the thread management structure
	stackend -= sizeof(THREAD) - 1;
	handle = (THREAD*)stackend;

	// stack gets setup in one of two ways, depending on where the thread entry point is located
	// one way for "near" (non banked) memory pointers 
	// one way for "far" (banked) memory pointers
	//
	// near pointers are 16 bits long and can point the CPU "local" memory map
	//	  0000 - FFFF --> directly to local memory
	// far pointers are 24 bits long and use the local P-flash window to view extended memory
	//     low 8 bits is PPAGE window
	//     upper 16 bits is local memory pointer into P-flash window

	// push return address on the stack
	*--stackend = (uint8)((uint16)ThreadExit >> 0);
	*--stackend = (uint8)((uint16)ThreadExit >> 8);
	if (((uint8*)&entrypoint)[0])
	{
		// upper 8 bits is non-zero
		// this indicates a far pointer
		*--stackend = PPAGE; // caller is going to do an RTC, so we need to give them a page register to pop
	}

	// push the variables for the initial RTI in the context switch interrupt
	entrypoint = FixPointer(entrypoint); 
	*--stackend = ((uint8*)&entrypoint)[1]; // PC.lo
	*--stackend = ((uint8*)&entrypoint)[0]; // PC.hi
	*--stackend = 0; // Y.lo
	*--stackend = 0; // Y.hi
	*--stackend = ((uint8*)&argument)[1]; // X.lo
	*--stackend = ((uint8*)&argument)[0]; // X.hi
	*--stackend = ((uint8*)&argument)[2]; // A
	*--stackend = ((uint8*)&argument)[3]; // B
	*--stackend = CCR_AT_THREAD_ENTRY;

	// push the PPAGE register for the thread entrypoint
	// this will be popped directly by the context switch interrupt
	*--stackend = ((uint8*)&entrypoint)[2];

	// setup thread structure
	handle->SP = handle->HighWaterMark = stackend;
	handle->State = RangeCheckThreadPriority(priority);
	handle->u.Thread.Prev = handle->u.Thread.Next = NULL; // thread is initially unscheduled

	// thread is now ready to run, remember it
	EnterCriticalSection();
	ThreadCount++;  
//	ScheduleThread(handle); // start the thread
	LeaveCriticalSection();  

	return handle;
}

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

// activates multi-threading
void Sys12_StartMultiThreadingKernel(THREAD_ENTRY_POINT main_thread)
{
	EnterCriticalSection();

	// setup RTI interrupt to run off of internal clock IRCCLK (approx 1 MHz)
	CPMUPROT = 0x26; // disable protection of clock-source register
	CPMUCLKS_RTIOSCSEL = 0; // RTI clock source is IRCCLK
	CPMUPROT = 0x00; // re-enable protection of clock-source register

	// enable real time interrupt
	CPMUINT_RTIE = 1;
	CPMURTI = 0x80; // program first RTI timeout
	CPMUFLG_RTIF = 1; // clear any previous interrupt

	LeaveCriticalSection();	

	ThreadExit(FixPointer(main_thread)(0));
}

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

// MUTEX support

// Mutexes are actually two 8-bit values packed into a 16 bit value
//
// Mutex acquisition keeps track of the "place in line", so that a thread can detect
// when it is blocking someone.  Therefore, when the mutex is released, the thread
// will check to see if someone else is waiting for it -- if so then the thread will
// automatically yield to unblock the waiting thread.
//
// Keeping track of "place in line" also ensures that all threads get an equal shot
// at acquiring the lock.  Because a blocked thread has a valid "place in line", no other
// threads can sucessfully acquire a recently released mutex -- they have to wait their
// turn.  Thus, the oldest thread with a "place in line" is guaranteed to receive the first
// chance to acquire a free mutex.
//
// the low address (mutex[0]) is the "place in line"
// 0 = a thread is currently being blocked, waiting for the mutex
// 1 = no threads are waiting for the mutex
//
// the high address (mutex[1]) is the mutex itself
// 0 = someone has the mutex
// 1 = mutex is free

// specialized critical section to prevent block simulataneous thread operations
// NOTE: this can only be used by code that does not effect real-time interrupt operation
//       so scheduling changes can not be done from within this critical section

// unlocking is simple -- simply reset the lock to a non-zero value
#define ReleaseObj(lock) (*(lock) = 1)

#define PlaceInLine ((uint8*)mutex + 0)
#define Lock        ((uint8*)mutex + 1)

#pragma CODE_SEG __NEAR_SEG NON_BANKED

#pragma MESSAGE DISABLE C5703 /* lock not used */
static void AcquireObj(uint8* lock)
{
	// 0 = resource locked
	// 1 = resource available
	//
	// MINM instruction is used to perform locking
	// it finds the smaller of register [A] and [memory location]
	// the smaller value is stored in [memory location]
	// the CCR is set to [A] - [memory location]
	// CCR.C = 1 when the value in A is smaller and has replaced the value in [memory location]
	// CCR.C = 0 when the value in A is equal to or greater than [memory location]
	asm
	{
		TFR  D,X
		CLRA

		// set lock to zero
		// we are done if it was non-zero prior to us setting it
loop:  
		MINM 0,X    // try and clear the mutex
		BCS  done   // if C = 1, we cleared the mutex and have acquired it

yield:
		SWI          // yield remaining time quantum
		BRA  loop    // try again

done:
	}
}
#pragma MESSAGE DEFAULT C5703

void Thread_AcquireMutex(MUTEX *mutex)
{
	AcquireObj(PlaceInLine); // get in line
	AcquireObj(Lock);        // now that we are in line, get the lock
	ReleaseObj(PlaceInLine); // now that we have the lock, release place in line for someone else
}

void Thread_ReleaseMutex(MUTEX *mutex)
{ 
	ReleaseObj(Lock); // release lock
	if (*PlaceInLine == 0) // is someone else in line?
		Thread_Yield(); // yield because we've been blocking another thread waiting for this mutex
}

#pragma CODE_SEG DEFAULT

#endif // ENABLE_MULTITHREADING_SUPPORT

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#endif
