// Global.h
// Global header for 18589 LCI LincPad Switch to CAN Interface SW S12G
// (c) 2012, 2014, 2015, 2016 Innovative Design Solutions, Inc.
// All rights reserved

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#ifndef HEADER
#define HEADER

//lint ++flb  we are in a library

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#define PART_NUMBER "22720-A"
#define DESCRIPTION "MyRV Switch Block SW"

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// basic operating environment

#define MC9S12G96
#define XTAL            16000000
#define FBUS            25000000
#define __BIG_ENDIAN__
// include header information 
#include <hidef.h>      // common defines and macros
#include "derivative.h" // auto-generated derivative-specific definitions
#include "MCUinit.h"    // auto-generated initialization routines
#include "Sys HC12.c"   // IDS HC12 system/environment routines

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// port I/O macros

#define CAN_STANDBY           PTS_PTS7
#define Assert_CAN_STANDBY    (PTS_PTS7 = 1)
#define Deassert_CAN_STANDBY  (PTS_PTS7 = 0)

#define S1_SIGNAL             PTP_PTP0
#define S2_SIGNAL             PTP_PTP1
#define S3_SIGNAL             PTP_PTP2
#define S4_SIGNAL             PTP_PTP3
#define S5_SIGNAL             PTP_PTP4
#define S6_SIGNAL             PTP_PTP5
#define S7_SIGNAL             PT01AD_PT01AD0
#define S8_SIGNAL             PT01AD_PT01AD1

#define BACKLIGHT_EN				PTT_PTT5
#define Assert_BACKLIGHT_EN	(PTT_PTT5 = 1)
#define Deassert_BACKLIGHT_EN (PTT_PTT5 = 0)


//#define DEBUG_1               PTT_PTT0

//#define DEBUG_2               PTT_PTT1

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// digital input debounce information
// Usage: INPUT(name, TEST(expression), DEBOUNCE(type, count), init, event)
//       name - software name of input (Input.Digital.name and Input.Digital.nameEvent)
// expression - expression that returns actual digital value of input port
//       type - debounce counter variable type (uint8, uint16, etc...)
//      count - debounce trigger count (when count reached input is debounced)
//       init - initial boot value (INIT_TRUE / INIT_FALSE / INIT_ACTUAL)
//      event - defines optional event support (EVENT_SUPPORT / NO_EVENT_SUPPORT)
#define DIGITAL_INPUTS \
	INPUT(Switch1,  TEST(!S1_SIGNAL), DEBOUNCE(uint8, 5), INIT_ACTUAL,  NO_EVENT_SUPPORT) \
	INPUT(Switch2,  TEST(!S2_SIGNAL), DEBOUNCE(uint8, 5), INIT_ACTUAL,  NO_EVENT_SUPPORT) \
	INPUT(Switch3,  TEST(!S3_SIGNAL), DEBOUNCE(uint8, 5), INIT_ACTUAL,  NO_EVENT_SUPPORT) \
	INPUT(Switch4,  TEST(!S4_SIGNAL), DEBOUNCE(uint8, 5), INIT_ACTUAL,  NO_EVENT_SUPPORT) \
	INPUT(Switch5,  TEST(!S5_SIGNAL), DEBOUNCE(uint8, 5), INIT_ACTUAL,  NO_EVENT_SUPPORT) \
	INPUT(Switch6,  TEST(!S6_SIGNAL), DEBOUNCE(uint8, 5), INIT_ACTUAL,  NO_EVENT_SUPPORT) \
	INPUT(Switch7,  TEST(!S7_SIGNAL), DEBOUNCE(uint8, 5), INIT_ACTUAL,  NO_EVENT_SUPPORT) \
	INPUT(Switch8,  TEST(!S8_SIGNAL), DEBOUNCE(uint8, 5), INIT_ACTUAL,  NO_EVENT_SUPPORT) \

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#define VDD  5.0

// enumerate switch states
typedef enum { SWITCH_STATE_OFF = 0, SWITCH_STATE_1 = 0x01, SWITCH_STATE_2 = 0x02 } SWITCH_STATE;

// SWITCH(name, pressed, val)
// name = name of the switch 
// pressed = boolean expression of switch actuation
// val = value of the pressed switch

#define DECLARE_SWITCH_DEVICES \
	SWITCH(1, Input.Digital.Switch1, SWITCH_STATE_1) \
	SWITCH(2, Input.Digital.Switch2, SWITCH_STATE_2) \
	SWITCH(3, Input.Digital.Switch3, SWITCH_STATE_1) \
	SWITCH(4, Input.Digital.Switch4, SWITCH_STATE_2) \
	SWITCH(5, Input.Digital.Switch5, SWITCH_STATE_1) \
	SWITCH(6, Input.Digital.Switch6, SWITCH_STATE_2) \
	SWITCH(7, Input.Digital.Switch7, SWITCH_STATE_1) \
	SWITCH(8, Input.Digital.Switch8, SWITCH_STATE_2) \

// enumerate the switches
typedef enum {
  #undef SWITCH
  #define SWITCH(name, pressed, val) SWITCH_##name,
  DECLARE_SWITCH_DEVICES
  NUM_SWITCHES
} SWITCHES;

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#define DECLARE_CONFIGURATION                    \
                                                 \
CONFIG_BLOCK(IDS_CAN_Block, USE_NVM, NO_KAM)                         \
   CONFIG(IDS_CAN_FUNCTION_NAME, FunctionName[NUM_IDS_CAN_DEVICES])  \
   CONFIG(uint8, FunctionInstance[NUM_IDS_CAN_DEVICES])              \
	CONFIG(uint32, CircuitID[NUM_IDS_CAN_DEVICES])                  \
                                                                     \
CONFIG_BLOCK(ProductionBlock, USE_NVM, NO_KAM)                       \
     CONFIG(uint8, ProductionByte)                                   \
     CONFIG(uint8, MAC[6])                                           \
  
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// SCI parameters
#define SCI_PORT(reg)                 SCI0##reg /* SCI, SCI0, SCI1, etc... */
#define SCI_DEFAULT_BAUD_RATE         57600
#define SCI_TX_BUFFER_SIZE_BYTES      256
#define SCI_RX_BUFFER_SIZE_BYTES      256 /* only needed in interrupt operation */
#define SCI_RX_MAX_MESSAGE_LENGTH     40
#define SCI_DRIVER_INTERRUPT
#define SCI_MAINTAIN_STATISTICS       /* usage statistics are maintained if this is defined */
//#define DEBUG_SCI_INTERRUPT_TASK_PORT   DEBUG_1
//#define DEBUG_SCI_BACKGROUND_TASK_PORT  DEBUG_1

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// CAN driver settings

// CAN timing parameters (TSEG1, TSEG2, SJW, etc...)
// need to be coordinated with the network
#define CAN_BAUD_RATE   250000  /* 250kbps */
#define CAN_TSEG1       12
#define CAN_TSEG2       3
#define CAN_SJW         3

// CAN driver parameters
// NOTE: buffer sizes do not need to be a power of 2
#define CAN_DRIVER_INTERRUPT           /* define either CAN_DRIVER_POLLED or CAN_DRIVER_INTERRUPT */
#define CAN_BUFFER_SIZE_BYTES  2048  
#define CAN_MAINTAIN_STATISTICS        /* usage statistics are maintained if this is enabled */
//#define DEBUG_CAN_TX_INTERRUPT_TASK_PORT   DEBUG_1
//#define DEBUG_CAN_RX_INTERRUPT_TASK_PORT   DEBUG_1
//#define DEBUG_CAN_BACKGROUND_TASK_PORT  DEBUG_1

// CAN message filters
// messages that match these identifiers will trigger the appropriate callback
// only MASK bits set to 1 are matched -- other bits are assumed to match
// filters are scanned in the order listed here
//
// there are two types of filters, TX and RX
// CAN_TX_MESSAGE_FILTER(filter_id, filter_mask, callback)
// CAN_RX_MESSAGE_FILTER(filter_id, filter_mask, callback)
//    filter match occurs when (message_id & filter_mask) == filter_id
//    callback is called if tx/rx match is made
#define CAN_MESSAGE_FILTERS \
	CAN_RX_MESSAGE_FILTER( ID(0), MASK(0), CALLBACK(IDS_CAN_OnCanMessageRx)) \

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// IDS CAN settings

#define IDS_CAN_NUM_DAQ_CHANNELS   16
#define CAPABILITY_DFLT 0 	

// declare all devices that are supported
// IDS_CAN_DEVICE(name, address, DEVICE(type, instance, capability))
//   	name = local name passed to functions
//   	address = network address of the device
//             0 = address claim
//             non-zero = fixed address (only used during debugging)
//		DEVICE
//			type = IDS_CAN det
//
//

#define DECLARE_IDS_CAN_DEVICES \
	DECLARE_IDS_CAN_DEVICE(DEVICE_SWITCH_1, DEVICE(IDS_CAN_DEVICE_TYPE_SWITCH, 1, CAPABILITY_DFLT)) \
	DECLARE_IDS_CAN_DEVICE(DEVICE_SWITCH_2, DEVICE(IDS_CAN_DEVICE_TYPE_SWITCH, 2, CAPABILITY_DFLT)) \
	DECLARE_IDS_CAN_DEVICE(DEVICE_SWITCH_3, DEVICE(IDS_CAN_DEVICE_TYPE_SWITCH, 3, CAPABILITY_DFLT)) \
	DECLARE_IDS_CAN_DEVICE(DEVICE_SWITCH_4, DEVICE(IDS_CAN_DEVICE_TYPE_SWITCH, 4, CAPABILITY_DFLT)) \
	DECLARE_IDS_CAN_DEVICE(DEVICE_SWITCH_5, DEVICE(IDS_CAN_DEVICE_TYPE_SWITCH, 5, CAPABILITY_DFLT)) \
	DECLARE_IDS_CAN_DEVICE(DEVICE_SWITCH_6, DEVICE(IDS_CAN_DEVICE_TYPE_SWITCH, 6, CAPABILITY_DFLT)) \
	DECLARE_IDS_CAN_DEVICE(DEVICE_SWITCH_7, DEVICE(IDS_CAN_DEVICE_TYPE_SWITCH, 7, CAPABILITY_DFLT)) \
	DECLARE_IDS_CAN_DEVICE(DEVICE_SWITCH_8, DEVICE(IDS_CAN_DEVICE_TYPE_SWITCH, 8, CAPABILITY_DFLT)) \

// IDS-CAN message filters
// messages that match these identifiers will trigger the appropriate callback
// only MASK bits set to 1 are matched -- other bits are assumed to match
// filters are scanned in the order listed here
//
// there are two types of filters, TX and RX
// IDS_CAN_TX_MESSAGE_FILTER(message_type, callback)
// IDS_CAN_RX_MESSAGE_FILTER(message_type, callback)
//    filter match occurs when message_type matches the message
//    message_type = 0xFF implies that all messages match
//    callback is called if tx/rx match is made
#define IDS_CAN_MESSAGE_FILTERS \
	IDS_CAN_RX_MESSAGE_FILTER(IDS_CAN_MESSAGE_TYPE_DEVICE_ID,  Session_OnDeviceIDMessageRx) \
	IDS_CAN_RX_MESSAGE_FILTER(IDS_CAN_MESSAGE_TYPE_CIRCUIT_ID, Session_OnCircuitIDMessageRx) \
	IDS_CAN_RX_MESSAGE_FILTER(IDS_CAN_MESSAGE_TYPE_STATUS,     Session_OnStatusMessageRx) \
	IDS_CAN_RX_MESSAGE_FILTER(IDS_CAN_MESSAGE_TYPE_RESPONSE,   Session_OnResponseMessageRx) \


// ** OPTIONAL **   comment out this block if PIDs support is not needed
//
// declare all PIDs supported, format is any combination of
// read only:
//     PID_READ_ONLY(id, read_expression)
// read/write (RAM):
//     PID_READ_WRITE(id, read_expression, session_id, write_expression)
// read/write (non-volatile)
//     PID_READ_WRITE_NVR(id, read_expression, session_id, write_expression)
// where:
//                 id : the 16-bit PID defined in the IDS-CAN specification
//         session_id : a session ID that must be unlocked/open to allow writing
//                      set to zero to allow writing at any time
//    read_expression : an expression which returns the value of the PID
//   write_expression : an expression which sets the value of the PID
#define IDS_CAN_PID_SUPPORT \
	PID_READ_WRITE_NVR(IDS_CAN_PID_PRODUCTION_BYTES,          VAL_U8 = Config.ProductionBlock.ProductionByte, IDS_CAN_SESSION_ID_MANUFACTURING, Config.ProductionBlock.ProductionByte = VAL_U8; Config_ProductionBlock_Flush()) \
	PID_READ_WRITE_NVR(IDS_CAN_PID_CAN_ADAPTER_MAC,           VAL_U48 = *(U48*) IDS_CAN_GetAdapterMAC(),      IDS_CAN_SESSION_ID_MANUFACTURING, IDS_CAN_SetAdapterMAC((const uint8*)&val)) \
	PID_READ_WRITE_NVR(IDS_CAN_PID_IDS_CAN_CIRCUIT_ID,        VAL_U32 = IDS_CAN_GetCircuitID(device),         IDS_CAN_SESSION_ID_DIAGNOSTIC,    IDS_CAN_SetCircuitID(device, VAL_U32); ) \
	PID_READ_WRITE_NVR(IDS_CAN_PID_IDS_CAN_FUNCTION_NAME,     VAL_U16 = IDS_CAN_GetFunctionName(device),      IDS_CAN_SESSION_ID_DIAGNOSTIC,    IDS_CAN_SetFunctionName(device, VAL_U16); ) \
	PID_READ_WRITE_NVR(IDS_CAN_PID_IDS_CAN_FUNCTION_INSTANCE, VAL_U8 = IDS_CAN_GetFunctionInstance(device),   IDS_CAN_SESSION_ID_DIAGNOSTIC,    IDS_CAN_SetFunctionInstance(device, VAL_U8); ) \
	PID_READ_ONLY(IDS_CAN_PID_REGULATOR_VOLTAGE,              VAL_U32 = (uint32)(VDD * 65536.0 + 0.5)) \
	PID_READ_ONLY(IDS_CAN_PID_MANUFACTURING_PID_1,            VAL_U8 = Input.Digital.Switch1) \
	PID_READ_ONLY(IDS_CAN_PID_MANUFACTURING_PID_2,            VAL_U8 = Input.Digital.Switch2) \
	PID_READ_ONLY(IDS_CAN_PID_MANUFACTURING_PID_3,            VAL_U8 = Input.Digital.Switch3) \
	PID_READ_ONLY(IDS_CAN_PID_MANUFACTURING_PID_4,            VAL_U8 = Input.Digital.Switch4) \
	PID_READ_ONLY(IDS_CAN_PID_MANUFACTURING_PID_5,            VAL_U8 = Input.Digital.Switch5) \
	PID_READ_ONLY(IDS_CAN_PID_MANUFACTURING_PID_6,            VAL_U8 = Input.Digital.Switch6) \
	PID_READ_ONLY(IDS_CAN_PID_MANUFACTURING_PID_7,            VAL_U8 = Input.Digital.Switch7) \
	PID_READ_ONLY(IDS_CAN_PID_MANUFACTURING_PID_8,            VAL_U8 = Input.Digital.Switch8) \

// ** OPTIONAL **   comment out this block if session support is not needed
//
// declare all SESSION_IDs supported, format is
//     SESSION(id, cypher, ids_session_allowed_callback, session_open_callback, session_close_callback)
// where:
//                             id :  the 16-bit SESSION_ID defined in the IDS-CAN specification
//                         cypher :  a unique 32-bit value that is used when generating the security key
//                                   this value should match the value in the IDS-CAN specification
//    is_session_allowed_callback :  function that is called to determine if a session is allowed at this time, can be NULL
//                                   function prototype is IDS_CAN_RESPONSE_CODE CALLBACK(IDS_CAN_DEVICE device, uint8 host_address, IDS_CAN_SESSION_ID id);
//          session_open_callback :  function that is called when the session is opened, can be NULL
//                                   function prototype is CALLBACK(IDS_CAN_DEVICE device, uint8 host_address, IDS_CAN_SESSION_ID id);
//          session_close_callback : function that is called when the session is opened, can be NULL
#define IDS_CAN_SESSION_SUPPORT \
	SESSION(IDS_CAN_SESSION_ID_MANUFACTURING,  0xB16BA115, NULL, Manufacturing_OnIDSCANSessionOpened, Manufacturing_OnIDSCANSessionClosed) \
	SESSION(IDS_CAN_SESSION_ID_DIAGNOSTIC,     0xBABECAFE, NULL, Manufacturing_OnIDSCANSessionOpened, Manufacturing_OnIDSCANSessionClosed) \

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// IDSCore
#define IDSCORE_DISABLE_ASSERT_CHECKING

#include "IDSCore.c"
#include "IDSCore.Assert.c"

// IDS-CAN support
#include "CAN.c"                // required - CAN driver
#include "IDS-CAN.c"            // required - IDS-CAN driver

// include software module header information
#include "Config_B.c"
#include "CRC.c"
#include "EEPROM.c"
#include "IDS-CAN_Integration.c"
#include "Input.c"
#include "Manufacturing.c"
#include "OS.c"
#include "SCI COBS.c"
#include "SCI Host.c"
#include "Session.c"

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

//lint --flb  we have left the library

#undef HEADER
#endif
