// IDS-CAN.c
// IDS-CAN common support
// Version 2.8
// (c) 2012, 2013, 2015, 2016, 2017 Innovative Design Solutions, Inc.
// All rights reserved

// History
// Version 1.0   First version
// Version 1.1   Removed CAN Network IDs and device state, these are deprecated and not used moving forward
// Version 1.1a  formatting
// Version 1.2   March 2013 IDS-CAN update
// Version 1.2a  Added IDS_CAN_ADDRESS type
//               added code to pump RNG whenever a message is received
// Version 1.3   Added IDS_CAN_MESSAGE_TYPE_EXT_STATUS
// Version 1.4   Added encryption support
// Version 1.5   June 2013 IDS-CAN v1.1 update
// Version 1.6   added Timestamp_ms to IDS_CAN_RX_MESSAGE
// Version 1.6a  added support for application specific message routing
//               via IDS_CAN_DECLARE_CUSTOM_MESSAGE_ROUTING macro
// Version 1.6b  Minor change to remove complier warning
// Version 1.6c  Updated to IDS-CAN v1.6 Jan 2016
//               Added IDS_CAN_MESSAGE_TYPE_BULK_XFER
//               Changed CAN_TX_CALLBACK to CAN_CALLBACK
// Version 2.1   Updated to IDS-CAN v2.1 Jan 2016
//               Added IDS_CAN_MESSAGE_TYPE_TEXT_CONSOLE
// Version 2.1a  Added management task support for IDS_CAN_MESSAGE_TYPE_TEXT_CONSOLE
// Version 2.4   IDS-CAN v2.4 Jun 2016
//               IDS_CAN_Task10ms() changed to IDS_CAN_Task()
//               29-bit message types changed from 5-bit to 4-bit
//               Added support for remote routing flag (cellular, etc)
//               Added support for DTCs
//               Eliminated IDS_CAN_ID structure, IDS-CAN ID now directly loaded into IDS_CAN_RX_MESSAGE structure
// Version 2.4a  Added call to _IDS_CAN_Network_Task1ms()
// Version 2.6   Updated to IDS-CAN v2.6 Mar 2017
//               Added support for network driven real-time clock
// Version 2.7   IDS-CAN v2.7 Apr 2017
// Version 2.8   Renamed macros for IDS_CAN_?X_MESSAGE() to IDS_CAN_?X_MESSAGE_FILTER()
//               Removed _IDS_CAN_RTC_Init() which was empty

#include "global.h"

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#ifdef HEADER

// include headers for IDS-CAN types and constants
#include "IDS-CAN_BLOCK_ID.h"
#include "IDS-CAN_DEVICE_TYPE.h"
#include "IDS-CAN_DTC_ID.h"
#include "IDS-CAN_FUNCTION_NAME.h"
#include "IDS-CAN_PID.h"
#include "IDS-CAN_PRODUCT_ID.h"
#include "IDS-CAN_SESSION_ID.h"
#include "IDS-CAN_VERSION_NUMBER.h"

typedef uint8 IDS_CAN_ADDRESS;
#define IDS_CAN_ADDRESS_BROADCAST  0x00

typedef uint8 IDS_CAN_MESSAGE_TYPE;

// bitmasks for IDS-CAN message types
// BIT5 = 0 for 11-bit types
// BIT5 = 1 for 29-bit types
#define IDS_CAN_MESSAGE_TYPE_NETWORK       0x00 /* 000b   11-bit */
#define IDS_CAN_MESSAGE_TYPE_CIRCUIT_ID    0x01 /* 001b   11-bit */
#define IDS_CAN_MESSAGE_TYPE_DEVICE_ID     0x02 /* 010b   11-bit */
#define IDS_CAN_MESSAGE_TYPE_STATUS        0x03 /* 011b   11-bit */
//#define IDS_CAN_MESSAGE_TYPE_???           0x04 /* 100b   11-bit */
//#define IDS_CAN_MESSAGE_TYPE_???           0x05 /* 101b   11-bit */
//#define IDS_CAN_MESSAGE_TYPE_???           0x06 /* 110b   11-bit */
#define IDS_CAN_MESSAGE_TYPE_TIME          0x07 /* 111b   11-bit */
#define IDS_CAN_MESSAGE_TYPE_REQUEST       0x20 /* 00000b   29-bit */
#define IDS_CAN_MESSAGE_TYPE_RESPONSE      0x21 /* 00001b   29-bit */
#define IDS_CAN_MESSAGE_TYPE_COMMAND       0x22 /* 00010b   29-bit */
#define IDS_CAN_MESSAGE_TYPE_EXT_STATUS    0x23 /* 00011b   29-bit */
#define IDS_CAN_MESSAGE_TYPE_TEXT_CONSOLE  0x24 /* 00100b   29-bit */
//#define IDS_CAN_MESSAGE_TYPE_???           0x25 /* 00101b   29-bit */
//#define IDS_CAN_MESSAGE_TYPE_???           0x26 /* 00110b   29-bit */
//#define IDS_CAN_MESSAGE_TYPE_???           0x27 /* 00111b   29-bit */
//#define IDS_CAN_MESSAGE_TYPE_???           0x28 /* 01000b   29-bit */
//#define IDS_CAN_MESSAGE_TYPE_???           0x29 /* 01001b   29-bit */
//#define IDS_CAN_MESSAGE_TYPE_???           0x2A /* 01010b   29-bit */
//#define IDS_CAN_MESSAGE_TYPE_???           0x2B /* 01011b   29-bit */
//#define IDS_CAN_MESSAGE_TYPE_???           0x2C /* 01100b   29-bit */
//#define IDS_CAN_MESSAGE_TYPE_???           0x2D /* 01101b   29-bit */
//#define IDS_CAN_MESSAGE_TYPE_???           0x2E /* 01110b   29-bit */
//#define IDS_CAN_MESSAGE_TYPE_???           0x2F /* 01111b   29-bit */
//#define IDS_CAN_MESSAGE_TYPE_???           0x30 /* 10000b   29-bit */
//#define IDS_CAN_MESSAGE_TYPE_???           0x31 /* 10001b   29-bit */
//#define IDS_CAN_MESSAGE_TYPE_???           0x32 /* 10010b   29-bit */
//#define IDS_CAN_MESSAGE_TYPE_???           0x33 /* 10011b   29-bit */
//#define IDS_CAN_MESSAGE_TYPE_???           0x34 /* 10100b   29-bit */
//#define IDS_CAN_MESSAGE_TYPE_???           0x35 /* 10101b   29-bit */
//#define IDS_CAN_MESSAGE_TYPE_???           0x36 /* 10110b   29-bit */
//#define IDS_CAN_MESSAGE_TYPE_???           0x37 /* 10111b   29-bit */
//#define IDS_CAN_MESSAGE_TYPE_???           0x38 /* 11000b   29-bit */
//#define IDS_CAN_MESSAGE_TYPE_???           0x39 /* 11001b   29-bit */
//#define IDS_CAN_MESSAGE_TYPE_???           0x3A /* 11010b   29-bit */
#define IDS_CAN_MESSAGE_TYPE_DAQ           0x3B /* 11011b   29-bit */
//#define IDS_CAN_MESSAGE_TYPE_???           0x3C /* 11100b   29-bit */
#define IDS_CAN_MESSAGE_TYPE_IOT           0x3D /* 11101b   29-bit */
//#define IDS_CAN_MESSAGE_TYPE_???           0x3E /* 11110b   29-bit */
#define IDS_CAN_MESSAGE_TYPE_BULK_XFER     0x3F /* 11111b   29-bit */

// support for application defined message routing
typedef enum {
#ifdef IDS_CAN_DECLARE_CUSTOM_MESSAGE_ROUTING
	#undef DECLARE_IDS_CAN_ROUTE
	#define DECLARE_IDS_CAN_ROUTE(name, callback) name,
	IDS_CAN_DECLARE_CUSTOM_MESSAGE_ROUTING
#endif // IDS_CAN_DECLARE_CUSTOM_MESSAGE_ROUTING
	ROUTE_OVER_CAN, NUM_IDS_CAN_ROUTES } IDS_CAN_ROUTE;

// structure for passing IDS-CAN ID data around
typedef struct
{
	IDS_CAN_MESSAGE_TYPE MessageType;
	IDS_CAN_ADDRESS SourceAddress;
	IDS_CAN_ADDRESS TargetAddress;
	uint8 ExtMessageData;
} IDS_CAN_ID;

// elements of structure arranged for optimal memory alignment
typedef struct
{
	uint8 Data[8]; // data bytes
	uint16 Timestamp_ms; // holds timestamp of rx message
	uint8 Length; // data length

	// CAN ID information converted to IDS-CAN format
	IDS_CAN_MESSAGE_TYPE MessageType:6;
	uint8 IsEcho:1; // set if message is an echo of a message transmitted by this product
	IDS_CAN_ADDRESS SourceAddress;
	IDS_CAN_ADDRESS TargetAddress;
	uint8 ExtMessageData; // extended payload byte
#ifdef IDS_CAN_DECLARE_CUSTOM_MESSAGE_ROUTING
	IDS_CAN_ROUTE Route; // holds which route the message came from
#endif // IDS_CAN_DECLARE_CUSTOM_MESSAGE_ROUTING
} IDS_CAN_RX_MESSAGE;

// enumerate the devices that are present
typedef enum {
	#undef DECLARE_IDS_CAN_DEVICE
	#define DECLARE_IDS_CAN_DEVICE(name, device) name,
	DECLARE_IDS_CAN_DEVICES
	NUM_IDS_CAN_DEVICES
} IDS_CAN_DEVICE;

// management routines
void IDS_CAN_Init(void);
void IDS_CAN_Task(void);

// helper function - creates a CAN ID from IDS-CAN MESSAGE_TYPE and address
uint32 IDS_CAN_CreateCanId29(IDS_CAN_MESSAGE_TYPE MessageType, IDS_CAN_ADDRESS SourceAddress, IDS_CAN_ADDRESS TargetAddress, uint8 ExtMessageData);
uint32 IDS_CAN_CreateCanId11(IDS_CAN_MESSAGE_TYPE MessageType, IDS_CAN_ADDRESS SourceAddress);
IDS_CAN_ID IDS_CAN_CreateIdsCanId(uint32 id);

// callback routines -- application can inject messages via one of these mechanisms
void IDS_CAN_OnRoutedMessageRx(IDS_CAN_ROUTE route, const CAN_RX_MESSAGE *rx);
void IDS_CAN_OnCanMessageRx(const CAN_RX_MESSAGE *rx);

// use this to transmit on the IDS-CAN bus (do not use CAN_Tx())
uint8 IDS_CAN_Tx(IDS_CAN_DEVICE device, const CAN_TX_MESSAGE * message);

// encryption support
// based loosely on the Tiny Encryption Algorithm
uint32 IDS_CAN_Encrypt(uint32 seed, uint32 cypher);

// include support for other IDS-CAN support modules
#include "IDS-CAN_Network.c"         // required - network management
#include "IDS-CAN_Request.c"         // required - default support message interface
#include "IDS-CAN_Status.c"          // required - default status message interface
#include "IDS-CAN_DAQ.c"             // required - data acquisition protocol
#include "IDS-CAN_DTC_Manager.c"     // required - default DTC management
#include "IDS-CAN_TEXT_CONSOLE.c"    // text console support
#include "IDS-CAN_REAL_TIME_CLOCK.c" // real-time clock support


#else // HEADER

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#if defined(__GNUC__)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunknown-pragmas"
#endif

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

#define IDS_CAN_MESSAGE_TYPE_11_BIT  0x00
#define IDS_CAN_MESSAGE_TYPE_29_BIT  0x20

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

// CAN transmit macro

#ifndef IDS_CAN_DECLARE_CUSTOM_MESSAGE_ROUTING
	#define TRANSMIT_OVER_CAN(message, callback) CAN_Tx(message, callback);
#else
	#pragma INLINE
	static uint8 TRANSMIT_OVER_CAN(const CAN_TX_MESSAGE * message, CAN_CALLBACK callback)
	{
		if (CAN_Tx(message, callback))
			return TRUE;

		// unable to transmit
		// this could be because the CAN bus has problems
		// a bad CAN bus could potentially cause the IDS-CAN layers to retry messages "forever" and stall the bus
		// so, if the CAN bus goes down, we don't want to "block" the other routes for transmission

		// basically we check to see if the bus appears to be up or down
		// if the bus looks like it is working, we return "FALSE" as this is a true failure to TX
		// if the bus looks like it has problems, we return "TRUE" faking a good TX, allowing the app to move on as if everything else was OK
		return !(CAN_TimeSinceLastRx_ms() < 1000 && CAN_TimeSinceLastTx_ms() < 1000);
	}
#endif

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

// custom routing
#ifdef IDS_CAN_DECLARE_CUSTOM_MESSAGE_ROUTING

// saves source addresses used on custom routes
static uint8 RouteAddress[NUM_IDS_CAN_ROUTES - 1] = { 0 };

// flags used when doing custom routing
static uint8 RouteTimeout[NUM_IDS_CAN_ROUTES - 1] = { 0 };

static IDS_CAN_ROUTE GetRouteAssociatedWithAddress(IDS_CAN_ADDRESS address)
{
	IDS_CAN_ROUTE r;

	if (address == IDS_CAN_ADDRESS_BROADCAST)
		return (IDS_CAN_ROUTE)0xFF; // all routes

	for (r=(IDS_CAN_ROUTE)0; r<ROUTE_OVER_CAN; r++)
	{
		if (RouteAddress[r] == address)
			break;
	}

	return r;
}

// manage routes
static void ManageRoutingTable(void)
{
	IDS_CAN_ROUTE r;

	for (r=(IDS_CAN_ROUTE)0; r<ROUTE_OVER_CAN; r++)
	{
		if (RouteAddress[r] && RouteTimeout[r] == 0xFF)
		{
			// transmit "kill address claim" message on all open routes

			// compose outgoing message
			CAN_TX_MESSAGE msg;
			msg.Length = 8;
			msg.ID = IDS_CAN_CreateCanId11(IDS_CAN_MESSAGE_TYPE_NETWORK, RouteAddress[r]);
			msg.Data[0] = 0; // status
			msg.Data[1] = SUPPORTED_IDS_CAN_VERSION_NUMBER;
			msg.Data[2] = 0; // MAC address 00:00:00:00:00:00
			msg.Data[3] = 0;
			msg.Data[4] = 0;
			msg.Data[5] = 0;
			msg.Data[6] = 0;
			msg.Data[7] = 0;

			// transmit on all routes except this route
			if (!TRANSMIT_OVER_CAN(&msg, NULL)) continue; // keep trying to transmit
			#undef DECLARE_IDS_CAN_ROUTE
			#define DECLARE_IDS_CAN_ROUTE(name, tx) if (r != name) tx(name, &msg);
			IDS_CAN_DECLARE_CUSTOM_MESSAGE_ROUTING
		}

		// decrement route
		if (RouteTimeout[r])
			RouteTimeout[r]--;
		else
			RouteAddress[r] = 0;
	}
}

#endif // IDS_CAN_DECLARE_CUSTOM_MESSAGE_ROUTING

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

// structure for packing 8 CAN data bytes
typedef struct { uint8 Data[8]; } DATA; /*lint !e770 This tag may be defined elsewhere, but this is a local definition so that is fine */

#pragma INLINE
IDS_CAN_ID IDS_CAN_CreateIdsCanId(uint32 in)
{
	IDS_CAN_ID result;
	DWORD id;

	// unpack message ID
	id.ui32 = in;
	if (id.ui32 & CAN_ID_EXTENDED)
	{
		// 29-bit mode
		// ---xxxxx xxxxxxxx xxxxxxxx xxxxxxxx
		//    ||||| |||||||| |||||||| ||||||||
		//    ||||| |||||||| ||||||||  \\\\\\\\_____ message data
		//    ||||| ||||||||  \\\\\\\\______________ target address
		//    ||||| |||||| \\_______________________ message type (lsb)
		//    ||| \\_\\\\\\_________________________ source address
		//     \\\__________________________________ message type (msb)
		result.MessageType = IDS_CAN_MESSAGE_TYPE_29_BIT
			| (id.word.hi.ui8.lo & 0x3)
			| (id.word.hi.ui8.hi & 0x1C);
		result.SourceAddress = (IDS_CAN_ADDRESS)(id.ui16.hi >> 2);
		result.TargetAddress = (IDS_CAN_ADDRESS)id.word.lo.ui8.hi;
		result.ExtMessageData = id.word.lo.ui8.lo;
	}
	else
	{
		// 11-bit mode
		// -----xxx xxxxxxxx
		//      ||| ||||||||
		//      ||| \\\\\\\\____ source address
		//       \\\____________ message type
		ASSERT((in & 0xFFFFF800) == 0);
		result.TargetAddress = IDS_CAN_ADDRESS_BROADCAST;
		result.ExtMessageData = 0;
		result.MessageType = IDS_CAN_MESSAGE_TYPE_11_BIT | (id.word.lo.ui8.hi & 0x7); /*lint !e835 IDS_CAN_MESSAGE_TYPE_11_BIT is zero */
		result.SourceAddress = (IDS_CAN_ADDRESS)id.word.lo.ui8.lo;
	}

	return result;
}

uint32 IDS_CAN_CreateCanId29(IDS_CAN_MESSAGE_TYPE MessageType, IDS_CAN_ADDRESS SourceAddress, IDS_CAN_ADDRESS TargetAddress, uint8 ExtMessageData)
{
	// 29-bit mode
	// ---xxxxx xxxxxxxx xxxxxxxx xxxxxxxx
	//    ||||| |||||||| |||||||| ||||||||
	//    ||||| |||||||| ||||||||  \\\\\\\\_____ message data
	//    ||||| ||||||||  \\\\\\\\______________ target address
	//    ||||| |||||| \\_______________________ message type (lsb)
	//    ||| \\_\\\\\\_________________________ source address
	//     \\\__________________________________ message type (msb)
	DWORD result;
	ASSERT(MessageType & IDS_CAN_MESSAGE_TYPE_29_BIT);
	result.ui16.hi = (uint16)SourceAddress << 2;
	result.word.hi.ui8.hi |= MessageType & 0x1C;
	result.word.hi.ui8.lo |= MessageType & 0x3;
	result.word.lo.ui8.hi = TargetAddress;
	result.word.lo.ui8.lo = ExtMessageData;
	result.ui32 |= CAN_ID_EXTENDED;
	return result.ui32;
}

uint32 IDS_CAN_CreateCanId11(IDS_CAN_MESSAGE_TYPE MessageType, IDS_CAN_ADDRESS SourceAddress)
{
	// 11-bit mode
	// -----xxx xxxxxxxx
	//      ||| ||||||||
	//      ||| \\\\\\\\____ source address
	//       \\\____________ message type
	DWORD result;
	ASSERT(MessageType <= 7);
	result.ui16.hi = 0;
	result.word.lo.ui8.hi = MessageType & 7;
	result.word.lo.ui8.lo = SourceAddress;
	return result.ui32;
}

#pragma INLINE
static void UnpackCANMessage(const CAN_RX_MESSAGE *in, IDS_CAN_RX_MESSAGE * out, uint8 is_echo)
{
	IDS_CAN_ID id = IDS_CAN_CreateIdsCanId(in->ID);

	out->SourceAddress = id.SourceAddress;
	out->TargetAddress = id.TargetAddress;
	out->ExtMessageData = id.ExtMessageData;
	out->MessageType = id.MessageType;

	out->IsEcho = (is_echo ? 1 : 0);

	// copy rest of payload
	*((DATA*)out->Data) = *((DATA*)in->Data);
	ASSERT(in->Length <= 8);
	out->Length = in->Length;
	out->Timestamp_ms = in->Timestamp_ms;
}


////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

// dispatch received messages to the appropriate callbacks
#pragma MESSAGE DISABLE C4000 /* condition always true */
#pragma MESSAGE DISABLE C4001 /* condition always false */
/*lint -save -e506 -e774 Depending on how masks are setup, they may always evaluate to TRUE or FALSE */
static void IDS_CAN_OnIDSCANMessageRx(const IDS_CAN_RX_MESSAGE *message)
{
	// IDS-CAN management callbacks
	switch (message->MessageType)
	{
	default: break;
	case IDS_CAN_MESSAGE_TYPE_NETWORK: _IDS_CAN_Network_OnMessageRx(message); break;
	case IDS_CAN_MESSAGE_TYPE_REQUEST: _IDS_CAN_OnRequestMessageRx(message); break;
	case IDS_CAN_MESSAGE_TYPE_TIME:    _IDS_CAN_RTC_OnTimeMessageRx(message); break;
	case IDS_CAN_MESSAGE_TYPE_DAQ:     _IDS_CAN_OnDAQMessageRx(message); break;
	}

	// execute user callbacks
	#undef IDS_CAN_TX_MESSAGE_FILTER
	#undef IDS_CAN_RX_MESSAGE_FILTER
	#define IDS_CAN_TX_MESSAGE_FILTER(type, callback)
	#define IDS_CAN_RX_MESSAGE_FILTER(type, callback) if ((type) == 0xFF || message->MessageType == (type)) callback(message);
	IDS_CAN_MESSAGE_FILTERS
}
#pragma MESSAGE DEFAULT C4000
#pragma MESSAGE DEFAULT C4001
/*lint -restore */

// convert message to routed message
#pragma MESSAGE DISABLE C5703 /* route not referenced */
/*lint -save -e715 route not refenced */
void IDS_CAN_OnRoutedMessageRx(IDS_CAN_ROUTE route, const CAN_RX_MESSAGE *rx)
{
	IDS_CAN_RX_MESSAGE msg;
	UnpackCANMessage(rx, &msg, FALSE);
#ifdef IDS_CAN_DECLARE_CUSTOM_MESSAGE_ROUTING
	ASSERT(route < NUM_IDS_CAN_ROUTES);
	msg.Route = route; // save the route the message came from
#endif // IDS_CAN_DECLARE_CUSTOM_MESSAGE_ROUTING

#ifdef IDS_CAN_DECLARE_CUSTOM_MESSAGE_ROUTING
	// was the message received on an alternate route (non CAN)
	if (route < ROUTE_OVER_CAN)
	{
		// if the device is transmitting its NETWORK message (claiming/using an address)
		if (msg.MessageType == IDS_CAN_MESSAGE_TYPE_NETWORK)
		{
			IDS_CAN_ROUTE existing_route;

			// whats the source address of the message
			// for an address claim message, the address is the first data byte
			uint8 address = (msg.SourceAddress ? msg.SourceAddress : msg.Data[0]);

			// make sure address isn't being used by a route
			existing_route = GetRouteAssociatedWithAddress(address);
			if (existing_route == route || existing_route >= ROUTE_OVER_CAN)
			{
				// we should associate that address with this route
				RouteAddress[route] = address;
				RouteTimeout[route] = 0xFF; // reset timeout -- forces a transmit of the "kill address claim" message on other routes

				// force the MAC to 00:00:00:00:00:00 -- let the routed message win address contention
				msg.Data[2] = 0;
				msg.Data[3] = 0;
				msg.Data[4] = 0;
				msg.Data[5] = 0;
				msg.Data[6] = 0;
				msg.Data[7] = 0;
			}
		}
		else if (RouteAddress[route] == msg.SourceAddress && RouteTimeout[route] < 0xFE)
			RouteTimeout[route] = 0xFE; // keep the route alive
	}
#endif // IDS_CAN_DECLARE_CUSTOM_MESSAGE_ROUTING

	IDS_CAN_OnIDSCANMessageRx(&msg);
}
#pragma MESSAGE DEFAULT C5703
/*lint -restore */

// convert CAN message to IDS-CAN message and dispatch
void IDS_CAN_OnCanMessageRx(const CAN_RX_MESSAGE *rx)
{
	IDS_CAN_OnRoutedMessageRx(ROUTE_OVER_CAN, rx);
}

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

// dispatch transmitted messages to the appropriate callbacks
#pragma MESSAGE DISABLE C4000 /* condition always true */
#pragma MESSAGE DISABLE C4001 /* condition always false */
static void IDS_CAN_OnCanMessageTx(const CAN_RX_MESSAGE *tx)
{
	IDS_CAN_RX_MESSAGE msg;
	UnpackCANMessage(tx, &msg, TRUE);
#ifdef IDS_CAN_DECLARE_CUSTOM_MESSAGE_ROUTING
	msg.Route = ROUTE_OVER_CAN; // save the route the message came from
#endif // IDS_CAN_DECLARE_CUSTOM_MESSAGE_ROUTING

	// execute user callbacks
	#undef IDS_CAN_TX_MESSAGE_FILTER
	#undef IDS_CAN_RX_MESSAGE_FILTER
	#define IDS_CAN_TX_MESSAGE_FILTER(type, callback) if ((type) == 0xFF || msg.MessageType == (type)) callback(&msg);
	#define IDS_CAN_RX_MESSAGE_FILTER(type, callback)
	IDS_CAN_MESSAGE_FILTERS

	// echo all transmitted messages back to ourselves
	IDS_CAN_OnIDSCANMessageRx(&msg);
}
#pragma MESSAGE DEFAULT C4000
#pragma MESSAGE DEFAULT C4001

// transmit, but only if the device is online
uint8 IDS_CAN_Tx(IDS_CAN_DEVICE device, const CAN_TX_MESSAGE * message)
{
	// do not transmit if the device is offline
	// the only exception is if the device is transmitting a broadcast network message
	if (IDS_CAN_GetDeviceAddress(device) == IDS_CAN_ADDRESS_BROADCAST && message->ID != 0)
		return FALSE;
	// when we get here, either we are online, or the ID is zero

#ifndef IDS_CAN_DECLARE_CUSTOM_MESSAGE_ROUTING
	// only one route
	// go ahead and transmit
	return CAN_Tx(message, IDS_CAN_OnCanMessageTx);
#else
	// application specific routing enabled
	if (message->ID & CAN_ID_EXTENDED)
	{
		// this is a device to device message
		// determine route to the target address of the message
		switch (GetRouteAssociatedWithAddress((IDS_CAN_ADDRESS)(message->ID >> 8)))
		{
			#undef DECLARE_IDS_CAN_ROUTE
			#define DECLARE_IDS_CAN_ROUTE(name, tx) case name: return tx(name, message);
			IDS_CAN_DECLARE_CUSTOM_MESSAGE_ROUTING

			case ROUTE_OVER_CAN: return TRANSMIT_OVER_CAN(message, IDS_CAN_OnCanMessageTx);
			default: break; // broadcast on all routes
		}
	}

	// this is a broadcast message
	// transmit on all adapters

	// route message on CAN bus
	if (!TRANSMIT_OVER_CAN(message, IDS_CAN_OnCanMessageTx)) return FALSE; // wait till message goes out over CAN bus before attempting alternate routing

	// use all alternate routes
	#undef DECLARE_IDS_CAN_ROUTE
	#define DECLARE_IDS_CAN_ROUTE(name, tx) (void)tx(name, message);
	IDS_CAN_DECLARE_CUSTOM_MESSAGE_ROUTING

	return TRUE;
#endif // IDS_CAN_DECLARE_CUSTOM_MESSAGE_ROUTING
}

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

// encryption function
// based loosely on the Tiny Encryption Algorithm
uint32 IDS_CAN_Encrypt(uint32 seed, uint32 cypher)
{
	// encryption constants
	#define MAGIC_CONSTANT 0x9e3779b9
	#define K0 0x436F7079 /* Copy */
	#define K1 0x72696768 /* righ */
	#define K2 0x74204944 /* t ID */
	#define K3 0x53736E63 /* Sinc */

	uint8 loops = 32;
	uint32 sum = MAGIC_CONSTANT;
	for(;;)
	{
		seed += ((cypher << 4) + K0) ^ (cypher + sum) ^ ((cypher >> 5) + K1);
		if (!--loops)
			return seed;
		cypher += ((seed << 4) + K2) ^ (seed + sum) ^ ((seed >> 5) + K3);
		sum += MAGIC_CONSTANT;
	}
}

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

void IDS_CAN_Init(void)
{
	// seed the RNG with a good source of entropy
#if defined MCU_MKE06Z4    // Kinetis KE06Z series
	Randomize( Hash32(0L, 12, (const uint8*) &SIM_UUIDL) );
#elif defined CPU_MK60DN512VLL10
	Randomize( Hash32(0L, 16, (const uint8*) &SIM_UIDH) );
#else
	Randomize( Hash32(0L, 6, IDS_CAN_GetAdapterMAC()) );
#endif

	_IDS_CAN_Network_Init();

	#ifdef DECLARE_IDS_CAN_TEXT_CONSOLES
	_IDS_CAN_TEXT_CONSOLE_Init();
	#endif // DECLARE_IDS_CAN_TEXT_CONSOLES

	#ifdef DECLARE_SUPPORTED_IDS_CAN_DTCS
	_IDS_CAN_DTC_Init();
	#endif // DECLARE_SUPPORTED_IDS_CAN_DTCS

	_IDS_CAN_DAQ_Init();
}

void IDS_CAN_Task(void)
{
	static uint16 clock_ms = 0;
	static uint8 _1ms = 0;
	static uint8 _10ms = 0;
	static uint16 _1000ms = 0;
	uint16 delta_ms;

	// read clock, and determine how much time has elapsed since last execution
	delta_ms = clock_ms;
	clock_ms = OS_GetElapsedMilliseconds16();
	delta_ms = clock_ms - delta_ms; // how much time has elapsed since last execution

	// continuous task
	_IDS_CAN_DAQ_Task(delta_ms); // run this first, as it needs to transmit at a high rate when active

	// 1 millisecond task
	if (_1ms != ((uint8)clock_ms))
	{
		_1ms = (uint8)clock_ms;

		// run networking layer 1ms task
		_IDS_CAN_Network_Task1ms();

		// 10 millisecond tasks
		if ((uint8)(clock_ms - _10ms) >= 10)
		{
			_10ms += 10;

			#ifdef IDS_CAN_DECLARE_CUSTOM_MESSAGE_ROUTING
			ManageRoutingTable();
			#endif // IDS_CAN_DECLARE_CUSTOM_MESSAGE_ROUTING

			// run networking layer 10ms task
			_IDS_CAN_Network_Task10ms();

			// run text console
			#ifdef DECLARE_IDS_CAN_TEXT_CONSOLES
			_IDS_CAN_TEXT_CONSOLE_Task10ms();
			#endif // DECLARE_IDS_CAN_TEXT_CONSOLES

			// DTC management
			#ifdef DECLARE_SUPPORTED_IDS_CAN_DTCS
			_IDS_CAN_DTC_Task10ms();
			#endif // DECLARE_SUPPORTED_IDS_CAN_DTCS

			_IDS_CAN_RTC_Task10ms(clock_ms);

			if ((uint16)(clock_ms - _1000ms) >= 1000)
			{
				_1000ms += 1000;

				// run 1 sec tasks
				_IDS_CAN_Network_Task1sec();
				_IDS_CAN_RTC_Task1sec();
			}
		}
	}

	// continuous tasks
	_IDS_CAN_Request_Task(clock_ms);
	_IDS_CAN_Status_Task(clock_ms);
}

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

#if defined(__GNUC__)
#pragma GCC diagnostic pop
#endif

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#endif // HEADER
