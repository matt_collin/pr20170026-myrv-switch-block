// Input.c
// Generic analog/digital input debounce routines for HCS12 platforms
// Version 1.2
// (c) 2012,2017 Innovative Design Solutions, Inc.
// All rights reserved

#include "global.h"

// HISTORY:
//
// 1.0   - first official release
//         based on HCS08 version 1.1
// 1.1   - compiler warnings removed
// 1.2   - Linted


// NOTES:
//
//
// Digital input debounce information.
//
// The DIGITAL_INPUTS macro defines all digital (true/false) inputs to be
// supported by the system.  This macro consists of a variable number of
// INPUT() macros -- each of which defines a single digital input.  If the
// DIGITAL_INPUTS macro is not defined, no digital input support is built.
//
// Usage: INPUT(name, TEST(expression), DEBOUNCE(type, count), init, event)
//       name - software name of input (Input.Digital.name and Input.Digital.nameEvent)
// expression - expression that returns actual digital value of input port
//       type - debounce counter variable type (uint8, uint16, etc...)
//      count - debounce trigger count (when count reached input is debounced)
//       init - initial boot value (INIT_TRUE / INIT_FALSE / INIT_ACTUAL)
//      event - defines optional event support (EVENT_SUPPORT / NO_EVENT_SUPPORT)
//
// Note that the actual debounce time depends on the frequency that Input_Task()
// is called
//
// Example:
//      #define DIGITAL_INPUTS \
//      INPUT(signal1, TEST(PORTA & BIT0), DEBOUNCE(uint8, 10), INIT_ACTUAL, NO_EVENT_SUPPORT)\
//      INPUT(signal2, TEST(PORTA & BIT1), DEBOUNCE(uint8, 20), INIT_TRUE,   EVENT_SUPPORT)\
//      INPUT(signal3, TEST(PORTB == 0),   DEBOUNCE(uint8, 30), INIT_FALSE,  EVENT_SUPPORT)\
//
//
// Analog input filter information
//
// The ANALOG_INPUTS macro defines all analog inputs to be regularly sampled
// and filtered by the system.  This macro consists of a variable number
// of INPUT() macros -- each of which defines a single analog input.  If
// the ANALOG_INPUTS macro is not defined, no analog input support is built.
//
// Usage: INPUT(name, read, filter)
//   name - software name of the input (creates Input_name() function)
//   read - expression that reads the analog value (16 bit result)
// filter - number of filter bits (value += (newvalue - value) >> filter)
//
// Note that the actual filter rate depends on the frequency that Input_Task()
// is called
//
// Example:
//      #define ANALOG_INPUTS                          \
//      INPUT(analog1, ADC_Convert(ADC_CHANNEL_0) << 8,  2) \
//      INPUT(analog2, ADC_Convert(ADC_CHANNEL_1) << 8,  8) \
//      INPUT(analog3, ADC_Convert(ADC_CHANNEL_2) << 8,  4) \

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#ifdef HEADER

void Input_Init(void);
void Input_Task(void);

// input structure
typedef struct {

	// digital inputs
	#ifdef DIGITAL_INPUTS
	struct {
		#undef EVENT_SUPPORT
		#define EVENT_SUPPORT(name) uint8 name##Event:1;
		#undef NO_EVENT_SUPPORT
		#define NO_EVENT_SUPPORT(name)
		#undef INPUT
		#define INPUT(name, test, debounce, init, event) uint8 name:1; event(name)
		DIGITAL_INPUTS
	} Digital;
	#endif // DIGITAL_INPUTS

	// analog inputs
	#ifdef ANALOG_INPUTS
	struct {
		#undef INPUT
		#define INPUT(name, read, filter) uint16 name;
		ANALOG_INPUTS
	} Analog;
	#endif // ANALOG_INPUTS

} INPUTS;

extern INPUTS Input;

#else

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// allocate memory for structure

INPUTS Input = {

	// digital inputs
	#ifdef DIGITAL_INPUTS
	{
		#undef INIT_TRUE
		#define INIT_TRUE        1
		#undef INIT_FALSE
		#define INIT_FALSE       0
		#undef INIT_ACTUAL
		#define INIT_ACTUAL      0
		#undef EVENT_SUPPORT
		#define EVENT_SUPPORT    0,
		#undef NO_EVENT_SUPPORT
		#define NO_EVENT_SUPPORT
		#undef INPUT
		#define INPUT(name, test, debounce, init, event) init, event
		DIGITAL_INPUTS
	},
	#endif // DIGITAL_INPUTS

	// analog inputs
	#ifdef ANALOG_INPUTS
	{
		#undef INPUT
		#define INPUT(name, read, filter) 0,
		ANALOG_INPUTS
	}
	#endif // ANALOG_INPUTS
};

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// digital section

#undef DIGITAL_INIT
#undef DIGITAL_TASK

#ifdef DIGITAL_INPUTS

#undef INIT_TRUE
#define INIT_TRUE   +0
#undef INIT_FALSE
#define INIT_FALSE  +0
#undef INIT_ACTUAL
#define INIT_ACTUAL +1
#undef INPUT
#define INPUT(name, test, debounce, init, event) init
#if DIGITAL_INPUTS > 0
  #define DIGITAL_INIT
#endif


#ifdef DIGITAL_INIT
#pragma INLINE
static void Digital_Init(void)
{
	// initialize "actual" digital input states
	#undef TEST
	#define TEST(test) ((test) ? TRUE : FALSE)
	#undef INIT_TRUE
	#define INIT_TRUE(name, test)
	#undef INIT_FALSE
	#define INIT_FALSE(name, test)
	#undef INIT_ACTUAL
	#define INIT_ACTUAL(name, test) Input.Digital.name = (uint8)(test);
	#undef INPUT
	#define INPUT(name, test, debounce, init, event) init(name,test)

	DIGITAL_INPUTS
}
#endif //INCLUDE_DIGITAL_INIT

#define DIGITAL_TASK
#pragma INLINE
static void Digital_Task(void)
{
	// allocate debounce timers
	static struct {
		#undef DEBOUNCE
		#define DEBOUNCE(size, val) size
		#undef INPUT
		#define INPUT(name, test, debounce, init, event) debounce name;
		DIGITAL_INPUTS
	} timer =
	{
		#undef INPUT
		#define INPUT(name, test, debounce, init, event) 0,
		DIGITAL_INPUTS
	};

	#undef TEST
	#define TEST(test) (test)
	#undef DEBOUNCE
	#define DEBOUNCE(size, val) (val)
	#undef EVENT_SUPPORT
	#define EVENT_SUPPORT(name, val) Input.Digital.name##Event = val;
	#undef NO_EVENT_SUPPORT
	#define NO_EVENT_SUPPORT(name, val)
	#undef INPUT
	#define INPUT(name, test, debounce, init, event)     \
	event(name,FALSE)                                    \
	if (test)                                            \
	{                                                    \
	    if (Input.Digital.name)                          \
	        /*lint -save -e(801) using a GOTO */         \
	        goto name##ClearTimer;                       \
	        /*lint -restore */                           \
	}                                                    \
	else                                                 \
	{                                                    \
	    if (!Input.Digital.name)                         \
	        /*lint -save -e(801) using a GOTO */         \
	        goto name##ClearTimer;                       \
	        /*lint -restore */                           \
	}                                                    \
	if (++timer.name >= (debounce))                      \
	{                                                    \
	    event(name,TRUE)                                 \
	    Input.Digital.name = (uint8)!Input.Digital.name; \
	    name##ClearTimer:                                \
	    timer.name = 0;                                  \
	}

	DIGITAL_INPUTS
}

#endif // DIGITAL_INPUTS

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// analog section

#ifdef ANALOG_INPUTS

static void Analog_Task(void)
{
	#undef INPUT
	#define INPUT(name, read, filter) Input.Analog.name = (Input.Analog.name - (uint16) (Input.Analog.name >> (filter))) + (uint16) ((uint16) (read) >> (filter));
	ANALOG_INPUTS
}

#pragma INLINE
static void Analog_Init(void)
{
	uint8 n;

	// initialize A/D filter
	#undef INPUT
	#define INPUT(name, read, filter) Input.Analog.name = (read);
	ANALOG_INPUTS

	// take several A/D samples to stabilize filter
	for (n=8; n; n--)
		Analog_Task();
}

#endif // ANALOG_INPUTS

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// this routine runs at initilization
void Input_Init(void)
{
#ifdef DIGITAL_INIT
	Digital_Init();
#endif

#ifdef ANALOG_INPUTS
	Analog_Init();
#endif
}

// this routine responsible for debouncing and filtering of inputs
// task runs periodically (actual rate depends on OS scheduler)
void Input_Task(void)
{
#ifdef DIGITAL_TASK
	Digital_Task();
#endif	

#ifdef ANALOG_INPUTS
	Analog_Task();
#endif
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#endif
